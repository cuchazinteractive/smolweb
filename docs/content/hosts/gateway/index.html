
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<base href="../../">
</head>
<body>

<page-main>

	<h1>Gateway Server</h1>

	<p>
		The gateway server connects your website or web service to the SmolWeb network.
	</p>
	<p>
		Once connected, the gateway server can host a folder full of files for people to see.
		Or, if you need to host something more advanced (like an app or other dynamic content),
		you can configure the gateway server to proxy another HTTP server.
	</p>

	<doc-notice type="note">
		Before you install and configure your gateway server, you should already have the address
		of a beacon server that has given you permission to connect
		(either someone else's beacon server, or one you run yourself).
		Your gateway server won't be very useful until it can connect to a beacon server.
	</doc-notice>


	<h2>Installation</h2>
	<doc-tabs name="os">

		<section name="Linux (Debian-based)">

			First, install the Cuchaz Interactive Debian package repository, if you haven't already:
			<doc-code lang="shell">
				echo "deb https://packages.cuchazinteractive.org/deb/ /" \
				| sudo tee -a "/etc/apt/sources.list.d/cuchazinteractive.list"
				wget -q -O - https://packages.cuchazinteractive.org/deb/KEY.gpg \
				| sudo tee /etc/apt/trusted.gpg.d/cuchazinteractive.asc
			</doc-code>

			Then update your package lists and install the gateway:
			<doc-code lang="shell">
				sudo apt update
				sudo apt install smolweb-gateway
			</doc-code>

			The install process will create an initial configuration file for you
			and also install a systemd daemon for the gateway server,
			but the server will not be started automatically.

		</section>

		<section name="MacOS">
			Not supported yet. Planned for the future.
		</section>

		<section name="Windows">
			Not supported yet. Planned for the future.
		</section>

	</doc-tabs>

	<h2>Identity Setup</h2>
	<doc-todo>
		This is pretty generic identity stuff. Move this into Burger docs?
	</doc-todo>
	<doc-tabs name="os">

		<section name="Linux (Debian-based)">

			<p>
				First, you'll need to create an <em>App Identity</em>, which is a bit of cryptography that
				lets everyone verify that your website or service is really yours.
			</p>
			<p>
				To create your app identity, install the Burger identity tools from the Cuchaz Interactive
				Debian package repository you set up before:
			</p>
			<doc-code lang="shell">
				sudo apt install burger
			</doc-code>
			<p>
				Next, we'll use the <doc-lit>burger</doc-lit> command line utility to create your app identity.
				But first, since app identities depend on <em>personal identities</em>, we'll need to either find
				or create your personal identity.
			</p>
			<doc-tabs>

				<section name="I don't have a personal identity yet">
					<p>
						To make a personal identity, run <doc-lit>burger</doc-lit>:
					</p>
					<doc-code lang="shell">
						burger new-personal
					</doc-code>
					<p>
						The interactive TUI will walk you through the process.
						You will be asked to enter a passphrase to protect your key file.
						When you're done, save your identity and key files somewhere you can find them again.
					</p>

					<doc-terminal>
						<font color="#8AE234"><b>jeff@mustard</b></font>:<font color="#729FCF"><b>~</b></font>$ burger new-personal
						Create a new personal identity:
						<font color="#8AE234">&gt;</font> Choose a name: <font color="#34E2E2">Test</font>
						<font color="#8AE234">&gt;</font> Choose a passphrase: <font color="#34E2E2">********</font>
						Creating new personal identity ...
						Done!
						<font color="#8AE234">&gt;</font> Save your identity file: <font color="#34E2E2">/home/jeff/test.burger.id</font>
						Identity file written to: /home/jeff/test.burger.id
						<font color="#8AE234">&gt;</font> Save your key file: <font color="#34E2E2">/home/jeff/test.burger.key</font>
						Key file written to: /home/jeff/test.burger.key
					</doc-terminal>
				</section>

				<section name="I already have a personal identity">
					Great! You're ready for the next step.
				</section>

			</doc-tabs>

			<p>
				Next, unlock your personal identity so you can use it to create the app identity:
			</p>
			<doc-code lang="shell">
				burger unlock
			</doc-code>
			<p>
				Then, select your identity file, select your key file, and enter the passphrase.
			</p>
			<doc-terminal>
				<font color="#8AE234"><b>jeff@mustard</b></font>:<font color="#729FCF"><b>~</b></font>$ burger unlock
				<font color="#8AE234">&gt;</font> Choose your identity file: <font color="#34E2E2">/home/jeff/test.burger.id</font>
				<font color="#8AE234">&gt;</font> Choose your key file: <font color="#34E2E2">/home/jeff/test.burger.key</font>
				<font color="#8AE234">&gt;</font> Enter the identity passphrase: <font color="#34E2E2">********</font>
			</doc-terminal>
			<p>
				Now your personal identity is available to use in-memory for a while. Next, create the
				app identity:
			</p>
			<doc-code lang="shell">
				burger new-app
			</doc-code>
			<doc-terminal>
				<font color="#8AE234"><b>jeff@mustard</b></font>:<font color="#729FCF"><b>~</b></font>$ burger new-app
				<font color="#8AE234">&gt;</font> Choose a name for your app: <font color="#34E2E2">Awesome Internet Stuff</font>
				Creating new app identity ...
				Done!
				<font color="#8AE234">&gt;</font> Save your app&apos;s identity file: <font color="#34E2E2">/home/jeff/awesome internet stuff.burger.id</font>
				Identity file written to: /home/jeff/awesome internet stuff.burger.id
				<font color="#8AE234">&gt;</font> Save your app&apos;s key file: <font color="#34E2E2">/home/jeff/awesome internet stuff.burger.key</font>
				Key file written to: /home/jeff/awesome internet stuff.burger.key
			</doc-terminal>
			<p>
				Save your key file in a safe place. Unlike personal identities, app keys
				aren't secured with a passphrase since saving a password for an app isn't any easier than saving
				the key itself. So <doc-lit>burger</doc-lit> creates the key file with filesystem permissions so
				only your user can read it.
			</p>
			<p>
				Your personal identity will lock itself again after a few minutes of inactivity
				(kind of like <doc-lit>sudo</doc-lit> does),
				but you can also choose to lock it again now if you want:
			</p>
			<doc-code lang="shell">
				burger lock
			</doc-code>
			<p>
				Choose your identity to lock (or just lock all of them) and you'll be good to go.
			</p>

		</section>

		<section name="MacOS">
			Not supported yet. Planned for the future.
		</section>

		<section name="Windows">
			Not supported yet. Planned for the future.
		</section>

	</doc-tabs>


	<h2>Configuration</h2>
	<p>
		Configuration is handled by a configuration file at <doc-lit>/etc/smolweb/gateway.toml</doc-lit>
		in the <a href="https://toml.io">TOML format</a>.
		It's a plain text file and you can edit is using your favorite text editor.
	</p>
	<p>
		The install process created a default configuration file for you.
		This default configuration file is self-documenting. Individual configuration options are described in the
		configuration file itself, so they won't be described here.
		Rather, these instructions will show you how read the documentation inside
		the configuration file, explain how to find what changes you need to make, and how to make them.
	</p>
	<p>
		By default, no options will be explicitly set (and therefore implicitly remain at their default values),
		but every option will still be described using comments.
		The default value is also described in the comments, and will usually look like:
	</p>
	<doc-code lang="toml">
		# DEFAULT: option = value
	</doc-code>
	<p>
		Uncommenting the line and removing the <doc-lit>DEFAULT</doc-lit> tag will explicitly set the option to its
		default value (rather implicitly):
	</p>
	<doc-code lang="toml">
		option = value
	</doc-code>
	<p>
		After making changes to your configuration file, restart the server to have the changes take effect.
	</p>


	<h3>Required first-time setup</h3>
	<p>
		The default configuration file has some options that cannot have default values, so you must explicitly set
		them before you can start your gateway server. These options are described using a <doc-lit>REQUIRED</doc-lit>
		tag instead of a <doc-lit>DEFAULT</doc-lit> tag:
	</p>
	<doc-code lang="toml">
		# REQUIRED: option = value
	</doc-code>
	<p>
		Find all of these <doc-lit>REQUIRED</doc-lit> options and supply meaningful values for them.
		For the gateway server, you'll need to supply your app identity and key files, along with connection
		information for the beacon server you'll be using.
	</p>


	<h3>Modes of operation</h3>
	<p>
		The gateway server has two main different modes of operation:
		One mode functions as a simple static file server, serving static files from a specified folder. This is the default
		setting. The other mode proxies requests to another HTTP server, which allows for dynamic content, or perhaps
		a more fully-featured static file server.
	</p>


	<h4>Serve static files</h4>
	<p>
		To enable static file serving, make sure the
	</p>
	<doc-code lang="toml">
		[handlers.fetch_static]
	</doc-code>
	<p>
		section is present in the configuration file and uncommented.
		Then provide values for any required options, like the path to the root share folder.
	</p>


	<h4>Proxy another HTTP server</h4>
	<p>
		To enable proxy-based operation, make sure the
	</p>
	<doc-code lang="toml">
		[handlers.fetch_proxy]
	</doc-code>
	<p>
		section is present in the configuration file and uncommented.
		Then provide values for any required options, like the address of the HTTP server.
	</p>

	<doc-notice type="warn">
		<p>
			Of the two request handlers for your gateway,
			<doc-lit>fetch_static</doc-lit> and <doc-lit>fetch_proxy</doc-lit>,
			only one can be active at once.
		</p>
		<p>
			Trying to define both sections of your configuration file will result in a gateway server
			startup error and your server will fail to start.
		</p>
	</doc-notice>


	<h2>Starting and stopping your server</h2>
	<doc-tabs name="os">

		<section name="Linux (Debian-based)">

			<p>
				The SmolWeb gateway server is integrated into systemd with a service named <doc-lit>smolweb-gateway</doc-lit>.
			</p>
			<p>
				If you just installed the server, it shouldn't be running yet (since it was missing required configuration).
				If you added the required configuration, then you can start it now:
			</p>
			<doc-code lang="shell">
				sudo systemctl start smolweb-gateway
			</doc-code>

			<p>
				To see if the server started successfully, check on the status of your gateway server:
			</p>
			<doc-code lang="shell">
				sudo systemctl status smolweb-gateway
			</doc-code>
			<p>
				If all is running well, you should see something like:
			</p>
			<doc-terminal>
				<font color="#8AE234"><b>jeff@potatofarm</b></font>:<font color="#729FCF"><b>~ $</b></font> sudo systemctl status smolweb-gateway
				<font color="#8AE234"><b>●</b></font> smolweb-gateway.service - Smolweb Gateway
					 Loaded: loaded (/lib/systemd/system/smolweb-gateway.service; <font color="#8AE234"><b>enabled</b></font>; preset: <font color="#8AE234"><b>enabled</b></font>)
					 Active: <font color="#8AE234"><b>active (running)</b></font> since Thu 2024-08-22 09:27:43 EDT; 5 days ago
				   Main PID: 541 (smolweb-gateway)
					  Tasks: 5 (limit: 455)
						CPU: 1h 33min 22.614s
					 CGroup: /system.slice/smolweb-gateway.service
							 └─<font color="#8A8A8A">541 /usr/bin/smolweb-gateway --config /etc/smolweb/gateway.toml</font>
			</doc-terminal>

			<p>
				The stop and restart commands are:
			</p>
			<doc-code lang="shell">
				sudo systemctl stop smolweb-gateway
				sudo systemctl restart smolweb-gateway
			</doc-code>

			<p>
				To check the logs for your server, run:
			</p>
			<doc-code lang="shell">
				sudo journalctl -o cat -u smolweb-gateway
			</doc-code>

			<doc-notice type="tip">
				The <doc-lit>-o cat</doc-lit> option for <doc-lit>journalctl</doc-lit> tells systemd to omit any
				systemd-specific metadata from the log file and show just the raw log entries. Much of the systemd
				metadata is redundant relative to the information already in the log file, so omitting it makes for
				a much cleaner log reading experience.
			</doc-notice>

			<p>
				If you want to have the gateway server start automatically at system boot, then you can
				<em>enable</em> the systemd service with:
			</p>
			<doc-code lang="shell">
				sudo systemctl enable smolweb-gateway
			</doc-code>

		</section>

		<section name="MacOS">
			Not supported yet. Planned for the future.
		</section>

		<section name="Windows">
			Not supported yet. Planned for the future.
		</section>

	</doc-tabs>


	<h2>Updates</h2>
	<doc-tabs name="os">

		<section name="Linux (Debian-based)">

			Update your package lists and upgrade the gateway package:
			<doc-code lang="shell">
				sudo apt update
				sudo apt upgrade smolweb-gateway
			</doc-code>

			The upgrade process will stop your gateway server, apply the update,
			and then restart the gateway server.
			Your configuration file should remain the same as before.

			<doc-notice type="tip">
				If you've configured your OS to automatically download and upgrade packages,
				then you won't need to run the above steps yourself. Your gateway server will
				receive automatic security updates along with the rest of the OS.
			</doc-notice>

		</section>

		<section name="MacOS">
			Not supported yet. Planned for the future.
		</section>

		<section name="Windows">
			Not supported yet. Planned for the future.
		</section>

	</doc-tabs>


	<h2>Connect to your host</h2>
	<p>
		Now that your host is up and running, connect to it using a guest, like the
		<a href="https://smolweb-portal.cuchazinteractive.org" target="_blank">Portal Website</a>.
	</p>
	<p>
		But first, learn about <a href="hosts/addressing.html">SmolWeb addressing</a>,
		so you know how to get the right address.
	</p>


</page-main>

</body>
</html>
