
/**
 * Get the main area of a document, where the headers are
 * @param {Document} doc
 * @returns {?HTMLElement}
 */
export function findMainBeforeSlotted(doc) {
	return doc
		.childNodes.find(n => n.tagName === 'html')
		?.childNodes.find(n => n.tagName === 'body')
		?.childNodes.find(n => n.tagName === 'page-main');
}


/**
 * Get the main area of a document, where the headers are
 * @param {Document} doc
 * @returns {?HTMLElement}
 */
export function findMainAfterSlotted(doc) {
	return findMainBeforeSlotted(doc)
		?.childNodes.find(n => n.tagName === 'div')
		?.childNodes.find(n => n.tagName === 'main');
}



/**
 * @typedef {Object} DocHeader
 * @property {HTMLElement} elem
 * @property {string} id
 * @property {String} text
 * @property {number} level
 * @property {number[]} location
 * @property {DocHeader[]} subheaders
 */



/**
 * @param {HTMLElement} main
 * @returns {DocHeader[]}
 */
export function indexHeaders(main) {

	/**
	 * @param {HTMLElement} elem
	 * @param {DocHeader[]} headers
	 */
	function handle(elem, headers) {

		/** @type {DocHeader} */
		const header = {
			elem,
			id: undefined, // will determine later
			text: elem.textContent,
			level: (() => {
				switch (elem.tagName) {
					case 'h2': return 2;
					case 'h3': return 3;
					case 'h4': return 4;
					case 'h5': return 5;
					case 'h6': return 6;
					default: throw new Error(`Not a header element: ${elem.tagName}}`)
				}
			})(),
			location: [], // will determine later
			subheaders: []
		};

		// add the header to the tree
		let list = headers;
		for (let i=2; i<header.level; i++) {
			if (list.length <= 0) {
				throw new Error(`Illegal header level ${header.level}`);
			}
			header.location.push(list.length);
			list = list[list.length - 1].subheaders;
		}
		list.push(header);
		header.location.push(list.length);

		// make a machine-readable name for this header
		let name = header.text
			.replaceAll(/[^A-Za-z0-9]/g, '_')
			.toLowerCase();

		// prepend the hierarchy location to make the name unique in this document
		header.id = `${header.location.join('.')}_${name}`;
	}

	/** @type {DocHeader[]} */
	const headers = [];

	// find all the headers from h2,h3,h4,h5,h6 tags
	for (const n of main.childNodes) {
		switch (n.tagName) {
			case 'h2':
			case 'h3':
			case 'h4':
			case 'h5':
			case 'h6':
				handle(n, headers);
		}
	}

	return headers;
}


/**
 * @typedef {Object} Dir
 * @property {string} name
 * @property {function():File[]} files
 * @property {function():Dir[]} dirs
 */


/**
 * @typedef {Object} DocInfo
 * @property {string} title
 * @property {string} url
 * @property {number} order
 * @property {DocInfo[]} subdocs
 */


/**
 * @param {Dir} dir
 * @return {?DocInfo}
 */
export function indexDocs(dir) {

	/**
	 * @param {File} file
	 * @param {string} url
	 * @returns {?DocInfo}
	 */
	function indexDoc(file, url) {

		// read and parse the html
		const doctext = new FileReaderSync().readAsText(file);
		const doc = new DOMParser().parseFromString(doctext);

		// find the metadata
		const metas = doc
			.childNodes.find(n => n.tagName === 'html')
			?.childNodes.find(n => n.tagName === 'head')
			?.childNodes.filter(n => n.tagName === 'meta');

		/**
		 * @param {string} name
		 * @returns {?string}
		 */
		function getMeta(name) {
			if (metas == null) {
				return null;
			}
			const attr = metas
				.map(m => m.attributes.getNamedItem(name))
				.filter(attr => attr != null)
				[0];
			if (attr == null) {
				return undefined;
			}
			return attr.value;
		}

		// filter out docs with hidden in the metadata
		if (getMeta('hidden') === '') {
			return null;
		}

		// find the order
		const order = parseInt(getMeta('order')) ?? 50;

		// get the main content area
		const main = findMainBeforeSlotted(doc);
		if (main ==  null) {
			return null;
		}

		// the title is the first <h1> content
		const h1 = main.childNodes.find(n => n.tagName === 'h1');
		if (h1 == null) {
			return null;
		}

		return {
			title: h1.textContent,
			url,
			order,
			subdocs: []
		};
	}

	/**
	 * @param {string} path
	 * @param {Dir} dir
	 * @returns {?DocInfo}
	 */
	function handle(dir, path) {

		const files = dir.files()
			.filter(file => file.name.endsWith('.html'));

		// pull out the index file, or skip this folder
		const iindex = files.findIndex(file => file.name === 'index.html');
		const indexFile = iindex < 0 ? null : files.splice(iindex, 1)[0];
		if (indexFile == null) {
			return null;
		}

		// index the index file (hah!)
		const indexInfo = indexDoc(indexFile, path);
		if (indexInfo == null) {
			return null;
		}

		// index the other files
		for (const file of files) {
			const info = indexDoc(file, `${path}${file.name}`);
			if (info != null) {
				indexInfo.subdocs.push(info);
			}
		}

		// recurse
		for (const subdir of dir.dirs()) {
			const subdoc = handle(subdir, `${path}${subdir.name}/`);
			if (subdoc != null) {
				indexInfo.subdocs.push(subdoc);
			}
		}

		// sort subdocs by order
		indexInfo.subdocs.sort((a, b) => a.order - b.order);

		return indexInfo;
	}

	return handle(dir, '');
}


/**
 * @returns {string}
 */
export function hereUrl() {

	const /** @type {string[]} */ parts = [];

	const hereFile = document.getFile();
	parts.unshift(hereFile.name);

	let dir = hereFile.parent;
	while (dir != null) {
		parts.unshift(dir.name);
		dir = dir.parent;
	}

	return parts
		.filter(p => p !== '')
		.join('/');
}


/**
 * @returns {string}
 */
export function hereUrlDir() {

	const /** @type {string[]} */ parts = [];

	const hereFile = document.getFile();

	let dir = hereFile.parent;
	while (dir != null) {
		parts.unshift(dir.name);
		dir = dir.parent;
	}

	return parts
		.filter(p => p !== '')
		.join('/');
}
