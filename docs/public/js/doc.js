
// wire up the menu show/hide button, after the DOM loads
window.addEventListener('DOMContentLoaded', () => {
	initNav();
	initTabs();
});


function initNav() {

	// tragically, details/summary tag open state isn't controllable by CSS,
	// so we can't set that using media queries,
	// so just make simple disclosure button behavior here
	const button = document.getElementById('nav-button');
	const content = document.getElementById('nav-content');
	button.onclick = () => {
		switch (content.style.display) {
			case 'block': content.style.display = ''; break;
			case '': content.style.display = 'block'; break;
		}
	};
}


class TabGroup {

	constructor(elem) {

		this.elem = elem;

		this.name = elem.attributes.getNamedItem('name')?.value ?? null;
		this.buttonsBar = [... elem.children]
			.find(e => e.getAttribute('class') === 'buttons');

		// build the tabs from the buttons and sections
		const buttons = [... this.buttonsBar.children]
			.filter(e => e.localName === 'button');
		const sections = [... this.elem.children]
			.filter(e => e.localName === 'section');
		this.tabs = sections.map((section, i) => new Tab(this, section, buttons[i]));

		// init styles
		this.buttonsBar.classList.add('shown');

		// find the previously-selected tab, if any
		let selectedTab = null;
		const selectedName = this.loadSelection();
		if (selectedName != null) {
			selectedTab = this.tabs.find(t => t.name === selectedName);
		} else {
			// otherwise, select the first tab by default
			selectedTab = this.tabs[0];
		}
		if (selectedTab != null) {
			this.select(selectedTab.name);
		}
	}

	select(name) {
		for (const tab of this.tabs) {
			if (tab.name === name) {
				tab.button.classList.add('selected');
				tab.section.classList.remove('hidden');
				tab.section.classList.add('selected');
			} else {
				tab.button.classList.remove('selected');
				tab.section.classList.add('hidden');
				tab.section.classList.remove('selected');
			}
		}
	}

	#storageName() {
		return `tab-${this.name}`
	}

	loadSelection() {
		return localStorage.getItem(this.#storageName());
	}

	saveSelection(value) {
		localStorage.setItem(this.#storageName(), value);
	}
}

class Tab {

	constructor(group, section, button) {

		this.group = group;
		this.section = section;
		this.button = button;

		this.header = [... section.children]
			.find(e => e.localName === 'header');
		this.name = this.header.textContent;

		// init styles
		this.section.classList.add('hidden');
		this.header.classList.add('hidden');

		// wire up events
		this.button.addEventListener('click', () => selectTab(this.group, this));
	}
}

const tabGroups = [];

function initTabs() {
	for (const elem of document.getElementsByTagName('doc-tabs')) {
		tabGroups.push(new TabGroup(elem));
	}
}

function selectTab(group, tab) {

	group.saveSelection(tab.name);

	// find all the groups with the same name, if any
	const groups = [group];
	if (group.name != null) {
		tabGroups
			.filter(g => g !== group && g.name === group.name)
			.forEach(g => groups.push(g));
	}

	for (const g of groups) {
		g.select(tab.name);
	}
}
