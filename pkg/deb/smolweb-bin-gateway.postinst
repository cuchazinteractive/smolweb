#!/bin/sh

set -e

# https://www.debian.org/doc/debian-policy/ch-maintainerscripts.html
# https://www.debian.org/doc/debian-policy/ap-flowcharts.html
# https://www.debian.org/doc/manuals/securing-debian-manual/bpp-lower-privs.en.html

pkg=smolweb-gateway

# try to source a default file for this packge, if any exists
# shellcheck source=/dev/null
[ -f "/etc/default/$pkg" ] && . "/etc/default/$pkg"

# defaults
[ -z "$SERVER_USER" ] && SERVER_USER=smolweb-gateway
[ -z "$SERVER_GROUP" ] && SERVER_GROUP=smolweb-gateway
[ -z "$SERVER_CONFIG" ] && SERVER_CONFIG=/etc/smolweb


case "$1" in

  configure)

    # add group, if needed
    if ! getent group | grep -q "^$SERVER_GROUP:"; then
      echo "Adding system group $SERVER_GROUP.."
      addgroup --quiet --system "$SERVER_GROUP" 2>/dev/null || true
      echo "..done"
    fi

    # add user, if needed
    if ! getent passwd | grep -q "^$SERVER_USER:"; then
      echo "Adding system user $SERVER_USER.."
      adduser --quiet \
              --system \
              --ingroup "$SERVER_GROUP" \
              --no-create-home \
              --disabled-password \
              "$SERVER_USER" 2>/dev/null || true
      echo "..done"
    fi

    # copy config files from templates, if needed
    if [ ! -f "$SERVER_CONFIG/gateway.toml" ]; then
      echo "Creating gateway config file.."
      cp "$SERVER_CONFIG/gateway.toml.templ" "$SERVER_CONFIG/gateway.toml"
      chmod u=rw,go=r "$SERVER_CONFIG/gateway.toml"
      echo "..done"
    fi

    ;;

esac


#DEBHELPER#
