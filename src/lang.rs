
use std::error::Error;

use display_error_chain::ErrorChainExt;
use tracing::{error, warn};


pub trait JoinToString<T> {
	fn join_to_string(&self, sep: &str, f: impl Fn(&T) -> String) -> String;
}


impl<T> JoinToString<T> for [T] {

	fn join_to_string(&self, sep: &str, f: impl Fn(&T) -> String) -> String {
		self.iter()
			.map(f)
			.collect::<Vec<_>>()
			.join(sep)
	}
}


pub trait TrimMargin {
	fn trim_margin(self) -> String;
}

impl TrimMargin for String {

	fn trim_margin(self) -> String {
		self.lines()
			.filter_map(|line| {
				line.find('|')
					.map(|pos| &line[pos + 1 ..])
			})
			.collect::<Vec<_>>()
			.join("\n")
	}
}


pub trait ErrorReporting<T> {
	fn print_err(self) -> Result<T,()>;
	fn log_err(self) -> Result<T,()>;
	fn warn_err(self) -> Result<T,()>;
}

impl<T,E> ErrorReporting<T> for Result<T,E>
	where
		E: AsRef<dyn Error>
{
	fn print_err(self) -> Result<T,()> {
		self.map_err(|e| {
			println!("{}", e.as_ref().chain());
			()
		})
	}

	fn log_err(self) -> Result<T,()> {
		self.map_err(|e| {
			error!("{}", e.as_ref().chain());
			()
		})
	}

	fn warn_err(self) -> Result<T,()> {
		self.map_err(|e| {
			warn!("{}", e.as_ref().chain());
			()
		})
	}
}
