
use anyhow::{Context, Result};
use futures::stream::FuturesUnordered;
use futures::StreamExt;
use tokio::signal::unix::{signal, Signal, SignalKind};


pub struct SignalHandler {
	name: &'static str,
	signal: Signal
}

impl SignalHandler {

	fn install(kind: SignalKind, name: &'static str) -> Result<Self> {
		let signal = signal(kind)
			.context(format!("Can't install {} handler", name))?;
		Ok(Self {
			name,
			signal
		})
	}

	pub fn install_sigint() -> Result<Self> {
		Self::install(SignalKind::interrupt(), "SIGINT")
	}

	pub fn install_sigterm() -> Result<Self> {
		Self::install(SignalKind::terminate(), "SIGTERM")
	}

	pub async fn recv(&mut self) -> &mut Self {
		self.signal.recv()
			.await;
		self
	}

	pub fn name(&self) -> &'static str {
		self.name
	}
}


pub struct Signals {
	signals: Vec<SignalHandler>
}

impl Signals {

	pub fn of(signals: Vec<SignalHandler>) -> Self {
		Self {
			signals
		}
	}

	pub async fn recv(&mut self) -> &mut SignalHandler {
		self.signals.iter_mut()
			.map(|s| s.recv())
			.collect::<FuturesUnordered<_>>()
			.next()
			.await
			.expect("stream was empty")
	}
}
