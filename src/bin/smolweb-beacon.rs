
use std::path::PathBuf;
use std::process::{ExitCode, Termination};

use anyhow::{Context, Result};
use gumdrop::Options;
use tracing::info;

use smolweb::beacon::{ArgsBeacon, BeaconServer, ConfigBeacon};
use smolweb::config::read_config;

use smolweb_bin::lang::ErrorReporting;
use smolweb_bin::logging::init_logging;
use smolweb_bin::signals::SignalHandler;


/// SmolWeb beacon server:
/// Make connections between guests and hosts over the SmolWeb network.
#[derive(Options, Debug)]
struct Args {

	/// print help message
	#[options()]
	help: bool,

	/// print version
	#[options()]
	version: bool,

	/// start server with configuration from file
	#[options(meta = "PATH")]
	config: Option<String>
}


// NOTE: start in single-threaded mode
// ie, no #[tokio::main]
// so we can safely get the timezone
// because somehow that's a thing we need to do?!?!
fn main() -> ExitCode {

	let args = Args::parse_args_default_or_exit();

	if args.version {
		println!("SmolWeb Beacon server v{}", env!("CARGO_PKG_VERSION"));
		return ExitCode::SUCCESS;
	}

	let Some(path) = &args.config else {
		println!("No options given\n\n{}", Args::usage());
		return ExitCode::FAILURE;
	};

	// read the config, convert into args
	let Ok(args) = read_config::<ConfigBeacon>(path)
		.context(format!("Failed to read Beacon config file: {:?}", path))
		.print_err()
		else { return ExitCode::FAILURE };

	// log tracing events to stdout
	let Ok(_) = init_logging(&args.log)
		.context("Failed to initialize logging")
		.print_err()
		else { return ExitCode::FAILURE };

	// show the path to the config file
	let abs_path = match PathBuf::from(path).canonicalize() {
		Ok(p) => p.to_string_lossy().to_string(),
		Err(_) => path.clone()
	};
	info!(path = abs_path, "Read config file");

	// run the beacon, in a tokio runtime
	let Ok(runtime) = tokio::runtime::Builder::new_multi_thread()
		.enable_all()
		.build()
		.context("Failed to init tokio runtime")
		.log_err()
		else { return ExitCode::FAILURE };
	runtime.block_on(async {
		run(args)
			.await
	})
		.log_err()
		.report()
}


async fn run(args: ArgsBeacon) -> Result<()> {

	// install signal handlers
	let mut sigint = SignalHandler::install_sigint()?;
	let mut sigterm = SignalHandler::install_sigterm()?;

	let beacon_server = BeaconServer::start(args)
		.await
		.context("Failed to start beacon server")?;

	// wait for signals
	tokio::select! {
			_ = sigint.recv() => info!("received {}, shutting down ...", sigint.name()),
			_ = sigterm.recv() => info!("received {}, shutting down ...", sigterm.name())
		}

	beacon_server.shutdown()
		.await;

	Ok(())
}
