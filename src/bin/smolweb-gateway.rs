
use std::path::PathBuf;
use std::process::{ExitCode, Termination};

use anyhow::{Context, Result};
use base64::Engine;
use base64::engine::general_purpose::{URL_SAFE_NO_PAD as BASE64};
use gumdrop::Options;
use tracing::info;

use smolweb::config::read_config;
use smolweb::gateway::{ArgsGateway, ConfigGateway, GatewayServer};

use smolweb_bin::lang::ErrorReporting;
use smolweb_bin::logging::init_logging;
use smolweb_bin::signals::SignalHandler;


/// SmolWeb gateway server:
/// Host your website or service on the SmolWeb network.
#[derive(Options, Debug)]
struct Args {

	/// print help message
	#[options()]
	help: bool,

	/// print version
	#[options()]
	version: bool,

	/// start server with configuration from file
	#[options(meta = "PATH")]
	config: Option<String>
}


// NOTE: start in single-threaded mode
// ie, no #[tokio::main]
// so we can safely get the timezone
// because somehow that's a thing we need to do?!?!
fn main() -> ExitCode {

	let args = Args::parse_args_default_or_exit();

	if args.version {
		println!("SmolWeb Gateway server v{}", env!("CARGO_PKG_VERSION"));
		return ExitCode::SUCCESS;
	}

	let Some(path) = &args.config else {
		println!("No options given\n\n{}", Args::usage());
		return ExitCode::FAILURE;
	};

	// read the config, convert into args
	let Ok(args) = read_config::<ConfigGateway>(path)
		.context(format!("Failed to read Gateway config file: {:?}", path))
		.print_err()
		else { return ExitCode::FAILURE };

	// log tracing events to stdout
	let Ok(_) = init_logging(&args.log)
		.context("Failed to initialize logging")
		.print_err()
		else { return ExitCode::FAILURE };

	// init logging used by WebRTC crates
	//std::env::set_var("RUST_LOG", "webrtc=trace,webrtc_ice=trace,webrtc_mdns=warn");
	env_logger::init();

	// show the path to the config file
	let abs_path = match PathBuf::from(path).canonicalize() {
		Ok(p) => p.to_string_lossy().to_string(),
		Err(_) => path.clone()
	};
	info!(path = abs_path, "Read config file");

	// run the beacon, in a tokio runtime
	let Ok(runtime) = tokio::runtime::Builder::new_multi_thread()
		.enable_all()
		.build()
		.context("Failed to init tokio runtime")
		.log_err()
		else { return ExitCode::FAILURE };
	runtime.block_on(async {
		run(args)
			.await
	})
		.log_err()
		.report()
}


async fn run(args: ArgsGateway) -> Result<()> {

	// install signal handlers
	let mut sigint = SignalHandler::install_sigint()?;
	let mut sigterm = SignalHandler::install_sigterm()?;

	// start the gateway server
	let gateway_server = GatewayServer::start(args.clone());

	let name = gateway_server.app_id().name();
	let uid = BASE64.encode(gateway_server.app_id().uid());
	info!(app = name, uid, "Started");

	// wait for signals
	tokio::select! {
			_ = sigint.recv() => info!("received {}, shutting down ...", sigint.name()),
			_ = sigterm.recv() => info!("received {}, shutting down ...", sigterm.name())
		}

	// cleanup
	gateway_server.shutdown()
		.await;

	Ok(())
}
