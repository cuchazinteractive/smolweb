
use std::future::Future;
use std::process::{ExitCode, Termination};

use anyhow::Context;
use gumdrop::Options;

use smolweb_bin::lang::ErrorReporting;
use smolweb_bin::logging::init_logging;
use smolweb_bin::tool;
use smolweb_bin::tool::{ArgsConnect, ArgsStun, ArgsGuest, ArgsHost, ArgsGuest2, ArgsHost2, ArgsPortmap, ArgsLocal};


/// SmolWeb tool:
/// Helps debug issues with peer-to-peer networking.
#[derive(Options, Debug)]
struct Args {

	/// print help message
	#[options()]
	help: bool,

	/// print version
	#[options()]
	version: bool,

	/// configure logging
	#[options()]
	log: Option<String>,

	#[options(command)]
	cmd: Option<Command>
}


#[derive(Options, Debug)]
pub enum Command {

	/// query STUN servers to determine your public network address(es)
	#[options()]
	Stun(ArgsStun),

	/// tries to establish a P2P connection
	#[options()]
	Connect(ArgsConnect),

	/// tries to connect to a tool host
	#[options()]
	Guest(ArgsGuest),

	/// listens for incoming connections from tool guests
	#[options()]
	Host(ArgsHost),

	/// tries to connect to a tool host using the double-connection protocol
	#[options()]
	Guest2(ArgsGuest2),

	/// listens for incoming connections from tool guests using the double-connection protocol
	#[options()]
	Host2(ArgsHost2),

	/// UPnP-IGD functions
	#[options()]
	Portmap(ArgsPortmap),

	/// Local network diagnostics
	#[options()]
	Local(ArgsLocal)
}


// NOTE: start in single-threaded mode
// ie, no #[tokio::main]
// so we can safely get the timezone
// because somehow that's a thing we need to do?!?!
fn main() -> ExitCode {

	let args = Args::parse_args_default_or_exit();

	if args.version {
		println!("SmolWeb Tool v{}", env!("CARGO_PKG_VERSION"));
		return ExitCode::SUCCESS;
	}

	// log tracing events to stdout
	let log = args.log
		.unwrap_or("smolweb=trace".to_string());
	let Ok(_) = init_logging(&log)
		.context("Failed to initialize logging")
		.print_err()
		else { return ExitCode::FAILURE };

	// init logging used by WebRTC crates
	std::env::set_var("RUST_LOG", "webrtc=trace,webrtc_ice=trace,webrtc_mdns=warn");
	env_logger::init();

	match &args.cmd {

		Some(Command::Stun(args)) => {
			let Ok(runtime) = make_runtime()
				else { return ExitCode::FAILURE };
			run(runtime, async {
				tool::stun(args)
					.await
			})
				.log_err()
				.report()
		}

		Some(Command::Connect(args)) => {
			let Ok(runtime) = make_runtime()
				else { return ExitCode::FAILURE };
			run(runtime, async {
				tool::connect(args)
					.await
			})
				.log_err()
				.report()
		}

		Some(Command::Guest(args)) => {
			let Ok(runtime) = make_runtime()
				else { return ExitCode::FAILURE };
			run(runtime, async {
				tool::guest(args)
					.await
			})
				.log_err()
				.report()
		}

		Some(Command::Host(args)) => {
			let Ok(runtime) = make_runtime()
				else { return ExitCode::FAILURE };
			run(runtime, async {
				tool::host(args)
					.await
			})
				.log_err()
				.report()
		}

		Some(Command::Guest2(args)) => {
			let Ok(runtime) = make_runtime()
				else { return ExitCode::FAILURE };
			run(runtime, async {
				tool::guest2(args)
					.await
			})
				.log_err()
				.report()
		}

		Some(Command::Host2(args)) => {
			let Ok(runtime) = make_runtime()
				else { return ExitCode::FAILURE };
			run(runtime, async {
				tool::host2(args)
					.await
			})
				.log_err()
				.report()
		}

		Some(Command::Portmap(args)) => {
			let Ok(runtime) = make_runtime()
				else { return ExitCode::FAILURE };
			run(runtime, async {
				tool::portmap(args)
					.await
			})
				.log_err()
				.report()
		}

		Some(Command::Local(args)) => {
			let Ok(runtime) = make_runtime()
				else { return ExitCode::FAILURE };
			run(runtime, async {
				tool::local(args)
					.await
			})
				.log_err()
				.report()
		}

		None => {
			println!("No command given. Try one of:\n{}", Args::command_list().unwrap());
			ExitCode::FAILURE
		}
	}
}


fn make_runtime() -> Result<tokio::runtime::Runtime,()> {
	tokio::runtime::Builder::new_current_thread()
		.enable_io()
		.enable_time()
		.build()
		.context("Failed to init tokio runtime")
		.log_err()
}

fn run<F:Future>(runtime: tokio::runtime::Runtime, f: F) -> F::Output {
	runtime.block_on(async {
			let local_set = tokio::task::LocalSet::new();
			local_set.run_until(async {
				f.await
			}).await
		})
}
