
use anyhow::{Context, Result};
use display_error_chain::ErrorChainExt;
use time::macros::format_description;
use time::UtcOffset;
use tracing_subscriber::fmt::format::Format;
use tracing_subscriber::fmt::time::OffsetTime;
use tracing_subscriber::{EnvFilter, FmtSubscriber};


pub fn init_logging(log_directive: impl AsRef<str>) -> Result<()> {

	let log_directive = log_directive.as_ref();
	println!("Initializing logging: {}", log_directive);

	let filter = EnvFilter::builder()
		.parse(log_directive)
		.context(format!("Failed to parse log directive: {}", log_directive))?;

	// format times into something humans can actually read
	let time_format = format_description!(
		version = 2,
		"[year]-[month]-[day] [hour]:[minute]:[second].[subsecond digits:4] [offset_hour sign:mandatory]:[offset_minute]"
	);

	// try to get the local timezone ... this is hard for some weird reason
	let time_offset = UtcOffset::current_local_offset()
		.unwrap_or_else(|e| {

			// weird...
			println!("\tWarning: Failed to get local time offset: {}", e.into_chain());

			// but just fall back to UTC
			UtcOffset::UTC
		});

	let format = Format::default()
		.with_timer(OffsetTime::new(time_offset, time_format))
		.with_target(false);

	let subscriber = FmtSubscriber::builder()
		.with_env_filter(filter)
		.event_format(format)
		.finish();

	tracing::subscriber::set_global_default(subscriber)
		.context("Failed to set logging subscriber")?;

	Ok(())
}
