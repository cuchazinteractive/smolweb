
use anyhow::{Context, Result};
use gumdrop::Options;
use tracing::info;

use smolweb::net::gateways::GatewayInfo;

use crate::lang::ErrorReporting;


#[derive(Options, Debug)]
pub struct ArgsPortmap {

	/// true to query gateways from all interfaces,
	/// false to only query gateways from default interfaces
	#[options(default_expr = "false")]
	all: bool
}


pub async fn run(args: &ArgsPortmap) -> Result<()> {

	const LOCAL_PORT: u16 = 42542;

	// lookup gateways
	for gateway_info in GatewayInfo::get_all() {

		let route = gateway_info.default_route();

		// filter gateways
		if !args.all && !gateway_info.default {
			info!("Skipping non-default gateway: {}", route);
			continue
		}

		info!("Testing Gateway: {}", route);

		let Ok(mapped_port) = gateway_info.map_port(LOCAL_PORT)
			.await
			.warn_err()
			else { continue };

		info!("Mapped port successfully via {}! {} -> {}", mapped_port.kind_name(), mapped_port.local_port(), mapped_port.external_port());

		mapped_port.close()
			.await
			.context("Failed to drop mapped port")
			.warn_err()
			.ok();
	}

	Ok(())
}
