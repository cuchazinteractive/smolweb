
use std::net::IpAddr;
use std::str::FromStr;

use anyhow::{Context, Result};
use gumdrop::Options;
use tracing::{info, warn};
use smolweb::net::ConfigAddr;
use smolweb::net::dns::LookupAddrs;
use smolweb::webrtc::ConnectionRace;

use crate::lang::{ErrorReporting, JoinToString};
use crate::signals::{SignalHandler, Signals};
use crate::tool::webrtc::{abort_bail, Channel, close_bail, Side, test_messages_guest};


#[derive(Options, Debug)]
pub struct ArgsGuest2 {

	/// guest public address
	#[options(free, default_expr = "ConfigAddr::localhost()", required, parse(try_from_str = "ConfigAddr::from_str"))]
	guest_addr: ConfigAddr,

	/// host public address
	#[options(free, default_expr = "ConfigAddr::localhost()", required, parse(try_from_str = "ConfigAddr::from_str"))]
	host_addr: ConfigAddr,

	/// true to add ICE candidates with mapped ports
	#[options(default_expr = "false")]
	map_ports: bool,

	/// choose the gateway for port mapping by giving the ip address
	#[options(parse(try_from_str = "IpAddr::from_str"))]
	gateway: Option<IpAddr>,

	/// mapped ports of the host, if any, in controlling,controlled order as separate args
	#[options()]
	host_mapped_ports: Option<(u16,u16)>
}


pub async fn run(args: &ArgsGuest2) -> Result<()> {

	info!("Starting guest2 side of connection ...");

	let guest_addrs = args.guest_addr.lookup_addrs(0)
		.await
		.context("Failed to lookup guest addresses")?;
	let host_addrs = args.host_addr.lookup_addrs(0)
		.await
		.context("Failed to lookup host addresses")?;

	// make the sides
	let guest = Side::new_guest(&guest_addrs)
		.context("Failed to create guest side")?;
	let host = Side::new_host(&host_addrs)
		.context("Failed to create host side")?;
	info!("guest addresses 1: {}", guest.addrs(Channel::One).join_to_string(", ", |a| a.to_string()));
	info!("guest addresses 2: {}", guest.addrs(Channel::Two).join_to_string(", ", |a| a.to_string()));
	info!("host addresses 1: {}", host.addrs(Channel::One).join_to_string(", ", |a| a.to_string()));
	info!("host addresses 2: {}", host.addrs(Channel::Two).join_to_string(", ", |a| a.to_string()));

	// install signal handlers
	let mut signals = Signals::of(vec![
		SignalHandler::install_sigint()?,
		SignalHandler::install_sigterm()?
	]);

	// create the WebRTC connections
	let mut conn = ConnectionRace::new(
		guest.config(Channel::One, args.map_ports, args.gateway),
		guest.config(Channel::Two, args.map_ports, args.gateway)
	)
		.await
		.context("Failed to create new connection race")?
		.to_guest();

	// NOTE: connection created, we can't drop it without calling abort() or open()
	//       meaning, we can't use the ? operator unless it's on a function that moved conn!

	// attempt to connect
	let Ok(_offer1) = conn.offer()
		.await
		.context("Failed to create guest offer")
		.log_err()
		else { abort_bail!(conn); };
	let Ok(answer1) = host.imagine_answer(Channel::One, args.host_mapped_ports.map(|(_, p)| p))
		.context("Failed to imagine host answer")
		.log_err()
		else { abort_bail!(conn); };
	let Ok(offer2) = host.imagine_offer(Channel::Two, args.host_mapped_ports.map(|(p, _)| p))
		.context("Failed to imagine host offer")
		.log_err()
		else { abort_bail!(conn); };
	let Ok(_answer2) = conn.connect(answer1, offer2)
		.await
		.context("Failed to start connection")
		.log_err()
		else { abort_bail!(conn); };
	let mut data_channel = tokio::select! {

		_ = conn.wait_for_open() => {
			match conn.open() {
				Err(conn) => {
					warn!("Failed to open connection");
					abort_bail!(conn);
				},
				Ok(c) => c
			}
		}

		signal = signals.recv() => {
			info!("received {}, shutting down ...", signal.name());
			conn.abort()
				.await;
			return Ok(());
		}
	};

	// NOTE: data channel open, we can't drop it without calling close()
	//       meaning, we can't use the ? operator

	tokio::select! {

		result = test_messages_guest(&mut data_channel) => {
			let Ok(_) = result
				.context("Failed to send test messages")
				.log_err()
				else { close_bail!(data_channel); };
		}

		signal = signals.recv() => {
			info!("received {}, shutting down ...", signal.name());
			data_channel.close()
				.await;
			return Ok(());
		}
	}

	info!("Done! cleaning up ...");

	// cleanup
	data_channel.close()
		.await;

	info!("SUCCESS!");

	Ok(())
}
