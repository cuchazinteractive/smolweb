
use std::fmt;
use std::net::{IpAddr, SocketAddr};
use std::time::Duration;

use anyhow::{Context, Result};
use tracing::info;

use smolweb::protobuf::framed::{Reader, Writer};
use smolweb::tool::webrtc::{cert_guest, cert_host, create_answer, create_offer, RTCCertificate};
use smolweb::webrtc::{CandidateOptions, ConnectionConfig, ConnectionConfigBuilder, MapPortsOptions, OpenDataChannel, SessionDescription};


pub const DEFAULT_GUEST_PORT: u16 = 42542;
pub const DEFAULT_HOST_PORT: u16 = 5425;

pub const DEFAULT_GUEST2_PORT: u16 = 57575;
pub const DEFAULT_HOST2_PORT: u16 = 7575;

pub const UFRAG_GUEST: &str = "uname+guest";
pub const PWD_GUEST: &str = "secret+guest+but+at+least+128+bits";

pub const UFRAG_HOST: &str = "uname+host";
pub const PWD_HOST: &str = "secret+host+but+at+least+128+bits";

pub const DEFAULT_PRIORITY: u32 = 5;

pub const GUEST_MSG: &[u8; 42] = b"I thought that you couldn't get hurt here.";
pub const HOST_MSG: &[u8; 22] = b"Only the right amount.";


#[derive(Debug, Clone, Copy)]
pub enum Channel {
	One,
	Two
}

impl Channel {

	fn index(&self) -> usize {
		match self {
			Self::One => 0,
			Self::Two => 1
		}
	}
}

impl fmt::Display for Channel {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", match self {
			Self::One => "1",
			Self::Two => "2"
		})
	}
}


pub struct Side {
	addrs: [Vec<SocketAddr>; 2],
	cert: RTCCertificate,
	ufrag: String,
	pwd: String
}

impl Side {

	pub fn new(addrs1: Vec<SocketAddr>, addrs2: Vec<SocketAddr>, cert: RTCCertificate, ufrag: impl AsRef<str>, pwd: impl AsRef<str>) -> Self {
		Self {
			addrs: [addrs1, addrs2],
			cert,
			ufrag: ufrag.as_ref().to_string(),
			pwd: pwd.as_ref().to_string()
		}
	}

	pub fn new_guest(addrs: &Vec<SocketAddr>) -> Result<Self> {

		let mut addrs1 = addrs.clone();
		for a in &mut addrs1 {
			a.set_port(DEFAULT_GUEST_PORT);
		}
		let mut addrs2 = addrs.clone();
		for a in &mut addrs2 {
			a.set_port(DEFAULT_GUEST2_PORT);
		}

		let cert = cert_guest()
			.context("Failed to get guest cert")?;

		Ok(Self::new(addrs1, addrs2, cert, UFRAG_GUEST, PWD_GUEST))
	}

	pub fn new_host(addrs: &Vec<SocketAddr>) -> Result<Self> {

		let mut addrs1 = addrs.clone();
		for a in &mut addrs1 {
			a.set_port(DEFAULT_HOST_PORT);
		}
		let mut addrs2 = addrs.clone();
		for a in &mut addrs2 {
			a.set_port(DEFAULT_HOST2_PORT);
		}

		let cert = cert_host()
			.context("Failed to get host cert")?;

		Ok(Self::new(addrs1, addrs2, cert, UFRAG_HOST, PWD_HOST))
	}

	pub fn config(&self, channel: Channel, map_ports: bool, gateway_ip: Option<IpAddr>) -> ConnectionConfig {
		ConnectionConfigBuilder::new()
			.certificates(vec![self.cert.clone()])
			.ice_credentials(self.ufrag.clone(), self.pwd.clone())
			.ice_only_srflx(true)
			.ice_timings(Duration::from_millis(500), 9999)
			.ice_failed_timeout(Duration::from_secs(9999))
			.candidate_options(CandidateOptions {
				local_addrs: self.addrs(channel).clone(),
				priority: DEFAULT_PRIORITY,
				map_ports: if map_ports {
					Some(MapPortsOptions {
						gateway_ip
					})
				} else {
					None
				}
			})
			.build()
	}

	pub fn addrs(&self, channel: Channel) -> &Vec<SocketAddr> {
		&self.addrs[channel.index()]
	}

	pub fn imagine_offer(&self, channel: Channel, mapped_port: Option<u16>) -> Result<SessionDescription> {
		let addrs = addrs_map_port(self.addrs(channel), mapped_port);
		create_offer(&addrs, &self.cert, &self.ufrag, &self.pwd)
	}

	pub fn imagine_answer(&self, channel: Channel, mapped_port: Option<u16>) -> Result<SessionDescription> {
		let addrs = addrs_map_port(self.addrs(channel), mapped_port);
		create_answer(&addrs, &self.cert, &self.ufrag, &self.pwd)
	}
}


fn addrs_map_port(addrs: &Vec<SocketAddr>, mapped_port: Option<u16>) -> Vec<SocketAddr> {
	addrs
		.iter()
		.map(|addr| {
			let mut addr = *addr;
			if let Some(mapped_port) = mapped_port {
				addr.set_port(mapped_port);
			}
			addr
		})
		.collect::<Vec<_>>()
}


pub async fn test_messages_guest(data_channel: &mut OpenDataChannel) -> Result<()> {

	// send a message to the host
	info!("Sending message to host: {}", String::from_utf8_lossy(GUEST_MSG));
	data_channel.write(GUEST_MSG.as_slice())
		.await
		.context("Failed to send text across data channel")?;

	// wait for the host to respond
	info!("Waiting for host to respond ...");
	loop {

		let msg = data_channel.read()
			.await
			.context("Failed to receive msg")?;
		info!("Received msg from host: {}", String::from_utf8_lossy(msg.as_slice()));

		if msg.as_slice() == HOST_MSG.as_slice() {
			info!("Received correct response from host");
			return Ok(());
		}
	}
}


pub async fn test_messages_host(data_channel: &mut OpenDataChannel) -> Result<()> {

	info!("Waiting for guest message ...");

	// wait for the guest to send the message
	loop {
		let msg = data_channel.read()
			.await
			.context("Failed to receive msg")?;
		info!("Received message from guest: {}", String::from_utf8_lossy(msg.as_slice()));

		if msg.as_slice() == GUEST_MSG.as_slice() {
			info!("Received correct message from guest");
			break;
		}
	};

	// send the response back to the guest
	info!("Sending message to guest: {}", String::from_utf8_lossy(HOST_MSG));
	data_channel.write(HOST_MSG.as_slice())
		.await
		.context("Failed to send test message over data channel")?;

	// TODO: no way to flush the send buffer in the WebRTC data channel ...
	// see: https://github.com/webrtc-rs/webrtc/issues/517
	tokio::time::sleep(Duration::from_millis(1000))
		.await;

	Ok(())
}


macro_rules! abort_bail {
	($conn:ident) => {
		$conn.abort()
			.await;
		anyhow::bail!("FAILED");
	}
}

pub(crate) use abort_bail;


macro_rules! close_bail {
	($data_channel:ident) => {
		$data_channel.close()
			.await;
		anyhow::bail!("FAILED");
	}
}

pub(crate) use close_bail;
