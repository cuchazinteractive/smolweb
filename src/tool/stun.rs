
use std::str::FromStr;
use std::time::Duration;

use anyhow::{Context, Result};
use gumdrop::Options;
use tracing::{info, warn};

use smolweb::net::{BindMode, ConfigAddr};
use smolweb::net::stun_client::StunClient;

use crate::signals::SignalHandler;


const DEFAULT_STUN_ADDRS: [&'static str; 2] = [
	"smolweb-beacon.cuchazinteractive.org",
	"stunserver.stunprotocol.org"
];


#[derive(Options, Debug)]
pub struct ArgsStun {

	/// use IPv4, IPv6 addresses, or both
	#[options(parse(try_from_str = "BindMode::from_str"), default_expr = "BindMode::Both")]
	bind_mode: BindMode,

	/// local address to use
	#[options(long = "local-addr", parse(try_from_str = "ConfigAddr::from_str"))]
	local_addrs: Vec<ConfigAddr>,

	/// STUN server addresses
	#[options(free, parse(try_from_str = "ConfigAddr::from_str"))]
	remote_addrs: Vec<ConfigAddr>
}


pub async fn run(args: &ArgsStun) -> Result<()> {

	// install signal handlers
	let mut sigint = SignalHandler::install_sigint()?;
	let mut sigterm = SignalHandler::install_sigterm()?;

	let mut stun_client = StunClient::bind(&args.local_addrs, args.bind_mode)
		.await
		.context("Failed to make stun client")?;

	// figure out the remote addresses
	let remote_addrs =
		 if args.remote_addrs.is_empty() {
			 DEFAULT_STUN_ADDRS.iter()
				 .map(|domain| ConfigAddr::new(domain, None))
				 .collect::<Vec<_>>()
		 } else {
			 args.remote_addrs.clone()
		 };

	stun_client.send_binding_requests(remote_addrs)
		.await
		.context("Failed to send binding requests")?;

	for request in stun_client.requests() {
		info!(%request, "Sent binding request");
	}

	// wait for binding responses
	loop {
		tokio::select! {

			_ = sigint.recv() => {
				info!("received {}, shutting down ...", sigint.name());
				break;
			}

			_ = sigterm.recv() => {
				info!("received {}, shutting down ...", sigterm.name());
				break;
			}

			_ = tokio::time::sleep(Duration::from_secs(10)) => {
				warn!("Timed out waiting for responses");
				break;
			}

			result = stun_client.next_response() => match result {
				Some((_, response)) => {
					info!(%response, "Received binding response");
					info!("Matched to request, have {} of {} responses", stun_client.responses().len(), stun_client.requests().len());
				}
				None => break
			}
		}
	}

	info!("Finished");

	Ok(())
}
