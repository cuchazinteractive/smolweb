
use std::cell::RefCell;
use std::fmt;
use std::io::Write;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr};
use std::ops::Deref;
use std::rc::Rc;
use std::str::FromStr;
use std::time::Duration;

use anyhow::{bail, Context, Result};
use bytes::BufMut;
use display_error_chain::ErrorChainExt;
use gumdrop::Options;
use tokio::net::UdpSocket;
use tracing::{debug, info, warn};

use smolweb::net::listeners::BufferedUdpSocket;

use crate::signals::SignalHandler;


const DEFAULT_PORTS: [u16; 2] = [42542, 5425];
const MAGIC: [u8; 2] = [5, 42];


#[derive(Options, Debug)]
pub struct ArgsConnect {

	/// which side of the connection are we on? A or B
	#[options(free, required, parse(try_from_str = "Side::from_str"))]
	side: Side,

	/// remote address
	#[options(free)]
	remote_addr: String,

	/// port to use for side A
	#[options(default_expr="DEFAULT_PORTS[Side::A.index()]")]
	port_a: u16,

	/// port to use for side B
	#[options(default_expr="DEFAULT_PORTS[Side::B.index()]")]
	port_b: u16
}


#[derive(Debug, Clone, Copy)]
enum Side {
	A,
	B
}

impl Side {

	fn other(&self) -> Self {
		match self {
			Side::A => Side::B,
			Side::B => Side::A
		}
	}

	fn index(&self) -> usize {
		match self {
			Side::A => 0,
			Side::B => 1
		}
	}

	fn name(&self) -> &'static str {
		match self {
			Side::A => "A",
			Side::B => "B"
		}
	}

	fn from_str(s: &str) -> Result<Self> {
		match s {
			"a" | "A" => Ok(Self::A),
			"b" | "B" => Ok(Self::B),
			_ => bail!("Not a side: {}", s)
		}
	}
}

impl fmt::Display for Side {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.name())
	}
}

// gumdrop requires a default value here, even though the app will neve use it
impl Default for Side {

	fn default() -> Self {
		Side::A
	}
}


pub async fn run(args: &ArgsConnect) -> Result<()> {

	// install signal handlers
	let mut sigint = SignalHandler::install_sigint()?;
	let mut sigterm = SignalHandler::install_sigterm()?;

	// figure out ports
	let ports = [args.port_a, args.port_b];
	let local_port = ports[args.side.index()];
	let remote_port = ports[args.side.other().index()];

	// figure out addresses
	let remote_addr = IpAddr::from_str(&args.remote_addr)
		.context("Failed to parse remote address")?;
	let remote_addr = SocketAddr::new(remote_addr, remote_port);
	let local_addr: IpAddr = match &remote_addr {
		SocketAddr::V4(_) => Ipv4Addr::UNSPECIFIED.into(),
		SocketAddr::V6(_) => Ipv6Addr::UNSPECIFIED.into()
	};
	let local_addr = SocketAddr::new(local_addr, local_port);

	let socket = BufferedUdpSocket::bind(&local_addr, 4096)?;
	info!(addr = %socket.local_addr(), "Listening");
	info!(%remote_addr, "Trying to connect ...");

	let state = Rc::new(RefCell::new(State::new()));

	// start the message sender
	let message_sender = tokio::task::spawn_local({
		let socket = socket.socket().clone();
		let state = state.clone();
		async move {

			loop {

				if state.borrow().action == Action::End {
					break;
				}

				tokio::time::sleep(Duration::from_secs(1))
					.await;

				if state.borrow().action == Action::End {
					break;
				}

				if let Err(e) = send(&state, &socket, remote_addr).await {
					warn!(err = %e.deref().chain(), "Failed to send message");
				}
			}
		}
	});

	// wait for something to happen
	while state.borrow().action == Action::KeepTrying {
		tokio::select! {

			result = socket.recv_from() => {
				match result {

					Ok((datagram,recv_remote_addr, socket)) => {
						let result = recv(&state, datagram, &socket, recv_remote_addr, remote_addr)
							.await;
						if let Err(e) = result {
							warn!(err = %e.deref().chain(), "Failed to receive message");
						}
					}

					Err(e) => {
						warn!(err = %e.into_chain(), "Failed to receive datagram");
					}
				}
			},

			_ = sigint.recv() => {
				info!("received {}, shutting down ...", sigint.name());
				state.borrow_mut().action = Action::End;
			}

			_ = sigterm.recv() => {
				info!("received {}, shutting down ...", sigterm.name());
				state.borrow_mut().action = Action::End;
			}
		}
	}

	// cleanup
	message_sender
		.await
		.context("Message sender task failed")?;

	info!("Finished");

	Ok(())
}


#[derive(Debug)]
struct State {
	action: Action,
	send_count: u64
}

impl State {

	fn new() -> Self {
		Self {
			action: Action::KeepTrying,
			send_count: 0
		}
	}
}


#[derive(Debug, PartialEq, Eq)]
enum Action {
	KeepTrying,
	End
}


async fn send(state: &Rc<RefCell<State>>, socket: &UdpSocket, remote_addr: SocketAddr) -> Result<()> {

	let send_count = {
		let mut state = state.borrow_mut();
		state.send_count += 1;
		state.send_count
	};

	// pack the message
	let mut msg = [0u8; 10];
	{
		let mut writer = msg.writer();
		writer.write_all(MAGIC.as_slice())
			.context("Failed to write magic to outgoing message")?;
		writer.write_all(send_count.to_be_bytes().as_slice())
			.context("Failed to write send count to outgoing message")?;
	}

	socket.send_to(&msg, remote_addr)
		.await
		.context("Failed to send datagram")?;

	info!(count = send_count, "Send message to remote");

	Ok(())
}


async fn recv(state: &Rc<RefCell<State>>, datagram: Vec<u8>, socket: &UdpSocket, recv_remote_addr: SocketAddr, exp_remote_addr: SocketAddr) -> Result<()> {

	debug!(size = datagram.len(), from = %recv_remote_addr, "Incoming Datagram");

	// is this one of our messages?
	if datagram.len() != 10 || &datagram[0..2] != MAGIC.as_slice() {
		debug!("Not our message");
		return Ok(());
	}

	// check the remote address
	if recv_remote_addr != exp_remote_addr {
		debug!(from = %recv_remote_addr, "Received message from unexpected source, ignoring");
		return Ok(());
	}

	// read the count
	let count_buf: [u8; 8] = datagram[2..10]
		.try_into()
		.expect("wrong size for count");
	let count = u64::from_be_bytes(count_buf);

	info!(count, "Received message from remote. We're connected!");

	// close the connection by sending one last message
	send(state, socket, exp_remote_addr)
		.await?;

	state.borrow_mut().action = Action::End;

	Ok(())
}
