
use anyhow::Result;
use gumdrop::Options;
use netdev::get_interfaces;
use tracing::info;

use smolweb::net::gateways::GatewayInfo;

use crate::lang::{JoinToString, TrimMargin};


#[derive(Options, Debug)]
pub struct ArgsLocal {
	// nothing needed here yet
}


pub async fn run(_args: &ArgsLocal) -> Result<()> {

	info!("Getting local interfaces ...");
	let interfaces = get_interfaces();
	for iface in interfaces {
		let name = iface.name;
		let name2 = iface.friendly_name
			.as_ref()
			.map(|s| s.as_str())
			.unwrap_or("(none)");
		let desc = iface.description
			.as_ref()
			.map(|s| s.as_ref())
			.unwrap_or("(none)");
		let default = iface.default;
		let itype = iface.if_type;
		let addrsv4 = iface.ipv4
			.join_to_string(", ", |a| a.to_string());
		let addrsv6 = iface.ipv6
			.join_to_string(", ", |a| a.to_string());
		let gateway_addrsv4 = iface.gateway
			.as_ref()
			.map(|d| d.ipv4.join_to_string(", ", |a| a.to_string()))
			.unwrap_or("(none)".to_string());
		let gateway_addrsv6 = iface.gateway
			.as_ref()
			.map(|d| d.ipv6.join_to_string(", ", |a| a.to_string()))
			.unwrap_or("(none)".to_string());
		info!("{}", format!(r#"
			|Interface:
			|      Name: {name}
			|            {name2}
			|      Desc: {desc}
			|   Default: {default}
			|      Type: {itype:?}
			|      IPv4: {addrsv4}
			|      IPv6: {addrsv6}
			|   Gateway: IPv4: {gateway_addrsv4}
			|            IPv6: {gateway_addrsv6}
		"#).trim_margin());
	}

	info!("Getting gateways ...");
	for gateway in GatewayInfo::get_all() {
		let default = gateway.default;
		let route_v4 = gateway.route_v4
			.map(|route| route.to_string())
			.unwrap_or("(none)".to_string());
		let route_v6 = gateway.route_v6
			.map(|route| route.to_string())
		.unwrap_or("(none)".to_string());
		info!("{}", format!(r#"
			|Gateway:
			|    Default: {default}
			|   Route V4: {route_v4}
			|   Route V6: {route_v6}
		"#).trim_margin());
	}

	Ok(())
}
