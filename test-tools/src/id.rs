
use burgerid::{IdentityApp, IdentityEphemeral, IdentityKeyApp, IdentityKeyEphemeral, IdentityKeyPersonal, IdentityPersonal};


pub fn personal_id(name: impl AsRef<str>) -> (IdentityPersonal, IdentityKeyPersonal) {
	IdentityPersonal::new(name, "passphrase")
		.unwrap()
}


pub fn app_id(name: impl AsRef<str>) -> (IdentityApp, IdentityKeyApp, IdentityPersonal, IdentityKeyPersonal) {

	// make the app owner
	let (idp, keyp) = IdentityPersonal::new("owner", "passphrase")
		.unwrap();
	let owner = idp.unlock(&keyp)
		.unwrap();

	// make the app
	let (ida, keya) = IdentityApp::new(name, &owner)
		.unwrap();

	(ida, keya, idp, keyp)
}


pub fn ephemeral_id() -> (IdentityEphemeral, IdentityKeyEphemeral) {
	IdentityEphemeral::new()
		.unwrap()
}
