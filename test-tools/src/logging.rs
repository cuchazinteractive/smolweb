
use display_error_chain::ErrorChainExt;
use time::macros::format_description;
use time::UtcOffset;
use tracing::subscriber::DefaultGuard;
use tracing_subscriber::fmt::format::Format;
use tracing_subscriber::fmt::time::OffsetTime;
use tracing_subscriber::{EnvFilter, FmtSubscriber};


pub fn init_test_logging() -> DefaultGuard {

	// init logging used by WebRTC crates
	std::env::set_var("RUST_LOG", "");
	//std::env::set_var("RUST_LOG", "webrtc_sctp=trace");
	//std::env::set_var("RUST_LOG", "webrtc=trace,webrtc_ice=trace,webrtc_mdns=warn");
	env_logger::try_init()
		.ok();

	const LOG_DIRECTIVE: &str = "smolweb=trace";
	let filter = EnvFilter::builder()
		.parse(LOG_DIRECTIVE)
		.expect(&format!("Failed to parse log directive: {}", LOG_DIRECTIVE));

	// format times into something humans can actually read
	let time_format = format_description!(
		version = 2,
		"[year]-[month]-[day] [hour]:[minute]:[second].[subsecond digits:4] [offset_hour sign:mandatory]:[offset_minute]"
	);

	// try to get the local timezone ... this is hard for some weird reason
	let time_offset = UtcOffset::current_local_offset()
		.unwrap_or_else(|e| {

			// weird...
			println!("\tWarning: Failed to get local time offset: {}", e.into_chain());

			// but just fall back to UTC
			UtcOffset::UTC
		});

	let format = Format::default()
		.with_timer(OffsetTime::new(time_offset, time_format))
		.with_target(false);

	let subscriber = FmtSubscriber::builder()
		.with_env_filter(filter)
		.event_format(format)
		.with_test_writer()
		.finish();

	tracing::subscriber::set_default(subscriber)
}
