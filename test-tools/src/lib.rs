
pub mod tokio;
pub mod id;
pub mod logging;


/// a more expressive version of galvanic_assert's is_variant! based on the excellent matches! macro
/// see: https://github.com/mindsbackyard/galvanic-assert/pull/2
#[macro_export]
macro_rules! is_match {

	( $(|)? $( $pattern:pat_param )|+ $( if $guard:expr )? $(,)? ) => {
		Box::new(|actual: &_| {
			use galvanic_assert::MatchResultBuilder;
			let builder = MatchResultBuilder::for_("is_match");
			if matches!(actual, $( $pattern )|+ $( if $guard )?) {
				builder.matched()
			} else {
				builder.failed_because(
					&format!("Enum value didn't match\n  Expected: {}\n  Got: {:?}", stringify!($( $pattern )|+ $( if $guard )?), actual)
				)
			}
		})
	}
}
