
use std::time::Duration;

use galvanic_assert::{assert_that, matchers::*, structure};
use tokio::sync::mpsc;
use smolweb::host::fetch_server::FetchServer;

use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use smolweb::protobuf::fetch::{client, GatewayResponse, Header, Request, Response, ResponseBodyType, server, ServerResponse};
use smolweb::protobuf::framed::{FrameEvent, FrameReader, FrameReaderTimeout, FrameReceiver, FrameWriter, MAX_PAYLOAD_SIZE};
use smolweb::protobuf::ReadProtobufError;
use smolweb::protobuf::smolweb::fetch::response::server_response::BodyType;


const MAX_PAYLOAD_SIZE_U64: u64 = MAX_PAYLOAD_SIZE as u64;

const TIMEOUT_200MS: Duration = Duration::from_millis(200);

fn timeout_now_200ms() -> FrameReaderTimeout {
	FrameReaderTimeout::Now(TIMEOUT_200MS)
}


type BytesSender = mpsc::UnboundedSender<Vec<u8>>;
type BytesReceiver = mpsc::UnboundedReceiver<Vec<u8>>;

fn init() -> (FrameReceiver<BytesSender,BytesReceiver>, FetchServer<BytesSender,BytesReceiver>) {
	
	let (request_tx, request_rx) = mpsc::unbounded_channel();
	let (response_tx, response_rx) = mpsc::unbounded_channel();

	let client_receiver = FrameReceiver::from(
		FrameWriter::from(request_tx),
		FrameReader::from(response_rx)
	);
	
	let server_receiver = FrameReceiver::from(
		FrameWriter::from(response_tx),
		FrameReader::from(request_rx)
	);
	let server = FetchServer::new(server_receiver);

	(client_receiver, server)
}


#[test]
fn request_garbage() {
	let _logging = init_test_logging();

	// encode something that's not a request at all
	let msg = vec![1, 2, 3];
	let result = server::decode_request(msg);

	assert_that!(&result, is_match!(Err(ReadProtobufError::Prost(..))));
}


#[test]
fn request() {
	let _logging = init_test_logging();

	// send an actual request
	let request = Request {
		method: "awesome".to_string(),
		path: "/foo/bar".to_string(),
		headers: vec![Header::new("foo", "bar")],
		has_body: false,
	};
	let msg = client::encode_request(request.clone());
	let request2 = server::decode_request(msg)
		.unwrap();

	assert_that!(&&request2, eq(&request));
}


#[test]
fn gateway_response_invalid_request() {
	let _logging = init_test_logging();

	let response = GatewayResponse::InvalidRequest {
		reason: "foo".to_string()
	};
	let msg = server::encode_gateway_response(response.clone());
	let response2 = client::decode_response(msg)
		.unwrap();
	let response2 = match response2 {
		Response::Gateway(g) => g,
		r => panic!("unexpected response: {:?}", r)
	};
	
	assert_that!(&response2, eq(response));
}


#[test]
fn gateway_response_server_unreachable() {
	let _logging = init_test_logging();

	let response = GatewayResponse::ServerUnreachable;
	let msg = server::encode_gateway_response(response.clone());
	let response2 = client::decode_response(msg)
		.unwrap();
	let response2 = match response2 {
		Response::Gateway(g) => g,
		r => panic!("unexpected response: {:?}", r)
	};

	assert_that!(&response2, eq(response));
}


#[test]
fn gateway_response_server_response_incomplete() {
	let _logging = init_test_logging();

	let response = GatewayResponse::ServerResponseIncomplete;
	let msg = server::encode_gateway_response(response.clone());
	let response2 = client::decode_response(msg)
		.unwrap();
	let response2 = match response2 {
		Response::Gateway(g) => g,
		r => panic!("unexpected response: {:?}", r)
	};

	assert_that!(&response2, eq(response));
}


#[test]
fn gateway_response_server_response_invalid() {
	let _logging = init_test_logging();

	let response = GatewayResponse::ServerResponseInvalid;
	let msg = server::encode_gateway_response(response.clone());
	let response2 = client::decode_response(msg)
		.unwrap();
	let response2 = match response2 {
		Response::Gateway(g) => g,
		r => panic!("unexpected response: {:?}", r)
	};

	assert_that!(&response2, eq(response));
}


#[test]
fn server_response_server() {
	let _logging = init_test_logging();

	let response = ServerResponse {
		status: 5,
		headers: vec![Header::new("foo", "bar")],
		body_type: BodyType::SingleStream,
	};
	let msg = server::encode_server_response(response.clone());
	let response2 = client::decode_response(msg)
		.unwrap();
	let response2 = match response2 {
		Response::Server(s) => s,
		r => panic!("unexpected response: {:?}", r)
	};

	assert_that!(&response2, eq(response));
}


#[tokio::test]
async fn request_no_body() {
	let _logging = init_test_logging();

	let (client, mut server) = init();

	// send a request with no body
	let request = Request {
		method: "awesome".to_string(),
		path: "/foo/bar".to_string(),
		headers: vec![],
		has_body: false
	};
	client.writer().write(client::encode_request(request))
		.await.unwrap();

	let request = server.recv_request(TIMEOUT_200MS)
		.await.unwrap();

	assert_that!(&request.has_body, eq(false));
}


#[tokio::test]
async fn request_body_all_one_frame() {
	let _logging = init_test_logging();

	let (client, mut server) = init();

	// send a request with a proceeding body
	let request = Request {
		method: "awesome".to_string(),
		path: "/foo/bar".to_string(),
		headers: vec![Header::new("foo", "bar")],
		has_body: true,
	};
	let body = vec![1, 2, 3];
	client.writer().write(client::encode_request(request))
		.await.unwrap();
	client.writer().write(body.clone())
		.await.unwrap();

	let request = server.recv_request(TIMEOUT_200MS)
		.await.unwrap();

	assert_that!(&request.has_body, eq(true));

	let body2 = server.recv_body_all(TIMEOUT_200MS)
		.await.unwrap();

	assert_that!(&&body2, eq(&body));
}


#[tokio::test]
async fn request_body_all_two_frames() {
	let _logging = init_test_logging();

	let (client, mut server) = init();

	// send an actual request with a proceeding body
	let request = Request {
		method: "awesome".to_string(),
		path: "/foo/bar".to_string(),
		headers: vec![Header::new("foo", "bar")],
		has_body: true,
	};
	let body = vec![5u8; MAX_PAYLOAD_SIZE + 5];
	client.writer().write(client::encode_request(request))
		.await.unwrap();
	client.writer().write(body.clone())
		.await.unwrap();

	let request = server.recv_request(TIMEOUT_200MS)
		.await.unwrap();

	assert_that!(&request.has_body, eq(true));

	let body2 = server.recv_body_all(TIMEOUT_200MS)
		.await.unwrap();

	assert_that!(&&body2, eq(&body));
}


#[tokio::test]
async fn request_body_three() {
	let _logging = init_test_logging();

	let (client, mut server) = init();

	// send an actual request with a proceeding body with three frames
	let request = Request {
		method: "awesome".to_string(),
		path: "/foo/bar".to_string(),
		headers: vec![Header::new("foo", "bar")],
		has_body: true,
	};
	let body = vec![
		vec![1u8; MAX_PAYLOAD_SIZE],
		vec![2u8; MAX_PAYLOAD_SIZE],
		vec![3u8; MAX_PAYLOAD_SIZE]
	].concat();
	client.writer().write(client::encode_request(request))
		.await.unwrap();
	client.writer().write(body.clone())
		.await.unwrap();

	let request = server.recv_request(TIMEOUT_200MS)
		.await.unwrap();

	assert_that!(&request.has_body, eq(true));

	// should get three frame events
	let event = server.recv_body()
		.await.unwrap();
	assert_that!(&&event, structure!(FrameEvent::Start {
		bytes_total: eq(MAX_PAYLOAD_SIZE_U64*3),
		payload: eq(vec![1u8; MAX_PAYLOAD_SIZE])
	}));

	let event = server.recv_body()
		.await.unwrap();
	assert_that!(&&event, structure!(FrameEvent::Continue {
		bytes_read: eq(MAX_PAYLOAD_SIZE_U64*2),
		payload: eq(vec![2u8; MAX_PAYLOAD_SIZE])
	}));
	let event = server.recv_body()
		.await.unwrap();
	assert_that!(&&event, structure!(FrameEvent::Finish {
		payload: eq(vec![3u8; MAX_PAYLOAD_SIZE])
	}));
}


#[tokio::test]
async fn response_gateway() {
	let _logging = init_test_logging();

	let (mut client, server) = init();

	let response = GatewayResponse::InvalidRequest {
		reason: "nope".to_string()
	};
	server.respond_gateway(response.clone())
		.await.unwrap();

	let msg = client.recv_all(timeout_now_200ms())
		.await.unwrap();
	let response2 = client::decode_response(msg)
		.unwrap();

	assert_that!(&&response2, is_match!(Response::Gateway(GatewayResponse::InvalidRequest { .. })));
}


#[tokio::test]
async fn response_server_no_body() {
	let _logging = init_test_logging();

	let (mut client, server) = init();

	let response = ServerResponse {
		status: 5,
		headers: vec![Header::new("k", "v")],
		body_type: ResponseBodyType::None
	};
	let body_writer = server.respond_server(response.clone())
		.await.unwrap();

	assert_that!(&body_writer, is_match!(None));

	let msg = client.recv_all(timeout_now_200ms())
		.await.unwrap();
	let response2 = client::decode_response(msg)
		.unwrap();

	assert_that!(&&response2, is_match!(Response::Server(..)));
}


#[tokio::test]
async fn response_server_body_write_all() {
	let _logging = init_test_logging();

	let (mut client, server) = init();

	let response = ServerResponse {
		status: 42,
		headers: vec![],
		body_type: ResponseBodyType::SingleStream
	};
	let body = vec![1, 2, 3];
	let body_writer = server.respond_server(response.clone())
		.await.unwrap()
		.unwrap();
	body_writer.write_all(body.clone())
		.await.unwrap();

	let msg = client.recv_all(timeout_now_200ms())
		.await.unwrap();
	let response2 = client::decode_response(msg)
		.unwrap();

	assert_that!(&&response2, is_match!(Response::Server(..)));

	let body2 = client.recv_all(timeout_now_200ms())
		.await.unwrap();
	assert_that!(&&body2, eq(&body));
}


#[tokio::test]
async fn response_server_body_stream_three() {
	let _logging = init_test_logging();

	let (mut client, server) = init();

	let response = ServerResponse {
		status: 42,
		headers: vec![],
		body_type: ResponseBodyType::SingleStream
	};
	let body = vec![
		vec![1u8; MAX_PAYLOAD_SIZE],
		vec![2u8; MAX_PAYLOAD_SIZE],
		vec![3u8; MAX_PAYLOAD_SIZE]
	].concat();
	let body_writer = server.respond_server(response.clone())
		.await.unwrap()
		.unwrap();
	let mut streamer = body_writer.stream(body.len() as u64)
		.unwrap();
	streamer.write(body[..MAX_PAYLOAD_SIZE].to_vec())
		.await.unwrap();
	streamer.write(body[MAX_PAYLOAD_SIZE..MAX_PAYLOAD_SIZE*2].to_vec())
		.await.unwrap();
	streamer.write(body[MAX_PAYLOAD_SIZE*2..].to_vec())
		.await.unwrap();
	drop(streamer);

	let msg = client.recv_all(timeout_now_200ms())
		.await.unwrap();
	let response2 = client::decode_response(msg)
		.unwrap();

	assert_that!(&&response2, is_match!(Response::Server(..)));

	let body2 = client.recv_all(timeout_now_200ms())
		.await.unwrap();
	assert_that!(&&body2, eq(&body));
}
