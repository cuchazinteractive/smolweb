
use burgerid::ProtobufSave;
use galvanic_assert::{assert_that, matchers::*};

use smolweb_test_tools::is_match;
use smolweb_test_tools::id::{app_id, ephemeral_id};
use smolweb_test_tools::webrtc::{answer, offer};
use smolweb_test_tools::logging::init_test_logging;

use smolweb::protobuf::guests::{guest, beacon, About};
use smolweb::protobuf::guests::guest::ConnectionReject;
use smolweb::protobuf::hosts;
use smolweb::protobuf::hosts::beacon::HostMessage;
use smolweb::webrtc::SessionDescription;


#[test]
fn about() {
	let _logging = init_test_logging();

	let about = About {
		version: "nope".to_string()
	};

	let msg = beacon::encode_about(about.clone());
	let about2 = guest::decode_about(msg)
		.unwrap();

	assert_that!(&&about2, eq(&about));
}


#[test]
fn host_identity_request() {
	let _logging = init_test_logging();

	let host_uid = vec![1, 2, 3];

	let msg = guest::encode_host_identity_request(&host_uid);
	let host_uid2 = beacon::decode_host_identity_request(msg)
		.unwrap();

	assert_that!(&&host_uid2, eq(&host_uid));
}


#[test]
fn host_identity_response() {
	let _logging = init_test_logging();

	let (host_id, _key, _owner, _owner_key) = app_id("Host");

	let msg = beacon::encode_host_identity_response(Some(&host_id));
	let id2 = guest::decode_host_identity_response(msg)
		.unwrap()
		.unwrap();

	assert_that!(&&id2, eq(&host_id));

	let msg = beacon::encode_host_identity_response(None);
	let result = guest::decode_host_identity_response(msg)
		.unwrap();

	assert_that!(&result, is_match!(None));
}


#[tokio::test]
async fn connect_request() {
	let _logging = init_test_logging();

	let (guest_id, guest_key) = ephemeral_id();
	let guest_id_unlocked = guest_id.unlock(&guest_key)
		.unwrap();
	let (host_id, _key, _owner, _owner_key) = app_id("Host");

	let offer1 = offer()
		.await;
	let msg = guest::encode_connection_request(&guest_id, &guest_id_unlocked, &host_id, offer1.into())
		.unwrap();
	let request = beacon::decode_connection_request(msg)
		.unwrap();

	assert_that!(&&request.host_uid, eq(host_id.uid()));
	assert_that!(&request.identity, eq(guest_id.save_bytes()));
}


#[tokio::test]
async fn connect_response_accept() {
	let _logging = init_test_logging();

	let (guest_id, guest_key) = ephemeral_id();
	let guest_id_unlocked = guest_id.unlock(&guest_key)
		.unwrap();
	let (host_id, host_key, _owner, _owner_key) = app_id("Host");
	let host_id_unlocked = host_id.unlock(&host_key)
		.unwrap();

	// get the secret from the host side
	let conn_id = 42;
	let offer1 = offer()
		.await;
	let answer1: SessionDescription = answer(offer1)
		.await
		.into();
	let offer2: SessionDescription = offer()
		.await
		.into();
	let host_msg = hosts::host::encode_guest_connect_accept(
		&host_id_unlocked,
		&guest_id,
		5,
		conn_id,
		answer1.clone(),
		offer2.clone()
	).unwrap();
	let host_msg = hosts::beacon::decode_msg(host_msg)
		.unwrap();
	let secret = match host_msg {
		HostMessage::GuestConnectResponse(response) => response.secret
	};

	let msg = beacon::encode_connection_response_accept(secret);
	let accept = guest::decode_connection_response(msg, &guest_id_unlocked, &host_id)
		.unwrap()
		.unwrap();

	assert_that!(&accept.connection_id, eq(conn_id));
	assert_that!(&&accept.answer1, eq(&answer1));
	assert_that!(&&accept.offer2, eq(&offer2));
}


#[tokio::test]
async fn connect_response_host_unknown() {
	let _logging = init_test_logging();

	let (guest_id, guest_key) = ephemeral_id();
	let guest_id_unlocked = guest_id.unlock(&guest_key)
		.unwrap();
	let (host_id, _key, _owner, _owner_key) = app_id("Host");

	let msg = beacon::encode_connection_response_host_unknown();
	let result = guest::decode_connection_response(msg, &guest_id_unlocked, &host_id)
		.unwrap();

	assert_that!(&result, is_match!(Err(ConnectionReject::HostUnknown)));
}


#[tokio::test]
async fn connect_response_host_unreachable() {
	let _logging = init_test_logging();

	let (guest_id, guest_key) = ephemeral_id();
	let guest_id_unlocked = guest_id.unlock(&guest_key)
		.unwrap();
	let (host_id, _key, _owner, _owner_key) = app_id("Host");

	let msg = beacon::encode_connection_response_host_unreachable();
	let result = guest::decode_connection_response(msg, &guest_id_unlocked, &host_id)
		.unwrap();

	assert_that!(&result, is_match!(Err(ConnectionReject::HostUnreachable)));
}


#[tokio::test]
async fn connect_confirm() {
	let _logging = init_test_logging();

	let (guest_id, guest_key) = ephemeral_id();
	let guest_id_unlocked = guest_id.unlock(&guest_key)
		.unwrap();
	let (host_id, _key, _owner, _owner_key) = app_id("Host");

	let offer2 = offer()
		.await;
	let answer2 = answer(offer2)
		.await
		.into();
	let conn_id = 5;

	let msg = guest::encode_connection_confirm(&guest_id_unlocked, &host_id, conn_id, answer2)
		.unwrap();
	let confirm = beacon::decode_connection_confirm(msg)
		.unwrap();

	assert_that!(&&confirm.host_uid, eq(host_id.uid()));
	assert_that!(&&confirm.guest_uid, eq(guest_id.uid()));
	assert_that!(&confirm.secret.len(), gt(0));
}
