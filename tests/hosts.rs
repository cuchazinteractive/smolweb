
use burgerid::ProtobufSave;
use galvanic_assert::{assert_that, matchers::*};
use prost::Message;

use smolweb_test_tools::id::{app_id, ephemeral_id};
use smolweb_test_tools::is_match;
use smolweb_test_tools::webrtc::{answer, offer};
use smolweb_test_tools::logging::init_test_logging;

use smolweb::protobuf::guests;
use smolweb::protobuf::hosts::{host, beacon, AuthnError};
use smolweb::protobuf::hosts::beacon::DecodeReadyRequestError;
use smolweb::protobuf::smolweb as proto;
use smolweb::webrtc::SessionDescription;


#[test]
fn ready_request() {
	let _logging = init_test_logging();

	let (host_id, _host_key, _owner, _owner_key) = app_id("Host");

	let msg = host::encode_ready_request(&host_id);
	let request = beacon::decode_ready_request(msg)
		.unwrap();

	assert_that!(&&request.host_id.uid(), eq(host_id.uid()));
}


#[test]
fn ready_request_bad_id() {
	let _logging = init_test_logging();

	let msg = proto::hosts::HostReadyRequest {
		app_id: vec![1, 2, 3]
	}.encode_to_vec();
	let result = beacon::decode_ready_request(msg);

	assert_that!(&result, is_match!(Err(DecodeReadyRequestError::Identity(..))));
}


#[test]
fn ready_challenge() {
	let _logging = init_test_logging();

	let msg = beacon::encode_ready_challenge_error_already_ready();
	let challenge = host::decode_ready_challenge(msg)
		.unwrap();
	assert_that!(&challenge, is_match!(host::ReadyChallenge::AlreadyReady));

	let msg = beacon::encode_ready_challenge_error_authn(AuthnError::Fail);
	let challenge = host::decode_ready_challenge(msg)
		.unwrap();
	assert_that!(&challenge, is_match!(host::ReadyChallenge::Error(AuthnError::Fail)));

	let nonce = vec![1, 2, 3]; // it's only ever used here, I swear
	let msg = beacon::encode_ready_challenge_nonce(nonce.clone());
	let host::ReadyChallenge::Challenge { nonce: nonce2 } = host::decode_ready_challenge(msg)
		.unwrap()
		else { panic!("not nonce") };
	assert_that!(&&nonce2, eq(&nonce));
}


#[test]
fn ready_response() {
	let _logging = init_test_logging();

	let (host_id, host_key, _owner, _owner_key) = app_id("Host");
	let host_id_unlocked = host_id.unlock(&host_key)
		.unwrap();

	let nonce = vec![1, 2, 3];
	let msg = host::encode_ready_response(&host_id_unlocked, &nonce)
		.unwrap();
	let response = beacon::decode_ready_response(msg)
		.unwrap();

	assert_that!(&&response.signed_nonce.len(), gt(0));
}


#[test]
fn ready_result() {
	let _logging = init_test_logging();

	let msg = beacon::encode_ready_result_ok();
	let result = host::decode_ready_result(msg)
		.unwrap();
	assert_that!(&result, is_match!(host::ReadyResult::Ok));

	let msg = beacon::encode_ready_result_error_authn(AuthnError::TooManyAttempts);
	let result = host::decode_ready_result(msg)
		.unwrap();
	assert_that!(&result, is_match!(host::ReadyResult::Error(AuthnError::TooManyAttempts)));

	let msg = beacon::encode_ready_result_error_not_authorized();
	let result = host::decode_ready_result(msg)
		.unwrap();
	assert_that!(&result, is_match!(host::ReadyResult::NotAuthorized));
}


#[tokio::test]
async fn guest_connect() {
	let _logging = init_test_logging();

	let (guest_id, guest_key) = ephemeral_id();
	let guest_id_unlocked = guest_id.unlock(&guest_key)
		.unwrap();
	let (host_id, host_key, _owner, _owner_key) = app_id("Host");
	let host_id_unlocked = host_id.unlock(&host_key)
		.unwrap();

	// get the secret from the guest side
	let offer1: SessionDescription = offer()
		.await
		.into();
	let msg = guests::guest::encode_connection_request(&guest_id, &guest_id_unlocked, &host_id, offer1.clone())
		.unwrap();
	let secret = guests::beacon::decode_connection_request(msg)
		.unwrap()
		.secret;

	let request_id = 5;
	let msg = beacon::encode_guest_connect(request_id, guest_id.save_bytes(), secret);
	let msg = host::decode_msg(msg)
		.unwrap();
	let request = match msg {
		host::BeaconMessage::GuestConnectRequest(r) => r,
		msg => panic!("unexpected beacon message: {:?}", msg)
	};
	let request = request.decode(&host_id_unlocked)
		.unwrap();

	assert_that!(&request.request_id, eq(request_id));
	assert_that!(&&request.guest_id, eq(&guest_id));
	assert_that!(&request.offer1, eq(offer1));
}


#[tokio::test]
async fn guest_connect_accept() {
	let _logging = init_test_logging();

	let (guest_id, _guest_key) = ephemeral_id();
	let (host_id, host_key, _owner, _owner_key) = app_id("Host");
	let host_id_unlocked = host_id.unlock(&host_key)
		.unwrap();

	let request_id = 42;
	let conn_id = 5;
	let offer1 = offer()
		.await;
	let answer1: SessionDescription = answer(offer1)
		.await
		.into();
	let offer2: SessionDescription = offer()
		.await
		.into();

	let msg = host::encode_guest_connect_accept(&host_id_unlocked, &guest_id, request_id, conn_id, answer1, offer2)
		.unwrap();
	let msg = beacon::decode_msg(msg)
		.unwrap();
	let request = match msg {
		beacon::HostMessage::GuestConnectResponse(r) => r
	};

	assert_that!(&request.request_id, eq(request_id));
	assert_that!(&request.secret.len(), gt(0));
}


#[tokio::test]
async fn guest_confirm() {
	let _logging = init_test_logging();

	let (guest_id, guest_key) = ephemeral_id();
	let guest_id_unlocked = guest_id.unlock(&guest_key)
		.unwrap();
	let (host_id, host_key, _owner, _owner_key) = app_id("Host");
	let host_id_unlocked = host_id.unlock(&host_key)
		.unwrap();

	// get the secret from the guest side
	let conn_id = 5;
	let offer2 = offer()
		.await;
	let answer2: SessionDescription = answer(offer2)
		.await
		.into();
	let msg = guests::guest::encode_connection_confirm(&guest_id_unlocked, &host_id, conn_id, answer2.clone())
		.unwrap();
	let secret = guests::beacon::decode_connection_confirm(msg)
		.unwrap()
		.secret;

	let msg = beacon::encode_guest_confirm(guest_id.uid(), secret);
	let msg = host::decode_msg(msg)
		.unwrap();
	let confirm = match msg {
		host::BeaconMessage::GuestConnectConfirm(c) => c,
		msg => panic!("unexpected beacon message: {:?}", msg)
	};

	assert_that!(&confirm.guest_uid(), eq(guest_id.uid()));

	let confirm = confirm.decode(&host_id_unlocked, &guest_id)
		.unwrap();

	assert_that!(&confirm.connection_id, eq(conn_id));
	assert_that!(&confirm.answer2, eq(answer2));
}
