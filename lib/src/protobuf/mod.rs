
use prost::Message;
use thiserror::Error;


pub mod hosts;
pub mod guests;
pub mod framed;
pub mod fetch;


pub(crate) mod smolweb {
	#![allow(unused)]
	pub mod hosts {
		include!(concat!(env!("OUT_DIR"), "/smolweb.hosts.rs"));
	}
	pub mod guests {
		include!(concat!(env!("OUT_DIR"), "/smolweb.guests.rs"));
	}
	pub mod secrets {
		include!(concat!(env!("OUT_DIR"), "/smolweb.secrets.rs"));
	}
	pub mod framed {
		include!(concat!(env!("OUT_DIR"), "/smolweb.framed.rs"));
	}
	pub mod fetch {
		include!(concat!(env!("OUT_DIR"), "/smolweb.fetch.rs"));
	}
	pub mod admin {
		include!(concat!(env!("OUT_DIR"), "/smolweb.admin.rs"));
	}
}


pub type HostUid = Vec<u8>;

const CONTEXT_CONNECTION_REQUEST: &[u8; 33] = b"smolweb/guests/connection/request";
const CONTEXT_CONNECTION_RESPONSE: &[u8; 34] = b"smolweb/guests/connection/response";
const CONTEXT_CONNECTION_CONFIRM: &[u8; 33] = b"smolweb/guests/connection/confirm";



pub(crate) trait IntoProtobuf<T> {
	fn into_protobuf(self) -> Result<T,ReadProtobufError>;
}

impl<T,S> IntoProtobuf<T> for S
	where
		T: Message + Default,
		S: AsRef<[u8]>
{
	fn into_protobuf(self) -> Result<T,ReadProtobufError> {
		Message::decode(self.as_ref())
			.map_err(|e| ReadProtobufError::Prost(e))
	}
}


#[derive(Error, Debug, Clone)]
pub enum ReadProtobufError {

	#[error("Failed to decode protobuf from bytes: {0}")]
	Prost(#[from] prost::DecodeError),

	#[error("Protobuf is missing field: {0}")]
	MissingField(&'static str),

	#[error("Protobuf field {0} has an invalid value: {1}")]
	InvalidValue(&'static str, String)
}
