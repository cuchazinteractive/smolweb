
use std::collections::VecDeque;
use std::convert::Infallible;
use std::sync::Arc;
use std::time::Duration;

use async_stream::stream;
use axum::async_trait;
use futures::{AsyncReadExt, StreamExt};
use futures::io::Cursor;
use galvanic_assert::{all_of, assert_that, structure, matchers::*};
use prost::Message;
use tokio::sync::{mpsc, Mutex, oneshot};
use tokio::time::sleep;
use tracing::info;

use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::protobuf::framed::*;
use crate::protobuf::smolweb;


// because rustc apparently won't let us have `as` operators in match expressions
const MAX_PAYLOAD_SIZE_I64: i64 = MAX_PAYLOAD_SIZE as i64;
const MAX_PAYLOAD_SIZE_U64: u64 = MAX_PAYLOAD_SIZE as u64;


#[test]
fn overhead() {
	let _logging = init_test_logging();

	let mut framer = Framer::new(i64::MAX as u64)
		.unwrap();

	let start_frame = framer.frame(&vec![0u8; MAX_PAYLOAD_SIZE])
		.unwrap();
	let start_overhead = start_frame.len() - MAX_PAYLOAD_SIZE;
	info!("start frame overhead: {}  {:?}", start_overhead, &start_frame[0..20]);

	let continue_frame = framer.frame(&vec![0u8; MAX_PAYLOAD_SIZE])
		.unwrap();
	let continue_overhead = continue_frame.len() - MAX_PAYLOAD_SIZE;
	info!("continue frame overhead: {}  {:?}", continue_overhead, &continue_frame[0..20]);

	// the start frame should hit the limits
	// start frame overhead: 16  [10, 253, 127, 8, 255, 255, 255, 255, 255, 255, 255, 255, 127, 18, 240, 127, 0, 0, 0, 0, ...]
	assert_that!(&start_overhead, eq(MAX_OVERHEAD_SIZE));
	assert_that!(&start_frame.len(), eq(MAX_MESSAGE_SIZE));

	// but the continue frame won't, because the index is low
	// continue frame overhead: 8  [18, 245, 127, 8, 1, 18, 240, 127, 0, 0, 0, 0, ...]
	assert_that!(&continue_overhead, leq(MAX_OVERHEAD_SIZE));
	assert_that!(&continue_frame.len(), leq(MAX_MESSAGE_SIZE));
}


#[test]
fn framer_valid_sizes() {
	let _logging = init_test_logging();

	assert_that!(&Framer::new(1), is_match!(Ok(..)));
	assert_that!(&Framer::new((i64::MAX as u64) - 1), is_match!(Ok(..)));
	assert_that!(&Framer::new(i64::MAX as u64), is_match!(Ok(..)));

	assert_that!(&Framer::new(0), is_match!(Err(NewFramerError::InvalidStreamSize {
		observed: 0,
		..
	})));
	assert_that!(&Framer::new((i64::MAX as u64) + 1), is_match!(Err(NewFramerError::InvalidStreamSize { .. })));
	assert_that!(&Framer::new(u64::MAX), is_match!(Err(NewFramerError::InvalidStreamSize {
		observed: u64::MAX,
		..
	})));
}


#[test]
fn frame_single() {
	let _logging = init_test_logging();

	let mut framer = Framer::new(3)
		.unwrap();
	let payload = vec![1, 2, 3];
	let frame = framer.frame(&payload)
		.unwrap();

	assert_that!(&frame.len(), all_of!(geq(payload.len()), leq(payload.len() + MAX_OVERHEAD_SIZE)));
	assert_that!(&framer.bytes_written(), eq(3));
	assert_that!(&framer.is_closed(), eq(true));
	assert_that!(&framer.frame(&vec![]), is_match!(Err(FramerError::Closed)));
}


#[test]
fn frame_unexpected_payload() {
	let _logging = init_test_logging();

	let mut framer = Framer::new(42)
		.unwrap();
	let payload = vec![1, 2, 3];
	let result = framer.frame(&payload);

	assert_that!(&result, is_match!(Err(FramerError::UnexpectedPayloadSize {
			expected: 42,
			observed: 3
		})));

	// but the stream should still be open to try again
	assert_that!(&framer.bytes_written(), eq(0));
	assert_that!(&framer.is_closed(), eq(false));
}


#[test]
fn frame_multi() {
	let _logging = init_test_logging();

	let mut framer = Framer::new(MAX_PAYLOAD_SIZE_U64 + 3)
		.unwrap();
	let payload = vec![0u8; MAX_PAYLOAD_SIZE];
	let frame = framer.frame(&payload)
		.unwrap();

	assert_that!(&frame.len(), all_of!(geq(payload.len()), leq(payload.len() + MAX_OVERHEAD_SIZE)));
	assert_that!(&framer.bytes_written(), eq(MAX_PAYLOAD_SIZE_U64));
	assert_that!(&framer.is_closed(), eq(false));

	let payload = vec![1, 2, 3];
	let frame = framer.frame(&payload)
		.unwrap();

	assert_that!(&frame.len(), all_of!(geq(payload.len()), leq(payload.len() + MAX_OVERHEAD_SIZE)));
	assert_that!(&framer.bytes_written(), eq(MAX_PAYLOAD_SIZE_U64 + 3));
	assert_that!(&framer.is_closed(), eq(true));
}


#[test]
fn frame_error() {
	let _logging = init_test_logging();

	let framer = Framer::new(3)
		.unwrap();

	assert_that!(&framer.frame_error(RemoteFrameError::Abort), is_match!(Ok(..)));
}


#[test]
fn frame_error_closed() {
	let _logging = init_test_logging();

	let mut framer = Framer::new(3)
		.unwrap();
	framer.frame(&vec![1, 2, 3])
		.unwrap();

	assert_that!(&framer.frame_error(RemoteFrameError::Abort), is_match!(Err(FramerError::Closed)));
}


#[test]
fn deframer_new_decode_error() {
	let _logging = init_test_logging();

	let result = Deframer::new(&[0u8; 0]);
	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::Decode(..)))));
}


fn start_frame<B:AsRef<[u8]>>(size: i64, payload: B) -> Vec<u8> {
	smolweb::framed::Frame {
		kind: Some(smolweb::framed::frame::Kind::Start(smolweb::framed::FrameStart {
			stream_size: size,
			payload: payload.as_ref().to_vec(),
		}))
	}.encode_to_vec()
}

fn continue_frame<B:AsRef<[u8]>>(index: i64, payload: B) -> Vec<u8> {
	smolweb::framed::Frame {
		kind: Some(smolweb::framed::frame::Kind::Continue(smolweb::framed::FrameContinue {
			frame_index: index,
			payload: payload.as_ref().to_vec(),
		}))
	}.encode_to_vec()
}

fn error_frame(e: Option<smolweb::framed::frame_error::Error>) -> Vec<u8> {
	smolweb::framed::Frame {
		kind: Some(smolweb::framed::frame::Kind::Error(smolweb::framed::FrameError {
			error: e,
		}))
	}.encode_to_vec()
}


#[test]
fn deframer_new_remote_error_invalid() {
	let _logging = init_test_logging();

	let frame = error_frame(None);

	let result = Deframer::new(&frame);
	assert_that!(&result, is_match!(Err(DeframerError::Remote(Err(..)))));
}


#[test]
fn deframer_new_remote_error() {
	let _logging = init_test_logging();

	let frame = error_frame(Some(smolweb::framed::frame_error::Error::Abort(true)));

	let result = Deframer::new(frame);
	assert_that!(&result, is_match!(Err(DeframerError::Remote(Ok(RemoteFrameError::Abort)))));
}


#[test]
fn deframer_new_unexpected_kind() {
	let _logging = init_test_logging();

	let frame = continue_frame(1, &vec![]);

	let result = Deframer::new(frame);
	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedKind { .. }))));
}


#[test]
fn deframer_new_sizes() {
	let _logging = init_test_logging();

	let frame = start_frame(-1, &vec![]);
	let result = Deframer::new(frame);

	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::InvalidStreamSize(-1)))));

	let frame = start_frame(0, &vec![]);
	let result = Deframer::new(frame);

	// TODO: is this technically an error?
	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::InvalidStreamSize(0)))));

	let frame = start_frame(1, &vec![5]);
	let (deframer, _) = Deframer::new(frame)
		.unwrap();

	assert_that!(&deframer.bytes_total(), eq(1));

	let frame = start_frame(i64::MAX, &vec![0u8; MAX_PAYLOAD_SIZE]);
	let (deframer, _) = Deframer::new(frame)
		.unwrap();

	assert_that!(&deframer.bytes_total(), eq(i64::MAX as u64));
}


#[test]
fn deframer_new_payloads() {
	let _logging = init_test_logging();

	let frame = start_frame(1, &vec![5]);
	assert_that!(&Deframer::new(frame), is_match!(Ok(..)));

	let frame = start_frame(1, &vec![5, 42]);
	assert_that!(&Deframer::new(frame), is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedPayloadSize {
		expected: 1,
		observed: 2
	}))));

	let frame = start_frame(2, &vec![5]);
	assert_that!(&Deframer::new(frame), is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedPayloadSize {
		expected: 2,
		observed: 1
	}))));

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64, &vec![5]);
	assert_that!(&Deframer::new(frame), is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedPayloadSize {
		expected: MAX_PAYLOAD_SIZE_U64,
		observed: 1
	}))));

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64, &vec![0u8; MAX_PAYLOAD_SIZE]);
	assert_that!(&Deframer::new(frame), is_match!(Ok(..)));

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 1, &vec![5]);
	assert_that!(&Deframer::new(frame), is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedPayloadSize {
		expected: MAX_PAYLOAD_SIZE_U64,
		observed: 1
	}))));

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 1, &vec![0u8; MAX_PAYLOAD_SIZE]);
	assert_that!(&Deframer::new(frame), is_match!(Ok(..)));
}


#[test]
fn deframer_new() {
	let _logging = init_test_logging();

	let payload = vec![1, 2, 2];
	let frame = start_frame(3, &payload);
	let (deframer, payload2) = Deframer::new(frame)
		.unwrap();

	assert_that!(&&payload2, eq(&payload));
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&deframer.is_closed(), eq(true));

	let payload = vec![5u8; MAX_PAYLOAD_SIZE];
	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 3, &payload);
	let (deframer, payload2) = Deframer::new(frame)
		.unwrap();

	assert_that!(&&payload2, eq(&payload));
	assert_that!(&deframer.is_complete(), eq(false));
	assert_that!(&deframer.is_closed(), eq(false));
}


#[test]
fn deframer_deframe_complete() {
	let _logging = init_test_logging();

	let frame = start_frame(3, &vec![1, 2, 3]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let result = deframer.deframe(&[]);
	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::Closed))));
}


#[test]
fn deframer_deframe_decode_error() {
	let _logging = init_test_logging();

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 3, &vec![0u8; MAX_PAYLOAD_SIZE]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let result = deframer.deframe(&[1, 2, 3]);
	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::Decode(..)))));
}


#[test]
fn deframer_deframe_unexpected_kind() {
	let _logging = init_test_logging();

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 3, &vec![0u8; MAX_PAYLOAD_SIZE]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let frame = start_frame(1, &vec![]);
	let result = deframer.deframe(frame);
	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedKind { .. }))));
}


#[test]
fn deframer_deframe_remote_error_error() {
	let _logging = init_test_logging();

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 3, &vec![0u8; MAX_PAYLOAD_SIZE]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let frame = error_frame(None);
	let result = deframer.deframe(frame);

	assert_that!(&result, is_match!(Err(DeframerError::Remote(Err(..)))));
	assert_that!(&deframer.is_complete(), eq(false));
	assert_that!(&deframer.is_closed(), eq(true));
}


#[test]
fn deframer_deframe_remote_error() {
	let _logging = init_test_logging();

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 3, &vec![0u8; MAX_PAYLOAD_SIZE]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let frame = error_frame(Some(smolweb::framed::frame_error::Error::Abort(true)));
	let result = deframer.deframe(frame);

	assert_that!(&result, is_match!(Err(DeframerError::Remote(Ok(RemoteFrameError::Abort)))));
	assert_that!(&deframer.is_complete(), eq(false));
	assert_that!(&deframer.is_closed(), eq(true));
}


#[test]
fn deframer_deframe_unexpected_index() {
	let _logging = init_test_logging();

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 3, &vec![0u8; MAX_PAYLOAD_SIZE]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let frame = continue_frame(-5, vec![]);
	let result = deframer.deframe(frame);

	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedFrameIndex {
		expected: 1,
		observed: -5
	}))));

	let frame = continue_frame(0, vec![]);
	let result = deframer.deframe(frame);

	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedFrameIndex {
		expected: 1,
		observed: 0
	}))));
}


#[test]
fn deframer_deframe_unexpected_payload() {
	let _logging = init_test_logging();

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 3, &vec![0u8; MAX_PAYLOAD_SIZE]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let frame = continue_frame(1, vec![]);
	let result = deframer.deframe(frame);

	assert_that!(&result, is_match!(Err(DeframerError::Local(LocalDeframerError::UnexpectedPayloadSize {
		expected: 3,
		observed: 0
	}))));
}


#[test]
fn deframer_deframe_one() {
	let _logging = init_test_logging();

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64 + 3, &vec![5u8; MAX_PAYLOAD_SIZE]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let payload = vec![1, 2, 3];
	let frame = continue_frame(1, &payload);
	let payload2 = deframer.deframe(frame)
		.unwrap();

	assert_that!(&&payload2, eq(&payload));
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&deframer.is_closed(), eq(true));
}


#[test]
fn deframer_deframe_two() {
	let _logging = init_test_logging();

	let frame = start_frame(MAX_PAYLOAD_SIZE_I64*2 + 3, &vec![5u8; MAX_PAYLOAD_SIZE]);
	let (mut deframer, _) = Deframer::new(frame)
		.unwrap();

	let payload = vec![42u8; MAX_PAYLOAD_SIZE];
	let frame = continue_frame(1, &payload);
	let payload2 = deframer.deframe(frame)
		.unwrap();

	assert_that!(&&payload2, eq(&payload));
	assert_that!(&deframer.is_complete(), eq(false));
	assert_that!(&deframer.is_closed(), eq(false));

	let payload = vec![1, 2, 3];
	let frame = continue_frame(2, &payload);
	let payload2 = deframer.deframe(frame)
		.unwrap();

	assert_that!(&&payload2, eq(&payload));
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&deframer.is_closed(), eq(true));
}


#[derive(Clone)]
struct TestWriter {
	written: Arc<Mutex<Vec<Vec<u8>>>>
}

impl TestWriter {

	fn new() -> Self {
		Self {
			written: Arc::new(Mutex::new(Vec::new()))
		}
	}

	fn written(self) -> Vec<Vec<u8>> {
		Arc::try_unwrap(self.written)
			.unwrap()
			.into_inner()
	}
}


#[async_trait]
impl Writer for TestWriter {

	type Error = Infallible;

	async fn write(&self, data: impl Into<Vec<u8>> + Send) -> Result<(),Self::Error> {

		let mut written = self.written.lock()
			.await;
		written.push(data.into());

		// writes never fail
		Ok(())
	}
}


#[tokio::test]
async fn write_one() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![1, 2, 3];
	writer.write(data.clone())
		.await.unwrap();

	// should have written one frame
	let written = writer.into_writer().written();
	let (deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&deframer.bytes_total(), eq(data.len() as u64));
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&&payload, eq(&data));

	assert_that!(&written.len(), eq(1));
}


#[tokio::test]
async fn write_two() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![5u8; MAX_PAYLOAD_SIZE + 3];
	writer.write(data.clone())
		.await.unwrap();

	// should have written two frames
	let written = writer.into_writer().written();
	let (mut deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&payload, eq(vec![5u8; MAX_PAYLOAD_SIZE]));
	assert_that!(&deframer.bytes_total(), eq(data.len() as u64));
	assert_that!(&deframer.is_complete(), eq(false));

	let payload = deframer.deframe(&written[1])
		.unwrap();
	assert_that!(&payload, eq(vec![5u8; 3]));
	assert_that!(&deframer.is_complete(), eq(true));

	assert_that!(&written.len(), eq(2));
}


#[tokio::test]
async fn write_error() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	writer.write_error(RemoteFrameError::Abort)
		.await.unwrap();

	// should have written one error frame
	let written = writer.into_writer().written();
	let result = Deframer::new(&written[0]);
	assert_that!(&result, is_match!(Err(DeframerError::Remote(Ok(RemoteFrameError::Abort)))));

	assert_that!(&written.len(), eq(1));
}


#[tokio::test]
async fn write_stream_one() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![1, 2, 3];
	let mut stream = writer.stream(data.len() as u64)
		.unwrap();

	assert_that!(&stream.is_closed(), eq(false));
	stream.write(data.clone())
		.await.unwrap();
	assert_that!(&stream.is_closed(), eq(true));

	// should have written one frame
	let written = writer.into_writer().written();
	let (deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&deframer.bytes_total(), eq(data.len() as u64));
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&&payload, eq(&data));

	assert_that!(&written.len(), eq(1));
}


#[tokio::test]
async fn write_stream_two() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![5u8; MAX_PAYLOAD_SIZE + 5];
	let mut stream = writer.stream(data.len() as u64)
		.unwrap();

	assert_that!(&stream.is_closed(), eq(false));
	stream.write(data[..MAX_PAYLOAD_SIZE].to_vec())
		.await.unwrap();
	assert_that!(&stream.is_closed(), eq(false));
	stream.write(data[MAX_PAYLOAD_SIZE..].to_vec())
		.await.unwrap();
	assert_that!(&stream.is_closed(), eq(true));

	// should have written two frames
	let written = writer.into_writer().written();
	let (mut deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&payload, eq(vec![5u8; MAX_PAYLOAD_SIZE]));
	assert_that!(&deframer.bytes_total(), eq(data.len() as u64));
	assert_that!(&deframer.is_complete(), eq(false));

	let payload = deframer.deframe(&written[1])
		.unwrap();
	assert_that!(&payload, eq(vec![5u8; 5]));
	assert_that!(&deframer.is_complete(), eq(true));

	assert_that!(&written.len(), eq(2));
}


#[tokio::test]
async fn write_stream_abort() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![5u8; MAX_PAYLOAD_SIZE + 5];
	let mut stream = writer.stream(data.len() as u64)
		.unwrap();

	assert_that!(&stream.is_closed(), eq(false));
	stream.write(data[..MAX_PAYLOAD_SIZE].to_vec())
		.await.unwrap();
	assert_that!(&stream.is_closed(), eq(false));
	stream.abort()
		.await.unwrap();

	// should have written two frames
	let written = writer.into_writer().written();
	let (mut deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&payload, eq(vec![5u8; MAX_PAYLOAD_SIZE]));
	assert_that!(&deframer.bytes_total(), eq(data.len() as u64));
	assert_that!(&deframer.is_complete(), eq(false));

	let result = deframer.deframe(&written[1]);
	assert_that!(&result, is_match!(Err(DeframerError::Remote(Ok(RemoteFrameError::Abort)))));
	assert_that!(&deframer.is_complete(), eq(false));

	assert_that!(&written.len(), eq(2));
}


#[tokio::test]
async fn write_from_stream() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![1, 2, 3];
	let stream = {
		let data = data.clone();
		stream! {
			yield Ok::<_,Infallible>(data);
		}
	};
	writer.write_from_stream(data.len() as u64, stream)
		.await.unwrap();

	// should have written one frame
	let written = writer.into_writer().written();
	let (deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&deframer.bytes_total(), eq(data.len() as u64));
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&&payload, eq(&data));

	assert_that!(&written.len(), eq(1));
}


#[tokio::test]
async fn write_from_stream_merge_chunks() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![
		vec![1, 2, 3],
		vec![4, 5, 6]
	];
	let len = data.iter()
		.map(|d| d.len())
		.sum::<usize>();
	let stream = {
		let data = data.clone();
		stream! {
			for d in data {
				yield Ok::<_,Infallible>(d);
			}
		}
	};
	writer.write_from_stream(len as u64, stream)
		.await.unwrap();

	// should have written one frame with both chunks
	let written = writer.into_writer().written();
	assert_that!(&written.len(), eq(1));
	let (deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&deframer.bytes_total(), eq(len as u64));
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&payload, eq(data.concat()));
}


#[tokio::test]
async fn write_from_stream_merge_chunks_two_frames() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![
		vec![5u8; MAX_PAYLOAD_SIZE - 2],
		vec![5u8, 5, 42, 42],
		vec![42u8; 16],
	];
	let len = data.iter()
		.map(|d| d.len())
		.sum::<usize>();
	let stream = {
		let data = data.clone();
		stream! {
			for d in data {
				yield Ok::<_,Infallible>(d);
			}
		}
	};
	writer.write_from_stream(len as u64, stream)
		.await.unwrap();

	// should have written two frames
	let written = writer.into_writer().written();
	assert_that!(&written.len(), eq(2));
	let (mut deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&deframer.bytes_total(), eq(len as u64));
	assert_that!(&deframer.is_complete(), eq(false));
	assert_that!(&payload, eq(vec![5u8; MAX_PAYLOAD_SIZE]));

	let payload = deframer.deframe(&written[1])
		.unwrap();
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&payload, eq(vec![42u8; 18]));
}


#[tokio::test]
async fn write_from_stream_read_err() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let stream = stream! {
		yield Err::<Vec<u8>,_>(std::io::Error::other("nope"))
	};
	let e = writer.write_from_stream(5, stream)
		.await
		.err().unwrap();
	assert_that!(&e, is_match!(FrameWriteStreamError::Read(..)));

	// should have written one abort frame
	let written = writer.into_writer().written();
	assert_that!(&written.len(), eq(1));
	let e = Deframer::new(&written[0])
		.err().unwrap();
	assert_that!(&e, is_match!(DeframerError::Remote(Ok(RemoteFrameError::Abort))));
}


#[tokio::test]
async fn write_from_stream_underflow() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let stream = stream! {
		yield Ok::<_,Infallible>(vec![5u8; MAX_PAYLOAD_SIZE])
	};
	let len = (MAX_PAYLOAD_SIZE + 2) as u64;
	let e = writer.write_from_stream(len, stream)
		.await
		.err().unwrap();
	let FrameWriteStreamError::Underflow { expected, read } = e
		else { panic!("wrong error: {:?}", e); };
	assert_that!(&expected, eq(len));
	assert_that!(&read, eq(MAX_PAYLOAD_SIZE as u64));
}


#[tokio::test]
async fn write_from_read_one() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![1, 2, 3];
	writer.write_from_read(data.len() as u64, Cursor::new(&data))
		.await.unwrap();

	// should have written one frame
	let written = writer.into_writer().written();
	let (deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&deframer.bytes_total(), eq(data.len() as u64));
	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&&payload, eq(&data));

	assert_that!(&written.len(), eq(1));
}


#[tokio::test]
async fn write_from_read_three() {
	let _logging = init_test_logging();

	let writer = FrameWriter::from(TestWriter::new());

	let data = vec![
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![42u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	].concat();
	writer.write_from_read(data.len() as u64, Cursor::new(&data))
		.await.unwrap();

	// should have written three frames
	let written = writer.into_writer().written();
	let (mut deframer, payload) = Deframer::new(&written[0])
		.unwrap();
	assert_that!(&deframer.bytes_total(), eq(data.len() as u64));
	assert_that!(&payload.as_slice(), eq(&data[0 .. MAX_PAYLOAD_SIZE]));

	let payload = deframer.deframe(&written[1])
		.unwrap();
	assert_that!(&payload.as_slice(), eq(&data[MAX_PAYLOAD_SIZE .. MAX_PAYLOAD_SIZE*2]));

	let payload = deframer.deframe(&written[2])
		.unwrap();
	assert_that!(&payload.as_slice(), eq(&data[MAX_PAYLOAD_SIZE*2 ..]));

	assert_that!(&deframer.is_complete(), eq(true));
	assert_that!(&written.len(), eq(3));
}


struct TestReader {
	to_read: Mutex<VecDeque<Vec<u8>>>
}

impl TestReader {

	fn from_stream(stream: &Vec<Vec<u8>>) -> Self {

		if stream.is_empty() {
			return Self::from_messages(vec![]);
		}

		let stream_size = stream.iter()
			.map(|p| p.len() as u64)
			.sum();

		let mut framer = Framer::new(stream_size)
			.unwrap();
		let messages = stream.iter()
			.map(|p| framer.frame(p).unwrap())
			.collect();

		Self::from_messages(messages)
	}

	fn from_streams(streams: &Vec<Vec<Vec<u8>>>) -> Self {

		let mut messages = Vec::<Vec<u8>>::new();
		for stream in streams {

			let stream_size = stream.into_iter()
				.map(|p| p.len() as u64)
				.sum();
			let mut framer = Framer::new(stream_size)
				.unwrap();

			for payload in stream {
				let frame = framer.frame(payload)
					.unwrap();
				messages.push(frame);
			}
		}

		Self::from_messages(messages)
	}

	fn from_messages(messages: Vec<Vec<u8>>) -> Self {
		Self {
			to_read: Mutex::new(VecDeque::from(messages))
		}
	}

	fn unread(self) -> Vec<Vec<u8>> {
		self.to_read.into_inner()
			.into_iter()
			.collect()
	}
}

#[async_trait]
impl Reader for TestReader {

	type Error = Infallible;

	async fn read(&self) -> Result<Vec<u8>,Self::Error> {
		let mut to_read = self.to_read.lock()
			.await;
		match to_read.pop_front() {
			Some(msg) => Ok(msg),
			None => {
				// don't return, just hang, to simulate waiting for the network
				// TODO: there has to be a better way to do this
				tokio::time::sleep(Duration::from_secs(9999))
					.await;
				panic!("I can't believe you actually waited this long for a test to run only to see it fail")
			}
		}
	}
}


#[tokio::test]
async fn read_bad_frame() {
	let _logging = init_test_logging();

	// send anything that would trigger an error from the frame reader
	let mut reader = FrameReader::from(TestReader::from_messages(vec![
		vec![1, 2, 3]
	]));

	// reader should drop the frame and we should get an error response
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Error {
		error_response: is_match!(Some(RemoteFrameError::Deframer(..)))
	}));

	assert_that!(&reader.into_reader().unread().len(), eq(0));
}


#[tokio::test]
async fn read_one() {
	let _logging = init_test_logging();

	// prep a stream with one frame
	let payloads = vec![
		vec![1, 2, 3]
	];
	let mut reader = FrameReader::from(TestReader::from_stream(&payloads));

	// reader should get it in one event
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Finish {
		payload: eq(payloads[0].clone())
	}));

	assert_that!(&reader.into_reader().unread().len(), eq(0));
}


#[tokio::test]
async fn read_two() {
	let _logging = init_test_logging();

	// prep a stream with two frame
	let payloads = vec![
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	let mut reader = FrameReader::from(TestReader::from_stream(&payloads));

	// first event should be a start frame
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Start {
		bytes_total: eq(MAX_PAYLOAD_SIZE_U64 + 3),
		payload: eq(payloads[0].clone())
	}));

	// second event should be the finish frame
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Finish {
		payload: eq(payloads[1].clone())
	}));

	assert_that!(&reader.into_reader().unread().len(), eq(0));
}


#[tokio::test]
async fn read_three() {
	let _logging = init_test_logging();

	// start a stream with three frames
	let payloads = vec![
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![42u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	let mut reader = FrameReader::from(TestReader::from_stream(&payloads));

	// first event should be a start frame
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Start {
		bytes_total: eq(MAX_PAYLOAD_SIZE_U64*2 + 3),
		payload: eq(payloads[0].clone())
	}));

	// second event should be a continue frame
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Continue {
		bytes_read: eq(MAX_PAYLOAD_SIZE_U64*2),
		payload: eq(payloads[1].clone())
	}));

	// third event should be the finish frame
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Finish {
		payload: eq(payloads[2].clone())
	}));

	assert_that!(&reader.into_reader().unread().len(), eq(0));
}


#[tokio::test]
async fn read_one_abort() {
	let _logging = init_test_logging();

	// start a stream with one frame and then an abort
	let mut framer = Framer::new(MAX_PAYLOAD_SIZE_U64 + 3)
		.unwrap();
	let mut reader = FrameReader::from(TestReader::from_messages(vec![
		framer.frame(vec![5u8; MAX_PAYLOAD_SIZE]).unwrap(),
		framer.frame_error(RemoteFrameError::Abort).unwrap()
	]));

	// first event should be a start frame
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Start {
		bytes_total: eq(MAX_PAYLOAD_SIZE_U64 + 3),
		payload: eq(vec![5u8; MAX_PAYLOAD_SIZE])
	}));

	// next event should be the abort frame
	let event = reader.read()
		.await.unwrap();
	assert_that!(&event, structure!(FrameEvent::Error {
		local_error: is_match!(DeframerError::Remote(Ok(RemoteFrameError::Abort))),
		error_response: is_match!(None)
	}));

	assert_that!(&reader.into_reader().unread().len(), eq(0));
}


#[tokio::test]
async fn read_all_error() {
	let _logging = init_test_logging();

	// send anything that would trigger an error from the frame reader
	let mut reader = FrameReader::from(TestReader::from_messages(vec![
		vec![1, 2, 3]
	]));

	// reader should get an error
	let e = reader.read_all(FrameReaderTimeout::None)
		.await
		.err()
		.unwrap();
	assert_that!(&e, structure!(FrameReaderBufError::Stream[
		structure!(FrameStreamError::Frame {
			local_error: is_match!(DeframerError::Local(LocalDeframerError::Decode(..))),
			error_response: is_match!(Some(RemoteFrameError::Deframer(LocalDeframerError::Decode(..))))
		})
	]));
}


#[tokio::test]
async fn read_all_now_timeout() {
	let _logging = init_test_logging();

	// start a stream with no frames
	let payloads = vec![];
	let mut reader = FrameReader::from(TestReader::from_stream(&payloads));

	// reader should timeout
	let result = reader.read_all(FrameReaderTimeout::Now(Duration::from_millis(100)))
		.await;
	assert_that!(&result, is_match!(Err(FrameReaderBufError::Timeout)));
}


#[tokio::test]
async fn read_all_one() {
	let _logging = init_test_logging();

	// prep a stream with one frame
	let payloads = vec![
		vec![1, 2, 3]
	];
	let mut reader = FrameReader::from(TestReader::from_stream(&payloads));

	let payload = reader.read_all(FrameReaderTimeout::None)
		.await.unwrap();
	assert_that!(&&payload, eq(&payloads[0]));
}


#[tokio::test]
async fn read_all_one_timeout() {
	let _logging = init_test_logging();

	// start a stream with two frames but only send one
	let mut framer = Framer::new(MAX_PAYLOAD_SIZE_U64 + 3)
		.unwrap();
	let mut reader = FrameReader::from(TestReader::from_messages(vec![
		framer.frame(vec![5u8; MAX_PAYLOAD_SIZE]).unwrap()
	]));

	// try to read both frames
	let result = reader.read_all(FrameReaderTimeout::Now(Duration::from_millis(100)))
		.await;
	assert_that!(&result, is_match!(Err(FrameReaderBufError::Timeout)));
}


#[tokio::test]
async fn read_all_two() {
	let _logging = init_test_logging();

	// prep a stream with two frames
	let payloads = vec![
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	let mut reader = FrameReader::from(TestReader::from_stream(&payloads));

	// read the stream
	let payload = reader.read_all(FrameReaderTimeout::None)
		.await.unwrap();
	assert_that!(&payload, eq(payloads.concat()));
}


#[tokio::test]
async fn read_all_three() {
	let _logging = init_test_logging();

	// prep a stream with three frames
	let payloads = vec![
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![42u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	let mut reader = FrameReader::from(TestReader::from_stream(&payloads));

	// read the stream
	let payload = reader.read_all(FrameReaderTimeout::None)
		.await.unwrap();
	assert_that!(&payload, eq(payloads.concat()));
}


#[tokio::test]
async fn read_all_now_three_delayed() {
	let _logging = init_test_logging();

	let (tx, rx) = mpsc::unbounded_channel::<Vec<u8>>();
	let mut reader = FrameReader::from(Arc::new(Mutex::new(rx)));
	let (start_tx, start_rx) = oneshot::channel();

	// send a stream with three frames
	let payloads = [
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![42u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	tokio::spawn({
		let payloads = payloads.clone();
		async move {
			let mut framer = Framer::new(payloads.iter().map(|p| p.len() as u64).sum())
				.unwrap();

			// wait for the start signal
			start_rx
				.await.unwrap();

			for payload in payloads {
				sleep(Duration::from_millis(100))
					.await;
				tx.send(framer.frame(payload).unwrap())
					.unwrap();
			}
		}
	});

	// read the stream, shouldn't timeout
	start_tx.send(())
		.unwrap();
	let payload = reader.read_all(FrameReaderTimeout::Now(Duration::from_millis(200)))
		.await.unwrap();
	assert_that!(&payload, eq(payloads.concat()));
}


#[tokio::test]
async fn read_all_twice() {
	let _logging = init_test_logging();

	// prep two streams with two frames each
	let payloads = vec![
		vec![
			vec![5u8; MAX_PAYLOAD_SIZE],
			vec![1, 2, 3]
		],
		vec![
			vec![42u8; MAX_PAYLOAD_SIZE],
			vec![1, 2, 3]
		]
	];
	let mut reader = FrameReader::from(TestReader::from_streams(&payloads));

	// read the stream
	let payload = reader.read_all(FrameReaderTimeout::None)
		.await.unwrap();
	assert_that!(&payload, eq(payloads[0].concat()));

	// read it again
	let payload = reader.read_all(FrameReaderTimeout::None)
		.await.unwrap();
	assert_that!(&payload, eq(payloads[1].concat()));
}


#[tokio::test]
async fn read_all_after_start_two() {
	let _logging = init_test_logging();

	let (tx, rx) = mpsc::unbounded_channel::<Vec<u8>>();
	let mut reader = FrameReader::from(Arc::new(Mutex::new(rx)));

	// send a stream with two frames, after a delay
	let payloads = [
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	tokio::spawn({
		let payloads = payloads.clone();
		async move {
			let mut framer = Framer::new(payloads.iter().map(|p| p.len() as u64).sum())
				.unwrap();

			sleep(Duration::from_millis(200))
				.await;

			for payload in payloads {
				tx.send(framer.frame(payload).unwrap())
					.unwrap();
			}
		}
	});

	// read the stream, shouldn't timeout
	let payload = reader.read_all(FrameReaderTimeout::AfterStart(Duration::from_millis(100)))
		.await.unwrap();
	assert_that!(&payload, eq(payloads.concat()));
}


#[tokio::test]
async fn read_all_start_start_two_timeout() {
	let _logging = init_test_logging();

	let (tx, rx) = mpsc::unbounded_channel::<Vec<u8>>();
	let mut reader = FrameReader::from(Arc::new(Mutex::new(rx)));

	// send a stream with two frames, after a delay
	let payloads = [
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	tokio::spawn({
		let payloads = payloads.clone();
		async move {
			let mut framer = Framer::new(payloads.iter().map(|p| p.len() as u64).sum())
				.unwrap();

			sleep(Duration::from_millis(200))
				.await;

			let [p1, p2] = payloads;
			tx.send(framer.frame(p1).unwrap())
				.unwrap();

			sleep(Duration::from_millis(200))
				.await;

			tx.send(framer.frame(p2).unwrap())
				.unwrap();
		}
	});

	// read the stream
	let result = reader.read_all(FrameReaderTimeout::AfterStart(Duration::from_millis(100)))
		.await;
	assert_that!(&result, is_match!(Err(FrameReaderBufError::Timeout)));
}


#[tokio::test]
async fn read_all_error_recovery() {
	let _logging = init_test_logging();

	let (tx, rx) = mpsc::unbounded_channel::<Vec<u8>>();
	let mut reader = FrameReader::from(Arc::new(Mutex::new(rx)));

	// send a partial stream
	let payloads = [
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	tokio::spawn({
		let payloads = payloads.clone();
		let tx = tx.clone();
		async move {
			let mut framer = Framer::new(payloads.iter().map(|p| p.len() as u64).sum())
				.unwrap();

			let [p1, _p2] = payloads;
			tx.send(framer.frame(p1).unwrap())
				.unwrap();
		}
	});

	// timeout reading the partial stream
	let result = reader.read_all(FrameReaderTimeout::AfterStart(Duration::from_millis(100)))
		.await;
	assert_that!(&result, is_match!(Err(FrameReaderBufError::Timeout)));

	// send a stream with two frames
	let payloads = [
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	tokio::spawn({
		let payloads = payloads.clone();
		async move {
			let mut framer = Framer::new(payloads.iter().map(|p| p.len() as u64).sum())
				.unwrap();

			let [p1, p2] = payloads;
			tx.send(framer.frame(p1).unwrap())
				.unwrap();
			tx.send(framer.frame(p2).unwrap())
				.unwrap();
		}
	});

	// read the stream
	let payload = reader.read_all(FrameReaderTimeout::AfterStart(Duration::from_millis(100)))
		.await.unwrap();
	assert_that!(&payload, eq(payloads.concat()));
}


#[tokio::test]
async fn read_stream_into_async_read() {
	let _logging = init_test_logging();

	// prep a stream with three frames
	let payloads = vec![
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![42u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	];
	let mut reader = FrameReader::from(TestReader::from_stream(&payloads));

	let mut stream = reader.stream()
		.unwrap()
		.into_async_read();

	let mut buf = Vec::<u8>::new();
	stream.read_to_end(&mut buf)
		.await.unwrap();
	assert_that!(&buf.len(), eq(MAX_PAYLOAD_SIZE*2 + 3));
	assert_that!(&&buf[0 .. MAX_PAYLOAD_SIZE], eq(payloads[0].as_slice()));
	assert_that!(&&buf[MAX_PAYLOAD_SIZE .. MAX_PAYLOAD_SIZE*2], eq(payloads[1].as_slice()));
	assert_that!(&&buf[MAX_PAYLOAD_SIZE*2 ..], eq(payloads[2].as_slice()));
}


type BytesSender = mpsc::UnboundedSender<Vec<u8>>;
type BytesReceiver = Arc<Mutex<mpsc::UnboundedReceiver<Vec<u8>>>>;

fn receiver_transport() -> (FrameReceiver<BytesSender,BytesReceiver>, BytesSender, BytesReceiver) {
	let (msg_tx, msg_rx) = mpsc::unbounded_channel();
	let (error_tx, error_rx) = mpsc::unbounded_channel();
	let msg_rx = Arc::new(Mutex::new(msg_rx));
	let error_rx = Arc::new(Mutex::new(error_rx));
	let rx = FrameReceiver::from(
		FrameWriter::from(error_tx),
		FrameReader::from(msg_rx)
	);
	(rx, msg_tx, error_rx)
}


#[tokio::test]
async fn recv_error() {
	let _logging = init_test_logging();

	let (mut rx, msg_tx, error_rx) = receiver_transport();

	// send a stream with one frame of gibberish
	tokio::spawn(async move {
		msg_tx.write(vec![1, 2, 3])
			.await.unwrap();
	});

	// receiver should get an error frame, but no response error
	let result = rx.recv()
		.await.unwrap();
	assert_that!(&result, structure!(FrameEvent::Error {
		local_error: is_match!(DeframerError::Local(LocalDeframerError::Decode(..))),
		error_response: is_match!(None)
	}));

	// the response error should show up on the channel
	let e = error_rx.read()
		.await.unwrap();
	let result = Deframer::new(&e);
	assert_that!(&result, is_match!(Err(DeframerError::Remote(Ok(RemoteFrameError::Deframer(LocalDeframerError::Decode(..)))))));
}


#[tokio::test]
async fn recv_all_error() {
	let _logging = init_test_logging();

	let (mut rx, msg_tx, error_rx) = receiver_transport();

	// send a stream with one frame of gibberish
	tokio::spawn(async move {
		msg_tx.write(vec![1, 2, 3])
			.await.unwrap();
	});

	// receiver should get an error frame, but no response error
	let e = rx.recv_all(FrameReaderTimeout::None)
		.await
		.err()
		.unwrap();
	assert_that!(&e, structure!(FrameReaderBufError::Stream[
		structure!(FrameStreamError::Frame {
			local_error: is_match!(DeframerError::Local(LocalDeframerError::Decode(..))),
			error_response: is_match!(None)
		})
	]));

	// the response error should show up on the channel
	let e = error_rx.read()
		.await.unwrap();
	let result = Deframer::new(&e);
	assert_that!(&result, is_match!(Err(DeframerError::Remote(Ok(RemoteFrameError::Deframer(LocalDeframerError::Decode(..)))))));
}


#[tokio::test]
async fn recv_stream() {
	let _logging = init_test_logging();

	let (mut rx, msg_tx, _error_rx) = receiver_transport();

	// prep a stream with three frames
	let payloads = Arc::new(vec![
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![42u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	]);

	tokio::spawn({
		let payloads = payloads.clone();
		async move {

			let payload_size = payloads.iter()
				.map(|p| p.len() as u64)
				.sum::<u64>();

			let mut framer = Framer::new(payload_size)
				.unwrap();

			for payload in payloads.iter() {
				let frame = framer.frame(&payload)
					.unwrap();
				msg_tx.write(frame)
					.await.unwrap();
			}
		}
	});

	// read the stream
	let mut stream = rx.stream()
		.unwrap();
	let frame = stream.next()
		.await.unwrap()
		.unwrap();
	assert_that!(&&frame, eq(&payloads[0]));
	let frame = stream.next()
		.await.unwrap()
		.unwrap();
	assert_that!(&&frame, eq(&payloads[1]));
	let frame = stream.next()
		.await.unwrap()
		.unwrap();
	assert_that!(&&frame, eq(&payloads[2]));
	let frame = stream.next()
		.await;
	assert_that!(&frame, is_match!(None));
}


#[tokio::test]
async fn recv_stream_error() {
	let _logging = init_test_logging();

	let (mut rx, msg_tx, error_rx) = receiver_transport();

	// send a stream with one frame of gibberish
	tokio::spawn(async move {
		msg_tx.write(vec![1, 2, 3])
			.await.unwrap();
	});

	let mut stream = rx.stream()
		.unwrap();

	// receiver should get an error frame, but no response error
	let e = stream.next()
		.await
		.unwrap()
		.err()
		.unwrap();
	assert_that!(&e, structure!(FrameStreamError::Frame {
		local_error: is_match!(DeframerError::Local(LocalDeframerError::Decode(..))),
		error_response: is_match!(None)
	}));

	// the response error should show up on the channel
	let e = error_rx.read()
		.await.unwrap();
	let result = Deframer::new(&e);
	assert_that!(&result, is_match!(Err(DeframerError::Remote(Ok(RemoteFrameError::Deframer(LocalDeframerError::Decode(..)))))));
}


#[tokio::test]
async fn recv_stream_into_async_read() {
	let _logging = init_test_logging();

	let (mut rx, msg_tx, _error_rx) = receiver_transport();

	// prep a stream with three frames
	let payloads = Arc::new(vec![
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![42u8; MAX_PAYLOAD_SIZE],
		vec![1, 2, 3]
	]);

	tokio::spawn({
		let payloads = payloads.clone();
		async move {

			let payload_size = payloads.iter()
				.map(|p| p.len() as u64)
				.sum::<u64>();

			let mut framer = Framer::new(payload_size)
				.unwrap();

			for payload in payloads.iter() {
				let frame = framer.frame(&payload)
					.unwrap();
				msg_tx.write(frame)
					.await.unwrap();
			}
		}
	});

	// read the stream
	let mut stream = rx.stream()
		.unwrap()
		.into_async_read();

	let mut buf = Vec::<u8>::new();
	stream.read_to_end(&mut buf)
		.await.unwrap();
	assert_that!(&buf.len(), eq(MAX_PAYLOAD_SIZE*2 + 3));
	assert_that!(&&buf[0 .. MAX_PAYLOAD_SIZE], eq(payloads[0].as_slice()));
	assert_that!(&&buf[MAX_PAYLOAD_SIZE .. MAX_PAYLOAD_SIZE*2], eq(payloads[1].as_slice()));
	assert_that!(&&buf[MAX_PAYLOAD_SIZE*2 ..], eq(payloads[2].as_slice()));
}
