
mod error;
mod framer;
mod deframer;
mod reader;
mod writer;
mod receiver;
mod channels;

#[cfg(test)]
mod test;


pub use error::*;
pub use framer::*;
pub use deframer::*;
pub use reader::*;
pub use writer::*;
pub use receiver::*;
pub use channels::*;


use crate::protobuf::smolweb;


/// MDN says we should stay under 16 KiB messages:
/// https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Using_data_channels#understanding_message_size_limits
pub const MAX_MESSAGE_SIZE: usize = 16*1024;

/// The maximum size of the portion of a frame not used for the payload.
/// Since protobufs use variable encodings, that complicates size analysis a bit.
/// This number shows the maximum size that could be taken up by the non-payload portion of the frame.
/// It's up to 0.1% overhead for a full frame. Not bad.
pub const MAX_OVERHEAD_SIZE: usize = 16;
// submessage(len<=16k)=1+2 + record(int64)=1+9 + record(bytes, len <=16k)=1+2
// assuming message field numbers are < 16
// See Protobuf3 encoding rules: https://protobuf.dev/programming-guides/encoding/

/// The max payload size in a frame.
pub const MAX_PAYLOAD_SIZE: usize = MAX_MESSAGE_SIZE - MAX_OVERHEAD_SIZE;

/// The maximum size allowed by a signed 64-bit integer.
pub const MAX_STREAM_SIZE: u64 = i64::MAX as u64;


// because the official function isn't stable yet
// but it soon will be, see: https://github.com/rust-lang/rust/issues/88581
const fn div_ceil(lhs: u64, rhs: u64) -> u64 {
	let d = lhs / rhs;
	let r = lhs % rhs;
	if r > 0 && rhs > 0 {
		d + 1
	} else {
		d
	}
}


pub const FRAME_KIND_START: &'static str = "Start";
pub const FRAME_KIND_CONTINUE: &'static str = "Continue";
pub const FRAME_KIND_ERROR: &'static str = "Error";


pub trait FrameKindProps {
	fn name(&self) -> &'static str;
}


impl FrameKindProps for smolweb::framed::frame::Kind {

	fn name(&self) -> &'static str {
		match self {
			Self::Start(..) => FRAME_KIND_START,
			Self::Continue(..) => FRAME_KIND_CONTINUE,
			Self::Error(..) => FRAME_KIND_ERROR
		}
	}
}
