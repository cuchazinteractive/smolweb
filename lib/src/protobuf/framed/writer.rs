
use std::cmp::min;
use std::collections::VecDeque;
use std::fmt::Debug;
use std::pin::pin;

use axum::async_trait;
use futures::{AsyncRead, AsyncReadExt, StreamExt, TryStream, TryStreamExt};
use thiserror::Error;
use tracing::error;

use crate::protobuf::framed::{Framer, FramerError, MAX_PAYLOAD_SIZE, NewFramerError, RemoteFrameError};


#[async_trait]
pub trait Writer {

	type Error: std::error::Error + 'static;

	async fn write(&self, data: impl Into<Vec<u8>> + Send) -> Result<(),Self::Error>;
}


#[derive(Clone)]
pub struct FrameWriter<W>
	where
		W: Writer + Send + Sync + Clone
{
	writer: W
}

impl<W> FrameWriter<W>
	where
		W: Writer + Send + Sync + Clone
{

	pub fn from(writer: W) -> Self {
		Self {
			writer
		}
	}

	pub fn into_writer(self) -> W {
		self.writer
	}

	pub async fn write(&self, data: impl Into<Vec<u8>> + Send) -> Result<(),FrameWriteError<W::Error>> {

		let data = data.into();

		let mut stream = self.stream(data.len() as u64)?;

		let mut slice = &data[..];
		while !stream.is_closed() {

			// make the next payload out of the data
			let payload = &slice[..min(MAX_PAYLOAD_SIZE, slice.len())];
			stream.write(payload)
				.await?;

			// advance to the next payload
			slice = &slice[payload.len()..];
		}

		Ok(())
	}

	pub async fn write_error(&self, e: RemoteFrameError) -> Result<(),FrameWriteError<W::Error>> {

		// pack the error into a frame
		let frame = e.frame();

		// send the frame on the channel
		self.writer.write(frame)
			.await
			.map_err(|e| FrameWriteError::Write(e))?;

		Ok(())
	}

	pub fn stream(&self, size: u64) -> Result<FrameStreamer<W>,NewFramerError> {

		let framer = Framer::new(size)?;

		Ok(FrameStreamer {
			framer,
			writer: &self.writer
		})
	}

	pub async fn write_from_stream<S,B,E>(&self, size: u64, stream: S) -> Result<(),FrameWriteStreamError<W::Error,E>>
		where
			S: TryStream<Ok=B,Error=E,Item=Result<B,E>> + Send + 'static,
			B: Into<Vec<u8>>,
			E: std::error::Error + 'static
	{

		let stream = stream
			.map_ok(|o| o.into())
			.map_err(FrameWriteStreamError::Read);

		let mut streamer = self.stream(size)
			.map_err(FrameWriteError::NewFramer)?;

		let mut stream = Box::pin(stream);
		let mut maybe_buf = None::<VecDeque<u8>>;
		let mut done = false;

		// for each chunk of the stream
		'body: loop {

			// handle the incoming chunk
			match stream.next().await {

				Some(Ok(chunk)) => {

					// create a new buffer if needed
					// start with enough capacity to hold a few frames
					let buf = maybe_buf.get_or_insert(VecDeque::with_capacity(MAX_PAYLOAD_SIZE*3));

					// append the chunk to the buffer
					buf.extend(chunk);
				}

				// end of stream, flush whatever's left
				None => done = true,

				Some(Err(e)) => {

					// try to abort the frame stream
					streamer.abort()
						.await.ok();

					return Err(e);
				}
			};

			// flush the buffer to the frame stream
			if let Some(buf) = &mut maybe_buf {
				'flush: loop {
					match poll_payload(buf, done) {

						Some(payload) => {

							// send the payload to the frame stream
							streamer.write(payload)
								.await?;
						}

						None => break 'flush,
					}
				}
			}

			if done {
				break 'body;
			}
		}

		// make sure we got everything
		if streamer.is_closed() {
			Ok(())
		} else {
			Err(FrameWriteStreamError::Underflow {
				expected: streamer.bytes_total(),
				read: streamer.bytes_written()
			})
		}
	}

	pub async fn write_from_read<R>(&self, size: u64, read: R) -> Result<(),FrameWriteStreamError<W::Error,std::io::Error>>
		where
			R: AsyncRead
	{
		let mut read = pin!(read);

		let mut streamer = self.stream(size)
			.map_err(FrameWriteError::NewFramer)?;

		let mut buf = [0u8; MAX_PAYLOAD_SIZE];
		let mut bytes_written = 0u64;

		while bytes_written < size {
			let frame_size = ((size - bytes_written) as usize).min(MAX_PAYLOAD_SIZE);
			let frame_buf = &mut buf[0 .. frame_size];
			read.read_exact(frame_buf)
				.await
				.map_err(FrameWriteStreamError::Read)?;
			bytes_written += frame_size as u64;
			streamer.write(frame_buf)
				.await
				.map_err(FrameWriteStreamError::Write)?;
		}

		Ok(())
	}
}


fn poll_payload(buf: &mut VecDeque<u8>, done: bool) -> Option<Vec<u8>> {

	let payload_size =
		if buf.len() >= MAX_PAYLOAD_SIZE {
			MAX_PAYLOAD_SIZE
		} else if done && buf.len() > 0 {
			buf.len()
		} else {
			return None;
		};

	// split off the payload from the buffer
	let mut payload = Vec::<u8>::with_capacity(payload_size);
	payload.extend(buf.drain(0 .. payload_size));

	Some(payload)
}


pub struct FrameStreamer<'t,T>
	where
		T: Writer + Clone
{
	framer: Framer,
	writer: &'t T
}

impl<'w,W> FrameStreamer<'w,W>
	where
		W: Writer + Send + Sync + Clone
{

	pub fn is_closed(&self) -> bool {
		self.framer.is_closed()
	}

	pub fn bytes_total(&self) -> u64 {
		self.framer.bytes_total()
	}

	pub fn bytes_written(&self) -> u64 {
		self.framer.bytes_written()
	}

	pub async fn write<B:Into<Vec<u8>>>(&mut self, payload: B) -> Result<(),FrameWriteError<W::Error>> {

		// pack the payload into a frame
		let frame = self.framer.frame(payload.into())?;

		// send the frame on the channel
		self.writer.write(frame)
			.await
			.map_err(|e| FrameWriteError::Write(e))?;

		Ok(())
	}

	pub async fn abort(self) -> Result<(),FrameWriteError<W::Error>> {

		// pack an abort error frame
		let frame = self.framer.frame_error(RemoteFrameError::Abort)?;

		// send the frame on the channel
		self.writer.write(frame)
			.await
			.map_err(|e| FrameWriteError::Write(e))?;

		Ok(())
	}
}


#[derive(Error, Debug)]
pub enum FrameWriteError<W>
	where
		W: std::error::Error + 'static
{

	#[error(transparent)]
	NewFramer(#[from] NewFramerError),

	#[error(transparent)]
	Framer(#[from] FramerError),

	#[error("Failed to write to the underlying transport")]
	Write(#[source] W),
}


#[derive(Error, Debug)]
pub enum FrameWriteStreamError<W,E>
	where
		W: std::error::Error + 'static,
		E: std::error::Error + 'static
{

	#[error(transparent)]
	Write(#[from] FrameWriteError<W>),

	#[error("Failed to read from the stream")]
	Read(#[source] E),

	#[error("Stream ended before read all bytes: expected={expected}, read={read}")]
	Underflow {
		expected: u64,
		read: u64
	}
}
