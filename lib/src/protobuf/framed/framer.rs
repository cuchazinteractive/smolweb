
use prost::Message;
use thiserror::Error;
use tracing::warn;

use crate::protobuf::framed::{div_ceil, MAX_PAYLOAD_SIZE, MAX_STREAM_SIZE};
use crate::protobuf::framed::error::RemoteFrameError;
use crate::protobuf::smolweb;


#[derive(Debug)]
pub struct Framer {
	bytes_total: u64,
	bytes_written: u64,
	next_frame: Option<u64>,
}


impl Framer {

	pub fn new(size: u64) -> Result<Self, NewFramerError> {

		// validate the size
		if size <= 0 || size > MAX_STREAM_SIZE {
			return Err(NewFramerError::InvalidStreamSize {
				max: MAX_STREAM_SIZE,
				observed: size
			});
		}

		// determine the frame sequence
		let frames_total = div_ceil(size, MAX_PAYLOAD_SIZE as u64);
		let next_frame =
			if frames_total >= 1 {
				Some(0)
			} else {
				None
			};

		Ok(Self {
			bytes_total: size,
			bytes_written: 0,
			next_frame
		})
	}

	/// Wraps the payload within a frame, suitable for sending as an `RTCDataChannel` message.
	pub fn frame<B:AsRef<[u8]>>(&mut self, payload: B) -> Result<Vec<u8>,FramerError> {

		let payload = payload.as_ref();

		// get the next frame index, if any
		let index = match self.next_frame {
			Some(i) => i,
			None => return Err(FramerError::Closed)
		};

		// check the payload size
		let bytes_remaining = self.bytes_total.checked_sub(self.bytes_written)
			.expect("wrote too many bytes somehow");
		let bytes_expected =
			if bytes_remaining < MAX_PAYLOAD_SIZE as u64 {
				bytes_remaining as usize
			} else {
				MAX_PAYLOAD_SIZE
			};
		if payload.len() != bytes_expected {
			return Err(FramerError::UnexpectedPayloadSize {
				expected: bytes_expected,
				observed: payload.len()
			});
		}

		// write the frame
		let frame = smolweb::framed::Frame {
			kind: Some(
				if index == 0 {
					// write a start frame
					smolweb::framed::frame::Kind::Start(smolweb::framed::FrameStart {
						stream_size: self.bytes_total as i64,
						payload: payload.to_vec()
					})
				} else {
					// write a continue frame
					smolweb::framed::frame::Kind::Continue(smolweb::framed::FrameContinue {
						frame_index: index as i64,
						payload: payload.to_vec()
					})
				}
			)
		}.encode_to_vec();

		// update state
		self.bytes_written += bytes_expected as u64;
		self.next_frame =
			if self.bytes_written < self.bytes_total {
				Some(index + 1)
			} else {
				None
			};

		Ok(frame)
	}

	pub fn frame_error(self, err: RemoteFrameError) -> Result<Vec<u8>,FramerError> {

		// if the stream is already closed, no need to send the error
		// the remote side isn't listening anyway
		if let None = self.next_frame {
			return Err(FramerError::Closed);
		}

		Ok(err.frame())
	}

	pub fn bytes_total(&self) -> u64 {
		self.bytes_total
	}

	pub fn bytes_total_i64(&self) -> i64 {
		// Self::new() should guarantee this can never panic
		self.bytes_total.try_into()
			.expect("bytes total too large")
	}

	pub fn bytes_written(&self) -> u64 {
		self.bytes_written
	}

	pub fn is_closed(&self) -> bool {
		self.next_frame.is_none()
	}
}

impl Drop for Framer {

	fn drop(&mut self) {
		if self.next_frame.is_some() {
			warn!("unfinished Framer dropped");
		}
	}
}


#[derive(Error, Debug, Clone)]
pub enum NewFramerError {

	#[error("The stream size {observed} is out of range [1,{max}]")]
	InvalidStreamSize {
		max: u64,
		observed: u64
	}
}


#[derive(Error, Debug, Clone)]
pub enum FramerError {

	#[error("Unexpected frame payload size {observed}, was expecting {expected}")]
	UnexpectedPayloadSize {
		expected: usize,
		observed: usize
	},

	#[error("No more frames can be written, the stream is closed")]
	Closed
}
