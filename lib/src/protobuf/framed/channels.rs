
use std::convert::Infallible;
use std::sync::Arc;

use axum::async_trait;
use thiserror::Error;
use tokio::sync::{mpsc, Mutex};

use crate::protobuf::framed::{Reader, Writer};


#[async_trait]
impl Writer for mpsc::UnboundedSender<Vec<u8>> {

	type Error = Infallible;

	async fn write(&self, data: impl Into<Vec<u8>> + Send) -> Result<(),Self::Error> {
		self.send(data.into())
			.unwrap();
		Ok(())
	}
}


#[async_trait]
impl Reader for Arc<Mutex<mpsc::UnboundedReceiver<Vec<u8>>>> {

	type Error = ClosedError;

	async fn read(&self) -> Result<Vec<u8>,Self::Error> {
		let mut rx = self.lock()
			.await;
		let data = rx.recv()
			.await
			.ok_or(ClosedError {})?;
		Ok(Vec::from(data))
	}
}


#[derive(Error, Debug)]
#[error("closed")]
pub struct ClosedError;
