
use std::io::ErrorKind;
use std::marker::PhantomPinned;
use std::pin::Pin;
use std::task::{Context, Poll};

use futures::{ready, AsyncRead, Future, FutureExt, StreamExt};
use futures::future::BoxFuture;
use pin_project::pin_project;
use tokio_util::compat::TokioAsyncReadCompatExt;
use tokio_util::io::StreamReader;

use crate::protobuf::framed::{FrameEvent, FrameReader, FrameReaderBufError, FrameReaderTimeout, FrameStream, FrameStreamError, FrameWriteError, FrameWriter, Reader, RemoteFrameError, Writer};


/// Reads frames from the underlying reader and automatically sends response errors
/// to the remote endpoint using the underlying writer.
pub struct FrameReceiver<W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{
	writer: FrameWriter<W>,
	reader: FrameReader<R>
}


impl<W,R> FrameReceiver<W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{

	pub fn from(writer: FrameWriter<W>, reader: FrameReader<R>) -> FrameReceiver<W,R> {
		Self {
			writer,
			reader
		}
	}

	pub fn writer(&self) -> &FrameWriter<W> {
		&self.writer
	}

	pub fn reader(&mut self) -> &mut FrameReader<R> {
		&mut self.reader
	}

	pub async fn recv(&mut self) -> Result<FrameEvent,R::Error> {

		let frame = self.reader.read()
			.await?;
		let frame = match frame {
			FrameEvent::Error { local_error, error_response } => {
				// consume the error response and pass the rest along
				self.maybe_respond_error(error_response)
					.await;
				FrameEvent::Error {
					local_error,
					error_response: None
				}
			}
			f => f
		};

		Ok(frame)
	}

	pub fn stream(&mut self) -> Result<FrameRecvStream<W,R>,FrameStreamError<R::Error>> {
		let stream = self.reader.stream()?;
		Ok(FrameRecvStream::new(self.writer.clone(), stream))
	}

	pub async fn recv_all(&mut self, timeout: FrameReaderTimeout) -> Result<Vec<u8>,FrameReaderBufError<R::Error>> {

		let result = self.reader.read_all(timeout)
			.await;
		match result {

			Ok(r) => Ok(r),

			Err(FrameReaderBufError::Stream(FrameStreamError::Frame { local_error, error_response })) => {
				// consume the error response and pass the rest along
				self.maybe_respond_error(error_response)
					.await;
				Err(FrameReaderBufError::Stream(FrameStreamError::Frame {
					local_error,
					error_response: None
				}))
			},

			// other errors don't need any transformations
			Err(e) => Err(e)
		}
	}

	async fn maybe_respond_error(&self, mut e: Option<RemoteFrameError>) {
		if let Some(e) = e.take() {

			// try to send back the error
			self.writer.write_error(e)
				.await
				.ok();
		}
	}
}


#[pin_project]
pub struct FrameRecvStream<'r,W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{
	writer: FrameWriter<W>,
	stream: FrameStream<'r,R>,
	#[pin]
	fut: Option<BoxFuture<'static,Option<<FrameStream<'r,R> as futures::Stream>::Item>>>,
	_phantom_pinned: PhantomPinned
}

impl<'r,W,R> FrameRecvStream<'r,W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{
	fn new(writer: FrameWriter<W>, stream: FrameStream<'r,R>) -> Self {
		Self {
			writer,
			stream,
			fut: None,
			_phantom_pinned: PhantomPinned::default()
		}
	}

	pub fn bytes_total(&self) -> Option<u64> {
		self.stream.bytes_total()
	}

	pub async fn abort(self) -> Result<(),FrameWriteError<W::Error>> {
		self.stream.abort();
		self.writer.write_error(RemoteFrameError::Abort)
			.await
	}

	pub fn into_async_read(self) -> impl AsyncRead + 'r
		where
			W: 'static,
			R::Error: Send + Sync
	{
		StreamReader::new(
			self.map(|result| {
				result
					.map(|buf| bytes::Bytes::from(buf))
					.map_err(|e| std::io::Error::new(ErrorKind::Other, e))
			})
		).compat()
	}
}

impl<'r,W,R> futures::Stream for FrameRecvStream<'r,W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{
	type Item = Result<Vec<u8>,FrameStreamError<R::Error>>;

	fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {

		let mut this = self.project();

		if this.fut.is_none() {

			let f = async move {
				let result = this.stream.next()
					.await?;
				Some(match result {

					Ok(r) => Ok(r),

					Err(FrameStreamError::Frame { local_error, error_response }) => {
						// consume the error response and pass the rest along
						if let Some(error_response) = error_response {
							this.writer.write_error(error_response)
								.await
								.ok();
						}
						Err(FrameStreamError::Frame {
							local_error,
							error_response: None
						})
					},

					// other errors don't need any transformations
					Err(e) => Err(e)
				})
			}.boxed();

			let f = unsafe {
				/* SAFETY:
					This future depends on self, but safe rust doesn't allow self-referential structs.
					So, to make this safe, we need to pin the reference to self and make self !Upin,
					but adding a PhantomPinned.
				*/
				std::mem::transmute::<
					BoxFuture<'_,_>,
					BoxFuture<'static,_>
				>(f)
			};

			this.fut.set(Some(f));
		}

		// now, forward the poll to the inner future
		let f = this.fut.as_mut().as_pin_mut()
			.unwrap(); // PANIC SAFETY: if fut was None, we just put Some there
		let result = ready!(f.poll(cx));
		this.fut.take();
		Poll::Ready(result)
	}
}
