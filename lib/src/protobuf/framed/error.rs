
use prost::Message;
use thiserror::Error;

use crate::protobuf::framed::deframer::LocalDeframerError;
use crate::protobuf::ReadProtobufError;
use crate::protobuf::smolweb::framed as proto;


#[derive(Error, Debug, Clone)]
pub enum RemoteFrameError {

	#[error("The remote side failed to read the frame")]
	Deframer(#[source] LocalDeframerError),

	#[error("The remote side has aborted the stream.")]
	Abort
}


impl RemoteFrameError {

	fn to_proto(&self) -> proto::FrameError {
		proto::FrameError {
			error: Some(match self {

				Self::Deframer(LocalDeframerError::Decode(msg)) =>
					proto::frame_error::Error::Decode(proto::frame_error::Decode {
						message: msg.clone(),
					}),

				Self::Deframer(LocalDeframerError::UnexpectedKind { expected, observed }) =>
					proto::frame_error::Error::UnexpectedKind(proto::frame_error::UnexpectedKind {
						expected: expected.clone(),
						observed: observed.clone()
					}),

				Self::Deframer(LocalDeframerError::UnexpectedFrameIndex { expected, observed }) =>
					proto::frame_error::Error::UnexpectedFrameIndex(proto::frame_error::UnexpectedFrameIndex {
						expected: *expected,
						observed: *observed
					}),

				Self::Deframer(LocalDeframerError::InvalidStreamSize(observed)) =>
					proto::frame_error::Error::InvalidStreamSize(proto::frame_error::InvalidStreamSize {
						observed: *observed
					}),

				Self::Deframer(LocalDeframerError::UnexpectedPayloadSize { expected, observed }) =>
					proto::frame_error::Error::UnexpectedPayloadSize(proto::frame_error::UnexpectedPayloadSize {
						expected: *expected as u64,
						observed: *observed as u64
					}),

				Self::Deframer(LocalDeframerError::Closed) =>
					proto::frame_error::Error::Closed(true),

				Self::Abort =>
					proto::frame_error::Error::Abort(true)
			})
		}
	}

	pub fn from_protobuf(msg: proto::FrameError) -> Result<Self,ReadProtobufError> {

		let error = msg.error
			.ok_or(ReadProtobufError::MissingField("error"))?;

		Ok(match error {

			proto::frame_error::Error::Decode(e) =>
				Self::Deframer(LocalDeframerError::Decode(e.message)),

			proto::frame_error::Error::UnexpectedKind(e) =>
				Self::Deframer(LocalDeframerError::UnexpectedKind {
					expected: e.expected,
					observed: e.observed
				}),

			proto::frame_error::Error::UnexpectedFrameIndex(e) =>
				Self::Deframer(LocalDeframerError::UnexpectedFrameIndex {
					expected: e.expected,
					observed: e.observed
				}),

			proto::frame_error::Error::InvalidStreamSize(e) =>
				Self::Deframer(LocalDeframerError::InvalidStreamSize(e.observed)),

			proto::frame_error::Error::UnexpectedPayloadSize(e) =>
				Self::Deframer(LocalDeframerError::UnexpectedPayloadSize {
					expected: e.expected,
					observed: e.observed
				}),

			proto::frame_error::Error::Closed(_) =>
				Self::Deframer(LocalDeframerError::Closed),

			proto::frame_error::Error::Abort(_) =>
				Self::Abort
		})
	}

	pub fn frame(&self) -> Vec<u8> {
		proto::Frame {
			kind: Some(proto::frame::Kind::Error(self.to_proto()))
		}.encode_to_vec()
	}
}
