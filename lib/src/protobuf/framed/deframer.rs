
use display_error_chain::ErrorChainExt;
use prost::Message;
use thiserror::Error;
use tracing::warn;

use crate::protobuf::framed::error::RemoteFrameError;
use crate::protobuf::smolweb::framed as proto;
use crate::protobuf::framed::{div_ceil, FRAME_KIND_CONTINUE, FRAME_KIND_ERROR, FRAME_KIND_START, FrameKindProps, MAX_PAYLOAD_SIZE};
use crate::protobuf::ReadProtobufError;


#[derive(Debug)]
pub struct Deframer {
	next_frame: Option<u64>,
	bytes_total: u64,
	bytes_read: u64
}

impl Deframer {

	pub fn new<B:AsRef<[u8]>>(msg: B) -> Result<(Self,Vec<u8>),DeframerError> {

		// decode the frame
		let frame = proto::Frame::decode(msg.as_ref())
			.map_err(|e| LocalDeframerError::Decode(e.into_chain().to_string()))?;
		let kind = frame.kind
			.ok_or(ReadProtobufError::MissingField("kind"))?;

		// expected a start frame, or an error
		let frame = match kind {

			proto::frame::Kind::Start(f) => f,

			proto::frame::Kind::Error(e) =>
				Err(DeframerError::Remote(RemoteFrameError::from_protobuf(e)))?,

			_ =>
				Err(LocalDeframerError::UnexpectedKind {
					expected: vec![FRAME_KIND_START.to_string(), FRAME_KIND_ERROR.to_string()],
					observed: kind.name().to_string()
				})?
		};

		// validate
		let stream_size =
			if frame.stream_size > 0 {
				frame.stream_size as u64
			} else {
				Err(LocalDeframerError::InvalidStreamSize(frame.stream_size))?
			};

		let next_payload_size =
			if stream_size < MAX_PAYLOAD_SIZE as u64 {
				stream_size as usize
			} else {
				MAX_PAYLOAD_SIZE
			};
		if frame.payload.len() != next_payload_size {
			Err(LocalDeframerError::UnexpectedPayloadSize {
				expected: next_payload_size as u64,
				observed: frame.payload.len() as u64
			})?;
		}

		// count how many frames there should be
		let num_frames = div_ceil(stream_size, MAX_PAYLOAD_SIZE as u64);

		let reader = Self {
			next_frame: match num_frames {
				1 => None,
				_ => Some(1)
			},
			bytes_total: stream_size,
			bytes_read: frame.payload.len() as u64
		};

		Ok((reader, frame.payload))
	}

	pub fn bytes_total(&self) -> u64 {
		self.bytes_total
	}

	pub fn bytes_total_i64(&self) -> i64 {
		// Self::new() guarantees this should never fail
		i64::try_from(self.bytes_total)
			.expect("bytes_total didn't fit in i64")
	}

	pub fn bytes_read(&self) -> u64 {
		self.bytes_read
	}

	pub fn is_closed(&self) -> bool {
		self.next_frame.is_none()
	}

	pub fn is_complete(&self) -> bool {
		self.bytes_read >= self.bytes_total
	}

	pub fn deframe<B:AsRef<[u8]>>(&mut self, msg: B) -> Result<Vec<u8>,DeframerError> {

		// are we even expecting another frame?
		let (next_frame, next_payload_size) = if let Some(i) = self.next_frame {

			// yup, what should the size of the next frame be?
			let bytes_remaining = self.bytes_total.checked_sub(self.bytes_read)
				.expect("read too many bytes somehow");

			let next_payload_size =
				if bytes_remaining < MAX_PAYLOAD_SIZE as u64 {
					bytes_remaining as usize
				} else {
					MAX_PAYLOAD_SIZE
				};

			(i, next_payload_size)

		} else {
			// nope
			Err(LocalDeframerError::Closed)?
		};

		// decode the frame
		let frame = proto::Frame::decode(msg.as_ref())
			.map_err(|e| LocalDeframerError::Decode(e.into_chain().to_string()))?;
		let kind = frame.kind
			.ok_or(ReadProtobufError::MissingField("kind"))?;

		// expected a continue frame, or an error
		let frame = match kind {

			proto::frame::Kind::Continue(f) => f,

			proto::frame::Kind::Error(e) => {

				// close the stream
				self.next_frame = None;

				Err(DeframerError::Remote(RemoteFrameError::from_protobuf(e)))?
			}

			_ =>
				Err(LocalDeframerError::UnexpectedKind {
					expected: vec![FRAME_KIND_CONTINUE.to_string(), FRAME_KIND_ERROR.to_string()],
					observed: kind.name().to_string()
				})?
		};

		// validate
		let frame_index =
			if frame.frame_index >= 1 {
				frame.frame_index as u64
			} else {
				Err(LocalDeframerError::UnexpectedFrameIndex {
					expected: next_frame as i64,
					observed: frame.frame_index
				})?
			};
		if frame_index != next_frame {
			Err(LocalDeframerError::UnexpectedFrameIndex {
				expected: next_frame as i64,
				observed: frame.frame_index
			})?
		}
		if frame.payload.len() != next_payload_size {
			Err(LocalDeframerError::UnexpectedPayloadSize {
				expected: next_payload_size as u64,
				observed: frame.payload.len() as u64
			})?
		}

		// consume the payload
		self.bytes_read += frame.payload.len() as u64;

		// should we expect a next frame?
		self.next_frame =
			if self.bytes_read < self.bytes_total {
				Some(frame_index + 1)
			} else {
				None
			};

		Ok(frame.payload)
	}
}

impl Drop for Deframer {

	fn drop(&mut self) {
		if self.next_frame.is_some() {
			warn!("unfinished FrameReader dropped");
		}
	}
}



#[derive(Error, Debug, Clone)]
pub enum DeframerError {

	#[error(transparent)]
	Local(#[from] LocalDeframerError),

	#[error("Frame contained an error from the remote side: {0:?}")]
	Remote(Result<RemoteFrameError,ReadProtobufError>)
}


#[derive(Error, Debug, Clone)]
pub enum LocalDeframerError {

	#[error("Error decoding frame: {0}")]
	Decode(String),

	#[error("Unexpected frame kind {observed}, expected one of {expected:?}")]
	UnexpectedKind {
		expected: Vec<String>,
		observed: String
	},

	#[error("Unexpected frame index {observed}, expected {expected}")]
	UnexpectedFrameIndex {
		expected: i64,
		observed: i64
	},

	#[error("Invalid stream size: {0}")]
	InvalidStreamSize(i64),

	#[error("Unexpected frame payload size {observed}, expected {expected}")]
	UnexpectedPayloadSize {
		expected: u64,
		observed: u64
	},

	#[error("No more frames were expected")]
	Closed
}


impl LocalDeframerError {

	pub fn frame(self) -> Vec<u8> {
		RemoteFrameError::Deframer(self).frame()
	}
}


impl From<ReadProtobufError> for DeframerError {

	fn from(value: ReadProtobufError) -> Self {
		Self::Local(LocalDeframerError::Decode(value.to_string()))
	}
}
