
use std::collections::TryReserveError;
use std::fmt::Debug;
use std::io::ErrorKind;
use std::marker::PhantomPinned;
use std::pin::Pin;
use std::task::{Context, Poll};
use std::time::Duration;

use axum::async_trait;
use futures::{ready, AsyncRead, Future, FutureExt, StreamExt};
use futures::future::BoxFuture;
use pin_project::pin_project;
use thiserror::Error;
use tokio_util::io::StreamReader;
use tokio_util::compat::TokioAsyncReadCompatExt;

use crate::protobuf::framed::{Deframer, DeframerError, LocalDeframerError, RemoteFrameError};


#[async_trait]
pub trait Reader {

	type Error: std::error::Error + Send + 'static;

	async fn read(&self) -> Result<Vec<u8>,Self::Error>;
}


pub struct FrameReader<R> {
	reader: R,
	event_stream: Option<EventStream>
}

impl<R> FrameReader<R>
	where
		R: Reader + Send
{

	pub fn from(reader: R) -> Self {
		Self {
			reader,
			event_stream: None
		}
	}

	pub fn reader(&mut self) -> &mut R {
		&mut self.reader
	}

	pub fn into_reader(self) -> R {
		self.reader
	}

	pub async fn read(&mut self) -> Result<FrameEvent,R::Error> {

		// wait for the next message
		let msg = self.reader.read()
			.await?;

		let (maybe_stream, event) = match self.event_stream.take() {

			// no current stream, try to start a new one
			None => EventStream::start(msg),

			// try to continue the existing stream
			Some(stream) => stream.resume(msg)
		};

		// save (or end) the stream
		self.event_stream = maybe_stream;

		Ok(event)
	}

	pub fn abandon_stream(&mut self) {
		self.event_stream = None;
	}

	pub fn stream(&mut self) -> Result<FrameStream<R>,FrameStreamError<R::Error>> {

		if self.event_stream.is_some() {
			return Err(FrameStreamError::AlreadyStreaming);
		}

		Ok(FrameStream::new(self))
	}

	pub async fn read_all(&mut self, timeout: FrameReaderTimeout) -> Result<Vec<u8>,FrameReaderBufError<R::Error>> {

		let mut stream = self.stream()?;

		macro_rules! ok_or_abort {
			($result:expr) => {
				match $result {
					Ok(o) => o,
					Err(e) => {
						stream.abort();
						return Err(e.into());
					}
				}
			};
		}

		// wait for the first frame
		let frame = stream.next();
		let frame = match timeout {
			FrameReaderTimeout::None | FrameReaderTimeout::AfterStart(_) =>
				frame.await,
			FrameReaderTimeout::Now(t) => {
				tokio::time::timeout(t, frame)
					.await
					.map_err(|_| FrameReaderBufError::Timeout)?
			}
		}.expect("first frame can't be missing")?;
		// PANIC SAFETY: it's not possible for the first call to next() to return None

		// NOTE: self.event_stream is Some now: don't exit this function without setting it to None again

		// check the stream size
		let bytes_total = stream.bytes_total()
			.expect("no size after first frame");
			// PANIC SAFETY: the steam must have the size after the first frame
		let result = usize::try_from(bytes_total)
			.map_err(|_| FrameReaderBufError::StreamTooLarge {
				size: bytes_total,
				error_response: RemoteFrameError::Deframer(LocalDeframerError::InvalidStreamSize(bytes_total as i64))
			});
		let stream_size = ok_or_abort!(result);

		// try to allocate the buffer
		let mut buf = frame;
		let result = buf.try_reserve_exact(stream_size - buf.len())
			.map_err(|e| {
				FrameReaderBufError::BufferAllocation {
					inner: e,
					error_response: RemoteFrameError::Deframer(LocalDeframerError::InvalidStreamSize(bytes_total as i64))
				}
			});
		ok_or_abort!(result);

		// keep streaming into the buffer
		loop {

			// wait for the next event
			let frame = stream.next();
			let result = match timeout {
				FrameReaderTimeout::None =>
					frame.await,
				FrameReaderTimeout::Now(t) | FrameReaderTimeout::AfterStart(t) => {
					let result = tokio::time::timeout(t, frame)
						.await
						.map_err(|_| FrameReaderBufError::Timeout);
					ok_or_abort!(result)
				}
			};

			let Some(result) = result
				else { break; };

			let mut frame = ok_or_abort!(result);
			buf.append(&mut frame);
		}

		Ok(buf)
	}
}


pub enum FrameReaderTimeout {

	/// wait indefinitely
	None,

	/// timeout if it takes too long to receive the first frame
	/// or if the time between any two consecutive frames is too long
	Now(Duration),

	/// wait indefinitely for the first frame
	/// and timeout if the time between any two consecutive frames is too long
	AfterStart(Duration)
}


#[derive(Error, Debug)]
pub enum FrameReaderBufError<R>
	where
		R: std::error::Error + 'static
{

	#[error("Failed to stream frame")]
	Stream(#[from] FrameStreamError<R>),

	#[error("Timed out waiting for the next frame")]
	Timeout,

	#[error("This stream ({size} bytes) is too large to be buffered")]
	StreamTooLarge {
		size: u64,
		error_response: RemoteFrameError
	},

	#[error("Failed to allocate buffer for stream")]
	BufferAllocation {
		inner: TryReserveError,
		error_response: RemoteFrameError
	}
}


#[derive(Debug)]
struct EventStream {
	deframer: Deframer
}

impl EventStream {

	fn start(data: Vec<u8>) -> (Option<EventStream>, FrameEvent) {

		match Deframer::new(data) {

			Err(DeframerError::Local(e)) => {

				// failed to start stream: ignore the frame, pass the error to the event listener, and also respond with it
				let event = FrameEvent::Error {
					local_error: DeframerError::Local(e.clone()),
					error_response: Some(RemoteFrameError::Deframer(e))
				};
				(None, event)
			}

			Err(DeframerError::Remote(e)) => {

				// remote error: pass it to the event listener, but don't respond
				let event = FrameEvent::Error {
					local_error: DeframerError::Remote(e),
					error_response: None
				};
				(None, event)
			}

			Ok((deframer, payload)) =>  {

				if deframer.is_complete() {

					// stream is complete: send the payload to the event listener and drop the deframer
					let event = FrameEvent::Finish {
						payload
					};
					(None, event)

				} else {

					// stream started and is expecting more frames
					let bytes_total = deframer.bytes_total();
					let stream = EventStream {
						deframer
					};
					let event = FrameEvent::Start {
						bytes_total,
						payload
					};
					(Some(stream), event)
				}
			}
		}
	}

	fn resume(mut self, data: Vec<u8>) -> (Option<EventStream>, FrameEvent) {

		match self.deframer.deframe(data) {

			Err(DeframerError::Local(e)) => {

				// failed to continue stream: ignore the frame, pass the error to the event listener, and also respond with it
				let event = FrameEvent::Error {
					local_error: DeframerError::Local(e.clone()),
					error_response: Some(RemoteFrameError::Deframer(e))
				};
				(None, event)
			}

			Err(DeframerError::Remote(e)) => {

				// remote error: pass it to the event listener, but don't respond
				let event = FrameEvent::Error {
					local_error: DeframerError::Remote(e),
					error_response: None
				};
				(None, event)
			}

			Ok(payload) => {

				if self.deframer.is_complete() {

					// stream is complete: get the payload and drop the stream
					let event = FrameEvent::Finish {
						payload
					};
					(None, event)

				} else {

					// stream expecting more frames: keep it going but report progress to the event listener
					let event = FrameEvent::Continue {
						bytes_read: self.deframer.bytes_read(),
						payload
					};
					(Some(self), event)
				}
			}
		}
	}
}


#[derive(Debug, Clone)]
pub enum FrameEvent {

	Error {
		local_error: DeframerError,
		error_response: Option<RemoteFrameError>
	},

	/// the first frame in a stream
	Start {
		/// guaranteed to be <= i64::MAX
		bytes_total: u64,
		payload: Vec<u8>
	},

	/// a middle frame in the stream
	Continue {
		bytes_read: u64,
		payload: Vec<u8>
	},

	/// the final frame in the stream
	Finish {
		payload: Vec<u8>
	}
}


#[pin_project(project = FrameReaderStreamProj)]
pub struct FrameStream<'r,R>
	where
		R: Reader + Send
{
	reader: &'r mut FrameReader<R>,
	size: Option<u64>,
	finished: bool,
	#[pin]
	fut_frame: Option<BoxFuture<'r,Result<FrameEvent,R::Error>>>,
	_phantom_pinned: PhantomPinned
}


impl<'r,R> FrameStream<'r,R>
	where
		R: Reader + Send
{

	fn new(reader: &'r mut FrameReader<R>) -> Self {
		Self {
			reader,
			size: None,
			finished: false,
			fut_frame: None,
			_phantom_pinned: PhantomPinned::default()
		}
	}

	pub fn bytes_total(&self) -> Option<u64> {
		self.size
	}

	pub fn abort(self) {
		self.reader.abandon_stream();
	}

	pub fn into_async_read(self) -> impl AsyncRead + 'r
		where
			R::Error: Send + Sync
	{
		StreamReader::new(
			self.map(|result| {
				result
					.map(|buf| bytes::Bytes::from(buf))
					.map_err(|e| std::io::Error::new(ErrorKind::Other, e))
			})
		).compat()
	}
}

impl<'r,R> futures::Stream for FrameStream<'r,R>
	where
		R: Reader + Send
{

	type Item = Result<Vec<u8>,FrameStreamError<R::Error>>;

	fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {

		if self.finished {
			return Poll::Ready(None);
		}

		let mut this = self.project();

		// get a new read future, if needed
		if this.fut_frame.is_none() {

			let f = this.reader.read().boxed();

			let f = unsafe {
				/* SAFETY:
					If I understand this future correctly (big if, haha),
					then rustc won't let us make the cast:
					let f: BoxFuture<'r,_> = f;
					because even though the self lifetime is guaranteed to live longer than 'r,
					there's no guarantee that the pinned reference will last that long.
				 	Meaning, the reference to self could be unpinned.
				 	So to make this cast safe, we added PhantomPinned to the struct to make it !Unpin.
				*/
				std::mem::transmute::<
					BoxFuture<'_,_>,
					BoxFuture<'r,_>
				>(f)
			};

			this.fut_frame.set(Some(f));
		}

		let f = this.fut_frame.as_mut().as_pin_mut()
			.unwrap(); // PANIC SAFETY: if fut_frame was None, we just put Some there

		let event = ready!(f.poll(cx));
		this.fut_frame.take();
		Poll::Ready(Some(match event {

			Err(e) => {
				Err(FrameStreamError::Read(e))
			}

			Ok(FrameEvent::Error { local_error, error_response }) => {
				Err(FrameStreamError::Frame { local_error, error_response })
			}

			Ok(FrameEvent::Start { bytes_total, payload }) => {
				*this.size = Some(bytes_total);
				Ok(payload)
			}

			Ok(FrameEvent::Continue { payload, .. }) => {
				Ok(payload)
			}

			Ok(FrameEvent::Finish { payload }) => {
				if this.size.is_none() {
					*this.size = Some(payload.len() as u64);
				}
				*this.finished = true;
				Ok(payload)
			}
		}))
	}
}


#[derive(Error, Debug)]
pub enum FrameStreamError<R>
	where
		R: std::error::Error + 'static
{

	#[error("The reader is already streaming and can't start another one")]
	AlreadyStreaming,

	#[error("Failed to read from underlying transport")]
	Read(#[source] R),

	#[error("Error deframing stream")]
	Frame {
		#[source] local_error: DeframerError,
		error_response: Option<RemoteFrameError>
	}
}
