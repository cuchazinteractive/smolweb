
use thiserror::Error;

use crate::protobuf::smolweb::fetch as proto;
use crate::protobuf::ReadProtobufError;


pub type Header = proto::Header;
pub type Request = proto::Request;
pub type ResponseBodyType = proto::response::server_response::BodyType;

#[derive(Debug, Clone, PartialEq)]
pub enum Response {
	Gateway(GatewayResponse),
	Server(ServerResponse)
}

#[derive(Debug, Clone, PartialEq)]
pub struct ServerResponse {
	pub status: i32,
	pub headers: Vec<Header>,
	pub body_type: ResponseBodyType,
}

#[derive(Error, Debug, Clone, PartialEq, Eq)]
pub enum GatewayResponse {

	#[error("The fetch request was invalid: {reason}")]
	InvalidRequest {
		reason: String
	},

	#[error("The HTTP server was unreachable")]
	ServerUnreachable,

	#[error("The HTTP server did not complete the response")]
	ServerResponseIncomplete,

	#[error("The HTTP server returned an invalid response")]
	ServerResponseInvalid
}


impl Header {

	pub fn new(name: impl Into<String>, value: impl Into<String>) -> Self {
		Self {
			name: name.into(),
			value: value.into()
		}
	}

	pub fn filter(name: impl Into<String>) -> Box<dyn FnMut(&&Header) -> bool> {
		let name = name.into();
		Box::new(move |h| {
			h.name == name
		})
	}
}


pub mod client {

	use prost::Message;

	use crate::protobuf::IntoProtobuf;

	use super::*;


	pub fn encode_request(request: Request) -> Vec<u8> {
		request.encode_to_vec()
	}

	pub fn decode_response(msg: impl AsRef<[u8]>) -> Result<Response,ReadProtobufError> {

		let response: proto::Response = msg.into_protobuf()?;
		let Some(response) = response.response
			else { return Err(ReadProtobufError::MissingField("response")); };
		match response {

			proto::response::Response::ServerResponse(response) => {
				let Ok(body_type) = ResponseBodyType::try_from(response.body_type)
					else { return Err(ReadProtobufError::InvalidValue("body_type", response.body_type.to_string())) };
				Ok(Response::Server(ServerResponse {
					status: response.status,
					headers: response.headers,
					body_type
				}))
			}

			proto::response::Response::GatewayResponse(response) => {
				let Some(response) = response.response
					else { return Err(ReadProtobufError::MissingField("response")); };
				Ok(Response::Gateway(match response {
					proto::response::gateway_response::Response::InvalidRequest(reason) =>
						GatewayResponse::InvalidRequest { reason },
					proto::response::gateway_response::Response::ServerUnreachable(_) =>
						GatewayResponse::ServerUnreachable,
					proto::response::gateway_response::Response::ServerResponseIncomplete(_) =>
						GatewayResponse::ServerResponseIncomplete,
					proto::response::gateway_response::Response::ServerResponseInvalid(_) =>
						GatewayResponse::ServerResponseInvalid
				}))
			}
		}
	}
}


pub mod server {

	use prost::Message;

	use crate::protobuf::IntoProtobuf;

	use super::*;


	pub fn decode_request(msg: impl AsRef<[u8]>) -> Result<Request,ReadProtobufError> {
		let request: proto::Request = msg.into_protobuf()?;
		Ok(request)
	}

	pub fn encode_server_response(response: ServerResponse) -> Vec<u8> {
		proto::Response {
			response: Some(proto::response::Response::ServerResponse(proto::response::ServerResponse {
				status: response.status,
				headers: response.headers,
				body_type: response.body_type as i32,
			}))
		}.encode_to_vec()
	}

	pub fn encode_gateway_response(response: GatewayResponse) -> Vec<u8> {
		proto::Response {
			response: Some(proto::response::Response::GatewayResponse(proto::response::GatewayResponse {
				response: Some(match response {
					GatewayResponse::InvalidRequest { reason } =>
						proto::response::gateway_response::Response::InvalidRequest(reason),
					GatewayResponse::ServerUnreachable =>
						proto::response::gateway_response::Response::ServerUnreachable(true),
					GatewayResponse::ServerResponseIncomplete =>
						proto::response::gateway_response::Response::ServerResponseIncomplete(true),
					GatewayResponse::ServerResponseInvalid =>
						proto::response::gateway_response::Response::ServerResponseInvalid(true)
				})
			}))
		}.encode_to_vec()
	}
}
