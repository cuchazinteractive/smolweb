
use crate::protobuf::smolweb as proto;


pub type About = proto::guests::About;


pub mod guest {

	use burgerid::{IdentityApp, IdentityDecryptError, IdentityEncryptError, IdentityEphemeral, IdentityRef, OpenIdentityError, ProtobufOpen, ProtobufSave, UnlockedIdentity};
	use prost::Message;
	use thiserror::Error;

	use crate::protobuf::{CONTEXT_CONNECTION_CONFIRM, CONTEXT_CONNECTION_REQUEST, CONTEXT_CONNECTION_RESPONSE, HostUid, IntoProtobuf, ReadProtobufError};
	use crate::webrtc::SessionDescription;

	use super::*;


	pub fn decode_about(msg: impl AsRef<[u8]>) -> Result<About,DecodeError> {
		let about: About = msg.as_ref().into_protobuf()?;
		Ok(about)
	}

	pub fn encode_host_identity_request(host_uid: &HostUid) -> Vec<u8> {
		proto::guests::HostIdentityRequest {
			host_uid: host_uid.clone(),
		}.encode_to_vec()
	}

	pub fn decode_host_identity_response(msg: impl AsRef<[u8]>) -> Result<Option<IdentityApp>,DecodeError> {
		let response: proto::guests::HostIdentityResponse = msg.as_ref().into_protobuf()?;
		let Some(response) = response.response
			else { Err(ReadProtobufError::MissingField("response"))? };
		match response {
			proto::guests::host_identity_response::Response::HostUnknown(_) => {
				// identity not found
				Ok(None)
			}
			proto::guests::host_identity_response::Response::Identity(id_saved) => {
				// try to open the identity
				let id = IdentityApp::open(id_saved)?;
				Ok(Some(id))
			}
		}
	}

	pub fn encode_connection_request(guest_id: &IdentityEphemeral, guest_id_unlocked: &UnlockedIdentity, host_id: &IdentityApp, offer1: SessionDescription) -> Result<Vec<u8>,IdentityEncryptError> {

		// encrypt the secret
		let plaintext = proto::secrets::ConnectionRequestSecret {
			offer1: offer1.into_string(),
		}.encode_to_vec();
		let secret = guest_id_unlocked.encrypt(plaintext, vec![IdentityRef::App(host_id)], CONTEXT_CONNECTION_REQUEST)?;

		Ok(proto::guests::ConnectionRequest {
			host_uid: host_id.uid().clone(),
			identity: guest_id.save_bytes(),
			secret,
		}.encode_to_vec())
	}

	pub fn decode_connection_response(msg: impl AsRef<[u8]>, guest_id: &UnlockedIdentity, host_id: &IdentityApp) -> Result<Result<ConnectionAccept,ConnectionReject>,DecodeError> {

		let response: proto::guests::ConnectionResponse = msg.as_ref().into_protobuf()?;
		let Some(response) = response.response
			else { Err(ReadProtobufError::MissingField("response"))? };

		match response {
			proto::guests::connection_response::Response::HostUnknown(_) => Ok(Err(ConnectionReject::HostUnknown)),
			proto::guests::connection_response::Response::HostUnreachable(_) => Ok(Err(ConnectionReject::HostUnreachable)),
			proto::guests::connection_response::Response::Accept(accept) => {

				// decrypt the secret
				let plaintext = guest_id.decrypt(accept.secret, IdentityRef::App(host_id), CONTEXT_CONNECTION_RESPONSE)?;
				let accept: proto::secrets::ConnectionAcceptSecret = plaintext.into_protobuf()?;

				Ok(Ok(ConnectionAccept {
					connection_id: accept.connection_id,
					answer1: SessionDescription::answer(accept.answer1)?,
					offer2: SessionDescription::offer(accept.offer2)?,
				}))
			}
		}
	}

	pub fn encode_connection_confirm(guest_id: &UnlockedIdentity, host_id: &IdentityApp, connection_id: u64, answer2: SessionDescription) -> Result<Vec<u8>,IdentityEncryptError> {

		// encrypt the secret
		let plaintext = proto::secrets::ConnectionConfirmSecret {
			connection_id,
			answer2: answer2.into_string()
		}.encode_to_vec();
		let secret = guest_id.encrypt(plaintext, vec![IdentityRef::App(host_id)], CONTEXT_CONNECTION_CONFIRM)?;

		Ok(proto::guests::ConnectionConfirm {
			host_uid: host_id.uid().clone(),
			guest_uid: guest_id.uid().clone(),
			secret,
		}.encode_to_vec())
	}


	#[derive(Debug, Clone, PartialEq, Eq)]
	pub struct ConnectionAccept {
		pub connection_id: u64,
		pub answer1: SessionDescription,
		pub offer2: SessionDescription
	}

	#[derive(Error, Debug, Clone, PartialEq, Eq)]
	pub enum ConnectionReject {

		#[error("Host was not known to the beacon server")]
		HostUnknown,

		#[error("Beacon server could not reach the host")]
		HostUnreachable
	}


	#[derive(Error, Debug)]
	pub enum DecodeError {

		#[error("Failed to read protobuf")]
		Protobuf(#[from] ReadProtobufError),

		#[error("Failed to parse WebRTC session description")]
		WebRTC(#[from] webrtc::Error),

		#[error("Failed to open app identity")]
		Identity(#[from] OpenIdentityError),

		#[error("Failed to decrypt secret")]
		Decrypt(#[from] IdentityDecryptError)
	}
}


pub mod beacon {

	use burgerid::{IdentityApp, ProtobufSave};
	use prost::Message;

	use crate::protobuf::{HostUid, IntoProtobuf, ReadProtobufError};

	use super::*;


	pub type ConnectionRequest = proto::guests::ConnectionRequest;
	pub type ConnectionConfirm = proto::guests::ConnectionConfirm;


	pub fn encode_about(about: About) -> Vec<u8> {
		about.encode_to_vec()
	}

	pub fn decode_host_identity_request(msg: impl AsRef<[u8]>) -> Result<HostUid,ReadProtobufError> {
		let request: proto::guests::HostIdentityRequest = msg.as_ref().into_protobuf()?;
		Ok(request.host_uid)
	}

	pub fn encode_host_identity_response(identity: Option<&IdentityApp>) -> Vec<u8> {
		proto::guests::HostIdentityResponse {
			response: Some(match identity {
				Some(identity) => proto::guests::host_identity_response::Response::Identity(identity.save_bytes()),
				None => proto::guests::host_identity_response::Response::HostUnknown(true)
			})
		}.encode_to_vec()
	}

	pub fn decode_connection_request(msg: impl AsRef<[u8]>) -> Result<ConnectionRequest,ReadProtobufError> {
		let request: proto::guests::ConnectionRequest = msg.as_ref().into_protobuf()?;
		Ok(request)
	}

	pub fn encode_connection_response_accept(secret: Vec<u8>) -> Vec<u8> {
		proto::guests::ConnectionResponse {
			response: Some(proto::guests::connection_response::Response::Accept(proto::guests::connection_response::Accept {
				secret
			}))
		}.encode_to_vec()
	}

	pub fn encode_connection_response_host_unknown() -> Vec<u8> {
		proto::guests::ConnectionResponse {
			response: Some(proto::guests::connection_response::Response::HostUnknown(true))
		}.encode_to_vec()
	}

	pub fn encode_connection_response_host_unreachable() -> Vec<u8> {
		proto::guests::ConnectionResponse {
			response: Some(proto::guests::connection_response::Response::HostUnreachable(true))
		}.encode_to_vec()
	}

	pub fn decode_connection_confirm(msg: impl AsRef<[u8]>) -> Result<ConnectionConfirm,ReadProtobufError> {
		let msg: proto::guests::ConnectionConfirm = msg.as_ref().into_protobuf()?;
		Ok(msg)
	}
}
