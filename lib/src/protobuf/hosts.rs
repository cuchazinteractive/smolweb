
use crate::protobuf::smolweb as proto;


pub const DEFAULT_PORT: u16 = 32532;
pub const LOGIN_CONTEXT: &[u8; 20] = b"smolweb/beacon/login";


pub type AuthnError = proto::hosts::HostAuthnError;


pub mod host {

	use burgerid::{IdentityApp, IdentityDecryptError, IdentityEncryptError, IdentitySignError, IdentityEphemeral, IdentityRef, OpenIdentityError, ProtobufOpen, ProtobufSave, UnlockedIdentity};
	use prost::Message;
	use thiserror::Error;

	use crate::protobuf::{CONTEXT_CONNECTION_CONFIRM, CONTEXT_CONNECTION_REQUEST, CONTEXT_CONNECTION_RESPONSE, IntoProtobuf, ReadProtobufError};
	use crate::webrtc::SessionDescription;

	use super::*;


	pub fn encode_ready_request(host_id: &IdentityApp) -> Vec<u8> {
		proto::hosts::HostReadyRequest {
			app_id: host_id.save_bytes()
		}.encode_to_vec()
	}

	pub fn decode_ready_challenge(msg: impl AsRef<[u8]>) -> Result<ReadyChallenge,ReadProtobufError> {
		let challenge: proto::hosts::HostReadyChallenge = msg.as_ref().into_protobuf()?;
		let result = challenge.result
			.ok_or(ReadProtobufError::MissingField("result"))?;
		match result {
			proto::hosts::host_ready_challenge::Result::Nonce(nonce) => Ok(ReadyChallenge::Challenge { nonce }),
			proto::hosts::host_ready_challenge::Result::ErrorAuthn(i) => {
				let e = proto::hosts::HostAuthnError::try_from(i)
					.map_err(|_| ReadProtobufError::InvalidValue("error_authn", i.to_string()))?;
				Ok(ReadyChallenge::Error(e))
			},
			proto::hosts::host_ready_challenge::Result::ErrorAlreadyReady(_) => Ok(ReadyChallenge::AlreadyReady)
		}
	}

	pub fn encode_ready_response(host_id_unlocked: &UnlockedIdentity, challenge_nonce: &Vec<u8>) -> Result<Vec<u8>,IdentitySignError> {

		// sign the nonce
		let signed_nonce = host_id_unlocked.sign(challenge_nonce, Some(LOGIN_CONTEXT))?;

		Ok(proto::hosts::HostReadyResponse {
			signed_nonce,
		}.encode_to_vec())
	}

	pub fn decode_ready_result(msg: impl AsRef<[u8]>) -> Result<ReadyResult,ReadProtobufError> {
		let result: proto::hosts::HostReadyResult = msg.as_ref().into_protobuf()?;
		let result = result.result
			.ok_or(ReadProtobufError::MissingField("result"))?;
		match result {
			proto::hosts::host_ready_result::Result::Ok(_) => Ok(ReadyResult::Ok),
			proto::hosts::host_ready_result::Result::ErrorAuthn(i) => {
				let e = proto::hosts::HostAuthnError::try_from(i)
					.map_err(|_| ReadProtobufError::InvalidValue("error_authn", i.to_string()))?;
				Ok(ReadyResult::Error(e))
			},
			proto::hosts::host_ready_result::Result::ErrorNotAuthorized(_) => Ok(ReadyResult::NotAuthorized),
		}
	}

	pub fn decode_msg(msg: impl AsRef<[u8]>) -> Result<BeaconMessage,ReadProtobufError> {
		let msg: proto::hosts::BeaconToHostMessage = msg.as_ref().into_protobuf()?;
		let Some(request) = msg.request
			else { Err(ReadProtobufError::MissingField("request"))? };
		match request {

			proto::hosts::beacon_to_host_message::Request::GuestConnect(connect) =>
				Ok(BeaconMessage::GuestConnectRequest(EncodedGuestConnectRequest(connect))),

			proto::hosts::beacon_to_host_message::Request::GuestConfirm(confirm) =>
				Ok(BeaconMessage::GuestConnectConfirm(EncodedGuestConnectConfirm(confirm))),

			proto::hosts::beacon_to_host_message::Request::Ping(request_id) =>
				Ok(BeaconMessage::Ping { request_id })
		}
	}

	pub fn encode_guest_connect_accept(host_id: &UnlockedIdentity, guest_id: &IdentityEphemeral, request_id: u64, connection_id: u64, answer1: SessionDescription, offer2: SessionDescription) -> Result<Vec<u8>,IdentityEncryptError> {

		// encrypt the secret
		let plaintext = proto::secrets::ConnectionAcceptSecret {
			connection_id,
			answer1: answer1.into_string(),
			offer2: offer2.into_string(),
		}.encode_to_vec();
		let secret = host_id.encrypt(plaintext, vec![IdentityRef::Ephemeral(guest_id)], CONTEXT_CONNECTION_RESPONSE)?;

		Ok(proto::hosts::HostToBeaconMessage {
			response: Some(proto::hosts::host_to_beacon_message::Response::GuestConnect(proto::hosts::GuestConnectResponse {
				request_id,
				secret,
			}))
		}.encode_to_vec())
	}

	pub fn encode_pong(request_id: u64) -> Vec<u8> {
		proto::hosts::HostToBeaconMessage {
			response: Some(proto::hosts::host_to_beacon_message::Response::Pong(request_id))
		}.encode_to_vec()
	}

	#[derive(Debug, Clone, PartialEq, Eq)]
	pub enum ReadyChallenge {
		Challenge {
			nonce: Vec<u8>
		},
		Error(AuthnError),
		AlreadyReady
	}

	#[derive(Debug, Clone, PartialEq, Eq)]
	pub enum ReadyResult {
		Ok,
		Error(AuthnError),
		NotAuthorized
	}

	#[derive(Debug, Clone, PartialEq)]
	pub enum BeaconMessage {
		GuestConnectRequest(EncodedGuestConnectRequest),
		GuestConnectConfirm(EncodedGuestConnectConfirm),
		Ping {
			request_id: u64
		}
	}


	#[derive(Debug, Clone, PartialEq)]
	pub struct EncodedGuestConnectRequest(proto::hosts::GuestConnectRequest);

	impl EncodedGuestConnectRequest {

		pub fn decode(self, host_id: &UnlockedIdentity) -> Result<GuestConnectRequest,DecodeError> {

			// open the guest identity
			let guest_id = IdentityEphemeral::open(self.0.guest_id)?;

			// decrypt the secret
			let plaintext = host_id.decrypt(self.0.secret, IdentityRef::Ephemeral(&guest_id), CONTEXT_CONNECTION_REQUEST)?;
			let request: proto::secrets::ConnectionRequestSecret = plaintext.into_protobuf()?;

			Ok(GuestConnectRequest {
				request_id: self.0.request_id,
				guest_id,
				offer1: SessionDescription::offer(request.offer1)?
			})
		}
	}


	#[derive(Debug, Clone, PartialEq)]
	pub struct EncodedGuestConnectConfirm(proto::hosts::GuestConnectConfirm);

	impl EncodedGuestConnectConfirm {

		pub fn guest_uid(&self) -> &Vec<u8> {
			&self.0.guest_uid
		}

		pub fn decode(self, host_id: &UnlockedIdentity, guest_id: &IdentityEphemeral) -> Result<GuestConnectConfirm,DecodeError> {

			// decrypt the secret
			let plaintext = host_id.decrypt(self.0.secret, IdentityRef::Ephemeral(guest_id), CONTEXT_CONNECTION_CONFIRM)?;
			let confirm: proto::secrets::ConnectionConfirmSecret = plaintext.into_protobuf()?;

			Ok(GuestConnectConfirm {
				connection_id: confirm.connection_id,
				answer2: SessionDescription::answer(confirm.answer2)?
			})
		}
	}


	#[derive(Debug, Clone, PartialEq, Eq)]
	pub struct GuestConnectRequest {
		pub request_id: u64,
		pub guest_id: IdentityEphemeral,
		pub offer1: SessionDescription
	}


	#[derive(Debug, Clone, PartialEq, Eq)]
	pub struct GuestConnectConfirm {
		pub connection_id: u64,
		pub answer2: SessionDescription
	}


	#[derive(Error, Debug)]
	pub enum DecodeError {

		#[error("Failed to read protobuf")]
		Protobuf(#[from] ReadProtobufError),

		#[error("Failed to parse WebRTC session description")]
		WebRTC(#[from] webrtc::Error),

		#[error("Failed to open identity")]
		Identity(#[from] OpenIdentityError),

		#[error("Failed to decrypt secret")]
		Decrypt(#[from] IdentityDecryptError)
	}
}


pub mod beacon {

	use burgerid::{IdentityApp, OpenIdentityError, ProtobufOpen};
	use prost::Message;
	use thiserror::Error;

	use crate::protobuf::{IntoProtobuf, ReadProtobufError};

	use super::*;


	pub type GuestConnectResponse = proto::hosts::GuestConnectResponse;


	pub fn decode_ready_request(msg: impl AsRef<[u8]>) -> Result<ReadyRequest,DecodeReadyRequestError> {

		let request: proto::hosts::HostReadyRequest = msg.as_ref().into_protobuf()?;

		Ok(ReadyRequest {
			host_id: IdentityApp::open(request.app_id)?
		})
	}

	pub fn encode_ready_challenge_nonce(nonce: Vec<u8>) -> Vec<u8> {
		proto::hosts::HostReadyChallenge {
			result: Some(proto::hosts::host_ready_challenge::Result::Nonce(nonce))
		}.encode_to_vec()
	}

	pub fn encode_ready_challenge_error_authn(e: AuthnError) -> Vec<u8> {
		proto::hosts::HostReadyChallenge {
			result: Some(proto::hosts::host_ready_challenge::Result::ErrorAuthn(e as i32))
		}.encode_to_vec()
	}

	pub fn encode_ready_challenge_error_already_ready() -> Vec<u8> {
		proto::hosts::HostReadyChallenge {
			result: Some(proto::hosts::host_ready_challenge::Result::ErrorAlreadyReady(true))
		}.encode_to_vec()
	}

	pub fn decode_ready_response(msg: impl AsRef<[u8]>) -> Result<ReadyResponse,ReadProtobufError> {

		let response: proto::hosts::HostReadyResponse = msg.as_ref().into_protobuf()?;

		Ok(response)
	}

	pub fn encode_ready_result_ok() -> Vec<u8> {
		proto::hosts::HostReadyResult {
			result: Some(proto::hosts::host_ready_result::Result::Ok(true))
		}.encode_to_vec()
	}

	pub fn encode_ready_result_error_authn(e: AuthnError) -> Vec<u8> {
		proto::hosts::HostReadyResult {
			result: Some(proto::hosts::host_ready_result::Result::ErrorAuthn(e as i32))
		}.encode_to_vec()
	}

	pub fn encode_ready_result_error_not_authorized() -> Vec<u8> {
		proto::hosts::HostReadyResult {
			result: Some(proto::hosts::host_ready_result::Result::ErrorNotAuthorized(true))
		}.encode_to_vec()
	}

	pub fn encode_guest_connect(request_id: u64, guest_id: impl AsRef<[u8]>, secret: Vec<u8>) -> Vec<u8> {
		proto::hosts::BeaconToHostMessage {
			request: Some(proto::hosts::beacon_to_host_message::Request::GuestConnect(proto::hosts::GuestConnectRequest {
				request_id,
				guest_id: guest_id.as_ref().to_vec(),
				secret,
			})),
		}.encode_to_vec()
	}

	pub fn encode_guest_confirm(guest_uid: impl AsRef<[u8]>, secret: Vec<u8>) -> Vec<u8> {
		proto::hosts::BeaconToHostMessage {
			request: Some(proto::hosts::beacon_to_host_message::Request::GuestConfirm(proto::hosts::GuestConnectConfirm {
				guest_uid: guest_uid.as_ref().to_vec(),
				secret,
			})),
		}.encode_to_vec()
	}

	pub fn encode_ping(request_id: u64) -> Vec<u8> {
		proto::hosts::BeaconToHostMessage {
			request: Some(proto::hosts::beacon_to_host_message::Request::Ping(request_id))
		}.encode_to_vec()
	}

	pub fn decode_msg(msg: impl AsRef<[u8]>) -> Result<HostMessage,ReadProtobufError> {
		let msg: proto::hosts::HostToBeaconMessage = msg.as_ref().into_protobuf()?;
		let Some(response) = msg.response
			else { Err(ReadProtobufError::MissingField("response"))? };
		match response {

			proto::hosts::host_to_beacon_message::Response::GuestConnect(response) =>
				Ok(HostMessage::GuestConnectResponse(response)),

			proto::hosts::host_to_beacon_message::Response::Pong(request_id) =>
				Ok(HostMessage::Pong { request_id })
		}
	}


	#[derive(Debug, Clone, PartialEq, Eq)]
	pub struct ReadyRequest {
		pub host_id: IdentityApp
	}

	pub type ReadyResponse = proto::hosts::HostReadyResponse;

	#[derive(Error, Debug)]
	pub enum DecodeReadyRequestError {

		#[error("Failed to read protobuf")]
		Protobuf(#[from] ReadProtobufError),

		#[error("Failed to open app identity")]
		Identity(#[from] OpenIdentityError)
	}


	#[derive(Debug, Clone, PartialEq)]
	pub enum HostMessage {
		GuestConnectResponse(GuestConnectResponse),
		Pong {
			request_id: u64
		}
	}
}
