
use std::fmt;
use std::io::ErrorKind;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr};
use std::str::FromStr;

use anyhow::anyhow;
use hyper::http::uri::Authority;
use serde::{Deserialize, Deserializer};
use thiserror::Error;

use crate::lang::JoinToString;
use crate::serde::{deserialize_from_str, OptStringOrStruct, OptStringOrVecOfStringOrStructs};


pub(crate) mod codec;
pub(crate) mod messages;
pub(crate) mod https_client;
pub mod dns;
pub mod listeners;
pub mod gateways;
pub mod stun_client;


#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum IpType {
	V4,
	V6
}

impl IpType {

	pub(crate) fn from_ip_addr(addr: &IpAddr) -> Self {
		match addr {
			IpAddr::V4(_) => Self::V4,
			IpAddr::V6(_) => Self::V6
		}
	}

	fn _to_string(&self) -> &'static str {
		match self {
			Self::V4 => "4",
			Self::V6 => "6"
		}
	}
}


// NOTE: In my OS, "localhost" will only resolve to the IPv4 address,
//       but "foo.localhost" will resolve to both the IPv4 and IPv6 addresses.
//       So always use a flavor of localhost that resolves to both addresses,
//       since we care about both types here.
pub(crate) const LOCALHOST: &str = "me.localhost";


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigAddr {
	pub host: String,
	pub port: Option<u16>
}

impl ConfigAddr {

	pub fn deserialize<'de,D>(deserializer: D) -> Result<Option<ConfigAddr>,D::Error>
		where
			D: Deserializer<'de>
	{
		OptStringOrStruct::<ConfigAddr>::deserialize(deserializer)
	}

	pub fn deserialize_default() -> Option<ConfigAddr> {
		OptStringOrStruct::<ConfigAddr>::default()
	}

	pub fn deserialize_vec<'de,D>(deserializer: D) -> Result<Option<Vec<ConfigAddr>>,D::Error>
		where
			D: Deserializer<'de>
	{
		OptStringOrVecOfStringOrStructs::<ConfigAddr>::deserialize(deserializer)
	}

	pub fn deserialize_vec_default() -> Option<Vec<ConfigAddr>> {
		OptStringOrVecOfStringOrStructs::<ConfigAddr>::default()
	}

	pub fn new(host: impl AsRef<str>, port: Option<u16>) -> Self {
		Self {
			host: host.as_ref().to_string(),
			port
		}
	}

	pub fn from_host(host: impl AsRef<str>) -> Self {
		Self::new(host, None)
	}

	pub fn localhost() -> Self {
		Self::new(LOCALHOST, None)
	}

	pub fn localhost_port(port: u16) -> Self {
		Self::new(LOCALHOST, Some(port))
	}

	pub fn localhost_auto_port() -> Self {
		Self::localhost_port(0)
	}

	pub fn all_interfaces() -> Vec<Self> {
		vec![
			Self::from_host("0.0.0.0"),
			Self::from_host("::")
		]
	}
}

impl FromStr for ConfigAddr {

	type Err = ConfigAddrParseError;

	fn from_str(s: &str) -> Result<Self,Self::Err> {
		let a = Authority::from_str(s)
			.map_err(|_| ConfigAddrParseError)?;
		if a.host().is_empty() {
			return Err(ConfigAddrParseError);
		}
		Ok(Self {
			host: a.host()
				// trim off the IPv6 brackets
				.trim_start_matches('[')
				.trim_end_matches(']')
				.to_string(),
			port: a.port_u16()
		})
	}
}

#[derive(Error, Debug)]
#[error("Failed to parse address")]
pub struct ConfigAddrParseError;

impl fmt::Display for ConfigAddr {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		if let Some(port) = self.port {
			if self.host.contains("::") {
				write!(f, "[{}]:{}", self.host, port)
			} else {
				write!(f, "{}:{}", self.host, port)
			}
		} else {
			write!(f, "{}", self.host)
		}
	}
}


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BindMode {
	Both,
	OnlyIpv6,
	OnlyIpv4
}

impl FromStr for BindMode {

	type Err = anyhow::Error;

	fn from_str(s: &str) -> Result<Self,Self::Err> {
		match s.to_lowercase().as_str() {
			"both" => Ok(Self::Both),
			"onlyipv6" => Ok(Self::OnlyIpv6),
			"onlyipv4" => Ok(Self::OnlyIpv4),
			_ => Err(anyhow!("unrecognized option \"{}\", expected one of \"both\", \"onlyipv6\", \"onlyipv4\"", s))
		}
	}
}

deserialize_from_str!(BindMode);

impl fmt::Display for BindMode {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self)
	}
}


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ConnectMode {
	PreferIpv6,
	PreferIpv4,
	OnlyIpv6,
	OnlyIpv4
}

impl ConnectMode {

	pub fn localhost_ip(&self) -> std::net::IpAddr {
		match self {
			ConnectMode::PreferIpv6 | ConnectMode::OnlyIpv6 => Ipv6Addr::LOCALHOST.into(),
			ConnectMode::PreferIpv4 | ConnectMode::OnlyIpv4 => Ipv4Addr::LOCALHOST.into()
		}
	}
}

impl FromStr for ConnectMode {

	type Err = anyhow::Error;

	fn from_str(s: &str) -> Result<Self,Self::Err> {
		match s.to_lowercase().as_str() {
			"preferipv6" => Ok(Self::PreferIpv6),
			"preferipv4" => Ok(Self::PreferIpv4),
			"onlyipv6" => Ok(Self::OnlyIpv6),
			"onlyipv4" => Ok(Self::OnlyIpv4),
			_ => Err(anyhow!("unrecognized option \"{}\", expected one of \"preferipv6\", \"preferipv4\", \"onlyipv6\", \"onlyipv4\"", s))
		}
	}
}

deserialize_from_str!(ConnectMode);

impl fmt::Display for ConnectMode {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self)
	}
}


/// returns the localhost socket address that matches the IP version of the connect address.
#[cfg(test)]
pub(crate) fn matching_localhost_addr(connect_addr: &SocketAddr) -> SocketAddr {
	match connect_addr {
		SocketAddr::V4(_) => SocketAddr::new(Ipv4Addr::LOCALHOST.into(), 0),
		SocketAddr::V6(_) => SocketAddr::new(Ipv6Addr::LOCALHOST.into(), 0)
	}
}


pub trait FilterAddrs {
	fn filter_addrs_for_bind(self, mode: BindMode) -> Vec<SocketAddr>;
	fn filter_addrs_for_connect(self, mode: ConnectMode) -> Vec<SocketAddr>;
}

impl FilterAddrs for &Vec<SocketAddr> {

	fn filter_addrs_for_bind(self, mode: BindMode) -> Vec<SocketAddr> {
		self.iter()
			.filter_map(|addr| {
				match mode {
					BindMode::Both => Some(addr.clone()),
					BindMode::OnlyIpv4 =>
						if addr.is_ipv4() {
							Some(addr.clone())
						} else {
							None
						},
					BindMode::OnlyIpv6 =>
						if addr.is_ipv6() {
							Some(addr.clone())
						} else {
							None
						}
				}
			})
			.collect()
	}

	fn filter_addrs_for_connect(self, mode: ConnectMode) -> Vec<SocketAddr> {
		let (addrs_v4, addrs_v6) = self
			.iter()
			.partition::<Vec<_>,_>(|addr| addr.is_ipv4());
		match mode {
			ConnectMode::PreferIpv6 => [addrs_v6, addrs_v4].concat(),
			ConnectMode::PreferIpv4 => [addrs_v4, addrs_v6].concat(),
			ConnectMode::OnlyIpv6 => addrs_v6,
			ConnectMode::OnlyIpv4 => addrs_v4
		}
	}
}


#[derive(Error, Debug, Clone)]
#[error("No compatible address was found for mode {mode} among [{}]", .addrs.join_to_string(", ", SocketAddr::to_string))]
pub struct NoCompatibleConnectAddr {
	mode: ConnectMode,
	addrs: Vec<SocketAddr>
}

impl NoCompatibleConnectAddr {

	pub fn new(mode: ConnectMode, addrs: Vec<SocketAddr>) -> Self {
		Self {
			mode,
			addrs
		}
	}
}


pub(crate) fn is_close_like_error(e: &std::io::Error) -> bool {
	match e.kind() {
		ErrorKind::UnexpectedEof => true,
		ErrorKind::NotConnected => true,
		ErrorKind::ConnectionReset => true,
		ErrorKind::BrokenPipe => true,
		_ => false
	}
}


#[cfg(test)]
pub(crate) mod test {

	use galvanic_assert::{assert_that, matchers::*};

	use smolweb_test_tools::is_match;
	use smolweb_test_tools::logging::init_test_logging;

	use super::*;


	#[test]
	fn from_str() {
		let _logging = init_test_logging();

		assert_that!(&ConfigAddr::from_str(""), is_match!(Err(..)));
		assert_that!(&ConfigAddr::from_str(":"), is_match!(Err(..)));
		assert_that!(&ConfigAddr::from_str("foo:").unwrap(), eq(ConfigAddr {
			host: "foo".to_string(),
			port: None
		}));
		assert_that!(&ConfigAddr::from_str("foo:bar").unwrap(), eq(ConfigAddr {
			host: "foo".to_string(),
			port: None
		}));
		assert_that!(&ConfigAddr::from_str("foo:5").unwrap(), eq(ConfigAddr {
			host: "foo".to_string(),
			port: Some(5)
		}));
		assert_that!(&ConfigAddr::from_str("10.0.0.1").unwrap(), eq(ConfigAddr {
			host: "10.0.0.1".to_string(),
			port: None
		}));
		assert_that!(&ConfigAddr::from_str("10.0.0.1:5").unwrap(), eq(ConfigAddr {
			host: "10.0.0.1".to_string(),
			port: Some(5)
		}));
		assert_that!(&ConfigAddr::from_str("[::]").unwrap(), eq(ConfigAddr {
			host: "::".to_string(),
			port: None
		}));
		assert_that!(&ConfigAddr::from_str("[::]:5").unwrap(), eq(ConfigAddr {
			host: "::".to_string(),
			port: Some(5)
		}));
		assert_that!(&ConfigAddr::from_str("[cc::cc]").unwrap(), eq(ConfigAddr {
			host: "cc::cc".to_string(),
			port: None
		}));
		assert_that!(&ConfigAddr::from_str("[cc::cc]:5").unwrap(), eq(ConfigAddr {
			host: "cc::cc".to_string(),
			port: Some(5)
		}));
	}
}
