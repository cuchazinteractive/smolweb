
use std::fmt;

use axum::async_trait;
use bytes::Bytes;
use display_error_chain::ErrorChainExt;
use hyper::StatusCode;
use hyper::header::CONTENT_TYPE;
use thiserror::Error;
use tracing::error;


pub const CONTENT_TYPE_PROTO: &'static str = "application/protobuf";


#[async_trait]
pub trait ResponseTools {
	fn content_type(&self) -> Option<String>;
	async fn into_success(self) -> Result<(),ResponseError>;
	async fn into_proto(self) -> Result<Bytes,ResponseProtobufError>;
}

#[async_trait]
impl ResponseTools for reqwest::Response {

	fn content_type(&self) -> Option<String> {
		self.headers().get(CONTENT_TYPE)
			.map(|val| String::from_utf8_lossy(val.as_bytes()).to_string())
	}

	async fn into_success(self) -> Result<(),ResponseError> {
		if self.status() == StatusCode::OK {
			Ok(())
		} else {
			let e = ResponseError::from(self)
				.await;
			Err(e)
		}
	}

	async fn into_proto(self) -> Result<Bytes,ResponseProtobufError> {

		// handle unsuccessful responses
		let status = self.status();
		if status != StatusCode::OK {
			let e = ResponseError::from(self)
				.await;
			return Err(ResponseProtobufError::NotOk(e));
		}

		// check for a content type
		let Some(content_type) = self.content_type()
			else { return Err(ResponseProtobufError::NoContentType); };

		// some content types look like:
		// text/plain; charset=utf-8
		// just check the first part
		let content_type_first = match content_type.split_once(';') {
			Some((first, _)) => first,
			None => content_type.as_str()
		};
		if content_type_first != CONTENT_TYPE_PROTO {
			return Err(ResponseProtobufError::InvalidContentType(content_type));
		}

		// read the payload
		let bytes = self.bytes()
			.await
			.map_err(|e| ResponseProtobufError::Body(e))?;

		Ok(bytes)
	}
}


#[derive(Error, Debug)]
pub struct ResponseError {
	pub status: StatusCode,
	pub body: Result<String,reqwest::Error>
}

impl ResponseError {

	async fn from(response: reqwest::Response) -> ResponseError {
		ResponseError {
			status: response.status(),
			// try to get something useful from the body
			body: response.bytes()
				.await
				.map(|bytes| String::from_utf8_lossy(bytes.as_ref()).to_string()),
		}
	}
}

impl fmt::Display for ResponseError {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self.body {
			Ok(body) => write!(f, "ResponseError[status={}, body={}]", self.status, body),
			Err(e) => write!(f, "ResponseError[status={}, body failed: {}]", self.status, e.into_chain())
		}
	}
}


#[derive(Error, Debug)]
pub enum ResponseProtobufError {

	#[error("HTTP request failed with status code")]
	NotOk(#[from] ResponseError),

	#[error("HTTP response did not contain a content type")]
	NoContentType,

	#[error("HTTP response has wrong content type: {0}")]
	InvalidContentType(String),

	#[error("Failed to read body")]
	Body(reqwest::Error),
}
