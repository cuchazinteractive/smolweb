
use bytes::{Bytes, BytesMut};
use futures::{SinkExt, StreamExt};
use thiserror::Error;
use tokio::io::{AsyncRead, AsyncWrite};
use tokio_util::codec::{Framed, LengthDelimitedCodec};

use crate::net::is_close_like_error;


pub struct FramedProtos<IO> {
	framed: Framed<IO,LengthDelimitedCodec>
}


impl<IO> FramedProtos<IO>
	where
		IO: AsyncRead + AsyncWrite + Unpin
{
	pub fn from(io: IO) -> Self {
		let codec = LengthDelimitedCodec::builder()
			.length_field_offset(0)
			.length_field_type::<u32>()
			.big_endian()
			.new_codec();
		let framed = Framed::new(io, codec);
		Self {
			framed
		}
	}

	pub async fn recv(&mut self) -> Result<BytesMut,RecvError> {
		match self.framed.next().await {
			Some(Ok(f)) => Ok(f),
			// translate close-like OS errors into Closed errors, so callers only have to worry about one of them
			Some(Err(e)) if is_close_like_error(&e) => Err(RecvError::Closed)?,
			Some(Err(e)) => Err(e)?,
			None => Err(RecvError::Closed)?
		}
	}

	pub async fn send(&mut self, msg: impl Into<Bytes>) -> Result<(),SendError> {
		self.framed.feed(msg.into())
			.await?;
		self.framed.flush()
			.await?;
		Ok(())
	}
}


#[derive(Error, Debug)]
pub enum RecvError {

	#[error("No message was received, the underlying transport was closed")]
	Closed,

	#[error("Failed to read the message from the underlying transport")]
	IO(#[from] std::io::Error)
}


#[derive(Error, Debug)]
pub enum SendError {

	#[error("Failed to write the message to the underlying transport")]
	IO(#[from] std::io::Error)
}
