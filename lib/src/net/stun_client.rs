
use std::fmt;
use std::mem::size_of;
use std::net::SocketAddr;
use std::ops::Deref;
use std::sync::Arc;

use anyhow::{bail, Context, Result};
use bytecodec::{DecodeExt, EncodeExt};
use display_error_chain::ErrorChainExt;
use stun_codec::{Message, MessageClass, MessageDecoder, MessageEncoder, rfc5389, TransactionId};
use stun_codec::rfc5389::Attribute;
use tokio::net::UdpSocket;
use tracing::{info, warn};


use crate::lang::JoinToString;
use crate::net::{BindMode, ConfigAddr, IpType};
use crate::net::dns::LookupAddrs;
use crate::net::listeners::UdpListeners;


const STUN_PORT: u16 = 3478;


pub struct StunClient {
	listeners: UdpListeners,
	requests: Vec<BindingRequest>,
	responses: Vec<BindingResponse>
}

impl StunClient {

	pub async fn bind(local_addrs: &Vec<ConfigAddr>, bind_mode: BindMode) -> Result<Self> {

		// bind to the local addresses
		let local_addrs =
			if local_addrs.is_empty() {
				ConfigAddr::all_interfaces()
			} else {
				local_addrs.clone()
			};
		let listeners = UdpListeners::bind_all(&local_addrs, 0, bind_mode, 4096)
			.await
			.context(format!("Failed to bind UDP sockets: {}", local_addrs.join_to_string(", ", |a| a.to_string())))?;
		for addr in listeners.addrs() {
			info!(%addr, "Listening on UDP socket");
		}

		Ok(Self {
			listeners,
			requests: Vec::new(),
			responses: Vec::new()
		})
	}

	pub fn requests(&self) -> &Vec<BindingRequest> {
		&self.requests
	}

	pub fn responses(&self) -> &Vec<BindingResponse> {
		&self.responses
	}

	pub async fn send_binding_requests(&mut self, remote_addrs: Vec<ConfigAddr>) -> Result<()> {

		// resolve the remote addresses
		let remote_socket_addrs = remote_addrs.lookup_addrs(STUN_PORT)
			.await
			.context("Failed to lookup remote addresses")?;

		// send out binding requests
		let mut requests = Vec::<BindingRequest>::new();
		for socket in self.listeners.sockets() {

			// get the remote addresses that match this socket type
			let ip_type = IpType::from_ip_addr(&socket.local_addr().ip());
			let matching_remote_addrs = remote_socket_addrs.iter()
				.filter(|addr| IpType::from_ip_addr(&addr.ip()) == ip_type)
				.collect::<Vec<_>>();
			for remote_addr in matching_remote_addrs {

				// pick a unique transaction id
				let transaction_id = {
					let mut buf = [0u8; 12];
					let index = requests.len() as u64 + 1;
					buf[0..size_of::<u64>()].copy_from_slice(index.to_be_bytes().as_slice());
					TransactionId::new(buf)
				};

				// build the STUN binding request
				let message = Message::<Attribute>::new(
					MessageClass::Request,
					rfc5389::methods::BINDING,
					transaction_id.clone()
				);

				let request = BindingRequest {
					local_addr: socket.local_addr(),
					remote_addr: remote_addr.clone(),
					transaction_id
				};
				requests.push(request.clone());

				// encode the message
				let buf = MessageEncoder::new()
					.encode_into_bytes(message)
					.context("Failed to encode STUN binding request")?;

				// send it
				socket.socket().send_to(buf.as_ref(), &remote_addr)
					.await
					.context("Failed to send STUN binding request")?;
			}
		}

		Ok(())
	}

	pub async fn next_response(&mut self) -> Option<(&BindingRequest,&BindingResponse)> {

		// wait for binding responses
		while self.responses.len() < self.requests.len() {

			let result = self.listeners.recv_from()
				.await;
			match result {

				None => {
					warn!("No sockets listening");
					break;
				}

				Some(Err(e)) => warn!(err = %e.into_chain(), "Failed to receive datagram"),

				Some(Ok((datagram, remote_addr, socket))) => {

					// try to decode the response
					let response = match read_response(&datagram, remote_addr, socket) {
						Err(e) => {
							warn!(err = %e.deref().chain(), "Failed to read STUN response (maybe datagram isn't a STUN response");
							continue
						}
						Ok(r) => r
					};

					// look for the request
					let Some(request) = self.requests.iter()
						.find(|request| request.transaction_id == response.transaction_id)
						else {
							warn!("No matching request, ignoring");
							continue;
						};

					self.responses.push(response);
					let response = self.responses.last()
						.unwrap(); // PANIC SAFETY: we just pushed it

					return Some((request, response));
				}
			}
		}

		None
	}
}



#[derive(Debug, Clone)]
pub struct BindingRequest {
	pub transaction_id: TransactionId,
	pub local_addr: SocketAddr,
	pub remote_addr: SocketAddr
}

impl fmt::Display for BindingRequest {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let id = self.transaction_id.as_bytes()
			.iter()
			.map(|b| format!("{:02x?}", b))
			.collect::<Vec<_>>()
			.join("");
		write!(f, "BindingRequest[\n\t  Id: {}\n\tFrom: {}\n\t  To: {}", id, self.local_addr, self.remote_addr)
	}
}


#[derive(Debug, Clone)]
pub struct BindingResponse {
	pub transaction_id: TransactionId,
	pub local_addr: SocketAddr,
	pub remote_addr: SocketAddr,
	pub response_addr: SocketAddr
}

impl fmt::Display for BindingResponse {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let id = self.transaction_id.as_bytes()
			.iter()
			.map(|b| format!("{:02x?}", b))
			.collect::<Vec<_>>()
			.join("");
		write!(f, "BindingResponse[\n\t  Id: {}\n\tFrom: {}\n\t  To: {}\n\tAddr: {}", id, self.remote_addr, self.local_addr, self.response_addr)
	}
}


fn read_response(datagram: &Vec<u8>, remote_addr: SocketAddr, socket: Arc<UdpSocket>) -> Result<BindingResponse> {

	let message = MessageDecoder::<Attribute>::new()
		.decode_from_bytes(datagram.as_slice())
		.context("Failed to decode (1) STUN response")?;
	let message = match message {
		Ok(m) => m,
		Err(_) => bail!("Failed to decode (2) STUN response")
	};

	// check the message type
	if message.class() != MessageClass::SuccessResponse {
		bail!("STUN message is not success response, instead it's {}", message.class());
	}
	if message.method() != rfc5389::methods::BINDING {
		bail!("STUN message is not binding, instead it's {}", message.method());
	}

	let addr = message.get_attribute::<rfc5389::attributes::XorMappedAddress>()
		.context("STUN binding response has no xor mapped address")?;

	Ok(BindingResponse {
		transaction_id: message.transaction_id(),
		local_addr: socket.local_addr()
			.context("socket has no local address")?,
		remote_addr,
		response_addr: addr.address()
	})
}
