
use axum::http::header::CONTENT_TYPE;
use axum::http::{HeaderName, HeaderValue};
use reqwest::Body;
use tracing::debug;

use crate::net::{ConfigAddr, ConnectMode};
use crate::net::messages::CONTENT_TYPE_PROTO;


/// An HTTPs client that can exchange protobuf messages with the server
pub struct HttpsProtoClient {
	addr: ConfigAddr,
	http: reqwest::Client
}


impl HttpsProtoClient {

	pub fn new(
		addr: ConfigAddr,
		connect_mode: ConnectMode,
		mut config: Option<Box<dyn FnOnce(reqwest::ClientBuilder) -> reqwest::ClientBuilder>>
	) -> Result<Self,reqwest::Error> {

		// build the http client
		let http = {

			let local_addr = connect_mode.localhost_ip();
			debug!(%local_addr, "getting local address");

			let mut builder = reqwest::Client::builder()
				.use_rustls_tls()
				.https_only(true)
				.local_address(Some(local_addr));

			if let Some(config) = config.take() {
				builder = config(builder);
			}

			builder.build()?
		};

		Ok(Self {
			addr,
			http
		})
	}

	pub fn url(&self, path: &str) -> String {
		if !path.starts_with('/') {
			panic!("path must be absolute");
		}
		format!("https://{}{}", self.addr.to_string(), path)
	}

	pub async fn get(&self, path: &str) -> Result<reqwest::Response,reqwest::Error> {
		self.get_headers(path, vec![])
			.await
	}

	pub async fn get_headers(&self, path: &str, headers: impl IntoIterator<Item=(HeaderName,HeaderValue)>) -> Result<reqwest::Response,reqwest::Error> {
		let mut builder = self.http
			.get(self.url(path));
		for (name, value) in headers.into_iter() {
			builder = builder.header(name, value);
		}
		builder.send()
			.await
	}

	pub async fn post(&self, path: &str, body: impl Into<Body>) -> Result<reqwest::Response,reqwest::Error> {
		self.post_headers(path, vec![], body)
			.await
	}

	#[allow(unused)]
	pub async fn post_headers(&self, path: &str, headers: impl IntoIterator<Item=(HeaderName,HeaderValue)>, body: impl Into<Body>) -> Result<reqwest::Response,reqwest::Error> {
		let mut builder = self.http
			.post(self.url(path))
			.header(CONTENT_TYPE, CONTENT_TYPE_PROTO);
		for (name, value) in headers.into_iter() {
			builder = builder.header(name, value);
		}
		builder.body(body)
			.send()
			.await
	}
}
