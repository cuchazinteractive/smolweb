use std::collections::HashSet;
use std::net::{IpAddr, SocketAddr};
use std::str::FromStr;
use axum::async_trait;

use thiserror::Error;
use crate::net::ConfigAddr;


#[cfg(not(test))]
pub async fn domain_lookup(domain: impl AsRef<str>) -> anyhow::Result<Vec<IpAddr>,DomainLookupError> {
	let domain = domain.as_ref();
	let domain_socket = format!("{domain}:0");
	let addrs = tokio::net::lookup_host(domain_socket)
		.await
		.map_err(|e| DomainLookupError::Resolve(domain.to_string(), e))?
		.into_iter()
		.map(|addr| addr.ip())
		.collect::<Vec<_>>();
	Ok(addrs)
}


#[cfg(test)]
pub async fn domain_lookup(_domain: impl AsRef<str>) -> anyhow::Result<Vec<IpAddr>, DomainLookupError> {
	// we are all localhost!
	Ok(vec![
		std::net::Ipv4Addr::LOCALHOST.into(),
		std::net::Ipv6Addr::LOCALHOST.into()
	])
}


pub async fn host_lookup(host: impl AsRef<str>) -> anyhow::Result<Vec<IpAddr>, DomainLookupError> {
	let host = host.as_ref();
	match IpAddr::from_str(host) {
		Ok(addr) => Ok(vec![addr]),
		Err(_) => domain_lookup(host)
			.await
	}
}


#[derive(Error, Debug)]
pub enum DomainLookupError {

	#[error("Failed to resolve domain {0}")]
	Resolve(String, #[source] std::io::Error),

	#[error("No addresses found for domain {0}")]
	NoAddresses(String)
}



#[async_trait]
pub trait LookupAddrs {
	async fn lookup_addrs(&self, default_port: u16) -> Result<Vec<SocketAddr>,DomainLookupError>;
}

#[async_trait]
impl LookupAddrs for ConfigAddr {

	async fn lookup_addrs(&self, default_port: u16) -> Result<Vec<SocketAddr>,DomainLookupError> {
		let ips = host_lookup(&self.host)
			.await?;
		let port = self.port
			.unwrap_or(default_port);
		let addrs = ips.into_iter()
			.map(|ip| SocketAddr::new(ip, port))
			.collect::<Vec<_>>();
		Ok(addrs)
	}
}

#[async_trait]
impl LookupAddrs for Vec<ConfigAddr> {

	async fn lookup_addrs(&self, default_port: u16) -> Result<Vec<SocketAddr>,DomainLookupError> {
		let mut out = Vec::<SocketAddr>::new();
		let mut uniques = HashSet::<SocketAddr>::new();
		for config_addr in self {
			let addrs = config_addr.lookup_addrs(default_port)
				.await?;
			for addr in addrs {
				if uniques.insert(addr.clone()) {
					out.push(addr)
				}
			}
		}
		Ok(out)
	}
}


#[cfg(test)]
pub(crate) mod test {

	use std::net::{Ipv4Addr, Ipv6Addr};

	use galvanic_assert::{assert_that, matchers::*};

	use smolweb_test_tools::logging::init_test_logging;

	use super::*;


	#[tokio::test]
	async fn lookup_addrs_ipv4() {
		let _logging = init_test_logging();

		let addrs = ConfigAddr {
			host: "0.0.0.0".to_string(),
			port: None
		}.lookup_addrs(5)
			.await.unwrap();
		assert_that!(&addrs, eq(vec![
			SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(), 5)
		]));
	}


	#[tokio::test]
	async fn lookup_addrs_ipv4_port() {
		let _logging = init_test_logging();

		let addrs = ConfigAddr {
			host: "0.0.0.0".to_string(),
			port: Some(42)
		}.lookup_addrs(5)
			.await.unwrap();
		assert_that!(&addrs, eq(vec![
			SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(), 42)
		]));
	}


	#[tokio::test]
	async fn lookup_addrs_ipv6() {
		let _logging = init_test_logging();

		let addrs = ConfigAddr {
			host: "::".to_string(),
			port: None
		}.lookup_addrs(5)
			.await.unwrap();
		assert_that!(&addrs, eq(vec![
			SocketAddr::new(Ipv6Addr::UNSPECIFIED.into(), 5)
		]));
	}


	#[tokio::test]
	async fn lookup_addrs_ipv6_port() {
		let _logging = init_test_logging();

		let addrs = ConfigAddr {
			host: "::".to_string(),
			port: Some(42)
		}.lookup_addrs(5)
			.await.unwrap();
		assert_that!(&addrs, eq(vec![
			SocketAddr::new(Ipv6Addr::UNSPECIFIED.into(), 42)
		]));
	}


	#[tokio::test]
	async fn lookup_addrs_domain() {
		let _logging = init_test_logging();

		let addrs = ConfigAddr {
			host: "foo.bar".to_string(),
			port: None
		}.lookup_addrs(5)
			.await.unwrap();
		assert_that!(&addrs, eq(vec![
			SocketAddr::new(Ipv4Addr::LOCALHOST.into(), 5),
			SocketAddr::new(Ipv6Addr::LOCALHOST.into(), 5)
		]));
	}


	#[tokio::test]
	async fn lookup_addrs_domain_port() {
		let _logging = init_test_logging();

		let addrs = ConfigAddr {
			host: "foo.bar".to_string(),
			port: Some(42)
		}.lookup_addrs(5)
			.await.unwrap();
		assert_that!(&addrs, eq(vec![
			SocketAddr::new(Ipv4Addr::LOCALHOST.into(), 42),
			SocketAddr::new(Ipv6Addr::LOCALHOST.into(), 42)
		]));
	}
}
