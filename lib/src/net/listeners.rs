
use std::net::SocketAddr;
use std::sync::Arc;
use anyhow::Context;

use futures::stream::FuturesUnordered;
use futures::StreamExt;
use socket2::{Domain, Protocol, Type};
use thiserror::Error;
use tokio::net::{TcpListener, TcpStream, UdpSocket};
use tokio::sync::Mutex;
use tracing::debug;

use crate::lang::JoinToString;
use crate::net::{BindMode, ConfigAddr, FilterAddrs};
use crate::net::dns::{DomainLookupError, LookupAddrs};


pub struct TcpListeners {
	listeners: Vec<TcpListener>,
	addrs: Vec<SocketAddr>
}

impl TcpListeners {

	pub async fn bind_all(addrs: &Vec<ConfigAddr>, default_port: u16, mode: BindMode) -> Result<Self,BindAllError> {

		// get the socket addresses
		let socket_addrs = addrs.lookup_addrs(default_port)
			.await?;
		let filtered_addrs = socket_addrs.filter_addrs_for_bind(mode);
		if filtered_addrs.is_empty() {
			return Err(BindAllError::NoIpAddresses {
				config_addrs: addrs.clone(),
				socket_addrs,
				bind_mode: mode
			});
		}

		// bind all the sockets
		let mut listeners = Vec::<TcpListener>::new();
		for socket_addr in filtered_addrs {

			debug!(addr = %socket_addr, "Binding TCP socket");

			// create the socket for this address
			// NOTE: use socket2 crate, so we can set advanced options before binding
			//   neither the stdlib nor tokio lets us actually configure sockets =(
			let socket = match socket_addr {

				SocketAddr::V4(..) => {
					socket2::Socket::new(Domain::IPV4, Type::STREAM, Some(Protocol::TCP))
						.map_err(|err| BindAllError::CreateSocket { err, kind: "IPv4 TCP" })?
				}

				SocketAddr::V6(..) => {
					let socket = socket2::Socket::new(Domain::IPV6, Type::STREAM, Some(Protocol::TCP))
						.map_err(|err| BindAllError::CreateSocket { err, kind: "IPv6 TCP" })?;
					// for IPv6 sockets, set IPV6_V6ONLY mode, to disable dual-stack processing
					// that way, we can bind an IPv4 and a IPv6 socket to the same port
					socket.set_only_v6(true)
						.map_err(|err| BindAllError::ConfigSocket { err, msg: "set IPv6 only mode" })?;
					socket
				}
			};

			// set reuse_addr, to fix issues with rebinding the socket quickly (eg, after service restart)
			// NOTE: this could cause stale TCP packets to be delivered to the socket,
			//       but our TLS connection handlers should be able to filter those out
			socket.set_reuse_address(true)
				.map_err(|err| BindAllError::ConfigSocket { err, msg: "set reuse address" })?;

			// bind awhile and listen!
			socket.bind(&socket_addr.into())
				.map_err(|err| BindAllError::Bind { err, addr: socket_addr })?;
			socket.listen(4096)
				.map_err(|err| BindAllError::Listen(err))?;

			// convert to a tokio socket (tokio requires nonblocking flag)
			socket.set_nonblocking(true)
				.map_err(|err| BindAllError::ConfigSocket { err, msg: "set nonblocking mode" })?;
			let listener = TcpListener::from_std(socket.into())
				.map_err(|err| BindAllError::ConfigSocket { err, msg: "convert to tokio" })?;

			listeners.push(listener);
		}

		// get the bound addresses
		let bound_addrs = listeners.iter()
			.map(|l| l.local_addr())
			.collect::<std::io::Result<Vec<_>>>()
			.map_err(|e| BindAllError::LocalAddr(e))?;

		Ok(Self {
			listeners,
			addrs: bound_addrs
		})
	}

	pub fn addrs(&self) -> &Vec<SocketAddr> {
		&self.addrs
	}

	pub async fn accept(&self) -> Option<Result<(TcpStream,SocketAddr),std::io::Error>> {
		self.listeners.iter()
			.map(|s| s.accept())
			.collect::<FuturesUnordered<_>>()
			.next()
			.await
	}
}


pub struct UdpListeners {
	sockets: Vec<BufferedUdpSocket>,
	addrs: Vec<SocketAddr>
}

impl UdpListeners {

	pub async fn bind_all(addrs: &Vec<ConfigAddr>, default_port: u16, mode: BindMode, buf_size: usize) -> Result<Self,BindAllError> {

		// get the socket addresses
		let socket_addrs = addrs.lookup_addrs(default_port)
			.await?;
		let filtered_addrs = socket_addrs.filter_addrs_for_bind(mode);
		if filtered_addrs.is_empty() {
			return Err(BindAllError::NoIpAddresses {
				config_addrs: addrs.clone(),
				socket_addrs,
				bind_mode: mode
			});
		}

		// bind all the sockets
		let mut sockets = Vec::<BufferedUdpSocket>::new();
		for socket_addr in filtered_addrs {

			debug!(addr = %socket_addr, "Binding UDP socket");

			// create the socket for this address
			// NOTE: use socket2 crate, so we can set advanced options before binding
			//   neither the stdlib nor tokio lets us actually configure sockets =(
			let socket = match socket_addr {

				SocketAddr::V4(..) => {
					socket2::Socket::new(Domain::IPV4, Type::DGRAM, Some(Protocol::UDP))
						.map_err(|err| BindAllError::CreateSocket { err, kind: "IPv4 UDP" })?
				}

				SocketAddr::V6(..) => {
					let socket = socket2::Socket::new(Domain::IPV6, Type::DGRAM, Some(Protocol::UDP))
						.map_err(|err| BindAllError::CreateSocket { err, kind: "IPv6 UDP" })?;
					// for IPv6 sockets, set IPV6_V6ONLY mode, to disable dual-stack processing
					// that way, we can bind an IPv4 and a IPv6 socket to the same port
					socket.set_only_v6(true)
						.map_err(|err| BindAllError::ConfigSocket { err, msg: "set IPv6 only mode" })?;
					socket
				}
			};

			// bind the socket
			socket.bind(&socket_addr.into())
				.map_err(|err| BindAllError::Bind { err, addr: socket_addr })?;

			// convert to a tokio socket (tokio requires nonblocking flag)
			socket.set_nonblocking(true)
				.map_err(|err| BindAllError::ConfigSocket { err, msg: "set nonblocking mode" })?;
			let socket = UdpSocket::from_std(socket.into())
				.map_err(|err| BindAllError::ConfigSocket { err, msg: "convert to tokio" })?;

			// get the bound address
			let local_addr = socket.local_addr()
				.map_err(|err| BindAllError::LocalAddr(err))?;

			// finally, add the damn buffer
			let buffered_socket = BufferedUdpSocket::from(socket, local_addr, buf_size);
			sockets.push(buffered_socket);
		}

		// get the bound addresses
		let bound_addrs = sockets.iter()
			.map(|s| s.local_addr().clone())
			.collect::<Vec<_>>();

		Ok(Self {
			sockets,
			addrs: bound_addrs
		})
	}

	pub fn addrs(&self) -> &Vec<SocketAddr> {
		&self.addrs
	}

	pub fn sockets(&self) -> &Vec<BufferedUdpSocket> {
		&self.sockets
	}

	pub async fn recv_from(&self) -> Option<Result<(Vec<u8>,SocketAddr,Arc<UdpSocket>),std::io::Error>> {
		self.sockets.iter()
			.map(|s| s.recv_from())
			.collect::<FuturesUnordered<_>>()
			.next()
			.await
	}
}


#[derive(Error, Debug)]
pub enum BindAllError {

	#[error(transparent)]
	Domain(#[from] DomainLookupError),

	#[error("No IP addresses found for [{}] using bind mode {bind_mode} among [{}]",
		.config_addrs.join_to_string(", ", ConfigAddr::to_string),
		.socket_addrs.join_to_string(", ", SocketAddr::to_string)
	)]
	NoIpAddresses {
		config_addrs: Vec<ConfigAddr>,
		socket_addrs: Vec<SocketAddr>,
		bind_mode: BindMode
	},

	#[error("Failed to create {kind} socket")]
	CreateSocket {
		#[source]
		err: std::io::Error,
		kind: &'static str
	},

	#[error("Failed to configure socket: {msg}")]
	ConfigSocket {
		#[source]
		err: std::io::Error,
		msg: &'static str
	},

	#[error("Failed to bind socket to address {addr}")]
	Bind {
		#[source]
		err: std::io::Error,
		addr: SocketAddr
	},

	#[error("Failed to listen on socket")]
	Listen(#[source] std::io::Error),

	#[error("Failed to determine local bound address")]
	LocalAddr(#[source] std::io::Error)
}


pub struct BufferedUdpSocket {
	socket: Arc<UdpSocket>,
	addr: SocketAddr,
	buf: Mutex<Vec<u8>>
}

impl BufferedUdpSocket {

	pub fn from(socket: UdpSocket, addr: SocketAddr, buf_size: usize) -> Self {
		Self {
			socket: Arc::new(socket),
			addr,
			buf: Mutex::new(vec![0u8; buf_size])
		}
	}

	pub fn bind(local_addr: &SocketAddr, buf_size: usize) -> anyhow::Result<Self> {

		// create the socket
		let socket = match local_addr {

			SocketAddr::V4(_) => {
				socket2::Socket::new(Domain::IPV4, Type::DGRAM, Some(Protocol::UDP))
					.context("Failed to create IPv4 socket")?
			}

			SocketAddr::V6(_) => {
				let socket = socket2::Socket::new(Domain::IPV6, Type::DGRAM, Some(Protocol::UDP))
					.context("FAiled to create IPv6 socket")?;
				// for IPv6 sockets, set IPV6_V6ONLY mode, to disable dual-stack processing
				// that way, we can bind an IPv4 and a IPv6 socket to the same port
				socket.set_only_v6(true)
					.context("Failed to set IPv6 only mode")?;
				socket
			}
		};

		// bind it
		socket.bind(&local_addr.clone().into())
			.context("Failed to bind socket")?;

		// convert to a tokio socket (tokio requires nonblocking flag)
		socket.set_nonblocking(true)
			.context("Failed to set socket non-blocking mode")?;
		let socket = UdpSocket::from_std(socket.into())
			.context("Failed to convert socket from std to tokio")?;

		let addr = socket.local_addr()
			.context("Failed to get local address")?;

		Ok(BufferedUdpSocket::from(socket, addr, buf_size))
	}

	pub fn socket(&self) -> &Arc<UdpSocket> {
		&self.socket
	}

	pub fn local_addr(&self) -> SocketAddr {
		self.addr
	}

	pub async fn recv_from(&self) -> Result<(Vec<u8>,SocketAddr,Arc<UdpSocket>),std::io::Error> {

		// wait for the incoming datagram
		let mut buf = self.buf.lock()
			.await;
		let (datagram_size, remote_addr) = self.socket.recv_from(buf.as_mut())
			.await?;

		// read the datagram from the buffer
		let datagram = buf.as_slice()[0..datagram_size].to_vec();

		Ok((datagram, remote_addr, self.socket.clone()))
	}
}



#[cfg(test)]
pub(crate) mod test {

	use smolweb_test_tools::logging::init_test_logging;

	use super::*;


	#[tokio::test]
	async fn listen_ipv4_ipv6_udp() {
		let _logging = init_test_logging();

		let port = 1234;
		let addrs = ConfigAddr::all_interfaces();
		let bind_mode = BindMode::Both;
		let _listeners = UdpListeners::bind_all(&addrs, port, bind_mode, 1024)
			.await
			.expect("Failed to bind addresses");
	}


	#[tokio::test]
	async fn listen_ipv4_ipv6_tcp() {
		let _logging = init_test_logging();

		let port = 1234;
		let addrs = ConfigAddr::all_interfaces();
		let bind_mode = BindMode::Both;
		let _listeners = TcpListeners::bind_all(&addrs, port, bind_mode)
			.await
			.expect("Failed to bind addresses");
	}
}
