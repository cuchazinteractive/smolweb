
use std::fmt;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr};
use std::num::NonZeroU16;
use std::time::Duration;

use anyhow::{bail, Context, Result};
use crab_nat::{InternetProtocol, natpmp, pcp, PortMapping, PortMappingOptions, TimeoutConfig};
use igd_next::aio::Gateway;
use igd_next::aio::tokio::{search_gateway, Tokio};
use igd_next::{PortMappingProtocol, SearchOptions};
use netdev::get_interfaces;
use tracing::debug;

use crate::lang::ErrorReporting;


#[derive(Debug, Clone)]
pub struct GatewayInfo {
	pub default: bool,
	pub route_v4: Option<GatewayRoutev4>,
	pub route_v6: Option<GatewayRoutev6>
}

#[derive(Debug, Clone)]
pub struct GatewayRoutev4 {
	pub iface_ip: Ipv4Addr,
	pub gateway_ip: Ipv4Addr
}

#[derive(Debug, Clone)]
pub struct GatewayRoutev6 {
	pub iface_ip: Ipv6Addr,
	pub gateway_ip: Ipv6Addr
}

#[derive(Debug, Clone)]
pub struct GatewayRoute {
	pub iface_ip: IpAddr,
	pub gateway_ip: IpAddr
}

impl<'a> From<&'a GatewayRoutev4> for GatewayRoute {

	fn from(value: &'a GatewayRoutev4) -> Self {
		Self {
			iface_ip: value.iface_ip.into(),
			gateway_ip: value.gateway_ip.into()
		}
	}
}

impl<'a> From<&'a GatewayRoutev6> for GatewayRoute {

	fn from(value: &'a GatewayRoutev6) -> Self {
		Self {
			iface_ip: value.iface_ip.into(),
			gateway_ip: value.gateway_ip.into()
		}
	}
}


impl GatewayInfo {

	pub fn get_default() -> Option<GatewayInfo> {
		Self::get_all()
			.into_iter()
			.find(|gateway| gateway.default)
	}

	/// Gets the list of gateways and their assocaited interfaces.
	/// Only one gateway will be default.
	pub fn get_all() -> Vec<GatewayInfo> {

		let mut infos = Vec::new();

		for iface in get_interfaces() {

			// just get the first IP address for each interface
			let iface_ip4 = iface.ipv4.iter()
				.map(|net| net.addr)
				.next();
			let iface_ip6 = iface.ipv6.iter()
				.map(|net| net.addr)
				.next();

			if let Some(gateway) = iface.gateway {

				// just get the first IP address for the gateway
				let gateway_ip4 = gateway.ipv4
					.first()
					.map(|a| *a);
				let gateway_ip6 = gateway.ipv6
					.first()
					.map(|a| *a);

				// collect the routes
				let route_v4 =
					if let (Some(iface_ip), Some(gateway_ip)) = (iface_ip4, gateway_ip4) {
						Some(GatewayRoutev4 { iface_ip, gateway_ip })
					} else {
						None
					};
				let route_v6 =
					if let (Some(iface_ip), Some(gateway_ip)) = (iface_ip6, gateway_ip6) {
						Some(GatewayRoutev6 { iface_ip, gateway_ip })
					} else {
						None
					};

				if route_v4.is_some() || route_v6.is_some() {
					infos.push(GatewayInfo {
						default: iface.default,
						route_v4,
						route_v6
					});
				}
			}
		}

		infos
	}

	pub fn has_gateway_ip(&self, ip: IpAddr) -> bool {
		match ip {

			IpAddr::V4(ip) => {
				if let Some(route) = &self.route_v4 {
					if route.gateway_ip == ip {
						return true;
					}
				}
			}

			IpAddr::V6(ip) => {
				if let Some(route) = &self.route_v6 {
					if route.gateway_ip == ip {
						return true;
					}
				}
			}
		}

		// none found
		return false;
	}

	/// return the ipv4 route, if any exists, otherwise, the ipv6 route
	pub fn default_route(&self) -> GatewayRoute {
		if let Some(route) = &self.route_v4 {
			return route.into();
		}
		if let Some(route) = &self.route_v6 {
			return route.into();
		}
		panic!("gateway has no routes, this is a bug");
	}

	async fn map_port_upnpigd(&self, local_port: u16) -> Result<MappedPort> {

		let route = self.default_route();

		let search_options = SearchOptions {
			bind_addr: SocketAddr::new(route.iface_ip, 0),
			.. Default::default()
		};

		debug!("Mapping port via UPnP-IGD ...");

		let gateway = search_gateway(search_options)
			.await
			.context("Failed to find UPnP-IGD gateway")?;

		let external_port = gateway.add_any_port(
			PortMappingProtocol::UDP,
			SocketAddr::new(route.iface_ip, local_port),
			0, // seconds, 0=infinite,
			"smolweb"
		)
			.await
			.context("Failed to map UPnP-IGD port")?;

		debug!("Mapped port via UPnP-IGD: {}", external_port);

		Ok(MappedPort {
			kind: PortMappingKind::UPNPIGD(gateway),
			local_port,
			external_port
		})
	}

	async fn map_port_natpmp(&self, local_port: u16) -> Result<MappedPort> {

		let route = self.default_route();

		let mapping_options = PortMappingOptions {
			timeout_config: Some(TimeoutConfig {
				initial_timeout: Duration::from_millis(400),
				max_retries: 0,
				max_retry_timeout: Some(Duration::from_millis(400)),
			}),
			.. Default::default()
		};

		debug!("Mapping port via NAT-PMP ...");

		let mapping = natpmp::try_port_mapping(
			route.gateway_ip,
			InternetProtocol::Udp,
			NonZeroU16::new(local_port)
				.context(format!("Invalid port number: {}", local_port))?,
			mapping_options
		)
			.await
			.context("Failed to bind port using NAT-PMP")?;
		let external_port = mapping.external_port().get();

		debug!("Mapped port via NAT-PMP: {}", external_port);

		Ok(MappedPort {
			kind: PortMappingKind::NATPMP(mapping),
			local_port,
			external_port
		})
	}

	async fn map_port_pcp(&self, local_port: u16) -> Result<MappedPort> {

		let route = self.default_route();

		let request = pcp::BaseMapRequest {
			gateway: route.gateway_ip,
			client: route.iface_ip,
			protocol: InternetProtocol::Udp,
			internal_port: NonZeroU16::new(local_port)
				.context(format!("Invalid port number: {}", local_port))?,
		};

		let mapping_options = PortMappingOptions {
			timeout_config: Some(TimeoutConfig {
				initial_timeout: Duration::from_millis(400),
				max_retries: 0,
				max_retry_timeout: Some(Duration::from_millis(400)),
			}),
			.. Default::default()
		};

		debug!("Mapping port via PCP ...");

		let mapping = pcp::try_port_mapping(request, None, None, mapping_options)
			.await
			.context("Failed to map port using PCP")?;
		let external_port = mapping.external_port().get();

		debug!("Mapped port via PCP: {}", external_port);

		Ok(MappedPort {
			kind: PortMappingKind::PCP(mapping),
			local_port,
			external_port
		})
	}

	pub async fn map_port(&self, local_port: u16) -> Result<MappedPort> {

		// try all three approaches concurrently
		let (result_upnpigd, result_natpmp, result_pcp) = tokio::join!(
			self.map_port_upnpigd(local_port),
			self.map_port_natpmp(local_port),
			self.map_port_pcp(local_port)
		);

		async fn cleanup(mapped_port: Result<MappedPort>) {
			if let Ok(mapped_port) = mapped_port {
				mapped_port.close()
					.await
					.warn_err()
					.ok();
			}
		}

		// prefer PCP mappings over others
		let e_pcp = match result_pcp {
			Ok(mapped_port) => {
				tokio::join!(
					cleanup(result_natpmp),
					cleanup(result_upnpigd)
				);
				return Ok(mapped_port)
			}
			Err(e) => e
		};

		// then NAT-PMP
		let e_natpmp = match result_natpmp {
			Ok(mapped_port) => {
				cleanup(result_upnpigd).await;
				return Ok(mapped_port);
			}
			Err(e) => e
		};

		// finally UPnP-IGD
		let e_upnpigd = match result_upnpigd {
			Ok(mapped_port) => {
				return Ok(mapped_port);
			}
			Err(e) => e
		};

		// nope, all failed
		Err::<(),anyhow::Error>(e_pcp)
			.context("Failed to map port using PCP")
			.warn_err()
			.ok();
		Err::<(),anyhow::Error>(e_natpmp)
			.context("Failed to map port using NAT-PMP")
			.warn_err()
			.ok();
		Err::<(),anyhow::Error>(e_upnpigd)
			.context("Failed to map port using UPnP-IGD")
			.warn_err()
			.ok();
		bail!("All port mapping options failed");
	}
}


impl fmt::Display for GatewayInfo {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.default_route())
	}
}

impl fmt::Display for GatewayRoutev4 {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{} -> {}", self.iface_ip, self.gateway_ip)
	}
}

impl fmt::Display for GatewayRoutev6 {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{} -> {}", self.iface_ip, self.gateway_ip)
	}
}

impl fmt::Display for GatewayRoute {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{} -> {}", self.iface_ip, self.gateway_ip)
	}
}


pub enum PortMappingKind {
	UPNPIGD(Gateway<Tokio>),
	NATPMP(PortMapping),
	PCP(PortMapping)
}

impl PortMappingKind {

	pub fn name(&self) -> &'static str {
		match self {
			Self::UPNPIGD(..) => "UPnP-IGD",
			Self::NATPMP(..) => "NAT-PMP",
			Self::PCP(..) => "PCP"
		}
	}
}


pub struct MappedPort {
	kind: PortMappingKind,
	local_port: u16,
	external_port: u16
}

impl MappedPort {

	pub fn kind_name(&self) -> &str {
		self.kind.name()
	}

	pub fn local_port(&self) -> u16 {
		self.local_port
	}

	pub fn external_port(&self) -> u16 {
		self.external_port
	}

	pub async fn close(self) -> Result<()> {
		match self.kind {

			PortMappingKind::UPNPIGD(gateway) => {
				debug!("Unapping port via UPnP-IGD ...");
				gateway.remove_port(
					PortMappingProtocol::UDP,
					self.external_port
				)
					.await
					.context("Failed to remove UPnP-IGD port mapping")?;
				debug!("Unmapped port via UPnP-IGD");
			}

			PortMappingKind::NATPMP(mapping) => {
				debug!("Unmapping port via NAT-PMP ...");
				mapping.try_drop()
					.await
					.map_err(|(e, _)| e) // drop the mapping for real this time
					.context("Failed to drop NAT-PMP port mapping")?;
				debug!("Unmapped port via NAT-PMP");
			}

			PortMappingKind::PCP(mapping) => {
				debug!("Unmapping port via PCP ...");
				mapping.try_drop()
					.await
					.map_err(|(e, _)| e) // drop the mapping for real this time
					.context("Failed to drop PCP port mapping")?;
				debug!("Unmapped port via PCP");
			}
		}

		Ok(())
	}
}
