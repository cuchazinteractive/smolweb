
use std::fmt;
use std::net::{IpAddr, SocketAddr};
use std::sync::Arc;
use std::time::Duration;

use anyhow::Context;
use futures::stream::FuturesUnordered;
use futures::StreamExt;
use thiserror::Error;
use tokio::sync::{mpsc, Mutex, oneshot};
use tracing::{debug, trace, warn};
use webrtc::api::APIBuilder;
use webrtc::api::setting_engine::SettingEngine;
use webrtc::data_channel::data_channel_init::RTCDataChannelInit;
use webrtc::ice::candidate::Candidate;
use webrtc::ice::candidate::candidate_base::CandidateBaseConfig;
use webrtc::ice::candidate::candidate_server_reflexive::CandidateServerReflexiveConfig;
use webrtc::ice_transport::ice_candidate::RTCIceCandidate;
use webrtc::ice_transport::ice_candidate_type::RTCIceCandidateType;
use webrtc::ice_transport::ice_gatherer_state::RTCIceGathererState;
use webrtc::peer_connection::certificate::RTCCertificate;
use webrtc::peer_connection::configuration::RTCConfiguration;
use webrtc::peer_connection::peer_connection_state::RTCPeerConnectionState;
use webrtc::peer_connection::RTCPeerConnection;
use webrtc::peer_connection::sdp::sdp_type::RTCSdpType;

use crate::lang::ErrorReporting;
use crate::net::gateways::{GatewayInfo, MappedPort};
use crate::webrtc::channels::RTCDataChannelIO;
use crate::webrtc::SessionDescription;


const DATA_CHANNEL_LABEL: &str = "data";
const DATA_CHANNEL_ID: u16 = 5;


struct Connection {
	inner: Option<ConnectionInner>
}

struct ConnectionInner {
	candidate_options: CandidateOptions,
	connection: RTCPeerConnection,
	channel_open_rx: Option<oneshot::Receiver<()>>,
	io: RTCDataChannelIO,
	mapped_ports: Vec<MappedPort>
}

#[derive(Debug, Clone)]
pub struct CandidateOptions {
	pub local_addrs: Vec<SocketAddr>,
	pub priority: u32,
	pub map_ports: Option<MapPortsOptions>
}

impl Default for CandidateOptions {

	fn default() -> Self {
		Self {
			local_addrs: vec![],
			priority: 0,
			map_ports: None
		}
	}
}

#[derive(Debug, Clone)]
pub struct MapPortsOptions {
	pub gateway_ip: Option<IpAddr>
}

impl MapPortsOptions {

	fn find_gateway(&self) -> Option<GatewayInfo> {
		if let Some(ip) = &self.gateway_ip {
			GatewayInfo::get_all()
				.into_iter()
				.find(|gateway| gateway.has_gateway_ip(*ip))
		} else {
			GatewayInfo::get_default()
		}
	}
}

impl Default for MapPortsOptions {

	fn default() -> Self {
		Self {
			gateway_ip: None
		}
	}
}

pub struct ControllingConnection(Connection);
pub struct ControlledConnection(Connection);


#[derive(Clone, Default)]
pub struct ConnectionConfig {
	pub rtc_config: RTCConfiguration,
	pub rtc_settings: SettingEngine,
	pub candidate_options: CandidateOptions
}


pub struct ConnectionConfigBuilder {
	config: ConnectionConfig
}

impl ConnectionConfigBuilder {

	pub fn new() -> Self {
		Self {
			config: ConnectionConfig::default()
		}
	}

	pub fn certificates(mut self, certs: Vec<RTCCertificate>) -> Self {
		self.config.rtc_config.certificates = certs;
		self
	}

	pub fn ice_credentials(mut self, ufrag: impl Into<String>, pwd: impl Into<String>) -> Self {
		self.config.rtc_settings.set_ice_credentials(ufrag.into(), pwd.into());
		self
	}

	pub fn ice_only_srflx(mut self, v: bool) -> Self {
		self.config.rtc_settings.set_ice_only_srflx(v);
		self
	}

	pub fn ice_timings(mut self, check_interval: Duration, max_binding_requests: u16) -> Self {
		self.config.rtc_settings.set_ice_timings(check_interval, max_binding_requests);
		self
	}

	pub fn ice_failed_timeout(mut self, timeout: Duration) -> Self {
		self.config.rtc_settings.set_ice_timeouts(None, Some(timeout), None);
		self
	}

	pub fn candidate_options(mut self, options: CandidateOptions) -> Self {
		self.config.candidate_options = options;
		self
	}

	pub fn build(self) -> ConnectionConfig {
		self.config
	}
}


impl Connection {

	async fn new(config: ConnectionConfig) -> Result<Self,NewConnectionError> {

		// log events
		let logging_span = tracing::Span::current();
		debug!("creating WebRTC connection, ICE servers: {:?}",
			config.rtc_config.ice_servers.iter()
				.flat_map(|s| s.urls.iter().map(|u| u.as_str()))
				.collect::<Vec<_>>()
		);

		// create the connection
		let api = APIBuilder::new()
			.with_setting_engine(config.rtc_settings)
			.build();
		let connection = api.new_peer_connection(config.rtc_config)
			.await
			.map_err(|e| NewConnectionError::NewPeerConnection(e))?;

		// listen to connection events
		connection.on_peer_connection_state_change(Box::new({
			let logging_span = logging_span.clone();
			move |state: RTCPeerConnectionState| {
				let _span = logging_span.enter();
				debug!("Connection state: {}", state);
				Box::pin(async move {})
			}
		}));

		// create a data channel
		let data_channel_options = RTCDataChannelInit {
			ordered: Some(true),
			negotiated: Some(DATA_CHANNEL_ID),
			.. Default::default()
		};
		let data_channel = connection.create_data_channel(DATA_CHANNEL_LABEL, Some(data_channel_options))
			.await
			.map_err(|e| NewConnectionError::NewDataChannel(e))?;

		// listen to data channel events
		let (channel_open_tx, channel_open_rx) = oneshot::channel::<()>();
		data_channel.on_open(Box::new({
			let logging_span = logging_span.clone();
			move || {
				let _span = logging_span.enter();
				debug!("Data channel open!");
				if let Err(_) = channel_open_tx.send(()) {
					warn!("Failed to send channel open signal");
				}
				Box::pin(async move {})
			}
		}));

		Ok(Self {
			inner: Some(ConnectionInner {
				candidate_options: config.candidate_options,
				connection,
				channel_open_rx: Some(channel_open_rx),
				io: RTCDataChannelIO::from(data_channel),
				mapped_ports: Vec::new()
			})
		})
	}

	fn inner(&self) -> &ConnectionInner {
		self.inner.as_ref()
			.expect("unpossible, right?")
			// only abort(),open() modify inner, but they also move self
	}

	fn inner_mut(&mut self) -> &mut ConnectionInner {
		self.inner.as_mut()
			.expect("unpossible, right?")
			// only abort(),open() modifiy inner, but they also move self
	}

	// NOTE: don't take ownership of self here,
	//       so we can drop the returned future without leaking resources
	async fn wait_for_open(&mut self) {

		let inner = self.inner_mut();

		// if the data channel is not open yet, wait for it
		if let Some(channel_open_rx) = inner.channel_open_rx.as_mut() {
			channel_open_rx
				.await
				.expect("Data channel on_open event handler lost");
				// PANIC SAFETY: awaiting the rx can only fail if the tx was dropped,
				//               which can't happen while self still exists

			// channel open, drop the receiver
			inner.channel_open_rx = None;
		}
	}

	fn open(mut self) -> Result<OpenDataChannel,Self> {

		let inner= self.inner.take()
			.expect("unpossible, right?");
			// only abort() modifies inner, but it also moves self

		// if the data channel is not open yet, we can't convert
		if inner.channel_open_rx.is_some() {
			return Err(self);
		}

		// re-listen to connection events, so we can use a new logging span
		let logging_span = tracing::error_span!("OpenDataChannel");
		inner.connection.on_peer_connection_state_change(Box::new({
			let logging_span = logging_span.clone();
			move |state: RTCPeerConnectionState| {
				let _span = logging_span.enter();
				debug!("Connection state: {}", state);
				Box::pin(async move {})
			}
		}));

		Ok(OpenDataChannel {
			inner: Some(inner)
		})
	}

	pub async fn abort(mut self) {
		if let Some(inner) = self.inner.take() {
			inner.close()
				.await;
		}
	}
}


// warn about improper cleanup
impl Drop for Connection {

	#[tracing::instrument(skip_all, level = 5, name = "Connection")]
	fn drop(&mut self) {
		if let Some(inner) = &self.inner {
			warn!("dropped without being abort()d or open()d, {} port(s) still mapped", inner.mapped_ports.len());
		}
	}
}


impl ConnectionInner {

	async fn gather_ice_candidates(&mut self, desc: SessionDescription) -> Result<SessionDescription,ConnectionCandidateError> {

		// log events
		let logging_span = tracing::Span::current();
		debug!("Gathering ICE candidates ...");

		// track the ICE candidate gathering complete event
		let (gathering_tx, mut gathering_rx) = mpsc::channel::<()>(1);
		self.connection.on_ice_gathering_state_change(Box::new({
			move |state: RTCIceGathererState| {
				let gathering_tx = gathering_tx.clone();
				Box::pin(async move {
					if let RTCIceGathererState::Complete = state {
						gathering_tx.send(())
							.await
							.ok();
					}
				})
			}
		}));

		// collect all the srflx candidates
		let srflx_candidates = Arc::new(Mutex::new(Vec::<RTCIceCandidate>::new()));
		self.connection.on_ice_candidate(Box::new({
			let logging_span = logging_span.clone();
			let srflx_candidates = srflx_candidates.clone();
			move |candidate: Option<RTCIceCandidate>| {
				let logging_span = logging_span.clone();
				let srflx_candidates = srflx_candidates.clone();
				Box::pin(async move {
					if let Some(candidate) = candidate {
						let _span = logging_span.enter();
						debug!(%candidate, "ICE candidate");
						if candidate.typ == RTCIceCandidateType::Srflx {
							srflx_candidates.lock()
								.await
								.push(candidate);
						}
					}
				})
			}
		}));

		self.connection.set_local_description(desc.0)
			.await
			.map_err(|e| ConnectionCandidateError::SetSessionDescription(e))?;

		// set local candidates
		for addr in &self.candidate_options.local_addrs {
			self.connection.add_local_ice_candidate_srflx(self.candidate_options.priority, *addr)
				.await
				.map_err(|e| ConnectionCandidateError::NewSrflxCandidate(e, *addr))?;
		}

		debug!("waiting for ICE candidates to finish gathering ...");

		gathering_rx.recv()
			.await
			.ok_or(ConnectionCandidateError::GatheringAborted)?;

		// reset the event handlers
		self.connection.on_ice_gathering_state_change(Box::new(move |_| {
			Box::pin(async move {})
			// NOTE: this should drop gathering_tx
		}));
		self.connection.on_ice_candidate(Box::new(move |_| {
			Box::pin(async move {})
			// NOTE: this should drop any extra Arcs on the srflx candidate vec
		}));

		debug!("ICE candidates gathered");

		// break apart the current session description so we can edit it
		let mut session_description = SessionDescriptionEditor::from(&self.connection)
			.await
			.map_err(|e| ConnectionCandidateError::EditSessionDescription(e))?;

		// get the collected srflx candidates
		let srflx_candidates = Arc::into_inner(srflx_candidates)
			.ok_or(ConnectionCandidateError::GatheringNotFinished)?
			.into_inner();

		// map ports for srflx candidates, if needed
		if let Some(map_ports_options) = &self.candidate_options.map_ports {

			debug!("Mapping ports for {} ICE candidate(s)", srflx_candidates.len());

			// find the gateway
			match map_ports_options.find_gateway() {
				None => warn!("Can't map ports, no gateway found"),
				Some(gateway) => {

					debug!("Using gateway: {}", gateway);

					// find the candidates in the session description and start the port mapping (concurrently)
					let mut infos = FuturesUnordered::new();
					for candidate in srflx_candidates {
						let info = session_description.find_candidate(candidate)
							.map_err(|e| ConnectionCandidateError::FindCandidate(e))?;
						let mapped_port = gateway.map_port(info.candidate.port);
						infos.push(async move {
							(info, mapped_port.await)
						});
					}

					// process each port mapping as it finishes (in arbitrary order)
					loop {
						let Some((info, mapped_port)) = infos.next()
							.await
							else { break; };

						// wait for the port mapping to finish (or fail)
						let Ok(mapped_port) = mapped_port
							.context(format!("Failed to map port for candidate: {}", info.candidate))
							.warn_err()
							else { continue };

						// overwrite the candidate port in the session description
						session_description.overwrite_port(info, &mapped_port)
							.map_err(|e| ConnectionCandidateError::WriteSessionDescription(e))?;

						// save the mapped ports to keep them alive for the duration of the connection
						self.mapped_ports.push(mapped_port);
					}
				}
			}
		}

		// put the session description back together
		let session_description = session_description.finish()
			.map_err(|e| ConnectionCandidateError::WriteSessionDescription(e))?;

		Ok(session_description)
	}

	async fn close(self) {

		debug!("close()ing connection");

		self.io.channel().close()
			.await
			.context("Failed to close WebRTC data channel")
			.warn_err()
			.ok();

		self.connection.close()
			.await
			.context("Failed to close WebRTC connection")
			.warn_err()
			.ok();

		for mapped_port in self.mapped_ports {
			mapped_port.close()
				.await
				.context("Failed to close port mapping")
				.warn_err()
				.ok();
		}
	}
}


struct SessionDescriptionEditor {
	session_description_type: RTCSdpType,
	session_description: webrtc::sdp::SessionDescription
}

impl SessionDescriptionEditor {

	async fn from(connection: &RTCPeerConnection) -> Result<Self,EditSessionDescriptionError> {

		let session_description = connection.local_description()
			.await
			.ok_or(EditSessionDescriptionError::NoSessionDescription)?;

		let session_description_type = session_description.sdp_type;
		match &session_description_type {
			RTCSdpType::Offer => (),
			RTCSdpType::Answer => (),
			_ => return Err(EditSessionDescriptionError::UnsupportedType(session_description_type))
		};

		let session_description = session_description.unmarshal()
			.map_err(|e| EditSessionDescriptionError::Parse(e))?;

		Ok(Self {
			session_description_type,
			session_description
		})
	}

	fn find_candidate(&self, candidate: RTCIceCandidate) -> Result<CandidateInfo,FindCandidateError> {

		let candidate_str = marshal_candidate(&candidate)
			.map_err(|e| FindCandidateError::Serialize(e))?;

		let media_index = self.session_description
			.media_descriptions
			.iter()
			.position(|media_description| {
				media_description.media_name.media.as_str() == "application"
					&& media_description.media_name.formats == vec!["webrtc-datachannel".to_string()]
			})
			.ok_or(FindCandidateError::NoDataChannel)?;

		let attr_index = self.session_description
			.media_descriptions[media_index]
			.attributes
			.iter()
			.position(|attr| {
				if !attr.is_ice_candidate() {
					return false;
				}
				let Some(attr_val) = attr.value
					.as_ref()
					.map(|s| s.as_str())
					else { return false };
				attr_val == candidate_str.as_str()
			})
			.ok_or_else(|| FindCandidateError::NoCandidateAttribute(candidate.clone()))?;

		Ok(CandidateInfo {
			candidate,
			media_index,
			attr_index
		})
	}

	fn overwrite_port(&mut self, candidate: CandidateInfo, mapped_port: &MappedPort) -> Result<(),webrtc::Error> {

		// re-find the candidate attribute in the session description
		let candidate_attr = self.session_description
			.media_descriptions
			.get_mut(candidate.media_index)
			.expect(&format!("failed to find media at index: {}", candidate.media_index))
			// PANIC SAFETY: Self::find_candidate() guarantees the media_index is correct
			.attributes
			.get_mut(candidate.attr_index)
			.expect(&format!("failed to candidate attribute at index: {}", candidate.attr_index));
			// PANIC SAFETY: Self::find_candidate() guarantees the attr_index is correct

		// update the candidate
		let mut candidate = candidate.candidate;
		candidate.port = mapped_port.external_port();
		candidate.related_port = mapped_port.external_port();
		let cand_str = marshal_candidate(&candidate)?;

		// update the session description
		candidate_attr.value = Some(cand_str);

		Ok(())
	}

	fn finish(self) -> Result<SessionDescription,webrtc::Error> {

		let session_description = self.session_description.marshal();
		let session_description= match self.session_description_type {
			RTCSdpType::Offer => SessionDescription::offer(session_description),
			RTCSdpType::Answer => SessionDescription::answer(session_description),
			_ => panic!("unrecognized session description type: {}", self.session_description_type)
			// PANIC SAFETY: Self::from() guarantees the type is supported
		}?;

		Ok(session_description)
	}
}


struct CandidateInfo {
	candidate: RTCIceCandidate,
	media_index: usize,
	attr_index: usize
}


#[derive(Error, Debug)]
pub enum EditSessionDescriptionError {

	#[error("connection has no session description to edit")]
	NoSessionDescription,

	#[error("Failed to parse session description")]
	Parse(#[source] webrtc::Error),

	#[error("Editing {0} sessions is not supported")]
	UnsupportedType(RTCSdpType),
}


#[derive(Error, Debug)]
pub enum FindCandidateError {

	#[error("Failed to serialize candidate")]
	Serialize(#[source] webrtc::Error),

	#[error("Session description has no data channel media description")]
	NoDataChannel,

	#[error("Failed to find attribute for candidate: {0}")]
	NoCandidateAttribute(RTCIceCandidate),
}


#[derive(Error, Debug)]
pub enum NewConnectionError {

	#[error("Failed to create new WebRTC connection")]
	NewPeerConnection(#[source] webrtc::Error),

	#[error("failed to create new WebRTC data channel")]
	NewDataChannel(#[source] webrtc::Error)
}


#[derive(Error, Debug)]
pub enum ConnectionCandidateError {

	#[error("Failed to set session description")]
	SetSessionDescription(#[source] webrtc::Error),

	#[error("Failed to add new server reflexive ICE candidate with address {1}")]
	NewSrflxCandidate(#[source] webrtc::Error, SocketAddr),

	#[error("ICE candidate gathering was aborted")]
	GatheringAborted,

	#[error("Failed to parse session description")]
	SessionDescriptionParse(#[source] webrtc::Error),

	#[error("ICE candidate gathering was not finished")]
	GatheringNotFinished,

	#[error("Failed to edit session description")]
	EditSessionDescription(#[source] EditSessionDescriptionError),

	#[error("Failed to find candidate info in session description")]
	FindCandidate(#[source] FindCandidateError),

	#[error("Failed to write to session description")]
	WriteSessionDescription(#[source] webrtc::Error)
}


impl ControllingConnection {

	#[tracing::instrument(skip_all, level = 5, name = "ControllingConnection")]
	pub async fn new(config: ConnectionConfig) -> Result<Self,NewConnectionError> {
		let connection = Connection::new(config)
			.await?;
		Ok(Self(connection))
	}

	#[tracing::instrument(skip_all, level = 5, name = "ControllingConnection")]
	pub async fn offer(&mut self) -> Result<SessionDescription,ConnectionError> {

		debug!("Create offer");

		let inner = self.0.inner_mut();

		// gather local ICE candidates
		let offer = inner.connection.create_offer(None)
			.await
			.map_err(|e| ConnectionError::Offer(e))?
			.into();
		let offer = inner.gather_ice_candidates(offer)
			.await?;

		trace!("Offer session description:\n{}", offer.sdp);

		Ok(offer)
	}

	#[tracing::instrument(skip_all, level = 5, name = "ControllingConnection")]
	pub async fn connect(&self, answer: SessionDescription) -> Result<(),ConnectionError> {

		debug!("Accept answer");

		let inner = self.0.inner();

		inner.connection.set_remote_description(answer.0)
			.await
			.map_err(|e| ConnectionError::SetSessionDescription(e))?;

		Ok(())
	}

	pub async fn wait_for_open(&mut self) {
		self.0.wait_for_open()
			.await
	}

	#[tracing::instrument(skip_all, level = 5, name = "ControllingConnection")]
	pub fn open(self) -> Result<OpenDataChannel,Self> {
		self.0.open()
			.map_err(|conn| Self(conn))
	}

	#[tracing::instrument(skip_all, level = 5, name = "ControllingConnection")]
	pub async fn abort(self) {
		self.0.abort()
			.await;
	}
}


impl ControlledConnection {

	#[tracing::instrument(skip_all, level = 5, name = "ControlledConnection")]
	pub async fn new(config: ConnectionConfig) -> Result<Self,NewConnectionError> {
		let connection = Connection::new(config)
			.await?;
		Ok(Self(connection))
	}

	#[tracing::instrument(skip_all, level = 5, name = "ControlledConnection")]
	pub async fn connect(&mut self, offer: SessionDescription) -> Result<SessionDescription,ConnectionError> {

		debug!("Create answer");

		let inner = self.0.inner_mut();

		// answer the offer
		inner.connection.set_remote_description(offer.0)
			.await
			.map_err(|e| ConnectionError::SetSessionDescription(e))?;
		let answer = inner.connection.create_answer(None)
			.await
			.map_err(|e| ConnectionError::Answer(e))?
			.into();

		// gather local ICE candidates
		let answer = inner.gather_ice_candidates(answer)
			.await?;

		trace!("Answer session description:\n{}", answer.0.sdp);

		Ok(answer)
	}

	pub async fn wait_for_open(&mut self) {
		self.0.wait_for_open()
			.await
	}

	#[tracing::instrument(skip_all, level = 5, name = "ControlledConnection")]
	pub fn open(self) -> Result<OpenDataChannel,Self> {
		self.0.open()
			.map_err(|conn| Self(conn))
	}

	#[tracing::instrument(skip_all, level = 5, name = "ControlledConnection")]
	pub async fn abort(self) {
		self.0.abort()
			.await;
	}
}


#[derive(Error, Debug)]
pub enum ConnectionError {

	#[error("Failed to create offer")]
	Offer(#[source] webrtc::Error),

	#[error("Failed to create answer")]
	Answer(#[source] webrtc::Error),

	#[error("Failed to gather ICE candidates")]
	GatherICECandidate(#[from] ConnectionCandidateError),

	#[error("Failed to set session description")]
	SetSessionDescription(#[source] webrtc::Error),
}


fn marshal_candidate(candidate: &RTCIceCandidate) -> Result<String,webrtc::Error> {

	let config = CandidateServerReflexiveConfig {
		base_config: CandidateBaseConfig {
			candidate_id: candidate.stats_id.clone(),
			network: candidate.protocol.to_string(),
			address: candidate.address.clone(),
			port: candidate.port,
			component: candidate.component,
			foundation: candidate.foundation.clone(),
			priority: candidate.priority,
			.. Default::default()
		},
		rel_addr: candidate.related_address.clone(),
		rel_port: candidate.related_port,
	};

	let base = config
		.new_candidate_server_reflexive()?;

	Ok(base.marshal())
}


pub struct OpenDataChannel {
	inner: Option<ConnectionInner>
}

impl OpenDataChannel {

	fn inner(&self) -> &ConnectionInner {
		self.inner
			.as_ref()
			.expect("unpossible, right?")
	}

	pub fn io(&self) -> &RTCDataChannelIO {
		&self.inner().io
	}

	/// Closes the connection, ignoring the contents of the send buffer.
	/// To finish processing the send buffer first, call flush().
	#[tracing::instrument(skip_all, level = 5, name = "OpenDataChannel")]
	pub async fn close(mut self) {

		debug!("close()");

		let inner = self.inner
			.take()
			.expect("unpossible, right?");

		// check the output buffer
		let buffered_bytes = inner.io.channel().buffered_amount()
			.await;
		if buffered_bytes > 0 {
			warn!("Closing OpenDataChannel, but send buffer is not empty: {} bytes", buffered_bytes);
		}

		inner.close()
			.await;
	}
}


// warn about improper cleanup
impl Drop for OpenDataChannel {

	#[tracing::instrument(skip_all, level = 5, name = "OpenDataChannel")]
	fn drop(&mut self) {
		if let Some(inner) = &self.inner {
			warn!("dropped without being close()d, {} port(s) still mapped", inner.mapped_ports.len());
		}
	}
}


impl fmt::Debug for OpenDataChannel {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "OpenDataChannel")
	}
}


pub trait SettingEngineExt {
	/// Keeps trying the connection for the maximum possible time.
	/// With this setting, an external timeout should be used to abort the WebRTC connection attempt.
	fn set_max_ice_timeouts(&mut self);
}

impl SettingEngineExt for SettingEngine {

	fn set_max_ice_timeouts(&mut self) {

		// With u16 holding the retry counter, that gives us 65535 possible STUN requests for each candidate pair.
		// At 200ms between attempts, that's a little over 3 1/2 hours of attempt time.
		// So this can't actually extend the timeout to infinity,
		// but that should be enough time for any reaasonably-set external timeout.
		self.set_ice_timings(Duration::from_millis(200), u16::MAX);
		self.set_ice_timeouts(
			None,
			Some(Duration::from_secs(60*60*4)),
			None
		);
	}
}
