
use std::collections::HashSet;
use std::net::{Ipv4Addr, Ipv6Addr, SocketAddr};
use std::sync::OnceLock;

use thiserror::Error;
use tokio::sync::Mutex;
use webrtc::api::setting_engine::SettingEngine;
use webrtc::peer_connection::configuration::RTCConfiguration;

use crate::webrtc::connection::{ConnectionError, NewConnectionError};
use crate::webrtc::{CandidateOptions, ConnectionConfig, ControlledConnection, ControllingConnection, OpenDataChannel};


/// Creates a WebRTC connection between two local endpoints, without involving any Beacon servers
pub async fn connect_local() -> Result<(OpenDataChannel,OpenDataChannel),LocalChannelConnectionError> {

	let config = ConnectionConfig {
		rtc_config: RTCConfiguration::default(),
		rtc_settings: SettingEngine::default(),
		candidate_options: CandidateOptions::default()
	};

	// Explicitly use the localhost addresses for these connections,
	// since for, some reason, the webrtc crate doesn't pick that address if the machine is offline.
	// Also, we have to pick explicit ports here (0 won't work),
	// so choose random ports and hope for the best.
	let mut config_controlling = config.clone();
	let port_controlling_1 = random_port()
		.await?;
	let port_controlling_2 = random_port()
		.await?;
	config_controlling.candidate_options.local_addrs = vec![
		SocketAddr::new(Ipv4Addr::LOCALHOST.into(), port_controlling_1).into(),
		SocketAddr::new(Ipv6Addr::LOCALHOST.into(), port_controlling_2).into()
	];

	let mut config_controlled = config;
	let port_controlled_1 = random_port()
		.await?;
	let port_controlled_2 = random_port()
		.await?;
	config_controlled.candidate_options.local_addrs = vec![
		SocketAddr::new(Ipv4Addr::LOCALHOST.into(), port_controlled_1).into(),
		SocketAddr::new(Ipv6Addr::LOCALHOST.into(), port_controlled_2).into()
	];

	// create the connections
	let mut connection_controlling = ControllingConnection::new(config_controlling)
		.await?;
	let mut connection_controlled = ControlledConnection::new(config_controlled)
		.await?;

	// connect!
	let offer = connection_controlling.offer()
		.await?;
	let answer = connection_controlled.connect(offer)
		.await?;
	connection_controlling.connect(answer)
		.await?;

	connection_controlling.wait_for_open()
		.await;
	connection_controlled.wait_for_open()
		.await;
	let channel_controlling = connection_controlling.open()
		.map_err(|_| LocalChannelConnectionError::Open)?;
	let channel_controlled = connection_controlled.open()
		.map_err(|_| LocalChannelConnectionError::Open)?;

	Ok((channel_controlling, channel_controlled))
}


static USED_PORTS: OnceLock<Mutex<HashSet<u16>>> = OnceLock::new();


async fn random_port() -> Result<u16,LocalChannelConnectionError> {
	for _ in 0 .. 1000 {

		// first, pick a random 16 bit unsigned integer
		let port = rand::random::<u16>();

		// if we got a privileged port, then we're just unucky, so try again
		if port <= 1024 {
			continue;
		}

		// make sure we haven't used this port before
		let mutex = USED_PORTS.get_or_init(|| {
			Mutex::new(HashSet::new())
		});
		let mut used_ports = mutex.lock()
			.await;
		if used_ports.contains(&port) {
			continue;
		}
		used_ports.insert(port);

		// should be a valid, unique port
		return Ok(port);
	}

	return Err(LocalChannelConnectionError::NoPort)
}


#[derive(Debug, Error)]
pub enum LocalChannelConnectionError {

	#[error("Failed to find a unique port to use")]
	NoPort,

	#[error(transparent)]
	New(#[from] NewConnectionError),

	#[error(transparent)]
	Connect(#[from] ConnectionError),

	#[error("Failed to open the connection")]
	Open
}


#[cfg(test)]
mod test {
	use std::time::Duration;
	use galvanic_assert::{assert_that, matchers::*};

	use smolweb_test_tools::logging::init_test_logging;

	use crate::protobuf::framed::{Reader, Writer};

	use super::*;


	#[tokio::test]
	async fn test() {
		let _logging = init_test_logging();

		let (a, b) = connect_local()
			.await.unwrap();

		let buf = vec![1u8, 2, 3];
		a.io().write(buf.clone())
			.await.unwrap();
		a.io().flush(Duration::from_millis(500))
			.await.unwrap();

		let buf2 = b.io().read()
			.await.unwrap();

		assert_that!(&&buf2, eq(&buf));

		a.close().await;
		b.close().await;
	}


	#[tokio::test]
	async fn test_many_connections() {
		let _logging = init_test_logging();

		// make a whole bunch of connections concurrently,
		// since cargo test tries to run tests in parallel
		// and each test may use its own connection
		let futures = (0 .. 10)
			.map(|_| connect_local())
			.collect::<Vec<_>>();

		for future in futures {
			let (a, b) = future
				.await.unwrap();
			a.close()
				.await;
			b.close()
				.await;
		}
	}
}
