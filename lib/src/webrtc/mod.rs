
use std::ops::Deref;

use webrtc::peer_connection::sdp::session_description::RTCSessionDescription;


pub(crate) mod channels;
pub(crate) mod connection;
pub(crate) mod connection_race;
pub(crate) mod local;


pub use connection::{
	ConnectionConfig, ConnectionConfigBuilder,
	CandidateOptions, MapPortsOptions,
	ControlledConnection, ControllingConnection,
	OpenDataChannel
};
pub use connection_race::{
	ConnectionRace
};
pub use channels::{
	RTCDataChannelIO, DataChannelReaderError
};
pub use local::{
	connect_local, LocalChannelConnectionError
};


/// because RTCSessionDescription doesn't implement PartialEq,Eq
#[derive(Debug, Clone)]
pub struct SessionDescription(RTCSessionDescription);

impl SessionDescription {

	pub fn offer(sdp: impl Into<String>) -> Result<SessionDescription,webrtc::Error> {
		let sd = RTCSessionDescription::offer(sdp.into())?;
		Ok(Self(sd))
	}

	pub fn answer(sdp: impl Into<String>) -> Result<SessionDescription,webrtc::Error> {
		let sd =RTCSessionDescription::answer(sdp.into())?;
		Ok(Self(sd))
	}

	pub fn as_str(&self) -> &str {
		self.0.sdp.as_str()
	}

	pub fn into_string(self) -> String {
		self.0.sdp
	}
}

impl Deref for SessionDescription {

	type Target = RTCSessionDescription;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl PartialEq for SessionDescription {

	fn eq(&self, other: &Self) -> bool {
		self.0.sdp == other.0.sdp
	}
}

impl Eq for SessionDescription {}

impl From<RTCSessionDescription> for SessionDescription {

	fn from(value: RTCSessionDescription) -> Self {
		SessionDescription(value)
	}
}

impl From<SessionDescription> for RTCSessionDescription {

	fn from(value: SessionDescription) -> Self {
		value.0
	}
}
