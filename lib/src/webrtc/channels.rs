
use std::ops::Deref;
use std::sync::Arc;
use std::time::{Duration, Instant};

use axum::async_trait;
use bytes::Bytes;
use thiserror::Error;
use tokio::sync::{mpsc, Mutex};
use tracing::trace;
use webrtc::data_channel::RTCDataChannel;

use crate::protobuf::framed::{Writer, Reader, FrameWriter, FrameReader, FrameReceiver};


#[async_trait]
impl Writer for RTCDataChannel {

	type Error = webrtc::Error;

	async fn write(&self, data: impl Into<Vec<u8>> + Send) -> Result<(),Self::Error> {
		let buf = Bytes::from(data.into());
		self.send(&buf)
			.await?;
		Ok(())
	}
}

#[async_trait]
impl Writer for Arc<RTCDataChannel> {

	type Error = <RTCDataChannel as Writer>::Error;

	async fn write(&self, data: impl Into<Vec<u8>> + Send) -> Result<(),Self::Error> {
		self.deref().write(data)
			.await
	}
}


#[derive(Clone)]
pub struct RTCDataChannelIO {
	channel: Arc<RTCDataChannel>,
	msg_rx: Option<Arc<Mutex<mpsc::Receiver<Bytes>>>>
}

impl RTCDataChannelIO {

	pub fn from(channel: Arc<RTCDataChannel>) -> Self {

		let (msg_tx, msg_rx) = mpsc::channel(1);

		// wire up the message channel to the frame read buffer
		// WARNING: this will replace any existing message handler,
		//          and there's no way to tell if we clobbered an old one or not =(
		channel.on_message(Box::new(move |msg| {
			let msg_tx = msg_tx.clone();
			Box::pin(async move {

				// send the message to the receiver
				msg_tx.send(msg.data)
					.await.ok();
			})
		}));

		channel.on_close(Box::new({
			let channel = channel.clone();
			move || {

				// remove the message handler when the channel closes
				// so the message transmitter drops
				Self::remove_message_handler(&channel);

				Box::pin(async move {})
			}
		}));

		Self {
			channel,
			msg_rx: Some(Arc::new(Mutex::new(msg_rx)))
		}
	}

	fn remove_message_handler(channel: &RTCDataChannel) {
		// replace the message listener with an empty one, to remove the old listener
		// since there doesn't seem to be a way to remove a listener entirely
		// the underlying Option inside the RTCDataChannel supports that, but it's not exposed in the API
		// NOTE: this should drop the message transmitter which should send a closed error to the reader
		channel.on_message(Box::new(|_| {
			Box::pin(async {})
		}));
	}

	pub fn channel(&self) -> &Arc<RTCDataChannel> {
		&self.channel
	}

	pub fn frame_writer(&self) -> FrameWriter<RTCDataChannelIO> {
		FrameWriter::from(self.clone())
	}

	pub fn frame_reader(&self) -> FrameReader<RTCDataChannelIO> {
		FrameReader::from(self.clone())
	}

	pub fn frame_receiver(&self) -> FrameReceiver<RTCDataChannelIO,RTCDataChannelIO> {
		FrameReceiver::from(self.frame_writer(), self.frame_reader())
	}

	#[tracing::instrument(skip_all, level = 5, name = "RTCDataChannelIO")]
	pub async fn flush(&self, timeout: Duration) -> Result<(),()> {

		let start = Instant::now();
		while Instant::now().duration_since(start) < timeout {

			// if the buffer is empty, we're done here
			let buffered_bytes = self.channel.buffered_amount()
				.await;
			trace!("Flushing send buffer: {} bytes", buffered_bytes);
			if buffered_bytes == 0 {
				return Ok(());
			}

			// wait a bit before trying again
			tokio::time::sleep(Duration::from_millis(100))
				.await;
		}

		trace!("Flushing send buffer: timeout after {:?}", timeout);
		Err(())
	}
}


impl Drop for RTCDataChannelIO {

	fn drop(&mut self) {
		// if this is the last ref, cleanup the message handler
		if let Some(rx) = self.msg_rx.take() {
			if let Some(_) = Arc::into_inner(rx) {
				Self::remove_message_handler(&self.channel);
			}
		}
	}
}


#[async_trait]
impl Writer for RTCDataChannelIO {

	type Error = <RTCDataChannel as Writer>::Error;

	async fn write(&self, data: impl Into<Vec<u8>> + Send) -> Result<(),Self::Error> {
		self.channel.write(data)
			.await
	}
}


#[async_trait]
impl Reader for RTCDataChannelIO {

	type Error = DataChannelReaderError;

	async fn read(&self) -> Result<Vec<u8>,Self::Error> {
		let mut rx = self.msg_rx.as_ref()
			.unwrap() // PANIC SAFETY: only taken on drop, so always Some
			.lock()
			.await;
		let buf = rx.recv()
			.await
			.ok_or(DataChannelReaderError::Closed)?;
		Ok(Vec::from(buf))
	}
}


impl From<RTCDataChannelIO> for FrameReader<RTCDataChannelIO> {

	fn from(io: RTCDataChannelIO) -> Self {
		io.frame_reader()
	}
}


impl From<RTCDataChannelIO> for FrameWriter<RTCDataChannelIO> {

	fn from(io: RTCDataChannelIO) -> Self {
		io.frame_writer()
	}
}


impl From<RTCDataChannelIO> for FrameReceiver<RTCDataChannelIO,RTCDataChannelIO> {

	fn from(io: RTCDataChannelIO) -> Self {
		io.frame_receiver()
	}
}


#[derive(Error, Debug)]
pub enum DataChannelReaderError {

	#[error("The channel has closed, no more frames can be read")]
	Closed
}


#[cfg(test)]
mod test {

	use std::time::Duration;

	use galvanic_assert::{assert_that, matchers::*};
	use tokio::time::timeout;

	use smolweb_test_tools::is_match;
	use smolweb_test_tools::logging::init_test_logging;

	use super::*;


	#[tokio::test]
	async fn write_read() {
		let _logging = init_test_logging();

		let (guest, host) = crate::webrtc::connect_local()
			.await.unwrap();

		let msg = vec![1, 2, 3];
		guest.io().write(msg.clone())
			.await.unwrap();

		let msg2 = host.io().read()
			.await.unwrap();

		assert_that!(&&msg2, eq(&msg));
	}

	#[tokio::test]
	async fn read_closed() {
		let _logging = init_test_logging();

		let (guest, host) = crate::webrtc::connect_local()
			.await.unwrap();

		guest.close()
			.await;

		// try to read from the channel, with a timmeout in case the closing doesn't work
		let result = host.io().read();
		let result = timeout(Duration::from_millis(200), result)
			.await.unwrap();

		assert_that!(&result, is_match!(Err(DataChannelReaderError::Closed)));
	}
}
