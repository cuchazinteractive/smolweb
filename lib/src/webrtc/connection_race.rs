
use std::time::Duration;

use futures::{FutureExt, pin_mut};
use tracing::debug;

use crate::webrtc::connection::{ConnectionConfig, ConnectionError, ControlledConnection, ControllingConnection, NewConnectionError, OpenDataChannel};
use crate::webrtc::SessionDescription;


pub struct ConnectionRace {
	conn_controlling: ControllingConnection,
	conn_controlled: ControlledConnection
}

pub struct GuestConnectionRace(ConnectionRace);
pub struct HostConnectionRace(ConnectionRace);

impl ConnectionRace {

	pub async fn new(
		controlling_config: ConnectionConfig,
		controlled_config: ConnectionConfig
	) -> Result<Self,NewConnectionError> {

		// make the connections
		let conn_controlling = ControllingConnection::new(controlling_config)
			.await?;
		let conn_controlled = ControlledConnection::new(controlled_config)
			.await?;

		Ok(Self {
			conn_controlling,
			conn_controlled
		})
	}

	pub fn to_guest(self) -> GuestConnectionRace {
		GuestConnectionRace(self)
	}

	pub fn to_host(self) -> HostConnectionRace {
		HostConnectionRace(self)
	}

	async fn wait_for_open(&mut self) {

		// race the connections until one of them connects
		let attempt_controlling = self.conn_controlling.wait_for_open().fuse();
		let attempt_controlled = self.conn_controlled.wait_for_open().fuse();
		pin_mut!(attempt_controlling, attempt_controlled);
		loop {
			tokio::select! {

				_ = &mut attempt_controlling => {
					return;
				}

				_ = &mut attempt_controlled => {

					// Give the controlling connection a little bit of extra time to connect too.
					// Otherwise, we can run into a weird race condition where both sides have a connected
					// controlled connection (because the controlled side seems to open about one transit
					// time before the controlling side) and then they both drop the other unconnected
					// controlling connection, so neither connection ends up fully connecting.
					// So we should wait about one transit time to see if the controlling
					// connection is going to open too, because we might actually prefer to
					// use the controlling connection if we're a guest.
					// But if the controlling connection actually shows up sooner, stop waiting.
					tokio::select! {
						_ = &mut attempt_controlling => (),
						// TODO: actually measure average (median?) transit time instead of just kludging 500ms in here?
						_ = tokio::time::sleep(Duration::from_millis(500)) => ()
					}

					return;
				}
			}
		}
	}

	pub async fn abort(self) {
		self.conn_controlling.abort()
			.await;
		self.conn_controlled.abort()
			.await;
	}
}

impl GuestConnectionRace {

	pub async fn offer(&mut self) -> Result<SessionDescription,ConnectionError> {

		// make an offer from the controlling connection
		let offer1 = self.0.conn_controlling.offer()
			.await?;

		Ok(offer1)
	}

	pub async fn connect(&mut self, answer1: SessionDescription, offer2: SessionDescription) -> Result<SessionDescription,ConnectionError> {

		// accept the answer on the controlling connection
		self.0.conn_controlling.connect(answer1)
			.await?;

		// make the answer from the controlled connection
		let answer2 = self.0.conn_controlled.connect(offer2)
			.await?;

		Ok(answer2)
	}

	pub async fn wait_for_open(&mut self) {
		self.0.wait_for_open()
			.await
	}

	pub async fn open(self) -> Result<OpenDataChannel,Self> {

		// prefer the opposite connection from the guest host side:
		// if controlling is available, prefer that one
		match self.0.conn_controlling.open() {

			Ok(channel_controlling) => {

				debug!("Controlling connection won the race");

				// cleanup the other connection
				self.0.conn_controlled.abort()
					.await;

				Ok(channel_controlling)
			}

			Err(conn_controlling) => {

				// otherwise, take the controlled one, if available
				match self.0.conn_controlled.open() {

					Ok(channel_controlled) => {

						debug!("Controlled connection won the race");

						// cleanup the other connection
						conn_controlling.abort()
							.await;

						Ok(channel_controlled)
					}

					// neither were connected
					Err(conn_controlled,) =>
						Err(Self(ConnectionRace {
							conn_controlling,
							conn_controlled
						}))
				}
			}
		}
	}

	pub async fn abort(self) {
		self.0.abort()
			.await;
	}
}

impl HostConnectionRace {

	pub async fn connect1(&mut self, offer1: SessionDescription) -> Result<(SessionDescription,SessionDescription),ConnectionError> {

		// make the answer from the controlled connection
		let answer1 = self.0.conn_controlled.connect(offer1)
			.await?;

		// make an offer from the controlling connection
		let offer2 = self.0.conn_controlling.offer()
			.await?;

		Ok((answer1, offer2))
	}

	pub async fn connect2(&self, answer2: SessionDescription) -> Result<(),ConnectionError> {

		// accept the answer on the controlling connection
		self.0.conn_controlling.connect(answer2)
			.await?;

		Ok(())
	}

	pub async fn wait_for_open(&mut self) {
		self.0.wait_for_open()
			.await
	}

	pub async fn open(self) -> Result<OpenDataChannel,Self> {

		// prefer the opposite connection from the guest side:
		// if controlled is available, prefer that one
		match self.0.conn_controlled.open() {

			Ok(channel_controlled) => {

				debug!("Controlled connection won the race");

				// clean up the other connection
				self.0.conn_controlling.abort()
					.await;

				Ok(channel_controlled)
			}

			Err(conn_controlled) => {

				// otherwise, take the controlling one, if available
				match self.0.conn_controlling.open() {

					Ok(channel_controlling) => {

						debug!("Controlling connection won the race");

						// clean up the other connection
						conn_controlled.abort()
							.await;

						Ok(channel_controlling)
					}

					// neither were connected
					Err(conn_controlling) =>
						Err(Self(ConnectionRace {
							conn_controlling,
							conn_controlled
						}))
				}
			}
		}
	}

	pub async fn abort(self) {
		self.0.abort()
			.await;
	}
}
