
#[cfg(test)]
pub(crate) mod test;


use std::io::BufReader;
use std::path::{Path, PathBuf};

use anyhow::{bail, Context, Result};
use rustls_pemfile::{certs, pkcs8_private_keys};
use serde::Deserialize;
use tokio_rustls::rustls::{Certificate, ClientConfig, PrivateKey, RootCertStore};


/// reads a PEM-encoded certificate chain file into a list of DER-encoded certificates
#[allow(unused)]
pub fn read_pem_certs(path: &Path) -> Result<Vec<Certificate>> {
	let pem = std::fs::read_to_string(path)
		.context(format!("Failed to read certificate file: {}", path.display()))?;
	let mut reader = BufReader::new(pem.as_bytes());
	let ders = certs(&mut reader)
		.context(format!("Failed to parse PEM-encoded certificate file: {}", path.display()))?
		.into_iter()
		.map(Certificate)
		.collect::<Vec<_>>();
	if ders.is_empty() {
		bail!("Certificate file contains no cerficates: {}", path.display())
	}
	Ok(ders)
}


/// reads a PEM-encoded certificate file into a DER-encoded certificate
pub fn read_pem_cert(path: &Path) -> Result<Certificate> {
	let pem = std::fs::read_to_string(path)
		.context(format!("Failed to read certificate file: {}", path.display()))?;
	let mut reader = BufReader::new(pem.as_bytes());
	let der = certs(&mut reader)
		.context(format!("Failed to parse PEM-encoded certificate file: {}", path.display()))?
		.into_iter()
		.next()
		.map(Certificate)
		.context(format!("Certificate file contains no cerficates: {}", path.display()))?;
	Ok(der)
}


/// reads a PEM-encoded private key file into a DER-encoded private key
#[allow(unused)]
pub fn read_pem_key(path: &Path) -> Result<PrivateKey> {
	let pem = std::fs::read_to_string(path)
		.context(format!("Failed to read key file: {}", path.display()))?;
	let mut reader = BufReader::new(pem.as_bytes());
	let der = pkcs8_private_keys(&mut reader)
		.context(format!("Failed to parse PEM-encoded key file: {}", path.display()))?
		.into_iter()
		.next()
		.context(format!("Key file contains no keys: {}", path.display()))?;
	Ok(PrivateKey(der))
}


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigTlsClient {
	pub other_roots: Option<Vec<String>>
}

impl ConfigTlsClient {

	pub fn to_args(self) -> Result<ArgsTlsClient> {
		Ok(ArgsTlsClient {
			other_roots: match self.other_roots {
				None => vec![],
				Some(paths) =>
					paths.iter()
						.map(|path| read_pem_cert(&PathBuf::from(path)))
						.collect::<Result<Vec<_>,_>>()?
			}
		})
	}
}

impl Default for ConfigTlsClient {

	fn default() -> Self {
		Self {
			other_roots: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsTlsClient {
	other_roots: Vec<Certificate>
}

impl Default for ArgsTlsClient {

	fn default() -> Self {
		Self {
			other_roots: vec![]
		}
	}
}


pub fn tls_config_client(args: &ArgsTlsClient) -> Result<ClientConfig,tokio_rustls::rustls::Error> {

	let mut store = RootCertStore::empty();

	#[cfg(test)]
	{
		// add our root cert
		let root_cert = test::root_cert();
		let root_cert_der = root_cert.serialize_der()
			.expect("Failed to encode root cert");
		store.add(&Certificate(root_cert_der))
			.expect("Failed to import root cert");
	}

	#[cfg(not(test))]
	{
		// add root certs from Mozilla's trusted store
		store.add_trust_anchors(webpki_roots::TLS_SERVER_ROOTS.iter().map(|ta| {
			tokio_rustls::rustls::OwnedTrustAnchor::from_subject_spki_name_constraints(
				ta.subject,
				ta.spki,
				ta.name_constraints,
			)
		}));
	}

	// add the other root certificates, if any
	for root_cert in &args.other_roots {
		store.add(root_cert)?;
	}

	let config = ClientConfig::builder()
		.with_safe_defaults()
		.with_root_certificates(store)
		.with_no_client_auth();

	Ok(config)
}
