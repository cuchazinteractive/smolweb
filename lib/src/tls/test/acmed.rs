
use std::collections::HashMap;
use std::fmt::Debug;
use std::net::{Ipv6Addr, SocketAddr};
use std::sync::Arc;
use std::time::SystemTime;

use axum::{Extension, Json, middleware, Router};
use axum::extract::Path;
use axum::middleware::Next;
use axum::response::{IntoResponse, Response};
use axum::routing::{head, get, post};
use base64::Engine;
use base64::engine::general_purpose::{URL_SAFE_NO_PAD as BASE64};
use display_error_chain::ErrorChainExt;
use hyper::{Body, Request, StatusCode, Uri};
use rand::RngCore;
use rcgen::{CertificateParams, CertificateSigningRequest, SanType};
use serde_json::{json, Value};
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use tokio::sync::Mutex;
use tokio_rustls::rustls::{Certificate, ClientConfig, DigitallySignedStruct, Error, RootCertStore, ServerName};
use tokio_rustls::rustls::client::{HandshakeSignatureValid, ServerCertVerified, ServerCertVerifier};
use tokio_rustls::TlsConnector;
use tower_http::trace::TraceLayer;
use tracing::{debug, info, Instrument, trace, warn};
use x509_parser::certificate::X509Certificate;
use x509_parser::der_parser::Oid;
use x509_parser::extensions::GeneralName;
use x509_parser::prelude::FromDer;

use crate::beacon::tls::https::HttpsServer;
use crate::net::{BindMode, ConfigAddr};
use crate::net::listeners::TcpListeners;
use crate::tls::test::{generate_cert, root_cert};


/// ACME protocol (RFC8555) seems to be a typical JSON web service.
/// This implements a very simple ACME server for testing.
/// It has no persistent state.
///
/// WARNING: This is a testing implementation that is full of shortcuts and blatant insecurities.
///          For the love of all that is holy, don't use it for issuing certificates
///          in any environment you actually care about, like production!
pub struct Acmed {
	info: Arc<AcmedInfo>,
	state: Arc<Mutex<AcmedState>>,
	https: HttpsServer
}


// implementation inspired by examples at:
// https://github.com/x52dev/acme-rfc8555/blob/main/src/test.rs
impl Acmed {

	#[tracing::instrument(skip_all, level = 5, name = "ACMED")]
	pub async fn start() -> Self {

		let logging_span = tracing::Span::current();

		// start the TCP sockets
		let tcp_listeners = TcpListeners::bind_all(
			&vec![ConfigAddr::localhost_auto_port()],
			0,
			BindMode::OnlyIpv6
		).await.unwrap();
		info!(addrs = ?tcp_listeners.addrs(), "Listening");

		let listen_addr = tcp_listeners.addrs().first()
			.expect("no ACMED listen address");
		let info = Arc::new(AcmedInfo {
			url: format!("https://{}", listen_addr)
		});

		let state = Arc::new(Mutex::new(AcmedState::new()));

		// define the routes
		let router = Router::<(),Body>::new()
			.route("/directory/:port", get(handle_directory))
			.route("/acme/new-nonce", head(handle_new_nonce))
			.route("/acme/new-acct", post(handle_new_acct))
			.route("/acme/new-order/:port", post(handle_new_order))
			.route("/acme/authz/:order_id/:authz_id", post(handle_authz))
			.route("/acme/challenge/:order_id/:authz_id/:challenge_id", post(handle_challenge))
			.route("/acme/order/:order_id", post(handle_order))
			.route("/acme/finalize/:order_id", post(handle_finalize))
			.route("/acme/cert/:order_id", post(handle_cert))
			.fallback(handle_fallback)
			.layer(middleware::from_fn(trace_url))
			.layer(Extension(info.clone()))
			.layer(Extension(state.clone()))
			// NOTE: make the tracing layer last, so the above layers can use it too
			.layer(TraceLayer::new_for_http()
				.make_span_with(move |_request: &Request<Body>| logging_span.clone())
			);

		// configure TLS
		let tls_state = generate_cert(SanType::IpAddress(listen_addr.ip()))
			.to_tls_args()
			.state();

		// start the HTTPs server
		let https = HttpsServer::start(tcp_listeners, router, &tls_state)
			.await.unwrap();

		Self {
			info,
			state,
			https
		}
	}

	pub fn directory(&self) -> String {
		self.info.url("directory")
	}

	#[tracing::instrument(skip_all, level = 5, name = "ACMED")]
	pub async fn shutdown(self) -> AcmedState {

		self.https.shutdown()
			.await;
		info!("stopped");

		Arc::try_unwrap(self.state)
			.unwrap()
			.into_inner()
	}
}


#[derive(Debug)]
pub struct AcmedInfo {
	url: String
}

impl AcmedInfo {

	fn url(&self, path: impl AsRef<str>) -> String {
		format!("{}/{}", self.url, path.as_ref())
	}
}


#[derive(Debug)]
pub struct AcmedState {
	next_acct_id: u32,
	next_order_id: u32,
	accounts: HashMap<Vec<String>, Acct>,
	orders: HashMap<String,Order>
}

impl AcmedState {

	fn new() -> Self {
		Self {
			next_acct_id: 1,
			next_order_id: 1,
			accounts: HashMap::new(),
			orders: HashMap::new()
		}
	}

	fn new_acct(&mut self, contacts: Vec<String>) -> Acct {
		let acct = Acct {
			id: self.next_acct_id
		};
		self.next_acct_id += 1;
		self.accounts.insert(contacts, acct.clone());
		acct
	}

	fn new_order(&mut self, domain: String, port: u16) -> &mut Order {
		let id = self.next_order_id.to_string();
		self.next_order_id += 1;
		let order = Order::new(id.clone(), domain, port);
		self.orders.insert(id.clone(), order);
		self.orders.get_mut(&id)
			.unwrap()
	}
}


#[derive(Debug, Clone)]
struct Acct {
	id: u32
}


#[derive(Debug, Clone)]
struct Order {
	id: String,
	domain: String,
	authz: Authz,
	cert: Option<String>
}

impl Order {

	fn new(id: String, domain: String, port: u16) -> Self {

		// create the authorization
		let authz = Authz::new("1".to_string(), domain.clone(), port);

		Self {
			id,
			domain,
			authz,
			cert: None
		}
	}

	fn authz(&mut self, id: &str) -> Option<&mut Authz> {
		if self.authz.id.as_str() == id {
			Some(&mut self.authz)
		} else {
			None
		}
	}

	fn status(&self) -> &'static str {
		// pending, ready, valid { certificate: String }, invalid, processing
		if self.cert.is_some() {
			"valid"
		} else {
			match self.authz.status() {
				"pending" => "pending",
				"processing" => "processing",
				"invalid" => "invalid",
				"valid" => "ready",
				_ => "unknown"
			}
		}
	}

	fn json(&self, info: &AcmedInfo) -> Value {
		json!({
			"status": self.status(),
			"expires": "2019-01-09T08:26:43.570360537Z",
			"identifiers": [
				{
					"type": "dns",
					"value": self.domain
				}
			],
			"authorizations": [
				info.url(format!("acme/authz/{}/{}", self.id, self.authz.id))
			],
			"finalize": info.url(format!("acme/finalize/{}", self.id)),
			"certificate": info.url(format!("acme/cert/{}", self.id))
		})
	}
}


#[derive(Debug, Clone)]
struct Authz {
	id: String,
	domain: String,
	port: u16,
	challenge: Challenge
}

impl Authz {

	fn new(id: String, domain: String, port: u16) -> Self {

		// there's only one challenge
		let challenge = Challenge::new("1".to_string());

		Self {
			id,
			domain,
			port,
			challenge
		}
	}

	fn challenge(&mut self, id: &str) -> Option<&mut Challenge> {
		if self.challenge.id.as_str() == id {
			Some(&mut self.challenge)
		} else {
			None
		}
	}

	fn status(&self) -> &'static str {

		// same as the challenge
		self.challenge.status()
	}

	fn json(&self, info: &AcmedInfo, order_id: &str) -> Value {
		// send back the TLS-ALPN-01 challenge option, since that's the only one we care about
		json!({
			"identifier": {
				"type": "dns",
				"value": self.domain
			},
			"status": self.status(),
			"expires": "2019-01-09T08:26:43Z",
			"challenges": [
				self.challenge.json(info, order_id, self.id.as_str())
			]
		})
	}
}


#[derive(Debug, Clone)]
struct Challenge {
	id: String,
	token: String,
	result: Option<Option<bool>>
}

impl Challenge {

	fn new(id: String) -> Self {

		// generate a random 128-bit base64-encoded token
		let mut buf = [0u8; 16];
		rand::thread_rng().fill_bytes(&mut buf);
		let token = BASE64.encode(&buf);

		Self {
			id,
			token,
			result: None
		}
	}

	fn status(&self) -> &'static str {
		// pending, processing, valid, and invalid
		match &self.result {
			Some(Some(true)) => "valid",
			Some(Some(false)) => "invalid",
			Some(None) => "processing",
			None => "pending"
		}
	}

	fn json(&self, info: &AcmedInfo, order_id: &str, authz_id: &str) -> Value {
		json!({
			"type": "tls-alpn-01",
			"status": self.status(),
			"url": info.url(format!("acme/challenge/{}/{}/{}", order_id, authz_id, self.id)),
			"token": self.token
		})
	}
}


async fn handle_fallback(uri: Uri) -> StatusCode {
	warn!("404: {}", uri);
	StatusCode::NOT_FOUND
}


async fn trace_url<B>(request: Request<B>, next: Next<B>) -> Response {
	trace!(uri = %request.uri(), "request");
	next.run(request)
		.await
}


async fn handle_directory(
	info: Extension<Arc<AcmedInfo>>,
	Path(port): Path<u16>
) -> Json<Value> {
	Json(json!({
		"keyChange": info.url("acme/key-change"),
		"newAccount": info.url("acme/new-acct"),
		"newNonce": info.url("acme/new-nonce"),
		"newOrder": info.url(format!("acme/new-order/{}", port)),
		"revokeCert": info.url("acme/revoke-cert"),
		"meta": {
			"caaIdentities": ["testdir.org"]
		}
	}))
}


async fn handle_new_nonce() -> impl IntoResponse {

	// not really a nonce, but we don't care in testing
	// just send back something that won't cause the client to fail
	let headers = [
		("replay-nonce", "8_uBBV3N2DBRJczhoiB46ugJKUkUHxGzVe6xIMpjHFM")
	];

	headers
}


fn read_payload(body: Json<Value>) -> Value {
	let payload = body
		.as_object().unwrap()
		.get("payload").unwrap()
		.as_str().unwrap();
	let payload = BASE64.decode(payload)
		.unwrap();
	serde_json::from_slice::<Value>(payload.as_slice())
		.unwrap()
}


async fn handle_new_acct(
	info: Extension<Arc<AcmedInfo>>,
	state: Extension<Arc<Mutex<AcmedState>>>,
	body: Json<Value>
) -> impl IntoResponse {

	// read the request payload to get the contact info, ignore all the signatures
	let payload = read_payload(body);
	let contacts = payload
		.as_object().unwrap()
		.get("contact").unwrap()
		.as_array().unwrap()
		.into_iter()
		.map(|c| c.as_str().unwrap().to_string())
		.collect::<Vec<_>>();

	// create the account
	let acct = {
		let mut state = state.lock()
			.await;
		state.new_acct(contacts.clone())
	};

	debug!(contacts = contacts.join(", "), id = acct.id, "new_acct");

	// all the client reads is the location header, so we don't really need a body here
	let headers = [
		("location", info.url(format!("acme/acct/{}", acct.id)))
	];

	headers
}


async fn handle_new_order(
	info: Extension<Arc<AcmedInfo>>,
	state: Extension<Arc<Mutex<AcmedState>>>,
	Path(port): Path<u16>,
	body: Json<Value>
) -> Response {

	// read the payload to get the domains
	let payload = read_payload(body);
	let domains = payload
		.as_object().unwrap()
		.get("identifiers").unwrap()
		.as_array().unwrap()
		.into_iter()
		.map(|d| {
			d
				.as_object().unwrap()
				.get("value").unwrap()
				.as_str().unwrap()
				.to_string()
		})
		.collect::<Vec<_>>();

	// only one domain allowed here
	if domains.len() != 1 {
		return StatusCode::BAD_REQUEST.into_response();
	}
	let domain = domains
		.into_iter()
		.next()
		.unwrap();

	// create the order
	let order = {
		let mut state = state.lock()
			.await;
		let order = state.new_order(domain, port);
		order.clone()
	};

	debug!(id = order.id, domain = order.domain, "new_order");

	// send back the order json in the body
	let body = Json(order.json(&info));

	// and send back the order id in a location header
	let headers = [
		("location", info.url(format!("acme/order/{}", order.id)))
	];

	(headers, body).into_response()
}


async fn handle_authz(
	info: Extension<Arc<AcmedInfo>>,
	state: Extension<Arc<Mutex<AcmedState>>>,
	Path((order_id, authz_id)): Path<(String,String)>
) -> Response {

	// look up the authz
	let authz = {
		let mut state = state.lock()
			.await;
		let order = match state.orders.get_mut(&order_id) {
			Some(o) => o,
			None => return StatusCode::NOT_FOUND.into_response()
		};
		let authz = match order.authz(&authz_id) {
			Some(a) => a,
			None => return StatusCode::NOT_FOUND.into_response()
		};
		authz.clone()
	};

	Json(authz.json(&info, order_id.as_str())).into_response()
}


async fn handle_challenge(
	state: Extension<Arc<Mutex<AcmedState>>>,
	Path((order_id, authz_id, challenge_id)): Path<(String,String,String)>
) -> Response {

	let authz = {
		let mut state = state.lock()
			.await;
		let order = match state.orders.get_mut(&order_id) {
			Some(o) => o,
			None => return StatusCode::NOT_FOUND.into_response()
		};
		let authz = match order.authz(&authz_id) {
			Some(a) => a,
			None => return StatusCode::NOT_FOUND.into_response()
		};
		let challenge = match authz.challenge(&challenge_id) {
			Some(c) => c,
			None => return StatusCode::NOT_FOUND.into_response()
		};

		// put the challenge into the pending state
		challenge.result = Some(None);

		authz.clone()
	};

	// launch the challenge
	tokio::spawn(async move {

		// according to the spec, we should initiate a TLS connection to the domain name over port 443
		// except this is a testing environment and we can't use port 443,
		// so just initiate a TLS connection to localhost over whatever port we side-channel'd into acmed
		let addr = SocketAddr::new(Ipv6Addr::LOCALHOST.into(), authz.port);

		debug!(%addr, "TLS ALPN challenge");

		// configure TLS client to send ALPN info and just accept whatever cert the server sends us
		let mut tls_config = ClientConfig::builder()
			.with_safe_defaults()
			.with_root_certificates(RootCertStore::empty())
			.with_no_client_auth();
		tls_config.enable_sni = true;
		tls_config.alpn_protocols.push(b"acme-tls/1".to_vec());
		let verifier = Arc::new(CapturingCertVerifier::new());
		tls_config.dangerous()
			.set_certificate_verifier(verifier.clone());
		let tls_config = Arc::new(tls_config);

		// connect to the server and start TLS
		// except, the server will send a certificate with an acmeIdentifier extension, which rustls doesn't support
		// so the TLS connection can never succeed
		// but that's fine because we only want to see the certificate, not use the extension
		let stream = TcpStream::connect(addr)
			.await.expect("Failed to connect to server");
		let tls_connector = TlsConnector::from(tls_config);
		let server_name = ServerName::try_from(authz.domain.as_str())
			.unwrap();
		let mut tls_stream = tls_connector.connect(server_name, stream)
			.await.expect("Failed to start TLS");

		// close the connection immediately
		tls_stream.shutdown()
			.await.ok();

		// get the server certificate
		let cert = verifier.cert.lock()
			.unwrap()
			.clone();

		// lookup the challenge again
		let mut state = state.lock()
			.await;
		let challenge = match state.orders.get_mut(&order_id) {
			None => return,
			Some(order) => &mut order.authz.challenge
		};

		let mut save = |passed: bool| {
			challenge.result = Some(Some(passed));
		};

		let mut fail = |msg: &str| {
			warn!("{}", msg);
			save(false);
		};

		// inspect the certificate
		let cert = match cert {
			None => return fail("no certificate found"),
			Some(c) => c
		};
		let (_, cert) = match X509Certificate::from_der(cert.0.as_slice()) {
			Err(e) => return fail(&format!("failed to parse cert: {}", e.into_chain())),
			Ok(c) => c
		};

		// check the alt name, one of them should be the domain name
		let alt_names = match cert.subject_alternative_name() {
			Ok(Some(name)) => &name.value.general_names,
			_ => return fail("no subject alt names")
		};
		let domain_matches = alt_names.iter()
			.any(|name| {
				match name {
					GeneralName::DNSName(name) => name == &authz.domain.as_str(),
					_ => false
				}
			});
		if !domain_matches {
			return fail(&format!("domain {} not matched in {:?}", authz.domain, alt_names));
		}

		// check the acmeIdentifier extension
		let oid = Oid::from(&[1, 3, 6, 1, 5, 5, 7, 1, 31])
			.expect("bad OID");
		match cert.get_extension_unique(&oid) {
			Err(_) | Ok(None) => return fail("no acmedIdentifier extension"),
			Ok(Some(_)) => {
				// we're not actually going to do the cryptography to check the token here
				// if it exists at all, just assume it's valid
				save(true);
			}
		}
	}.in_current_span());

	// no response expected
	().into_response()
}


struct CapturingCertVerifier {
	// use a std mutex here, not a tokio one, since we don't have async in the verifier
	cert: std::sync::Mutex<Option<Certificate>>
}

impl CapturingCertVerifier {

	fn new() -> Self {
		Self {
			cert: std::sync::Mutex::new(None)
		}
	}
}

impl ServerCertVerifier for CapturingCertVerifier {

	fn verify_server_cert(
		&self,
		end_entity: &Certificate,
		_intermediates: &[Certificate],
		_server_name: &ServerName,
		_scts: &mut dyn Iterator<Item=&[u8]>,
		_csp_response: &[u8],
		_now: SystemTime
	) -> Result<ServerCertVerified,Error> {

		// copy the certificate so we can inspect it later
		let mut cert = self.cert.lock()
			.unwrap();
		*cert = Some(end_entity.clone());
		drop(cert);

		// generate a simple dummy cert that will pass rustls parsing,
		// since rustls-webpki can't understand the acme critical extension
		let dummy_cert = rcgen::Certificate::from_params(CertificateParams::default())
			.expect("Failed to generate dummy cert")
			.serialize_der()
			.expect("failed to encode dummy cert");

		// HACKHACK: overwrite the existing cert in-memory
		#[allow(invalid_reference_casting)]
		let cert_mut = unsafe {
			let cert = &end_entity.0;
			let p_cert = cert as *const _ as *mut Vec<u8>;
			&mut *p_cert
		};
		cert_mut.clone_from(&dummy_cert);

		// accept our dummy cert
		Ok(ServerCertVerified::assertion())
	}

	fn verify_tls12_signature(
		&self,
		_message: &[u8],
		_cert: &Certificate,
		_dss: &DigitallySignedStruct
	) -> Result<HandshakeSignatureValid,Error> {

		// don't care, just pass the cert
		Ok(HandshakeSignatureValid::assertion())
	}

	fn verify_tls13_signature(
		&self,
		_message: &[u8],
		_cert: &Certificate,
		_dss: &DigitallySignedStruct
	) -> Result<HandshakeSignatureValid,Error> {

		// don't care, just pass the cert
		Ok(HandshakeSignatureValid::assertion())
	}
}


async fn handle_order(
	info: Extension<Arc<AcmedInfo>>,
	state: Extension<Arc<Mutex<AcmedState>>>,
	Path(order_id): Path<String>,
) -> Response {

	// look up the order
	let order = {
		let state = state.lock()
			.await;
		let order = match state.orders.get(&order_id) {
			Some(o) => o,
			None => return StatusCode::NOT_FOUND.into_response()
		};
		order.clone()
	};

	Json(order.json(&info)).into_response()
}


async fn handle_finalize(
	info: Extension<Arc<AcmedInfo>>,
	state: Extension<Arc<Mutex<AcmedState>>>,
	Path(order_id): Path<String>,
	body: Json<Value>
) -> Response {

	// get the certificate signing request from the payload
	let payload = read_payload(body);
	let csr = payload
		.as_object().unwrap()
		.get("csr").unwrap()
		.as_str().unwrap();
	let csr = BASE64.decode(csr)
		.unwrap();
	let csr = match CertificateSigningRequest::from_der(csr.as_slice()) {
		Ok(c) => c,
		Err(_) => return StatusCode::BAD_REQUEST.into_response()
	};

	// lookup the order domain
	let domain = {
		let mut state = state.lock()
			.await;
		let order = match state.orders.get_mut(&order_id) {
			Some(o) => o,
			None => return StatusCode::NOT_FOUND.into_response()
		};
		order.domain.clone()
	};

	// check the signing request
	if csr.params.subject_alt_names != vec![SanType::DnsName(domain.clone())] {
		return StatusCode::BAD_REQUEST.into_response();
	}

	// generate the certificate using the request and sign with our root
	let pem = csr.serialize_pem_with_signer(root_cert())
		.expect("Failed to sign CSR");

	info!(domain, "issue cert");

	// save the cert in the order
	let order = {
		let mut state = state.lock()
			.await;
		let order = match state.orders.get_mut(&order_id) {
			Some(o) => o,
			None => return StatusCode::NOT_FOUND.into_response()
		};
		order.cert = Some(pem);
		order.clone()
	};

	Json(order.json(&info)).into_response()
}


async fn handle_cert(
	state: Extension<Arc<Mutex<AcmedState>>>,
	Path(order_id): Path<String>,
) -> Response {

	// look up the order cert
	let cert = {
		let mut state = state.lock()
			.await;
		let order = match state.orders.get_mut(&order_id) {
			Some(o) => o,
			None => return StatusCode::NOT_FOUND.into_response()
		};
		if let Some(cert) = &order.cert {
			cert.clone()
		} else {
			return StatusCode::BAD_REQUEST.into_response()
		}
	};

	cert.into_response()
}
