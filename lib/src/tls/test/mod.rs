
pub(crate) mod acmed;


use std::fmt::Debug;
use std::sync::OnceLock;

use galvanic_assert::{assert_that, matchers::*};
use pem::{EncodeConfig, Pem};
use rcgen::{BasicConstraints, CertificateParams, IsCa, SanType};
use temp_dir::TempDir;
use tokio_rustls::rustls::{Certificate, PrivateKey};

use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::beacon::tls::ArgsTlsCert;
use crate::config::test::deserialize_from_toml;
use crate::net::LOCALHOST;
use crate::tls::{ArgsTlsClient, ConfigTlsClient, tls_config_client};


pub fn write_tmp_cert_key() -> (TempDir, String, String, Vec<u8>, Vec<u8>) {

	// generate a temporary certificate
	let mut params = CertificateParams::default();
	params.subject_alt_names = vec![SanType::DnsName("domain".to_string())];
	let cert = rcgen::Certificate::from_params(params)
		.unwrap();

	// DER-encode it
	let cert_der = cert.serialize_der()
		.unwrap();
	let key_der = cert.serialize_private_key_der();

	// PEM-encode it
	let cert_pem = pem::encode_config(
		&Pem::new("CERTIFICATE", cert_der.clone()),
		EncodeConfig::default()
	);
	let key_pem = pem::encode_config(
		&Pem::new("PRIVATE KEY", key_der.clone()),
		EncodeConfig::default()
	);

	// write it to a temporary folder
	let tmpdir = TempDir::with_prefix("smolweb-")
		.unwrap();
	let cert_path = tmpdir.child("cert")
		.to_string_lossy().to_string();
	std::fs::write(&cert_path, cert_pem)
		.unwrap();
	let key_path = tmpdir.child("key")
		.to_string_lossy().to_string();
	std::fs::write(&key_path, key_pem)
		.unwrap();

	(tmpdir, cert_path, key_path, cert_der, key_der)
}


/// DER-encoded certificate and private key
#[derive(Debug, Clone)]
pub struct CertInfo {
	pub cert: Vec<u8>,
	pub private_key: Vec<u8>
}

impl CertInfo {

	pub fn to_tls_args(self) -> ArgsTlsCert {
		ArgsTlsCert {
			cert_chain: vec![Certificate(self.cert)],
			private_key: PrivateKey(self.private_key),
		}
	}
}


trait CertEx {
	fn to_info(&self, signer: &rcgen::Certificate) -> CertInfo;
}

impl CertEx for rcgen::Certificate {

	fn to_info(&self, signer: &rcgen::Certificate) -> CertInfo {
		CertInfo {
			cert: self.serialize_der_with_signer(signer)
				.expect("failed to serialize cert into DER and sign"),
			private_key: self.serialize_private_key_der()
		}
	}
}


static ROOT_CERT: OnceLock<rcgen::Certificate> = OnceLock::new();

fn generate_root_cert(name: impl AsRef<str>) -> rcgen::Certificate {
	let mut params = CertificateParams::default();
	params.is_ca = IsCa::Ca(BasicConstraints::Unconstrained);
	params.subject_alt_names = vec![
		SanType::DnsName(name.as_ref().to_string())
	];
	rcgen::Certificate::from_params(params)
		.expect("Failed to generate root cert")
}

pub fn root_cert() -> &'static rcgen::Certificate {
	ROOT_CERT.get_or_init(|| generate_root_cert("smolweb.test"))
}


pub fn generate_cert(name: SanType) -> CertInfo {

	// generate a simple certificate for localhost
	let mut params = CertificateParams::default();
	params.subject_alt_names = vec![name];
	let cert = rcgen::Certificate::from_params(params)
		.expect("Failed to generate localhost cert");

	// encode and sign using the root cert
	cert.to_info(root_cert())
}

pub fn generate_domain_cert(domain: impl AsRef<str>) -> CertInfo {
	generate_cert(SanType::DnsName(domain.as_ref().to_string()))
}


#[tokio::test]
async fn config_self_signed() {
	let _logging = init_test_logging();

	// just make sure nothing crashes
	let args = generate_domain_cert(LOCALHOST).to_tls_args();
	let tls_state = args.state();
	tls_state.set_tls_port(5);
	let _server_config = tls_state.config()
		.await.unwrap();
	let args_client = ArgsTlsClient::default();
	let _client_config = tls_config_client(&args_client);
}


#[tokio::test]
async fn config_client_other_root() {
	let _logging = init_test_logging();

	// generate a root certificate in a temporary folder
	let (_tmpdir, cert_path, _cert_der) = write_tmp_root_cert();

	let args_client = ConfigTlsClient {
		other_roots: Some(vec![cert_path])
	}.to_args().unwrap();

	// just make sure this doesn't crash
	let _client_config = tls_config_client(&args_client)
		.unwrap();
}


#[test]
fn config_client() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigTlsClient>(r#"
	"#).unwrap();
	assert_that!(&config, eq(ConfigTlsClient::default()));

	let config = deserialize_from_toml::<ConfigTlsClient>(r#"
		other_roots = []
	"#).unwrap();
	assert_that!(&config, eq(ConfigTlsClient {
		other_roots: Some(vec![]),
		.. ConfigTlsClient::default()
	}));

	let config = deserialize_from_toml::<ConfigTlsClient>(r#"
		other_roots = ["foo"]
	"#).unwrap();
	assert_that!(&config, eq(ConfigTlsClient {
		other_roots: Some(vec!["foo".to_string()]),
		.. ConfigTlsClient::default()
	}));
}


#[test]
fn to_args_client() {
	let _logging = init_test_logging();

	assert_that!(&ConfigTlsClient::default().to_args().unwrap(), eq(ArgsTlsClient::default()));

	assert_that!(&ConfigTlsClient {
		other_roots: Some(vec![])
	}.to_args().unwrap(), eq(ArgsTlsClient::default()));

	// generate a root certificate in a temporary folder
	let (_tmpdir, cert_path, cert_der) = write_tmp_root_cert();

	assert_that!(&ConfigTlsClient {
		other_roots: Some(vec![cert_path])
	}.to_args().unwrap(), eq(ArgsTlsClient {
		other_roots: vec![Certificate(cert_der)]
	}));

	assert_that!(&ConfigTlsClient {
		other_roots: Some(vec!["not_a_cert".to_string()])
	}.to_args(), is_match!(Err(..)));
}


fn write_tmp_root_cert() -> (TempDir, String, Vec<u8>) {

	let cert = generate_root_cert("test");
	let tmpdir = TempDir::with_prefix("smolweb-")
		.unwrap();
	let cert_path = tmpdir.child("cert")
		.to_string_lossy().to_string();
	let cert_der = cert.serialize_der()
		.unwrap();
	let cert_pem = pem::encode_config(
		&Pem::new("CERTIFICATE", cert_der.clone()),
		EncodeConfig::default()
	);
	std::fs::write(&cert_path, cert_pem)
		.unwrap();

	(tmpdir, cert_path, cert_der)
}
