
use anyhow::{Context, Result};
use axum::async_trait;
use serde::Deserialize;
use tracing::{debug, info};

use crate::host::ChannelHandler;
use crate::lang::ErrorReporting;
use crate::protobuf::framed::{Reader, Writer};
use crate::webrtc::channels::DataChannelReaderError;
use crate::webrtc::RTCDataChannelIO;


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigEcho {
	// no options needed yet
}

impl ConfigEcho {

	pub fn to_args(self) -> ArgsEcho {
		ArgsEcho {
			// no options needed yet
		}
	}
}

impl Default for ConfigEcho {

	fn default() -> Self {
		Self {
			// no options needed yet
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsEcho {
	// no options needed yet
}

impl Default for ArgsEcho {

	fn default() -> Self {
		Self {
			// no options needed yet
		}
	}
}


pub struct Echo {
	_args: ArgsEcho
}

impl Echo {

	#[tracing::instrument(skip_all, level = 5, name = "Echo")]
	pub fn new(args: ArgsEcho) -> Self {

		info!("started");

		Self {
			_args: args
		}
	}
}


#[async_trait]
impl ChannelHandler for Echo {

	#[tracing::instrument(skip_all, level = 5, name = "Echo")]
	async fn handle(&self, channel: RTCDataChannelIO) -> Result<()> {

		// log events
		debug!("handling channel");

		loop {

			// wait for the next message
			let msg = match channel.read().await {
				Err(DataChannelReaderError::Closed) => break,
				Ok(m) => m
			};

			// echo all messages back to the sender
			channel.write(msg)
				.await
				.context("Failed to echo message")
				.warn_err()
				.ok();
		}

		Ok(())
	}
}
