
mod echo;
mod fetch_proxy;
mod fetch_static;


use anyhow::{bail, Result};
use serde::Deserialize;

pub use echo::{ArgsEcho, ConfigEcho, Echo};
pub use fetch_proxy::{ArgsFetchProxy, ConfigFetchProxy, FetchProxy};
pub use fetch_static::{ArgsFetchStatic, ConfigFetchStatic, FetchStatic};


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigHandlers {
	pub echo: Option<ConfigEcho>,
	pub fetch_proxy: Option<ConfigFetchProxy>,
	pub fetch_static: Option<ConfigFetchStatic>
}

impl ConfigHandlers {

	pub fn to_args(self) -> Result<ArgsChannelHandler> {
		// TODO: this doesn't scale super well ... D=
		match (self.echo, self.fetch_proxy, self.fetch_static) {
			(Some(echo), None, None) => Ok(ArgsChannelHandler::Echo(echo.to_args())),
			(None, Some(fetch_proxy), None) => Ok(ArgsChannelHandler::FetchProxy(fetch_proxy.to_args()?)),
			(None, None, Some(fetch_static)) => Ok(ArgsChannelHandler::FetchStatic(fetch_static.to_args()?)),
			_ => bail!("Expected at exactly one handler among: [echo, fetch_proxy, fetch_static]")
		}
	}
}

impl Default for ConfigHandlers {

	fn default() -> Self {
		Self {
			echo: None,
			fetch_proxy: None,
			fetch_static: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ArgsChannelHandler {
	Echo(ArgsEcho),
	FetchProxy(ArgsFetchProxy),
	FetchStatic(ArgsFetchStatic)
}


#[cfg(test)]
mod test {

	use galvanic_assert::{assert_that, matchers::*};

	use smolweb_test_tools::is_match;
	use smolweb_test_tools::logging::init_test_logging;

	use crate::config::test::deserialize_from_toml;

	use super::*;


	#[test]
	fn config_handlers() {
		let _logging = init_test_logging();

		let config = deserialize_from_toml::<ConfigHandlers>(r#"
		"#).unwrap();
		assert_that!(&config, eq(ConfigHandlers::default()));

		let config = deserialize_from_toml::<ConfigHandlers>(r#"
			[echo]
		"#).unwrap();
		assert_that!(&config, eq(ConfigHandlers {
			echo: Some(ConfigEcho::default()),
			.. ConfigHandlers::default()
		}));

		let config = deserialize_from_toml::<ConfigHandlers>(r#"
			[fetch_proxy]
		"#).unwrap();
		assert_that!(&config, eq(ConfigHandlers {
			fetch_proxy: Some(ConfigFetchProxy::default()),
			.. ConfigHandlers::default()
		}));

		let config = deserialize_from_toml::<ConfigHandlers>(r#"
			[fetch_static]
		"#).unwrap();
		assert_that!(&config, eq(ConfigHandlers {
			fetch_static: Some(ConfigFetchStatic::default()),
			.. ConfigHandlers::default()
		}));
	}


	#[test]
	fn to_args_handlers() {

		assert_that!(&ConfigHandlers::default().to_args(), is_match!(Err(..)));

		assert_that!(&ConfigHandlers {
			echo: Some(ConfigEcho::default()),
			.. ConfigHandlers::default()
		}.to_args().unwrap(), eq(ArgsChannelHandler::Echo(ArgsEcho::default())));

		assert_that!(&ConfigHandlers {
			fetch_proxy: Some(ConfigFetchProxy::default()),
			.. ConfigHandlers::default()
		}.to_args().unwrap(), eq(ArgsChannelHandler::FetchProxy(ArgsFetchProxy::default())));

		assert_that!(&ConfigHandlers {
			fetch_static: Some(ConfigFetchStatic::default()),
			.. ConfigHandlers::default()
		}.to_args().unwrap(), eq(ArgsChannelHandler::FetchStatic(ArgsFetchStatic::default())));

		// there can be only one!
		assert_that!(&ConfigHandlers {
			echo: Some(ConfigEcho::default()),
			fetch_proxy: Some(ConfigFetchProxy::default()),
			fetch_static: Some(ConfigFetchStatic::default()),
		}.to_args(), is_match!(Err(..)));
	}
}
