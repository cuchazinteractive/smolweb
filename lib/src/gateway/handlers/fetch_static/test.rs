
use bytes::Bytes;
use galvanic_assert::{assert_that, matchers::*, structure};
use temp_dir::TempDir;

use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::config::test::deserialize_from_toml;
use crate::guest::fetch_client::FetchClient;
use crate::host::server::{ChannelHandlerTester, ConnectedGuest};
use crate::protobuf::framed::{DeframerError, FrameReaderBufError, FrameReaderTimeout, LocalDeframerError, RemoteFrameError};

use super::*;


const TIMEOUT_1S: Duration = Duration::from_secs(1);

fn timeout_now_1s() -> FrameReaderTimeout {
	FrameReaderTimeout::Now(TIMEOUT_1S)
}


#[test]
fn config() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigFetchStatic>(r#"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchStatic::default()));

	let config = deserialize_from_toml::<ConfigFetchStatic>(r#"
		root = "/foo"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchStatic {
		root: Some("/foo".to_string()),
		.. ConfigFetchStatic::default()
	}));

	let config = deserialize_from_toml::<ConfigFetchStatic>(r#"
		timeout_guest_request = "5s"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchStatic {
		timeout_guest_request: Some(ConfigDuration::s(5)),
		.. ConfigFetchStatic::default()
	}));
}


#[test]
fn to_args() {
	let _logging = init_test_logging();

	assert_that!(&ConfigFetchStatic::default().to_args().unwrap(), eq(ArgsFetchStatic::default()));
}


#[tokio::test]
async fn not_a_request() {
	let _logging = init_test_logging();

	let args = ArgsFetchStatic::default();
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// send a gibberish message on the channel
	let guest = tester.connect_guest()
		.await;
	guest.io().channel().write(Bytes::from_static(&[1, 2, 3]))
		.await.unwrap();

	// wait for a response, an error about failing to decode a frame
	let e = guest.io().frame_reader().read_all(timeout_now_1s())
		.await
		.err()
		.unwrap();
	assert_that!(&e, structure!(FrameReaderBufError::Stream[
		structure!(FrameStreamError::Frame {
			local_error: is_match!(DeframerError::Remote(Ok(RemoteFrameError::Deframer(LocalDeframerError::Decode(..)))))
		})
	]));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn bad_method() {
	let _logging = init_test_logging();

	let args = ArgsFetchStatic::default();
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// send a fetch request with a bad method
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("foo", "/hi", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();
	assert_that!(&response.status, eq(405));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_txt() {
	let _logging = init_test_logging();

	// make a static site with a text file
	let tmpdir = tmpdir();
	let text_content = "bar";
	fs::write(tmpdir.child("foo.txt"), text_content)
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch a text file
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/foo.txt", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.headers, eq(vec![
		Header::new("content-length", text_content.len().to_string()),
		Header::new("content-type", "text/plain")
	]));

	let body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&String::from_utf8(body).unwrap(), eq(text_content.to_string()));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_html() {
	let _logging = init_test_logging();

	// make a static site with a text file
	let tmpdir = tmpdir();
	let html_content = "<html></html>";
	fs::write(tmpdir.child("foo.html"), html_content)
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch an HTML file
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/foo.html", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.headers, eq(vec![
		Header::new("content-length", html_content.len().to_string()),
		Header::new("content-type", "text/html")
	]));

	let body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();
	assert_that!(&String::from_utf8(body).unwrap(), eq(html_content.to_string()));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn relative_path() {
	let _logging = init_test_logging();

	// make a static site with a text file
	let tmpdir = tmpdir();
	let text_content = "bar";
	fs::write(tmpdir.child("foo.txt"), text_content)
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch a text file using a relative path
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "foo.txt", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();
	assert_that!(&response.status, eq(200));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_binary() {
	let _logging = init_test_logging();

	// make a static site with a binary file
	let tmpdir = tmpdir();
	let bin_content = vec![1, 2, 3];
	fs::write(tmpdir.child("foo"), &bin_content)
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the binary file
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/foo", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.headers, eq(vec![
		Header::new("content-length", bin_content.len().to_string()),
		Header::new("content-type", "application/octet-stream")
	]));

	let body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&body, eq(bin_content));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_binary_large() {
	let _logging = init_test_logging();

	// make a static site with a binary file
	let tmpdir = tmpdir();
	let bin_content = vec![
		vec![1u8; MAX_PAYLOAD_SIZE],
		vec![2u8; MAX_PAYLOAD_SIZE],
		vec![3u8; MAX_PAYLOAD_SIZE],
		vec![4u8; MAX_PAYLOAD_SIZE],
		vec![5u8; MAX_PAYLOAD_SIZE],
		vec![6u8; MAX_PAYLOAD_SIZE],
		vec![7u8; MAX_PAYLOAD_SIZE],
		vec![8u8; MAX_PAYLOAD_SIZE]
	].concat();
	fs::write(tmpdir.child("foo"), &bin_content)
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the binary file
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/foo", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.headers, eq(vec![
		Header::new("content-length", bin_content.len().to_string()),
		Header::new("content-type", "application/octet-stream")
	]));

	let body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&body, eq(bin_content));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_root() {
	let _logging = init_test_logging();

	// make a static site with an empty root folder
	let tmpdir = tmpdir();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the root
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(404));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_root_empty() {
	let _logging = init_test_logging();

	// make a static site with an empty root folder
	let tmpdir = tmpdir();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the root
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(404));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_default_html() {
	let _logging = init_test_logging();

	// make a static site with an index.html
	let tmpdir = tmpdir();
	let html_content = "<html></html>";
	fs::write(tmpdir.child("index.html"), &html_content)
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the root, hoping for the index file
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.headers, eq(vec![
		Header::new("content-length", html_content.len().to_string()),
		Header::new("content-type", "text/html")
	]));

	let body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&String::from_utf8(body).unwrap(), eq(html_content.to_string()));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_default_htm() {
	let _logging = init_test_logging();

	// make a static site with an index.htm
	let tmpdir = tmpdir();
	let html_content = "<html></html>";
	fs::write(tmpdir.child("index.htm"), &html_content)
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the root, hoping for the index file
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.headers, eq(vec![
		Header::new("content-length", html_content.len().to_string()),
		Header::new("content-type", "text/html")
	]));

	let body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&String::from_utf8(body).unwrap(), eq(html_content.to_string()));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_root_no_index() {
	let _logging = init_test_logging();

	// make a static site with an index.html
	let tmpdir = tmpdir();
	fs::write(tmpdir.child("index.html"), "<html></html>")
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	args.indices = vec![];
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the root
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(404));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_custom_index() {
	let _logging = init_test_logging();

	// make a static site with a custom index file
	let tmpdir = tmpdir();
	let bin_content = vec![1, 2, 3];
	fs::write(tmpdir.child("index"), &bin_content)
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	args.indices = vec!["index".to_string()];
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the root, hoping for the index file
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.headers, eq(vec![
		Header::new("content-length", bin_content.len().to_string()),
		Header::new("content-type", "application/octet-stream")
	]));

	let body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&body, eq(bin_content));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_malicious_paths() {
	let _logging = init_test_logging();

	// make a static site with some public and non-public files
	let tmpdir = tmpdir();
	fs::write(tmpdir.child("secret"), "secret")
		.unwrap();
	let pubdir = tmpdir.child("pub");
	fs::create_dir(&pubdir)
		.unwrap();
	fs::write(pubdir.join("index.html"), "<html></html>")
		.unwrap();
	fs::create_dir_all(pubdir.join("down").join("down"))
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(pubdir);
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	let guest = tester.connect_guest()
		.await;

	// the public index should be fetchable
	async fn yup(guest: &ConnectedGuest, path: &str) {
		let mut client = FetchClient::new(guest.io().clone());
		client.send_request("get", path, vec![], false)
			.await.unwrap();
		let response = client.recv_response(TIMEOUT_1S)
			.await.unwrap();
		assert_that!(&response.status, eq(200));
		match response.body_type {
			ResponseBodyType::None => (), // no body, don't need to recv it
			ResponseBodyType::SingleStream => {
				client.recv_body(TIMEOUT_1S)
					.await.unwrap();
			},
			ResponseBodyType::MultiStream => todo!()
		}
	}
	yup(&guest, "/")
		.await;
	yup(&guest, "/.")
		.await;
	yup(&guest, "/down/..")
		.await;
	yup(&guest, "/down/down/../..")
		.await;
	yup(&guest, format!("../../../../../../../../{}/pub", tmpdir.path().to_string_lossy()).as_str())
		.await;

	// but not the secret
	async fn nope(guest: &ConnectedGuest, path: &str) {
		let mut client = FetchClient::new(guest.io().clone());
		client.send_request("get", path, vec![], false)
			.await.unwrap();
		let response = client.recv_response(TIMEOUT_1S)
			.await.unwrap();
		assert_that!(&response.status, eq(404));
	}
	nope(&guest, "/../secret")
		.await;
	nope(&guest, "../secret")
		.await;
	nope(&guest, "/down/../../secret")
		.await;
	nope(&guest, "down/../../secret")
		.await;
	nope(&guest, "/./../secret")
		.await;
	nope(&guest, "./../secret")
		.await;
	nope(&guest, "/./down/down/../../../secret")
		.await;
	nope(&guest, "./down/down/../../../secret")
		.await;
	nope(&guest, "/down/down/../../../secret")
		.await;
	nope(&guest, "down/down/../../../secret")
		.await;
	nope(&guest, format!("../../../../../../../../{}/secret", tmpdir.path().to_string_lossy()).as_str())
		.await;

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_folder_as_file() {
	let _logging = init_test_logging();

	// make a static site with a subfolder
	let tmpdir = tmpdir();
	let subdir = tmpdir.child("sub");
	fs::create_dir(&subdir)
		.unwrap();
	fs::write(subdir.join("index.html"), "<html></html>")
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the folder, but as a file
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/sub", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_folder_as_folder() {
	let _logging = init_test_logging();

	// make a static site with a subfolder
	let tmpdir = tmpdir();
	let subdir = tmpdir.child("sub");
	fs::create_dir(&subdir)
		.unwrap();
	fs::write(subdir.join("index.html"), "<html></html>")
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the file, but as a folder
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/sub/", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));

	// cleanup
	guest.close()
		.await.unwrap();
}


#[tokio::test]
async fn get_file_as_folder() {
	let _logging = init_test_logging();

	// make a static site with only files
	let tmpdir = tmpdir();
	fs::write(tmpdir.child("index.html"), "<html></html>")
		.unwrap();

	let mut args = ArgsFetchStatic::default();
	args.root = Some(tmpdir.path().to_path_buf());
	let mut tester = ChannelHandlerTester::new(FetchStatic::new(args))
		.await;

	// fetch the file, but as a folder
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/index.html/", vec![], false)
		.await.unwrap();
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(404));

	// cleanup
	guest.close()
		.await.unwrap();
}


fn tmpdir() -> TempDir {
	TempDir::with_prefix("smolweb-fetch_static-")
		.unwrap()
}
