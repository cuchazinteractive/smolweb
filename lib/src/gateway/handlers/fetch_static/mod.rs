
#[cfg(test)]
mod test;


use std::fs;
use std::fs::File;
use std::io::{ErrorKind, Read};
use std::path::{Path, PathBuf};
use std::time::Duration;

use anyhow::{Context, Result};
use axum::async_trait;
use display_error_chain::ErrorChainExt;
use mime_guess::MimeGuess;
use serde::Deserialize;
use tracing::{debug, error, info, trace, warn};

use crate::config::ConfigDuration;
use crate::host::ChannelHandler;
use crate::host::fetch_server::{BodyWriter, FetchRecvRequestError, FetchServer};
use crate::protobuf::fetch::{GatewayResponse, Request, ResponseBodyType, ServerResponse};
use crate::protobuf::framed::{FrameReaderBufError, MAX_PAYLOAD_SIZE, NewFramerError, Reader, Writer, FrameStreamError};
use crate::protobuf::smolweb::fetch::Header;
use crate::webrtc::channels::DataChannelReaderError;
use crate::webrtc::RTCDataChannelIO;


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigFetchStatic {
	pub root: Option<String>,
	pub indices: Option<Vec<String>>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_guest_request: Option<ConfigDuration>,
}

impl ConfigFetchStatic {

	pub fn to_args(self) -> Result<ArgsFetchStatic> {
		let default = ArgsFetchStatic::default();
		Ok(ArgsFetchStatic {
			root: self.root
				.as_ref()
				.map(|r| {
					PathBuf::from(r).canonicalize()
						.context(format!("Failed to find fetch_static root folder: {}", r))
				})
				.transpose()?,
			indices: self.indices.unwrap_or(default.indices),
			timeout_guest_request: self.timeout_guest_request
				.map(|t| t.to_duration())
				.unwrap_or(default.timeout_guest_request)
		})
	}
}

impl Default for ConfigFetchStatic {

	fn default() -> Self {
		Self {
			root: None,
			indices: None,
			timeout_guest_request: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsFetchStatic {
	pub root: Option<PathBuf>,
	pub indices: Vec<String>,
	pub timeout_guest_request: Duration
}

impl Default for ArgsFetchStatic {

	fn default() -> Self {

		let args = Self {
			root: None,
			indices: vec!["index.html".to_string(), "index.htm".to_string()],
			timeout_guest_request: Duration::from_secs(30)
		};

		#[cfg(test)]
		let args = Self {
			timeout_guest_request: Duration::from_millis(200),
			.. args
		};

		args
	}
}


pub struct FetchStatic {
	args: ArgsFetchStatic
}

impl FetchStatic {

	#[tracing::instrument(skip_all, level = 5, name = "FetchStatic")]
	pub fn new(args: ArgsFetchStatic) -> Self {

		let root = args.root
			.as_ref()
			.map(|r| r.to_string_lossy().to_string())
			.unwrap_or("(none)".to_string());
		let indices = args.indices.join(", ");
		info!(root, indices, "Serving static website");

		Self {
			args
		}
	}
}


#[async_trait]
impl ChannelHandler for FetchStatic {

	#[tracing::instrument(skip_all, level = 5, name = "FetchStatic", fields(request_id))]
	async fn handle(&self, channel: RTCDataChannelIO) -> Result<()> {

		let args = self.args.clone();

		// log events
		let logging_span = tracing::Span::current();
		logging_span.record("request_id", rand::random::<u64>());
		debug!("handling channel");

		let mut server = FetchServer::new(channel);

		loop {

			// wait for the next fetch request
			let result = server.recv_request(args.timeout_guest_request)
				.await;
			match result {

				Ok(request) => {
					let result = dispatch(&args, &mut server, request)
						.await;
					if let Err(response) = result {
						server.respond_gateway(response)
							.await.ok();
					}
				}

				Err(FetchRecvRequestError::Read(FrameReaderBufError::Stream(FrameStreamError::Read(DataChannelReaderError::Closed)))) => {
					// WebRTC connection closed, stop listening
					debug!("channel closed");
					break;
				}

				Err(e) => {
					error!(err = %e.chain(), "Error receiving request");
					let response = GatewayResponse::InvalidRequest {
						reason: format!("{}", e.into_chain())
					};
					server.respond_gateway(response)
						.await.ok();

					// stop listening for more requests
					break;
				}
			}
		}

		Ok(())
	}
}


async fn dispatch<W,R>(args: &ArgsFetchStatic, server: &mut FetchServer<W,R>, request: Request) -> Result<(),GatewayResponse>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send,
		<R as Reader>::Error: 'static
{

	debug!(method = request.method, path = request.path, "Request");

	// only GET is allowed
	if request.method.to_uppercase().as_str() != "GET" {
		// method not allowed
		let response = ServerResponse {
			status: 405,
			headers: vec![],
			body_type: ResponseBodyType::None
		};
		debug!(status = response.status, "Response");
		server.respond_server(response).await.ok();
		return Ok(());
	}

	// make sure the path points to a file we can actually serve
	let path = match file_path(args, &request.path) {
		Some(p) => p,
		None => {
			// not found
			let response = ServerResponse {
				status: 404,
				headers: vec![],
				body_type: ResponseBodyType::None
			};
			debug!(status = response.status, "Response");
			server.respond_server(response).await.ok();
			return Ok(());
		}
	};

	// try to guess the mime type for the file
	let mime = MimeGuess::from_path(&path)
		.first_or_octet_stream();

	// try to open the file and get the size
	let (file, file_size) = match (File::open(&path), fs::metadata(&path)) {
		(Ok(file), Ok(meta)) => (file, meta.len()),
		_ => {
			// forbidden
			let status = 403;
			debug!(status, "Response");
			server.respond_server(ServerResponse {
				status,
				headers: vec![],
				body_type: ResponseBodyType::None
			}).await.ok();
			return Ok(());
		}
	};

	// build the response to the guest
	let response = ServerResponse {
		status: 200,
		headers: vec![
			Header::new("content-length", file_size.to_string()),
			Header::new("content-type", mime.to_string())
			// TODO: cache-control headers?
		],
		body_type: ResponseBodyType::SingleStream
	};
	debug!(status = response.status, "Response");

	// send the response back
	let result = server.respond_server(response)
		.await;
	if let Ok(Some(body_writer)) = result {
		stream_file_to_guest(file, &path, file_size, body_writer)
			.await;
	}

	// we're done. hooray!
	Ok(())
}


fn file_path(args: &ArgsFetchStatic, path: &String) -> Option<PathBuf> {

	// if there's no root, then no paths can be found
	let root = args.root.as_ref()?;

	// path must be relative
	let path =
		if path.starts_with('/') {
			&path[1..]
		} else {
			&path[..]
		};

	// build the absolute path to the file
	let abspath = root.join(path)
		.canonicalize()
		.ok()?;

	// make sure it's actually within the root
	if !abspath.starts_with(root) {
		return None;
	}

	// make sure the file exists
	let meta = abspath.metadata()
		.ok()?;
	if meta.is_file() {

		// regular file, we can serve it
		Some(abspath)

	} else if meta.is_dir() {

		// for folders, find the first index file that exists, if any
		args.indices
			.iter()
			.map(|index| abspath.join(index))
			.find(|p| p.exists())

	} else {

		None
	}
}


#[tracing::instrument(skip_all, level = 5, name = "stream_file_to_guest")]
async fn stream_file_to_guest<'w,W>(mut file: File, file_path: &Path, file_size: u64, body_writer: BodyWriter<'w,W>)
	where
		W: Writer + Send + Sync + Clone
{
	trace!(path = %file_path.to_string_lossy(), file_size, "start");

	let result = body_writer.stream(file_size);
	let mut streamer = match result {
		Ok(s) => s,
		Err(NewFramerError::InvalidStreamSize { max, observed }) => {
			warn!(path = %file_path.to_string_lossy(), size = observed, max, "File too large to stream");
			body_writer.abort()
				.await
				.ok();
			return;
		}
	};

	// stream the file to the guest
	let mut payload = Vec::with_capacity(MAX_PAYLOAD_SIZE);
	let mut buf = [0u8; MAX_PAYLOAD_SIZE];
	loop {

		// try to read from the file
		let bytes_read = match file.read(&mut buf) {
			Ok(b) => b,
			Err(e) if e.kind() == ErrorKind::Interrupted => continue,
			Err(e) => {
				// failed to read the file, abort the stream
				error!(path = %file_path.to_string_lossy(), err = %e.into_chain(), "Failed to read from file");
				streamer.abort()
					.await
					.ok();
				break;
			}
		};

		trace!(size = bytes_read, "read");

		if bytes_read == 0 {

			// end of file, flush the current payload, if any
			if payload.len() > 0 {
				trace!(size = payload.len(), "EOF: flush payload");
				streamer.write(payload.as_slice())
					.await
					.ok();
			}

			break;

		} else if payload.len() + bytes_read >= MAX_PAYLOAD_SIZE {

			// read something, have more than a full payload now

			// fill the payload
			let remaining = MAX_PAYLOAD_SIZE - payload.len();
			payload.extend_from_slice(&buf[..remaining]);

			trace!(size = payload.len(), "flush payload");

			// send it to the guest
			let result = streamer.write(payload.as_slice())
				.await;
			if let Err(e) = result {
				// failed to send the body payload to the guest
				// not much else we can do here except stop trying
				error!(err = %e.into_chain(), "error sending payload");
				break;
			}

			// start the next payload, if needed
			payload.clear();
			if bytes_read > remaining {
				trace!(size = payload.len(), max = MAX_PAYLOAD_SIZE, "new payload");
				payload.extend_from_slice(&buf[remaining..]);
			}

		} else {

			// read something, but not enough for a full payload

			// extend the payload
			payload.extend_from_slice(&buf[..bytes_read]);

			trace!(size = payload.len(), max = MAX_PAYLOAD_SIZE, "extend payload");
		}
	}
}
