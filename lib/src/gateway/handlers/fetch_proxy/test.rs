
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr};
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};
use std::time::Duration;

use axum::Router;
use axum::body::{Empty, HttpBody};
use axum::extract::State;
use axum::http::{HeaderMap, Request as AxumRequest};
use axum::response::{IntoResponse, Response as AxumResponse};
use axum::routing::get;
use bytes::Bytes;
use galvanic_assert::{assert_that, matchers::*, structure};
use hyper::{Body, body, Method};
use hyper::body::SizeHint;
use tokio::sync::{mpsc, oneshot};
use tokio::task::JoinHandle;
use tracing::info;

use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::config::ConfigDuration;
use crate::config::test::deserialize_from_toml;
use crate::gateway::handlers::{ConfigFetchProxy, FetchProxy};
use crate::gateway::handlers::fetch_proxy::ArgsFetchProxy;
use crate::guest::fetch_client::{FetchClient, FetchClientRecvError};
use crate::host::server::ChannelHandlerTester;
use crate::net::{BindMode, ConfigAddr, ConnectMode};
use crate::protobuf::fetch::{GatewayResponse, Header, ResponseBodyType};
use crate::protobuf::framed::{DeframerError, FrameReaderBufError, FrameReaderTimeout, LocalDeframerError, MAX_PAYLOAD_SIZE, RemoteFrameError, Writer, FrameStreamError};


const TIMEOUT_1S: Duration = Duration::from_secs(1);

fn timeout_now_1s() -> FrameReaderTimeout {
	FrameReaderTimeout::Now(TIMEOUT_1S)
}


#[test]
fn config() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigFetchProxy>(r#"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchProxy::default()));

	let config = deserialize_from_toml::<ConfigFetchProxy>(r#"
		connect_mode = "onlyipv6"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchProxy {
		connect_mode: Some(ConnectMode::OnlyIpv6),
		.. ConfigFetchProxy::default()
	}));

	let config = deserialize_from_toml::<ConfigFetchProxy>(r#"
		addr = "foo"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchProxy {
		addr: Some(ConfigAddr::new("foo", None)),
		.. ConfigFetchProxy::default()
	}));

	let config = deserialize_from_toml::<ConfigFetchProxy>(r#"
		timeout_guest_request = "5s"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchProxy {
		timeout_guest_request: Some(ConfigDuration::s(5)),
		.. ConfigFetchProxy::default()
	}));

	let config = deserialize_from_toml::<ConfigFetchProxy>(r#"
		timeout_idle_thread = "5s"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchProxy {
		timeout_idle_thread: Some(ConfigDuration::s(5)),
		.. ConfigFetchProxy::default()
	}));

	let config = deserialize_from_toml::<ConfigFetchProxy>(r#"
		timeout_http_request = "5s"
	"#).unwrap();
	assert_that!(&config, eq(ConfigFetchProxy {
		timeout_http_request: Some(ConfigDuration::s(5)),
		.. ConfigFetchProxy::default()
	}));
}


#[test]
fn to_args() {
	let _logging = init_test_logging();

	assert_that!(&ConfigFetchProxy::default().to_args().unwrap(), eq(ArgsFetchProxy::default()));
}


#[tokio::test]
async fn not_a_request() {
	let _logging = init_test_logging();

	let http_server = TestHttpServer::new(Router::new(), BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a gibberish message on the channel
	let guest = tester.connect_guest()
		.await;
	guest.io().channel().write(Bytes::from_static(&[1, 2, 3]))
		.await.unwrap();

	// wait for a response, an error about failing to decode a frame
	let e = guest.io().frame_reader().read_all(timeout_now_1s())
		.await
		.err()
		.unwrap();
	assert_that!(&e, structure!(FrameReaderBufError::Stream[
		structure!(FrameStreamError::Frame {
			local_error: is_match!(DeframerError::Remote(Ok(RemoteFrameError::Deframer(LocalDeframerError::Decode(..)))))
		})
	]));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_invalid() {
	let _logging = init_test_logging();

	let http_server = TestHttpServer::new(Router::new(), BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a properly framed stream that's a gibberish request
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.writer().write(vec![1, 2, 3])
		.await.unwrap();

	// wait for the response, an invalid request error
	let response = client.recv_response(TIMEOUT_1S)
		.await;
	assert_that!(&response, is_match!(Err(FetchClientRecvError::Gateway(GatewayResponse::InvalidRequest { .. }))));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_response_ipv6() {
	let _logging = init_test_logging();

	// start an HTTP server and a gateway
	async fn handler() -> impl IntoResponse {
		// send a response with no body
		Empty::new()
	}
	let router = Router::new()
		.route("/hi", get(handler));
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/hi", vec![], false)
		.await.unwrap();

	// wait for the response
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_response_ipv4() {
	let _logging = init_test_logging();

	// start an HTTP server and a gateway
	async fn handler() -> impl IntoResponse {
		// send a response with no body
		Empty::new()
	}
	let router = Router::new()
		.route("/hi", get(handler));
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/hi", vec![], false)
		.await.unwrap();

	// wait for the response
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_response_body_no_content_length() {
	let _logging = init_test_logging();

	// start an HTTP server and a gateway
	async fn handler() -> impl IntoResponse {
		AxumResponse::new(NoSizeBody::from(Body::from("hello")))
	}
	let router = Router::new()
		.route("/hi", get(handler));
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/hi", vec![], false)
		.await.unwrap();

	// wait for the response
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.body_type, eq(ResponseBodyType::SingleStream));

	// wait for the response body
	let response_body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();
	assert_that!(&String::from_utf8(response_body).unwrap(), eq("hello".to_string()));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_response_body() {
	let _logging = init_test_logging();

	// start an HTTP server and a gateway
	async fn handler() -> impl IntoResponse {
		"hello"
	}
	let router = Router::new()
		.route("/hi", get(handler));
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/hi", vec![], false)
		.await.unwrap();

	// wait for the response
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.body_type, eq(ResponseBodyType::SingleStream));

	// wait for the response body
	let response_body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();
	assert_that!(&String::from_utf8(response_body).unwrap(), eq("hello".to_string()));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_response_body_three() {
	let _logging = init_test_logging();

	// start an HTTP server and a gateway
	async fn handler() -> impl IntoResponse {
		vec![
			vec![1u8; MAX_PAYLOAD_SIZE],
			vec![2u8; MAX_PAYLOAD_SIZE],
			vec![3u8; MAX_PAYLOAD_SIZE],
		].concat()
	}
	let router = Router::new()
		.route("/hi", get(handler));
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/hi", vec![], false)
		.await.unwrap();

	// wait for the response
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.body_type, eq(ResponseBodyType::SingleStream));

	// wait for the response body
	let response_body = client.recv_body(TIMEOUT_1S)
		.await.unwrap();
	assert_that!(&response_body, eq(vec![
		vec![1u8; MAX_PAYLOAD_SIZE],
		vec![2u8; MAX_PAYLOAD_SIZE],
		vec![3u8; MAX_PAYLOAD_SIZE],
	].concat()));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_response_header() {
	let _logging = init_test_logging();

	// start an HTTP server and a gateway
	async fn handler() -> impl IntoResponse {
		let mut headers = HeaderMap::new();
		headers.insert("foo", "bar".parse().unwrap());
		headers
	}
	let router = Router::new()
		.route("/header", get(handler));
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/header", vec![], false)
		.await.unwrap();

	// wait for the response
	let response = client.recv_response(TIMEOUT_1S)
		.await.unwrap();

	assert_that!(&response.status, eq(200));
	assert_that!(&response.headers[0], eq(Header::new("foo", "bar")));
	assert_that!(&response.body_type, eq(ResponseBodyType::None));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request() {
	let _logging = init_test_logging();

	let (router, mut request_rx) = TestHttpServer::forwarding_router();
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request
	let guest = tester.connect_guest()
		.await;
	let client = FetchClient::new(guest.io().clone());
	client.send_request("get", "/forward", vec![], false)
		.await.unwrap();

	// wait for the forwarded request info
	let request_info = tokio::time::timeout(Duration::from_secs(1), request_rx.recv())
		.await.unwrap()
		.unwrap();

	assert_that!(&request_info.method, eq(Method::GET));
	assert_that!(&request_info.uri, eq("/forward".to_string()));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_header() {
	let _logging = init_test_logging();

	let (router, mut request_rx) = TestHttpServer::forwarding_router();
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request
	let guest = tester.connect_guest()
		.await;
	let client = FetchClient::new(guest.io().clone());
	let header = Header::new("foo", "bar");
	client.send_request("get", "/forward", vec![header.clone()], false)
		.await.unwrap();

	// wait for the forwarded request info
	let request_info = tokio::time::timeout(Duration::from_secs(1), request_rx.recv())
		.await.unwrap()
		.unwrap();

	let headers = request_info.headers.iter()
		.filter(Header::filter("foo"))
		.collect::<Vec<_>>();
	assert_that!(&headers, eq(vec![&header]));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_header_invalid_name() {
	let _logging = init_test_logging();

	let (router, _request_rx) = TestHttpServer::forwarding_router();
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request with an invalid header name
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	let header = Header::new("", "bar");
	client.send_request("get", "/forward", vec![header.clone()], false)
		.await.unwrap();

	// wait for the error response
	let response = client.recv_response(TIMEOUT_1S)
		.await;
	assert_that!(&response, is_match!(Err(FetchClientRecvError::Gateway(GatewayResponse::InvalidRequest { .. }))));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_header_invalid_value() {
	let _logging = init_test_logging();

	let (router, _request_rx) = TestHttpServer::forwarding_router();
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request with an invalid header value
	let guest = tester.connect_guest()
		.await;
	let mut client = FetchClient::new(guest.io().clone());
	let header = Header::new("foo", "\n");
	client.send_request("get", "/forward", vec![header.clone()], false)
		.await.unwrap();

	// wait for the error response
	let response = client.recv_response(TIMEOUT_1S)
		.await;
	assert_that!(&response, is_match!(Err(FetchClientRecvError::Gateway(GatewayResponse::InvalidRequest { .. }))));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_body_one() {
	let _logging = init_test_logging();

	let (router, mut request_rx) = TestHttpServer::forwarding_router();
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request with a body
	let guest = tester.connect_guest()
		.await;
	let client = FetchClient::new(guest.io().clone());
	client.send_request("post", "/forward", vec![], true)
		.await.unwrap();
	let body = vec![1, 2, 3];
	client.writer().write(body.clone())
		.await.unwrap();

	// wait for the forwarded request info
	let request_info = tokio::time::timeout(Duration::from_secs(1), request_rx.recv())
		.await.unwrap()
		.unwrap();
	assert_that!(&request_info.body, eq(body));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


#[tokio::test]
async fn request_body_three() {
	let _logging = init_test_logging();

	let (router, mut request_rx) = TestHttpServer::forwarding_router();
	let http_server = TestHttpServer::new(router, BindMode::OnlyIpv6)
		.await;
	let mut tester = ChannelHandlerTester::new(http_server.channel_handler())
		.await;

	// send a fetch request with a body
	let guest = tester.connect_guest()
		.await;
	let client = FetchClient::new(guest.io().clone());
	client.send_request("post", "/forward", vec![], true)
		.await.unwrap();
	let body = vec![
		vec![1u8; MAX_PAYLOAD_SIZE],
		vec![2u8; MAX_PAYLOAD_SIZE],
		vec![3u8; MAX_PAYLOAD_SIZE],
	].concat();
	client.writer().write(body.clone())
		.await.unwrap();

	// wait for the forwarded request info
	let request_info = tokio::time::timeout(Duration::from_secs(1), request_rx.recv())
		.await.unwrap()
		.unwrap();
	assert_that!(&request_info.body, eq(body));

	// cleanup
	guest.close()
		.await.unwrap();
	http_server.shutdown()
		.await;
}


struct TestHttpServer {
	addr: SocketAddr,
	shutdown_tx: oneshot::Sender<()>,
	handle: JoinHandle<()>
}

impl TestHttpServer {

	#[tracing::instrument(skip_all, level = 5, name = "TestHttpServer")]
	async fn new(router: Router, bind_mode: BindMode) -> Self {

		// get a bind address from the mode
		let ip_addr: IpAddr = match bind_mode {
			BindMode::Both => panic!("pick one: IPv4 or IPv6"),
			BindMode::OnlyIpv6 => Ipv6Addr::LOCALHOST.into(),
			BindMode::OnlyIpv4 => Ipv4Addr::LOCALHOST.into()
		};

		// start a TCP socket listener on an arbitrary port
		let server = axum::Server::bind(&SocketAddr::new(ip_addr, 0))
			.serve(router.into_make_service());
		let addr = server.local_addr();
		info!(%addr, "listening");

		// start the HTTP server asynchronously (otherwise the main thread will block it)
		let (shutdown_tx, shutdown_rx) = oneshot::channel::<()>();
		let handle = tokio::spawn(async move {

			// wire up the shutdown signal
			let server_finish = server.with_graceful_shutdown(async move {
				shutdown_rx
					.await.unwrap();
			});

			server_finish
				.await.unwrap();
		});

		Self {
			addr,
			shutdown_tx,
			handle
		}
	}

	fn channel_handler(&self) -> FetchProxy {

		let mut args_fetch = ArgsFetchProxy::default();
		args_fetch.connect_mode = match self.addr {
			SocketAddr::V6(_) => ConnectMode::OnlyIpv6,
			SocketAddr::V4(_) => ConnectMode::OnlyIpv4
		};
		args_fetch.addr = ConfigAddr::localhost_port(self.addr.port());

		FetchProxy::new(args_fetch)
	}

	fn forwarding_router() -> (Router, mpsc::UnboundedReceiver<RequestInfo>) {

		let (tx, rx) = mpsc::unbounded_channel::<RequestInfo>();

		async fn handler(
			State(tx): State<Arc<mpsc::UnboundedSender<RequestInfo>>>,
			request: AxumRequest<Body>
		) -> impl IntoResponse {

			let info = RequestInfo {
				method: request.method().clone(),
				uri: request.uri().to_string(),
				headers: request.headers().iter()
					.map(|(name, val)| {
						Header::new(name.as_str(), val.to_str().unwrap_or(""))
					})
					.collect(),
				body: body::to_bytes(request.into_body())
					.await
					.unwrap()
					.to_vec()
			};

			tx.send(info)
				.unwrap();

			Empty::new()
		}

		let router = Router::new()
			.route("/forward", get(handler).post(handler))
			.with_state(Arc::new(tx));

		(router, rx)
	}

	async fn shutdown(self) {
		self.shutdown_tx.send(())
			.unwrap();
		self.handle
			.await.unwrap();
	}
}


#[derive(Debug)]
struct RequestInfo {
	method: Method,
	uri: String,
	headers: Vec<Header>,
	body: Vec<u8>
}


struct NoSizeBody {
	body: Body
}

impl NoSizeBody {

	fn from(body: Body) -> Self {
		Self {
			body
		}
	}

	fn pin(&mut self) -> Pin<&mut Body> {
		Pin::new(&mut self.body)
	}
}

impl HttpBody for NoSizeBody {

	type Data = <Body as HttpBody>::Data;
	type Error = <Body as HttpBody>::Error;

	fn poll_data(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Result<Self::Data,Self::Error>>> {
		self.pin().poll_data(cx)
	}

	fn poll_trailers(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<Option<HeaderMap>,Self::Error>> {
		self.pin().poll_trailers(cx)
	}

	fn size_hint(&self) -> SizeHint {
		// override the regular size hint so axum won't send a content-length header
		// NOTE: the default value here is [0,inf)
		SizeHint::default()
	}
}
