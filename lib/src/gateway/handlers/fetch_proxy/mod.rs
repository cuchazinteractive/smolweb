
#[cfg(test)]
mod test;


use std::fmt::Debug;
use std::str::FromStr;
use std::time::Duration;

use anyhow::Result;
use axum::async_trait;
use axum::body::Body;
use display_error_chain::ErrorChainExt;
use axum::http::HeaderValue;
use bytes::Bytes;
use hyper::body::Sender;
use hyper::header::HeaderName;
use reqwest::header::HeaderMap;
use reqwest::{Method, Response};
use serde::Deserialize;
use thiserror::Error;
use tracing::{debug, error, info, warn};

use crate::config::ConfigDuration;
use crate::host::ChannelHandler;
use crate::host::fetch_server::{FetchRecvRequestError, FetchServer};
use crate::net::{ConfigAddr, ConnectMode};
use crate::protobuf::fetch::{GatewayResponse, Request, ResponseBodyType, ServerResponse};
use crate::protobuf::framed::{DeframerError, FrameEvent, FrameReaderBufError, NewFramerError, Reader, Writer, FrameWriteStreamError, FrameWriteError, FrameStreamError};
use crate::protobuf::smolweb::fetch::Header;
use crate::webrtc::channels::DataChannelReaderError;
use crate::webrtc::RTCDataChannelIO;


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigFetchProxy {
	pub connect_mode: Option<ConnectMode>,
	#[serde(deserialize_with = "ConfigAddr::deserialize")]
	#[serde(default = "ConfigAddr::deserialize_default")]
	pub addr: Option<ConfigAddr>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_guest_request: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_idle_thread: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_http_request: Option<ConfigDuration>
}

impl ConfigFetchProxy {

	pub fn to_args(self) -> Result<ArgsFetchProxy> {
		let default = ArgsFetchProxy::default();
		Ok(ArgsFetchProxy {
			connect_mode: self.connect_mode.unwrap_or(default.connect_mode),
			addr: self.addr.unwrap_or(default.addr),
			timeout_guest_request: self.timeout_guest_request
				.map(|t| t.to_duration())
				.unwrap_or(default.timeout_guest_request),
			timeout_idle_thread: self.timeout_idle_thread
				.map(|t| t.to_duration())
				.unwrap_or(default.timeout_idle_thread),
			timeout_http_request: self.timeout_http_request
				.map(|t| t.to_duration())
				.unwrap_or(default.timeout_http_request)
		})
	}
}

impl Default for ConfigFetchProxy {

	fn default() -> Self {
		Self {
			connect_mode: None,
			addr: None,
			timeout_guest_request: None,
			timeout_idle_thread: None,
			timeout_http_request: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsFetchProxy {
	pub connect_mode: ConnectMode,
	pub addr: ConfigAddr,
	pub timeout_guest_request: Duration,
	pub timeout_idle_thread: Duration,
	pub timeout_http_request: Duration
}

impl Default for ArgsFetchProxy {

	fn default() -> Self {

		let args = Self {
			connect_mode: ConnectMode::PreferIpv6,
			addr: ConfigAddr::localhost(),
			timeout_guest_request: Duration::from_secs(30),
			timeout_idle_thread: Duration::from_secs(30),
			timeout_http_request: Duration::from_secs(30)
		};

		#[cfg(test)]
		let args = Self {
			addr: ConfigAddr::localhost_auto_port(),
			timeout_guest_request: Duration::from_millis(200),
			timeout_http_request: Duration::from_millis(200),
			.. args
		};

		args
	}
}


pub struct FetchProxy {
	args: ArgsFetchProxy
}

impl FetchProxy {

	#[tracing::instrument(skip_all, level = 5, name = "FetchProxy")]
	pub fn new(args: ArgsFetchProxy) -> Self {

		info!(addr = %args.addr, connect_mode = %args.connect_mode, "proxying");

		Self {
			args
		}
	}
}


#[async_trait]
impl ChannelHandler for FetchProxy {

	#[tracing::instrument(skip_all, level = 5, name = "FetchProxy", fields(request_id))]
	async fn handle(&self, channel: RTCDataChannelIO) -> Result<()> {

		let args = self.args.clone();

		// log events
		let logging_span = tracing::Span::current();
		logging_span.record("request_id", rand::random::<u64>());
		debug!("handling channel");

		let mut server = FetchServer::new(channel);

		loop {

			// wait for the next fetch request
			let result = server.recv_request(args.timeout_guest_request)
				.await;
			match result {

				Ok(request) => {
					let result = dispatch(&args, &mut server, request)
						.await;
					if let Err(response) = result {
						server.respond_gateway(response)
							.await.ok();
					}
				}

				Err(FetchRecvRequestError::Read(FrameReaderBufError::Stream(FrameStreamError::Read(DataChannelReaderError::Closed)))) => {
					// WebRTC connection closed, stop listening
					debug!("channel closed");
					break;
				}

				Err(e) => {
					error!(err = %e.chain(), "Error receiving request");
					let response = GatewayResponse::InvalidRequest {
						reason: format!("{}", e.into_chain())
					};
					server.respond_gateway(response)
						.await.ok();

					// stop listening for more requests
					break;
				}
			}
		}

		Ok(())
	}
}


async fn dispatch<W,R>(args: &ArgsFetchProxy, server: &mut FetchServer<W,R>, request: Request) -> Result<(),GatewayResponse>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send,
		<R as Reader>::Error: 'static
{

	debug!(method = request.method, path = request.path, "Received request");

	// validate the method
	// WARNING: the HTTP crate requires uppercase method values,
	//          otherwise you get a Method instance that looks like Method:GET but doesn't equal it
	let method = Method::from_str(request.method.to_uppercase().as_str())
		.map_err(|_| GatewayResponse::InvalidRequest {
			reason: format!("invalid method: {}", request.method)
		})?;

	// validate the path
	if !request.path.starts_with('/') {
		return Err(GatewayResponse::InvalidRequest {
			reason: format!("paths must be absolute: {}", request.path)
		});
	}

	// validate the headers
	let mut headers = HeaderMap::new();
	for header in request.headers {

		// do the HTTP header parsing dance
		match (HeaderName::try_from(&header.name), HeaderValue::try_from(&header.value)) {
			(Ok(n), Ok(v)) =>
				headers.insert(n, v),
			(Err(e), Ok(_)) =>
				return Err(GatewayResponse::InvalidRequest {
					reason: format!("Invalid header name: {:?}\n{}", &header.name, e.into_chain())
				}),
			(Ok(_), Err(e)) =>
				return Err(GatewayResponse::InvalidRequest {
					reason: format!("Invalid header value: {:?}={:?}\n{}", &header.name, &header.value, e.into_chain())
				}),
			(Err(en), Err(ev)) =>
				return Err(GatewayResponse::InvalidRequest {
					reason: format!("Invalid header: {:?}={:?}\n{}\n{}", &header.name, &header.value, en.into_chain(), ev.into_chain())
				})
		};
	}

	// build the outgoing HTTP request
	let http_client = reqwest::Client::builder()
		.use_rustls_tls()
		.local_address(Some(args.connect_mode.localhost_ip()))
		.build()
		.map_err(|e| {
			error!(error = %e.into_chain(), "Failed to initialize reqwest client");
			GatewayResponse::InvalidRequest {
				reason: "Can't relay request, failed to initialize HTTP client".to_string(),
			}
		})?;
	let (body_to_server_tx, body_to_server) =
		if request.has_body {
			let (tx, body) = Body::channel();
			(Some(tx), body)
		} else {
			(None, Body::empty())
		};
	let request_to_server = http_client
		.request(method.clone(), format!("http://{}{}", args.addr.to_string(), request.path))
		.headers(headers)
		.body(body_to_server);

	// send the HTTP request, but on another task,
	// so we don't deadlock with this one while trying to stream the ougoing body from the guest
	let response_from_server = tokio::spawn(async move {
		request_to_server.send()
			.await
	});

	debug!(%method, body = body_to_server_tx.is_some(), "Sent proxy request to target HTTP server");

	// stream the outgoing body, if needed
	if let Some(body_tx) = body_to_server_tx {
		let result = send_body_guest_to_server(server, body_tx)
			.await;
		match result {

			Ok(_) => (),

			// failed to send the body to the HTTP server
			Err(SendBodyError::Send(_)) =>
				return Err(GatewayResponse::ServerUnreachable),

			// failed to read the body from the guest
			Err(e) =>
				return Err(GatewayResponse::InvalidRequest {
					reason: format!("Failed to read body: {}", e.into_chain())
				})
		}
	}

	// wait for the response from the HTTP server
	let result = tokio::time::timeout(args.timeout_http_request, response_from_server)
		.await;
	let response_from_server = match result {
		Ok(Ok(Ok(r))) => r,
		Ok(Ok(Err(e))) => {
			error!(err = %e.into_chain(), "Failed to send request to HTTP server");
			return Err(GatewayResponse::ServerUnreachable);
		}
		Ok(Err(e)) => {
			error!(err = %e.into_chain(), "Failed to wait for request to HTTP server to finish");
			return Err(GatewayResponse::ServerUnreachable);
		}
		Err(_) => {
			warn!("Timed out waiting for response from HTTP server");
			return Err(GatewayResponse::ServerUnreachable)
		}
	};

	// build the response to the guest
	let mut response_to_guest = ServerResponse {
		status: response_from_server.status().as_u16() as i32,
		headers: response_from_server.headers().iter()
			.map(|(name, val)| {
				Header::new(
					name.to_string(),
					String::from_utf8_lossy(val.as_bytes()).to_string()
				)
			})
			.collect(),
		body_type: ResponseBodyType::None // NOTE: will update later
	};
	debug!(status = response_to_guest.status, "Response from target HTTP server");

	// get the HTTP server body, if any
	let result = ResponseBodyInfo::from(response_from_server)
		.await;
	let body_info = match result {
		Ok(i) => {
			response_to_guest.body_type = i.body_type();
			i
		}
		Err(e) => {
			error!(err = %e.into_chain(), "Failed to buffer body from HTTP server");
			return Err(GatewayResponse::ServerResponseIncomplete);
		}
	};

	// send the response back
	let result = server.respond_server(response_to_guest)
		.await;
	match result {

		// send the response body
		Ok(Some(body_writer)) => {

			match body_info {

				ResponseBodyInfo::None => (), // nothing to do

				// send the buffered body
				ResponseBodyInfo::Buffer { buf } => {
					body_writer.write_all(buf)
						.await.ok();
					// NOTE: if this fails, there isn't much we can do execpt maybe log the error?
				}

				// stream the body from the HTTP server to the guest
				ResponseBodyInfo::SingleStream { response, size } => {
					let result = body_writer.write_stream(size, response.bytes_stream())
						.await;
					match result {
						Ok(()) => (), // all is well
						Err(FrameWriteStreamError::Write(FrameWriteError::NewFramer(NewFramerError::InvalidStreamSize { max, observed }))) => {
							warn!(size = observed, max, "Response body too large to stream");
							body_writer.abort()
								.await
								.ok();
						}
						Err(e) => {
							error!(err = %e.into_chain(), "Failed to stream body from HTTP server");
						}
					}
				}

				ResponseBodyInfo::_MultiStream { .. } => {
					warn!("MultiStream responses not implemented yet, this shouldn't be getting called");
				}
			}
		}

		Ok(None) => (), // nothing left to do

		// failed to send response back to client
		// not much else we can do other than stop processing the request
		Err(_) => return Ok(())
	};

	// sent the response back to the guest, and optionally the body too
	// finally, we're done. hooray!
	Ok(())
}


async fn send_body_guest_to_server<W,R>(server: &mut FetchServer<W,R>, mut tx: Sender) -> Result<(),SendBodyError<R::Error>>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{

	loop {

		let result = server.recv_body()
			.await;
		match result {

			// relay frames from the guest to the HTTP server
			Ok(FrameEvent::Start { payload, .. })
			| Ok(FrameEvent::Continue { payload, .. }) => {
				tx.send_data(Bytes::from(payload))
					.await?;
			}

			// last frame
			Ok(FrameEvent::Finish { payload, .. }) => {
				tx.send_data(Bytes::from(payload))
					.await?;
				return Ok(());
			}

			// failed to read body from guest, abort the request to the HTTP server
			Ok(FrameEvent::Error { local_error: e, .. }) => {
				tx.abort();
				return Err(SendBodyError::Deframe(e));
			}
			Err(e) => {
				tx.abort();
				return Err(SendBodyError::Read(e));
			}
		}
	}
}


#[derive(Error, Debug)]
enum SendBodyError<R>
	where
		R: std::error::Error + 'static
{

	#[error(transparent)]
	Deframe(#[from] DeframerError),

	#[error("Failed to read from underlying transport")]
	Read(#[source] R),

	#[error(transparent)]
	Send(#[from] hyper::Error)
}


#[derive(Debug)]
enum ResponseBodyInfo {

	None,

	Buffer {
		buf: Vec<u8>
	},

	SingleStream {
		response: Response,
		size: u64
	},

	_MultiStream {
		response: Response
	}
}

impl ResponseBodyInfo {

	async fn from(response: Response) -> Result<Self,reqwest::Error> {

		// TODO: support chunked transfers via ResponseBodyType::MultiStream

		match response.content_length() {

			// have size hint, can stream
			Some(size) => {
				if size > 0 {
					// has body, return the streamer
					Ok(Self::SingleStream {
						response,
						size
					})
				} else {
					// nothing to stream
					Ok(Self::None)
				}
			}

			// can't stream without a size hint since the framed protocol doesn't support unbounded streams,
			// so need to buffer the body to get the size
			_ => {

				let buf = response.bytes()
					.await?;

				if buf.is_empty() {
					Ok(Self::None)
				} else {
					Ok(Self::Buffer {
						buf: Vec::from(buf)
					})
				}
			}
		}
	}

	fn body_type(&self) -> ResponseBodyType {
		match self {
			Self::None => ResponseBodyType::None,
			Self::Buffer { .. } | Self::SingleStream { .. } => ResponseBodyType::SingleStream,
			Self::_MultiStream { .. } => ResponseBodyType::MultiStream
		}
	}
}
