use std::net::Ipv4Addr;
use std::sync::Arc;

use burgerid::{IdentityKeyApp, ProtobufSave};
use bytes::Bytes;
use galvanic_assert::{assert_that, matchers::*};
use temp_dir::TempDir;
use tokio::sync::RwLock;
use tracing::debug;

use smolweb_test_tools::is_match;
use smolweb_test_tools::id::app_id;
use smolweb_test_tools::logging::init_test_logging;

use crate::beacon::Beacon;
use crate::beacon::database::{ArgsDatabase, Database};
use crate::beacon::database::hosts::{Authz, Hosts};
use crate::beacon::guests::ArgsGuests;
use crate::beacon::guests::test::TestGuestsServer;
use crate::beacon::hosts::{ArgsHosts, HostsServer};
use crate::beacon::stun::{ArgsStun, StunServer};
use crate::beacon::tls::{ArgsTls, ArgsTlsAcme, TlsState};
use crate::config::ConfigDuration;
use crate::config::test::deserialize_from_toml;
use crate::gateway::*;
use crate::gateway::handlers::{ArgsEcho, ConfigEcho, ConfigFetchProxy, ConfigFetchStatic, ConfigHandlers};
use crate::guest::client::GuestsClient;
use crate::host::server::{ChannelHandlerTester, ConfigHostPorts};
use crate::net::{ConfigAddr, ConnectMode, FilterAddrs};
use crate::protobuf::framed::{MAX_MESSAGE_SIZE, Reader, Writer};
use crate::tls::test::acmed::Acmed;
use crate::tls::test::generate_domain_cert;
use crate::webrtc::connection::MapPortsOptions;


const TIMEOUT_2S: Duration = Duration::from_secs(2);


#[test]
fn config() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigGateway>(r#"
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway::default()));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[host]
		beacon_addr = "foo"
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		host: Some(ConfigHost {
			beacon_addr: Some(ConfigAddr::new("foo", None)),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[host]
		stun_addr = "bar"
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		host: Some(ConfigHost {
			stun_addr: Some(ConfigAddr::new("bar", None)),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[host]
		map_ports = false
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		host: Some(ConfigHost {
			map_ports: Some(ConfigHostPorts {
				enabled: Some(false),
				gateway_ip: None
			}),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[host]
		map_ports = true
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		host: Some(ConfigHost {
			map_ports: Some(ConfigHostPorts {
				enabled: Some(true),
				gateway_ip: None
			}),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[host]
		map_ports = { gateway_ip = "0.0.0.0" }
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		host: Some(ConfigHost {
			map_ports: Some(ConfigHostPorts {
				enabled: None,
				gateway_ip: Some("0.0.0.0".to_string())
			}),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[host]
		map_ports = { enabled = false, gateway_ip = "0.0.0.0" }
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		host: Some(ConfigHost {
			map_ports: Some(ConfigHostPorts {
				enabled: Some(false),
				gateway_ip: Some("0.0.0.0".to_string())
			}),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[handlers.echo]
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		handlers: Some(ConfigHandlers {
			echo: Some(ConfigEcho::default()),
			.. ConfigHandlers::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[handlers.fetch_proxy]
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		handlers: Some(ConfigHandlers {
			fetch_proxy: Some(ConfigFetchProxy::default()),
			.. ConfigHandlers::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[handlers.fetch_static]
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		handlers: Some(ConfigHandlers {
			fetch_static: Some(ConfigFetchStatic::default()),
			.. ConfigHandlers::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[host]
		timeout_ready = "5s"
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		host: Some(ConfigHost {
			timeout_ready: Some(ConfigDuration::s(5)),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	}));

	let config = deserialize_from_toml::<ConfigGateway>(r#"
		[host]
		timeout_ice_gathering = "42m"
	"#).unwrap();
	assert_that!(&config, eq(ConfigGateway {
		host: Some(ConfigHost {
			timeout_ice_gathering: Some(ConfigDuration::m(42)),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	}));
}


#[test]
fn to_args() {
	let _logging = init_test_logging();

	assert_that!(&ConfigGateway::default().to_args(), is_match!(Err(..)));

	let (app_id, app_key, _owner_id, _owner_key) = app_id("app");
	let connect_mode = ConnectMode::OnlyIpv6;
	let hosts_addr = ConfigAddr::new("hosts", None);
	let stun_addr = ConfigAddr::new("stun", None);

	// write the app bits to a tmp folder
	let tmpdir = TempDir::with_prefix("smolweb-")
		.unwrap();
	let app_id_path = tmpdir.child("app_id");
	std::fs::write(&app_id_path, app_id.save_string())
		.unwrap();
	let app_key_path = tmpdir.child("app_key");
	std::fs::write(&app_key_path, app_key.save_string())
		.unwrap();

	// make defaults
	let default_config = ConfigGateway {
		handlers: Some(ConfigHandlers {
			echo: Some(ConfigEcho {}),
			.. ConfigHandlers::default()
		}),
		host: Some(ConfigHost {
			app_id: Some(app_id_path.to_string_lossy().to_string()),
			app_key: Some(app_key_path.to_string_lossy().to_string()),
			connect_mode: Some(connect_mode),
			beacon_addr: Some(hosts_addr.clone()),
			stun_addr: Some(stun_addr.clone()),
			.. ConfigHost::default()
		}),
		.. ConfigGateway::default()
	};
	let default_args = ArgsGateway::default(
		ArgsChannelHandler::Echo(ArgsEcho {}),
		ArgsHost::default(app_id, app_key, connect_mode, hosts_addr, stun_addr)
	);

	assert_that!(&&default_config.clone().to_args().unwrap(), eq(&default_args));

	let config = ConfigGateway {
		host: Some(ConfigHost {
			map_ports: Some(ConfigHostPorts {
				enabled: Some(false),
				gateway_ip: None,
			}),
			.. default_config.host.as_ref().unwrap().clone()
		}),
		.. default_config.clone()
	};
	assert_that!(&config.to_args().unwrap(), eq(ArgsGateway {
		host: ArgsHost {
			map_ports: None,
			.. default_args.host.clone()
		},
		.. default_args.clone()
	}));

	let config = ConfigGateway {
		host: Some(ConfigHost {
			map_ports: Some(ConfigHostPorts {
				enabled: Some(true),
				gateway_ip: None,
			}),
			.. default_config.host.as_ref().unwrap().clone()
		}),
		.. default_config.clone()
	};
	assert_that!(&config.to_args().unwrap(), eq(ArgsGateway {
		host: ArgsHost {
			map_ports: Some(MapPortsOptions {
				gateway_ip: None
			}),
			.. default_args.host.clone()
		},
		.. default_args.clone()
	}));

	let config = ConfigGateway {
		host: Some(ConfigHost {
			map_ports: Some(ConfigHostPorts {
				enabled: Some(true),
				gateway_ip: Some("0.0.0.0".to_string()),
			}),
			.. default_config.host.as_ref().unwrap().clone()
		}),
		.. default_config.clone()
	};
	assert_that!(&config.to_args().unwrap(), eq(ArgsGateway {
		host: ArgsHost {
			map_ports: Some(MapPortsOptions {
				gateway_ip: Some(Ipv4Addr::new(0, 0, 0, 0).into())
			}),
			.. default_args.host.clone()
		},
		.. default_args.clone()
	}));
}


#[tokio::test]
async fn connection_refused() {
	let _logging = init_test_logging();

	// start a gateway server and try to connect to a non-existent beacon
	let (ida, keya, _idp, _keyp) = app_id("Test");
	let args_gateway = ArgsGateway::default(
		ArgsChannelHandler::Echo(ArgsEcho {}),
		ArgsHost::default(
			ida,
			keya,
			ConnectMode::PreferIpv6,
			ConfigAddr::localhost(),
			ConfigAddr::localhost()
		)
	);
	GatewayServer::start_connected(args_gateway, TIMEOUT_2S)
		.await
		.err()
		.unwrap();
}


#[tokio::test]
async fn connect() {
	let _logging = init_test_logging();

	// start a beacon server
	let args_stun = ArgsStun::default();
	let args_guests = ArgsGuests::default();
	let args_hosts = ArgsHosts::default();
	let args_tls = TestBeaconServer::args_tls_cert();
	let beacon_server = TestBeaconServer::new(&args_stun, &args_guests, &args_hosts, args_tls)
		.await;

	// create the app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	beacon_server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// start a gateway and connect to the beacon
	let args_gateway = beacon_server.args_gateway(app_id, app_key);
	let gateway_server = GatewayServer::start_connected(args_gateway, TIMEOUT_2S)
		.await.unwrap();

	gateway_server.shutdown()
		.await;
	beacon_server.shutdown(true)
		.await;
}


#[tokio::test]
async fn connect_reconnect() {
	let _logging = init_test_logging();

	// start a beacon server
	let args_stun = ArgsStun::default();
	let args_guests = ArgsGuests::default();
	let args_hosts = ArgsHosts::default();
	let args_tls = TestBeaconServer::args_tls_cert();
	let mut beacon_server = TestBeaconServer::new(&args_stun, &args_guests, &args_hosts, args_tls)
		.await;

	// create the app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	beacon_server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// start a gateway and connect to the beacon
	let args_gateway = beacon_server.args_gateway(app_id, app_key);
	let mut gateway_server = GatewayServer::start_connected(args_gateway, TIMEOUT_2S)
		.await.unwrap();

	// disconnect the gateway by closing all the connections
	beacon_server.hosts_server.disconnect_hosts()
		.await;
	tokio::time::timeout(TIMEOUT_2S, gateway_server.disconnected())
		.await.unwrap()
		.unwrap();

	// wait for the reconnect
	tokio::time::timeout(TIMEOUT_2S, gateway_server.connected())
		.await.unwrap()
		.unwrap();

	gateway_server.shutdown()
		.await;
	beacon_server.shutdown(true)
		.await;
}


#[tokio::test]
async fn guest_connect_cert() {
	let _logging = init_test_logging();

	// start a beacon server with a self-signed cert
	let args_stun = ArgsStun::default();
	let args_guests = ArgsGuests::default();
	let args_hosts = ArgsHosts::default();
	let args_tls = TestBeaconServer::args_tls_cert();
	let beacon_server = TestBeaconServer::new(&args_stun, &args_guests, &args_hosts, args_tls)
		.await;

	// create the app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	beacon_server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// start a gateway and connect to the beacon
	let args_gateway = beacon_server.args_gateway(app_id, app_key);
	let gateway_server = GatewayServer::start_connected(args_gateway, TIMEOUT_2S)
		.await.unwrap();

	// connect a guest and then disconnect
	let guest = beacon_server.guest_client(ConnectMode::PreferIpv6);
	let data_channel = guest.connect(&gateway_server.app_id())
		.await.unwrap();
	data_channel.close()
		.await;

	gateway_server.shutdown()
		.await;
	beacon_server.shutdown(true)
		.await;
}


#[tokio::test]
async fn guest_connect_acme() {
	let _logging = init_test_logging();

	let acmed = Acmed::start()
		.await;

	// start a beacon server with an ACME-issued cert
	let args_stun = ArgsStun::default();
	let args_guests = ArgsGuests::default();
	let args_hosts = ArgsHosts::default();
	let args_tls = TestBeaconServer::args_tls_acme(&acmed);
	let beacon_server = TestBeaconServer::new(&args_stun, &args_guests, &args_hosts, args_tls)
		.await;

	// create the app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	beacon_server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// start a gateway and connect to the beacon
	let args_gateway = beacon_server.args_gateway(app_id, app_key);
	let gateway_server = GatewayServer::start_connected(args_gateway, TIMEOUT_2S)
		.await.unwrap();

	// connect a guest and then disconnect
	let guest = beacon_server.guest_client(ConnectMode::PreferIpv6);
	let data_channel = guest.connect(&gateway_server.app_id())
		.await.unwrap();
	data_channel.close()
		.await;

	gateway_server.shutdown()
		.await;
	beacon_server.shutdown(true)
		.await;
	acmed.shutdown()
		.await;
}


#[tokio::test]
async fn channel_echo() {
	let _logging = init_test_logging();

	// start a beacon server
	let args_stun = ArgsStun::default();
	let args_guests = ArgsGuests::default();
	let args_hosts = ArgsHosts::default();
	let args_tls = TestBeaconServer::args_tls_cert();
	let beacon_server = TestBeaconServer::new(&args_stun, &args_guests, &args_hosts, args_tls)
		.await;

	// create the app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	beacon_server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// start a gateway and connect to the beacon
	let mut args_gateway = beacon_server.args_gateway(app_id, app_key);
	args_gateway.channel_handler = ArgsChannelHandler::Echo(ArgsEcho {});
	let gateway_server = GatewayServer::start_connected(args_gateway, TIMEOUT_2S)
		.await.unwrap();

	// connect a guest
	let guest = beacon_server.guest_client(ConnectMode::PreferIpv6);
	let data_channel = guest.connect(&gateway_server.app_id())
		.await.unwrap();

	// send a short string message on the channel
	let msg_string = "Hello World!".to_string();
	data_channel.io().write(msg_string.as_str())
		.await.unwrap();

	// send a long data message
	let msg_data = vec![5u8; MAX_MESSAGE_SIZE];
	data_channel.io().write(Bytes::from(msg_data.clone()))
		.await.unwrap();

	// wait for the echos
	let echo = data_channel.io().read()
		.await.unwrap();
	assert_that!(&String::from_utf8_lossy(&echo).to_string(), eq(msg_string));

	let echo = data_channel.io().read()
		.await.unwrap();
	assert_that!(&echo, eq(msg_data));

	// cleanup
	gateway_server.shutdown()
		.await;
	data_channel.close()
		.await;
	beacon_server.shutdown(true)
		.await;
}


#[tokio::test]
async fn local_channel_echo() {
	let _logging = init_test_logging();

	let mut tester = ChannelHandlerTester::new(Echo::new(ArgsEcho {}))
		.await;

	// connect a guest
	let guest = tester.connect_guest()
		.await;

	// send a short string message on the channel
	let msg_string = "Hello World!".to_string();
	guest.io().write(msg_string.as_str())
		.await.unwrap();

	// send a long data message
	let msg_data = vec![5u8; MAX_MESSAGE_SIZE];
	guest.io().write(Bytes::from(msg_data.clone()))
		.await.unwrap();

	// wait for the echos
	let echo = guest.io().read()
		.await.unwrap();
	assert_that!(&String::from_utf8_lossy(&echo).to_string(), eq(msg_string));

	let echo = guest.io().read()
		.await.unwrap();
	assert_that!(&echo, eq(msg_data));

	// cleanup
	guest.close()
		.await.unwrap();
}


pub struct TestBeaconServer {
	beacon: Arc<RwLock<Beacon>>,
	pub tls_state: TlsState,
	pub stun_server: StunServer,
	pub guests_server: TestGuestsServer,
	pub hosts_server: HostsServer
}

impl TestBeaconServer {

	pub fn args_tls_acme(acmed: &Acmed) -> ArgsTls {
		ArgsTls::Acme(ArgsTlsAcme {
			domains: vec!["test.beacon.localhost".to_string()],
			emails: vec!["me@domain.tld".to_string()],
			directory: acmed.directory(),
			timeout_cert: ArgsTlsAcme::DEFAULT_TIMEOUT_CERT
		})
	}

	pub fn args_tls_cert() -> ArgsTls {
		ArgsTls::Cert(
			generate_domain_cert("test.beacon.localhost")
				.to_tls_args()
		)
	}

	pub async fn new(args_stun: &ArgsStun, args_guests: &ArgsGuests, args_hosts: &ArgsHosts, args_tls: ArgsTls) -> Self {

		let owner_uid = vec![1, 2, 3];
		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let database = Arc::new(database);
		let beacon = Arc::new(RwLock::new(Beacon::new(owner_uid, args_hosts, database.clone())));

		let stun_server = StunServer::start_udp(args_stun.clone())
			.await.unwrap();
		let tls_state = args_tls.state(database.clone());
		let guests_server = TestGuestsServer::start(beacon.clone(), database.clone(), args_guests, &tls_state)
			.await;
		let hosts_server = HostsServer::start(args_hosts.clone(), beacon.clone(), database, &tls_state)
			.await.unwrap();

		Self {
			beacon,
			tls_state,
			stun_server,
			guests_server,
			hosts_server,
		}
	}

	pub fn stun_addr(&self, connect_mode: ConnectMode) -> ConfigAddr {
		let host = connect_mode.localhost_ip()
			.to_string();
		let port = self.stun_server.addrs()
			.filter_addrs_for_connect(connect_mode)
			.into_iter()
			.next()
			.unwrap()
			.port();
		ConfigAddr::new(host, Some(port))
	}

	pub fn hosts_addr(&self, connect_mode: ConnectMode) -> ConfigAddr {
		let port = self.hosts_server.addrs()
			.filter_addrs_for_connect(connect_mode)
			.into_iter()
			.next()
			.unwrap()
			.port();
		ConfigAddr::new(self.tls_state.any_domain(), Some(port))
	}

	pub fn args_gateway(&self, app_id: IdentityApp, app_key: IdentityKeyApp) -> ArgsGateway {
		let connect_mode = ConnectMode::PreferIpv6;
		let args = ArgsGateway::default(
			ArgsChannelHandler::Echo(ArgsEcho {}),
			ArgsHost::default(
				app_id,
				app_key,
				connect_mode,
				self.hosts_addr(connect_mode),
				self.stun_addr(connect_mode)
			)
		);
		debug!(hosts_addr = %args.host.hosts_addr, stun_addr = %args.host.stun_addr, "Made ArgsGateway for test beacon server");
		args
	}

	pub fn hosts(&self) -> Hosts {
		self.hosts_server.hosts()
	}

	pub fn guest_client(&self, connect_mode: ConnectMode) -> GuestsClient {
		self.guests_server.client(Some(&self.stun_server), connect_mode)
	}

	pub async fn shutdown(mut self, wait_for_hosts_to_leave: bool) -> Beacon {
		self.stun_server.shutdown()
			.await;
		self.guests_server.shutdown()
			.await;
		if !wait_for_hosts_to_leave {
			self.hosts_server.disconnect_hosts()
				.await;
		}
		self.hosts_server.connections_finished()
			.await;
		self.hosts_server.shutdown()
			.await;
		Arc::into_inner(self.beacon)
			.expect("beacon reference leak")
			.into_inner()
	}
}
