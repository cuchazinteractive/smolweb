
mod handlers;


#[cfg(test)]
pub(crate) mod test;


use std::time::Duration;

use anyhow::{Context, Result};
use burgerid::IdentityApp;
use serde::Deserialize;
use tracing::{info, warn};

use crate::gateway::handlers::{ArgsChannelHandler, ConfigHandlers, Echo, FetchProxy, FetchStatic};
use crate::config::ToArgs;
use crate::host::server::{ArgsHost, ConfigHost, Host};


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigGateway {
	pub log: Option<String>,
	pub handlers: Option<ConfigHandlers>,
	pub host: Option<ConfigHost>
}


impl ToArgs for ConfigGateway {

	type Args = ArgsGateway;

	fn to_args(self) -> Result<ArgsGateway> {
		Ok(ArgsGateway {
			log: self.log.unwrap_or(ArgsGateway::DEFAULT_LOG.to_string()),
			channel_handler: self.handlers
				.context("handlers is a required section")?
				.to_args()
				.context("Failed to read channel_handler")?,
			host: self.host
				.context("host is a required section")?
				.to_args()
				.context("Failed to read host configuration")?
		})
	}
}

impl Default for ConfigGateway {

	fn default() -> Self {
		Self {
			log: None,
			handlers: None,
			host: None
		}
	}
}


#[derive(Debug, Clone)]
pub struct ArgsGateway {
	pub log: String,
	pub channel_handler: ArgsChannelHandler,
	pub host: ArgsHost
}

impl ArgsGateway {

	pub const DEFAULT_LOG: &'static str =
		if cfg!(test) {
			"smolweb=trace"
		} else {
			"smolweb=info"
		};

	#[cfg(test)]
	pub fn default(channel_handler: ArgsChannelHandler, host: ArgsHost) -> Self {
		Self {
			log: Self::DEFAULT_LOG.to_string(),
			channel_handler,
			host
		}
	}
}

impl PartialEq for ArgsGateway {

	fn eq(&self, other: &Self) -> bool {
		self.log == other.log
			&& self.channel_handler == other.channel_handler
			&& self.host == other.host
	}
}


pub struct GatewayServer {
	args: ArgsGateway,
	host: Host
}

impl GatewayServer {

	#[tracing::instrument(skip_all, level = 5, name = "Gateway")]
	pub fn start(args: ArgsGateway) -> Self {

		let args_host = args.host.clone();

		// make the channel handler and start the host
		let host = match &args.channel_handler {
			ArgsChannelHandler::Echo(args) => Host::start(args_host, Echo::new(args.clone())),
			ArgsChannelHandler::FetchProxy(args) => Host::start(args_host, FetchProxy::new(args.clone())),
			ArgsChannelHandler::FetchStatic(args) => Host::start(args_host, FetchStatic::new(args.clone()))
		};

		Self {
			args,
			host
		}
	}

	pub async fn start_connected(args: ArgsGateway, timeout: Duration) -> Result<Self> {
		let mut s = Self::start(args);
		tokio::time::timeout(timeout, s.connected())
			.await??;
		Ok(s)
	}

	/// waits until the next connection to the beacon is established
	pub async fn connected(&mut self) -> Result<()> {
		self.host.connected()
			.await
	}

	/// waits until the current connection to the beacon is closed
	pub async fn disconnected(&mut self) -> Result<()> {
		self.host.disconnected()
			.await
	}

	/// sends a shutdown signal to the server and waits for it to stop
	#[tracing::instrument(skip_all, level = 5, name = "Gateway")]
	pub async fn shutdown(self) {

		info!("shutting down ...");

		self.host.shutdown()
			.await;

		info!("finished");
	}

	pub fn app_id(&self) -> &IdentityApp {
		&self.args.host.app_id
	}
}
