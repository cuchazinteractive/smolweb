
use std::net::IpAddr;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{anyhow, Context, Result};
use burgerid::{IdentityApp, IdentityKeyApp, ProtobufOpen};
use display_error_chain::ErrorChainExt;
use serde::Deserialize;

use tokio::sync::{Mutex, oneshot};
use tokio::task::JoinHandle;
use tokio::time::timeout;
use tracing::{debug, error, error_span, info, Instrument, warn};

use crate::config::{ConfigDuration, ToArgs};
use crate::host::client::{ArgsHostsClient, HostsClient, HostsClientMsgError};
use crate::host::exponential_backoff::ExponentialBackoff;
use crate::host::beacon_listener::{ChannelHandler, BeaconListener};
use crate::lang::ErrorReporting;
use crate::net::{ConfigAddr, ConnectMode};
use crate::net::codec::RecvError;
use crate::serde::OptBoolOrStruct;
use crate::tls::{ArgsTlsClient, ConfigTlsClient};
use crate::webrtc::connection::MapPortsOptions;
use crate::webrtc::{OpenDataChannel, RTCDataChannelIO};


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigHost {
	pub app_id: Option<String>,
	pub app_key: Option<String>,
	pub connect_mode: Option<ConnectMode>,
	#[serde(deserialize_with = "ConfigAddr::deserialize")]
	#[serde(default = "ConfigAddr::deserialize_default")]
	pub beacon_addr: Option<ConfigAddr>,
	#[serde(deserialize_with = "ConfigAddr::deserialize")]
	#[serde(default = "ConfigAddr::deserialize_default")]
	pub stun_addr: Option<ConfigAddr>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_ready: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_ice_gathering: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_guest_connect: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_guests_shutdown: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub reconnect_first_wait: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub reconnect_max_wait: Option<ConfigDuration>,
	pub tls: Option<ConfigTlsClient>,
	#[serde(deserialize_with = "OptBoolOrStruct::<ConfigHostPorts>::deserialize")]
	#[serde(default = "OptBoolOrStruct::<ConfigHostPorts>::default")]
	pub map_ports: Option<ConfigHostPorts>
}


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigHostPorts {
	pub enabled: Option<bool>,
	pub gateway_ip: Option<String>
}

impl From<bool> for ConfigHostPorts {

	fn from(value: bool) -> Self {
		Self {
			enabled: Some(value),
			gateway_ip: None
		}
	}
}


impl ToArgs for ConfigHost {

	type Args = ArgsHost;

	fn to_args(self) -> Result<ArgsHost> {
		let beacon_addr = self.beacon_addr
			.context("beacon_addr is required")?;
		let stun_addr = self.stun_addr
			// use the same beacon address, but ignore any port
			.unwrap_or_else(|| ConfigAddr::from_host(&beacon_addr.host));
		Ok(ArgsHost {
			app_id: {
				let path = self.app_id
					.context("app_id is required")?;
				let content = std::fs::read_to_string(&path)
					.context(format!("Failed to read {}", path))?;
				let id = IdentityApp::open(content)
					.context(format!("Failed to App ID from {}", path))?;
				id
			},
			app_key: {
				let path = self.app_key
					.context("app_key is required")?;
				let content = std::fs::read_to_string(&path)
					.context(format!("Failed to read {}", path))?;
				let key = IdentityKeyApp::open(content)
					.context(format!("Failed to App ID key from {}", path))?;
				key
			},
			connect_mode: self.connect_mode
				.unwrap_or(ConnectMode::PreferIpv6),
			hosts_addr: beacon_addr,
			stun_addr,
			timeout_ready: self.timeout_ready
				.map(|t| t.to_duration())
				.unwrap_or(ArgsHost::DEFAULT_TIMEOUT_READY),
			timeout_ice_gathering: self.timeout_ice_gathering
				.map(|t| t.to_duration())
				.unwrap_or(ArgsHost::DEFAULT_TIMEOUT_ICE_GATHERING),
			timeout_guest_connect: self.timeout_guest_connect
				.map(|t| t.to_duration())
				.unwrap_or(ArgsHost::DEFAULT_TIMEOUT_GUEST_CONNECT),
			timeout_guests_shutdown: self.timeout_guests_shutdown
				.map(|t| t.to_duration())
				.unwrap_or(ArgsHost::DEFAULT_TIMEOUT_GUESTS_SHUTDOWN),
			reconnect_first_wait: self.reconnect_first_wait
				.map(|t| t.to_duration())
				.unwrap_or(ArgsHost::DEFAULT_RECONNECT_FIRST_WAIT),
			reconnect_max_wait: self.reconnect_max_wait
				.map(|t| t.to_duration())
				.unwrap_or(ArgsHost::DEFAULT_RECONNECT_MAX_WAIT),
			tls: match self.tls {
				None => ArgsTlsClient::default(),
				Some(c) => c.to_args()
					.context("Failed to read TLS client configuration")?
			},
			map_ports: {
				let map_ports = self.map_ports
					.unwrap_or(ConfigHostPorts {
						enabled: Some(true), // port mapping is on by default
						gateway_ip: None,
					});
				if map_ports.enabled == Some(true) {
					Some(MapPortsOptions {
						gateway_ip: match map_ports.gateway_ip {
							None => None,
							Some(ip) => {
								let ip = IpAddr::from_str(ip.as_str())
									.context(format!("Failed to parse gateway ip: {}", ip))?;
								Some(ip)
							}
						}
					})
				} else {
					None
				}
			}
		})
	}
}

impl Default for ConfigHost {

	fn default() -> Self {
		Self {
			app_id: None,
			app_key: None,
			connect_mode: None,
			beacon_addr: None,
			stun_addr: None,
			timeout_ready: None,
			timeout_ice_gathering: None,
			timeout_guest_connect: None,
			timeout_guests_shutdown: None,
			reconnect_first_wait: None,
			reconnect_max_wait: None,
			tls: None,
			map_ports: None
		}
	}
}


#[derive(Debug, Clone)]
pub struct ArgsHost {
	pub app_id: IdentityApp,
	pub app_key: IdentityKeyApp,
	pub connect_mode: ConnectMode,
	pub hosts_addr: ConfigAddr,
	pub stun_addr: ConfigAddr,
	pub timeout_ready: Duration,
	pub timeout_ice_gathering: Duration,
	pub timeout_guest_connect: Duration,
	pub timeout_guests_shutdown: Duration,
	pub reconnect_first_wait: Duration,
	pub reconnect_max_wait: Duration,
	pub tls: ArgsTlsClient,
	pub map_ports: Option<MapPortsOptions>
}


impl ArgsHost {

	pub const DEFAULT_TIMEOUT_READY: Duration =
		if cfg!(test) {
			Duration::from_millis(100)
		} else {
			Duration::from_secs(10)
		};

	pub const DEFAULT_TIMEOUT_ICE_GATHERING: Duration =
		if cfg!(test) {
			Duration::from_millis(200)
		} else {
			Duration::from_secs(5)
		};

	pub const DEFAULT_TIMEOUT_GUEST_CONNECT: Duration =
		if cfg!(test) {
			Duration::from_millis(1_000)
		} else {
			Duration::from_secs(15)
		};

	pub const DEFAULT_TIMEOUT_GUESTS_SHUTDOWN: Duration =
		if cfg!(test) {
			Duration::from_millis(500)
		} else {
			Duration::from_secs(5)
		};

	pub const DEFAULT_RECONNECT_FIRST_WAIT: Duration =
		if cfg!(test) {
			Duration::from_millis(200)
		} else {
			Duration::from_secs(5)
		};

	pub const DEFAULT_RECONNECT_MAX_WAIT: Duration =
		if cfg!(test) {
			Duration::from_millis(1_000)
		} else {
			Duration::from_secs(15*60)
		};

	pub fn default(app_id: IdentityApp, app_key: IdentityKeyApp, connect_mode: ConnectMode, hosts_addr: ConfigAddr, stun_addr: ConfigAddr) -> Self {
		Self {
			app_id,
			app_key,
			connect_mode,
			hosts_addr,
			stun_addr,
			timeout_ready: Self::DEFAULT_TIMEOUT_READY,
			timeout_ice_gathering: Self::DEFAULT_TIMEOUT_ICE_GATHERING,
			timeout_guest_connect: Self::DEFAULT_TIMEOUT_GUEST_CONNECT,
			timeout_guests_shutdown: Self::DEFAULT_TIMEOUT_GUESTS_SHUTDOWN,
			reconnect_first_wait: Self::DEFAULT_RECONNECT_FIRST_WAIT,
			reconnect_max_wait: Self::DEFAULT_RECONNECT_MAX_WAIT,
			tls: ArgsTlsClient::default(),
			map_ports: None
		}
	}
}

impl PartialEq for ArgsHost {

	fn eq(&self, other: &Self) -> bool {
		self.app_id == other.app_id
			// skip app_key, since that needs ct_eq to compare
			&& self.connect_mode == other.connect_mode
			&& self.hosts_addr == other.hosts_addr
			&& self.stun_addr == other.stun_addr
			&& self.timeout_ready == other.timeout_ready
			&& self.timeout_ice_gathering == other.timeout_ice_gathering
			&& self.timeout_guest_connect == other.timeout_guest_connect
			&& self.timeout_guests_shutdown == other.timeout_guests_shutdown
			&& self.reconnect_first_wait == other.reconnect_first_wait
			&& self.reconnect_max_wait == other.reconnect_max_wait
	}
}


pub struct Host {
	args: ArgsHost,
	shutdown_tx: oneshot::Sender<()>,
	connection_manager: JoinHandle<()>,
	state: Arc<Mutex<State>>
}

impl Host {

	#[tracing::instrument(skip_all, level = 5, name = "Host")]
	pub fn start<H>(args: ArgsHost, channel_handler: H) -> Self
		where
			H: ChannelHandler + Send + Sync + 'static
	{

		// init state
		let state = Arc::new(Mutex::new(State::new()));

		let channel_handler = Arc::new(channel_handler);

		// start a task to manage the connection to the beacon server
		let (shutdown_tx, mut shutdown_rx) = oneshot::channel();
		let connection_manager = tokio::spawn({
			let args = args.clone();
			let state = state.clone();
			async move {

				let mut exponential_backoff = ExponentialBackoff::new(
					args.reconnect_first_wait.as_millis() as u64,
					args.reconnect_max_wait.as_millis() as u64
				);
				let mut is_reconnect = false;

				// keep trying to connect until we get shut down
				loop {

					if is_reconnect {

						// wait a bit before trying again
						let current_backoff_ms = exponential_backoff.current();
						let wait_ms = exponential_backoff.sample_current()
							.context("Failed to sample current exponential backoff, using fallback wait time")
							.warn_err()
							.unwrap_or(Duration::from_secs(5*60).as_millis() as u64);
						let wait_time = Duration::from_secs(wait_ms.div_ceil(1000));
						info!("Waiting {:?} (of {:?}) to connect again ...", &wait_time, Duration::from_secs(current_backoff_ms.div_ceil(1000)));

						tokio::select! {
							_ = tokio::time::sleep(wait_time) => (),
							_ = &mut shutdown_rx => {
								info!("Received shutdown signal, aborting reconnect");
								break;
							}
						}
					}

					// start the hosts client
					let args_client = ArgsHostsClient {
						app_id: &args.app_id,
						app_key: &args.app_key,
						connect_mode: args.connect_mode.clone(),
						addr: args.hosts_addr.clone(),
						timeout_ready: args.timeout_ready.clone(),
						tls: args.tls.clone()
					};

					// try to connect
					info!("connecting to beacon at {}", &args.hosts_addr);
					let client = tokio::select! {

						result = HostsClient::connect(args_client) => {
							match result {

								// goddem!
								Ok(client) => {

									// connected successfully, reset the wait time
									info!("connected to beacon at {}", client.remote_addr());
									exponential_backoff.reset();

									client
								}

								// if the beacon server explicitly said no, then stop trying to reconnect
								Err(e) if !e.is_recoverable() => {
									Err::<(),_>(e)
										.context("Beacon server rejected connection attempt")
										.warn_err()
										.ok();
									break;
								}

								// otherwise, keep trying
								Err(e) => {

									Err::<(),_>(e)
										.context("Failed to connect to hosts server at beacon")
										.warn_err()
										.ok();

									// wait longer next time
									exponential_backoff.failed()
										.context("Failed to increase exponential backoff")
										.warn_err()
										.ok();

									is_reconnect = true;
									continue;
								}
							}
						}

						_ = &mut shutdown_rx => {
							info!("Received shutdown signal, aborting connection attempts");
							break;
						}
					};

					// connected, update connection state
					state.lock()
						.await
						.connected(true);

					// all good, start listening on the connection
					let result = listen_to_beacon(&args, channel_handler.clone(), client, &mut shutdown_rx)
						.await;

					info!("disconnected from beacon");

					// disconnected, update connection state
					state.lock()
						.await
						.connected(false);

					match result {
						ListenResult::Reconnect => {
							is_reconnect = true;
							continue;
						}
						ListenResult::Shutdown => break
					}
				}
			}
		}.in_current_span());

		Self {
			args,
			shutdown_tx,
			connection_manager,
			state
		}
	}

	// TODO: thiserr errors?
	/// waits until the next connection to the beacon is established
	pub async fn connected(&mut self) -> Result<()> {

		let rx = {
			let mut state = self.state.lock()
				.await;

			if state.connected_to_beacon {
				// already connected
				return Ok(());
			}

			// not connected, wait
			let (tx, rx) = oneshot::channel();
			state.connection_txs.push(tx);

			rx
		};

		rx
			.await
			.context("Failed to wait for connection signal")?;

		Ok(())
	}

	// TODO: thiserr errors?
	/// waits until the current connection to the beacon is closed
	pub async fn disconnected(&mut self) -> Result<()> {

		let rx = {
			let mut state = self.state.lock()
				.await;

			if !state.connected_to_beacon {
				// already disconnected
				return Ok(());
			}

			// connected, wait
			let (tx, rx) = oneshot::channel();
			state.disconnection_txs.push(tx);

			rx
		};

		rx
			.await
			.context("Failed to wait for disconnection signal")?;

		Ok(())
	}

	/// sends a shutdown signal to the server and waits for it to stop
	#[tracing::instrument(skip_all, level = 5, name = "Host")]
	pub async fn shutdown(self) {

		// shutdown the connection to the hosts server at the beacon
		if !self.shutdown_tx.is_closed() {

			if let Err(_) = self.shutdown_tx.send(()) {
				warn!("Failed to send shutdown signal");
			}

			let result = self.connection_manager
				.await;
			if let Err(e) = result {
				warn!(cause = %e.into_chain(), "Failed to wait for server to shutdown");
			}
		}

		// kick out all the guests
	}

	pub fn host_id(&self) -> &IdentityApp {
		&self.args.app_id
	}
}


struct State {
	connected_to_beacon: bool,
	connection_txs: Vec<oneshot::Sender<bool>>,
	disconnection_txs: Vec<oneshot::Sender<bool>>
}

impl State {

	fn new() -> Self {
		Self {
			connected_to_beacon: false,
			connection_txs: vec![],
			disconnection_txs: vec![]
		}
	}

	fn connected(&mut self, connected: bool) {

		self.connected_to_beacon = connected;

		// send out signals
		let txs =
			if connected {
				self.connection_txs.drain(..)
			} else {
				self.disconnection_txs.drain(..)
			};
		for tx in txs {
			tx.send(connected)
				.map_err(|_| anyhow!("Failed to send connection({}) signal to waiter", connected))
				.warn_err()
				.ok();
		}
	}
}


enum ListenResult {
	Reconnect,
	Shutdown
}


async fn listen_to_beacon<H>(
	args: &ArgsHost,
	channel_handler: Arc<H>,
	mut client: HostsClient,
	mut shutdown_rx: &mut oneshot::Receiver<()>
) -> ListenResult
	where
		H: ChannelHandler + Send + Sync + 'static
{

	let listener = BeaconListener::new(args.clone());

	debug!("listening for incoming connections");

	// keep the connection open
	let result = loop {

		// listen for messages from the hosts server at the beacon
		let msg = tokio::select! {
			msg = client.recv_message() => msg,
			_ = &mut shutdown_rx => {
				info!("Received shutdown signal, shutting down connection listener");
				break ListenResult::Shutdown;
			}
		};
		match msg {

			// connection closed by server
			Err(HostsClientMsgError::Recv(RecvError::Closed)) => {
				warn!("Connection to beacon server closed remotely");
				break ListenResult::Reconnect;
			}

			Err(e) => {
				error!(err = %e.into_chain(), "Error listening to beacon server");
				break ListenResult::Reconnect;
			}

			// forward messages to the listener
			Ok(request) => {
				listener.handle(&args, &channel_handler, &mut client, request)
					.await;
			}
		}
	};

	debug!("stopped listening for connections");

	// cleanup the listener
	listener.disconnect_guests()
		.await;
	tokio::time::timeout(args.timeout_guests_shutdown, listener.wait_for_guests())
		.await
		.context("Timed out waiting for guests to shutdown")
		.warn_err()
		.ok();

	result
}


pub struct ChannelHandlerTester<H> {
	handler: Arc<H>,
	next_guest_id: u32,
	pub timeout: Duration
}

impl<H> ChannelHandlerTester<H>
	where
		H: ChannelHandler + Send + Sync + 'static
{

	pub async fn new(handler: H) -> Self {
		Self {
			handler: Arc::new(handler),
			next_guest_id: 1,
			timeout: Duration::from_millis(1000)
		}
	}

	#[tracing::instrument(skip_all, level = 5, name = "ChannelHandlerTester", fields(guest_id))]
	pub async fn connect_guest(&mut self) -> ConnectedGuest {

		let (guest_channel, host_channel) = crate::webrtc::connect_local()
			.await.unwrap();

		// pick a guest id
		let guest_id = self.next_guest_id;
		self.next_guest_id += 1;
		let span = tracing::span::Span::current();
		span.record("guest_id", guest_id);
		debug!("Connected guest");

		// create the host task to drive the connection
		let host_task = tokio::spawn({
			let handler = self.handler.clone();
			let host_io = host_channel.io().clone();
			async move {

				debug!("Handling");
				let result = handler.handle(host_io.clone())
					.await
					.context("Failed to handle channel");
				debug!("Handled: {:?}", &result);

				result
			}.instrument(error_span!("Host"))
		});

		ConnectedGuest {
			guest_id,
			guest_channel,
			host_task,
			host_channel,
			timeout: self.timeout
		}
	}
}


pub struct ConnectedGuest {
	guest_id: u32,
	guest_channel: OpenDataChannel,
	host_task: JoinHandle<Result<()>>,
	host_channel: OpenDataChannel,
	timeout: Duration
}

impl ConnectedGuest {

	pub fn io(&self) -> &RTCDataChannelIO {
		&self.guest_channel.io()
	}

	#[tracing::instrument(skip_all, level = 5, name = "ChannelHandlerTester", fields(guest_id))]
	pub async fn close(self) -> Result<()> {

		let span = tracing::span::Span::current();
		span.record("guest_id", self.guest_id);
		debug!("Disconnecting guest");

		// flush the channels
		self.guest_channel.io().flush(self.timeout)
			.await
			.map_err(|()| anyhow!("Timed out waiting {:?} for guest to flush", self.timeout))?;
		self.host_channel.io().flush(self.timeout)
			.await
			.map_err(|()| anyhow!("Timed out waiting {:?} for host to flush", self.timeout))?;

		// hang up the guest side
		self.guest_channel.close()
			.await;

		// then, wait for the host task to exit
		let result = timeout(self.timeout, self.host_task)
			.await
			.context(format!("Timed out waiting {:?} for the host task to finish", self.timeout))?
			.context("Failed to join with host task")?;
		result
			.context("Host task failed")?;

		// finally, close the host side too
		self.host_channel.close()
			.await;

		Ok(())
	}
}
