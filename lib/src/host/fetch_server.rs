
use std::fmt;
use std::fmt::Debug;
use std::time::Duration;
use futures::TryStream;
use thiserror::Error;

use crate::protobuf::fetch::{GatewayResponse, Request, ResponseBodyType, ServerResponse};
use crate::protobuf::fetch::server::{decode_request, encode_gateway_response, encode_server_response};
use crate::protobuf::framed::{FrameEvent, FrameReaderBufError, FrameReaderTimeout, FrameReceiver, FrameStreamer, FrameWriteError, FrameWriteStreamError, FrameWriter, NewFramerError, Reader, RemoteFrameError, Writer};
use crate::protobuf::ReadProtobufError;


/// The server side of the smolweb fetch protocol
pub struct FetchServer<W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{
	receiver: FrameReceiver<W,R>
}

impl<W,R> FetchServer<W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{

	pub fn new(receiver: impl Into<FrameReceiver<W,R>>) -> Self {
		Self {
			receiver: receiver.into()
		}
	}

	pub async fn recv_request(&mut self, timeout_after_start: Duration) -> Result<Request,FetchRecvRequestError<R::Error>> {

		// wait for the next request
		let msg = self.receiver.recv_all(FrameReaderTimeout::AfterStart(timeout_after_start))
			.await?;

		// parse it
		let request = decode_request(msg)?;

		Ok(request)
	}

	pub async fn recv_body_all(&mut self, timeout: Duration) -> Result<Vec<u8>,FrameReaderBufError<R::Error>> {
		self.receiver.recv_all(FrameReaderTimeout::Now(timeout))
			.await
	}

	pub async fn recv_body(&mut self) -> Result<FrameEvent,R::Error> {
		self.receiver.recv()
			.await
	}

	pub async fn respond_gateway(&self, response: GatewayResponse) -> Result<(),FrameWriteError<W::Error>> {

		// send the response
		let msg = encode_gateway_response(response);
		self.receiver.writer().write(msg)
			.await?;

		Ok(())
	}

	pub async fn respond_server(&self, response: ServerResponse) -> Result<Option<BodyWriter<W>>,FrameWriteError<W::Error>> {

		// make a body writer, if needed
		let body_writer = match response.body_type {
			ResponseBodyType::None => None,
			ResponseBodyType::SingleStream
			| ResponseBodyType::MultiStream => Some(BodyWriter::from(self.receiver.writer()))
		};

		// send the response
		let msg = encode_server_response(response);
		self.receiver.writer().write(msg)
			.await?;

		Ok(body_writer)
	}
}


#[derive(Error, Debug)]
pub enum FetchRecvRequestError<R>
	where
		R: std::error::Error + 'static
{

	#[error("Failed to receive request")]
	Read(#[from] FrameReaderBufError<R>),

	#[error("Failed to read request")]
	Protobuf(#[from] ReadProtobufError)
}


pub struct BodyWriter<'w,W>
	where
		W: Writer + Send + Sync + Clone
{
	writer: &'w FrameWriter<W>
}

impl<'w,W> BodyWriter<'w,W>
	where
		W: Writer + Send + Sync + Clone
{

	fn from(writer: &'w FrameWriter<W>) -> Self {
		Self {
			writer
		}
	}

	pub async fn write_all(&self, body: impl Into<Vec<u8>> + Send) -> Result<(),FrameWriteError<W::Error>> {
		self.writer.write(body)
			.await
	}

	pub fn stream(&self, size: u64) -> Result<FrameStreamer<'w,W>,NewFramerError> {
		self.writer.stream(size)
	}

	pub async fn write_stream<S,B,E>(&self, size: u64, stream: S) -> Result<(),FrameWriteStreamError<W::Error,E>>
		where
			S: TryStream<Ok=B,Error=E,Item=Result<B,E>> + Send + 'static,
			B: Into<Vec<u8>>,
			E: std::error::Error + 'static
	{
		self.writer.write_from_stream(size, stream)
			.await
	}

	pub async fn abort(self) -> Result<(),FrameWriteError<W::Error>> {

		// pack an abort error frame
		let frame = RemoteFrameError::Abort.frame();
		self.writer.write(frame)
			.await?;

		Ok(())
	}
}

impl<'w,W> Debug for BodyWriter<'w,W>
	where
		W: Writer + Send + Sync + Clone
{

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "BodyWriter")
	}
}
