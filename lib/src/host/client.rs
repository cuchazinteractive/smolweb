
use std::net::SocketAddr;
use std::sync::Arc;
use std::time::Duration;

use burgerid::{IdentityApp, IdentityKeyApp, IdentitySignError};
use bytes::Bytes;
use thiserror::Error;
use tokio::net::TcpStream;
use tokio::time::timeout;
use tokio_rustls::TlsConnector;
use tokio_rustls::client::TlsStream;
use tokio_rustls::rustls::client::InvalidDnsNameError;
use tokio_rustls::rustls::ServerName;

use crate::net::{ConfigAddr, ConnectMode, FilterAddrs, NoCompatibleConnectAddr};
use crate::net::codec::{FramedProtos, RecvError, SendError};
use crate::net::dns::{DomainLookupError, LookupAddrs};
use crate::protobuf::{hosts, ReadProtobufError};
use crate::protobuf::hosts::{AuthnError, DEFAULT_PORT};
use crate::protobuf::hosts::host::{ReadyChallenge, ReadyResult};
use crate::tls::{ArgsTlsClient, tls_config_client};


pub struct ArgsHostsClient<'i> {
	pub connect_mode: ConnectMode,
	pub addr: ConfigAddr,
	pub app_id: &'i IdentityApp,
	pub app_key: &'i IdentityKeyApp,
	pub timeout_ready: Duration,
	pub tls: ArgsTlsClient
}

impl<'i> ArgsHostsClient<'i> {

	#[cfg(test)]
	pub fn default(app_id: &'i IdentityApp, app_key: &'i IdentityKeyApp) -> Self {
		Self {
			connect_mode: ConnectMode::PreferIpv6,
			app_id,
			app_key,
			addr: ConfigAddr::localhost_auto_port(),
			timeout_ready: Duration::from_millis(100),
			tls: ArgsTlsClient::default()
		}
	}
}

/// A client for connecting to the hosts server at a beacon server
pub struct HostsClient {
	remote_addr: SocketAddr,
	messages: FramedProtos<TlsStream<TcpStream>>
}

impl HostsClient {

	pub async fn connect<'i>(args: ArgsHostsClient<'i>) -> Result<Self,HostsClientConnectError> {

		// resolve the connect address
		let addrs = args.addr.lookup_addrs(DEFAULT_PORT)
			.await?;
		let addr = addrs
			.filter_addrs_for_connect(args.connect_mode)
			.into_iter()
			.next()
			.ok_or_else(|| NoCompatibleConnectAddr::new(args.connect_mode, addrs))?;

		// connect to the hosts server at the beacon server
		let stream = TcpStream::connect(addr)
			.await
			.map_err(|e| HostsClientConnectError::TCP(e, addr))?;
		let peer_addr = stream.peer_addr()
			.map_err(|e| HostsClientConnectError::Addr(e))?;

		// start TLS
		let tls_config = tls_config_client(&args.tls)
			.map_err(|e| HostsClientConnectError::TLSConfig(e))?;
		let tls_connector = TlsConnector::from(Arc::new(tls_config));
		let server_name = ServerName::try_from(args.addr.host.as_str())?;
		let tls_stream = tls_connector.connect(server_name, stream)
			.await
			.map_err(|e| HostsClientConnectError::TLS(e))?;

		let mut messages = FramedProtos::from(tls_stream);

		// send the ready request
		let request = hosts::host::encode_ready_request(&args.app_id);
		messages.send(request)
			.await?;

		// wait for the ready challenge
		let challenge = timeout(args.timeout_ready, messages.recv())
			.await
			.map_err(|_| HostsClientConnectError::Timeout)??;
		let nonce = match hosts::host::decode_ready_challenge(challenge)? {
			ReadyChallenge::Challenge { nonce } => nonce,
			ReadyChallenge::Error(e) => return Err(HostsClientConnectError::Authn(e)),
			ReadyChallenge::AlreadyReady => return Err(HostsClientConnectError::AlreadyReady)
		};

		// respond to the challenge
		let response = {
			let unlocked = args.app_id.unlock(&args.app_key)
				.ok_or(HostsClientConnectError::UnlockId)?;
			hosts::host::encode_ready_response(&unlocked, &nonce)?
		};
		messages.send(response)
			.await?;

		// wait for the result
		let result = timeout(args.timeout_ready, messages.recv())
			.await
			.map_err(|_| HostsClientConnectError::Timeout)??;
		let result = hosts::host::decode_ready_result(result)?;
		match result {
			ReadyResult::Ok => (), // hooray!
			ReadyResult::Error(e) => return Err(HostsClientConnectError::Authn(e)),
			ReadyResult::NotAuthorized => return Err(HostsClientConnectError::NotAuthorized)
		}

		Ok(Self {
			remote_addr: peer_addr,
			messages
		})
	}

	pub fn remote_addr(&self) -> &SocketAddr {
		&self.remote_addr
	}

	pub async fn recv_message(&mut self) -> Result<hosts::host::BeaconMessage,HostsClientMsgError> {
		let msg = self.messages.recv()
			.await?;
		let msg = hosts::host::decode_msg(msg)?;
		Ok(msg)
	}

	pub async fn send_message(&mut self, msg: impl Into<Bytes>) -> Result<(),SendError> {
		self.messages.send(msg.into())
			.await
	}

	#[allow(unused)]
	pub fn disconnect(self) {
		// not magic, just drop self
	}
}


#[derive(Error, Debug)]
pub enum HostsClientConnectError {

	#[error("Failed to resolve domain name")]
	DNS(#[from] DomainLookupError),

	#[error("Failed to choose a connect address")]
	ConnectAddr(#[from] NoCompatibleConnectAddr),

	#[error("Failed to connect TCP Stream at {1}")]
	TCP(#[source] std::io::Error, SocketAddr),

	#[error("Failed to get socket peer address")]
	Addr(#[source] std::io::Error),

	#[error("Failed to parse the DNS name")]
	InvalidDnsName(#[from] InvalidDnsNameError),

	#[error("Failed to configure TLS client")]
	TLSConfig(#[source] tokio_rustls::rustls::Error),

	#[error("Failed to start TLS session")]
	TLS(#[source] std::io::Error),

	#[error("Failed to unlock identity")]
	UnlockId,

	#[error("Failed to sign using identity")]
	IdSign(#[from] IdentitySignError),

	#[error("Failed to send message to beacon")]
	Send(#[from] SendError),

	#[error("Timed out waiting for ready response from beacon server")]
	Timeout,

	#[error("Failed to receive message from beacon server")]
	Recv(#[from] RecvError),

	#[error("Failed to decode protobuf")]
	Protobuf(#[from] ReadProtobufError),

	#[error("The beacon server already has a connection for this host")]
	AlreadyReady,

	#[error("Authentication with the beacon server failed: {0:?}")]
	Authn(AuthnError),

	#[error("The beacon server did not authorize the connection")]
	NotAuthorized
}

impl HostsClientConnectError {

	pub fn is_recoverable(&self) -> bool {
		match self {

			// authn/authz errors are not recoverable
			Self::Authn(..)
			| Self::NotAuthorized => false,

			// everything else is recoverable
			// NOTE: AlreadyReady errors are now recoverable too
			_ => true
		}
	}
}


#[derive(Error, Debug)]
pub enum HostsClientMsgError {

	#[error("Failed to receive message from beacon server")]
	Recv(#[from] RecvError),

	#[error("Failed to decode protobuf")]
	Protobuf(#[from] ReadProtobufError)
}
