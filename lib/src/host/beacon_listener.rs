
use std::collections::HashMap;
use std::ops::Deref;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{anyhow, bail, Context, Result};
use axum::async_trait;
use burgerid::IdentityEphemeral;
use display_error_chain::ErrorChainExt;
use tokio::sync::{Mutex, oneshot, RwLock};
use tracing::{debug, error, Instrument, trace, warn};
use webrtc::api::setting_engine::SettingEngine;
use webrtc::ice_transport::ice_server::RTCIceServer;
use webrtc::peer_connection::configuration::RTCConfiguration;

use crate::host::client::HostsClient;
use crate::host::server::ArgsHost;
use crate::lang::ErrorReporting;
use crate::protobuf::hosts;
use crate::protobuf::hosts::host::{EncodedGuestConnectConfirm, EncodedGuestConnectRequest, GuestConnectConfirm};
use crate::webrtc::connection::{CandidateOptions, ConnectionConfig, SettingEngineExt};
use crate::webrtc::connection_race::{ConnectionRace, HostConnectionRace};
use crate::webrtc::RTCDataChannelIO;


#[async_trait]
pub trait ChannelHandler {
	async fn handle(&self, channel: RTCDataChannelIO) -> Result<()>;
}


pub(super) struct BeaconListener {
	args: ArgsHost,
	guest_ids: Arc<Mutex<HashMap<Vec<u8>,IdentityEphemeral>>>,
	guest_connections: Arc<RwLock<HashMap<u64,GuestConnection>>>
}

impl BeaconListener {

	pub(super) fn new(args: ArgsHost) -> Self {
		Self {
			args,
			guest_ids: Arc::new(Mutex::new(HashMap::new())),
			guest_connections: Arc::new(RwLock::new(HashMap::new()))
		}
	}

	pub(super) async fn handle<H>(&self, args: &ArgsHost, channel_handler: &Arc<H>, client: &mut HostsClient, msg: hosts::host::BeaconMessage)
		where
			H: ChannelHandler + Send + Sync + 'static
	{

		let result = match msg {

			// listen for connections from guests
			hosts::host::BeaconMessage::GuestConnectRequest(request) => {
				self.handle_connect(args, channel_handler, request, client)
					.await
			}

			hosts::host::BeaconMessage::GuestConnectConfirm(confirm) => {
				self.handle_confirm(args, confirm)
					.await
			}

			hosts::host::BeaconMessage::Ping { request_id } => {
				self.handle_ping(client, request_id)
					.await
			}
		};

		if let Err(e) = result {
			error!(err = %e.deref().chain(), "Failed to handle request");
		}
	}

	#[tracing::instrument(skip_all, level = 5, name = "Guest", fields(conn_id))]
	async fn handle_connect<H>(&self, args: &ArgsHost, channel_handler: &Arc<H>, request: EncodedGuestConnectRequest, client: &mut HostsClient) -> Result<()>
		where
			H: ChannelHandler + Send + Sync + 'static
	{

		// decode the request
		let unlocked = args.app_id.unlock(&args.app_key)
			.context("Failed to unlocked app identity")?;
		let request = request.decode(&unlocked)
			.context("Failed to decode guest connect request")?;

		// save the guest id
		{
			let mut guest_ids = self.guest_ids.lock()
				.await;
			guest_ids.insert(request.guest_id.uid().clone(), request.guest_id.clone());
		}

		// find a new connection id
		let (conn_id, disconnect_rx) = {
			let mut guest_connections = self.guest_connections.write()
				.await;
			loop {
				let id = rand::random::<u64>();
				if !guest_connections.contains_key(&id) {
					let (disconnect_tx, disconnect_rx) = oneshot::channel();
					guest_connections.insert(id, GuestConnection::new(request.guest_id.uid().clone(), disconnect_tx));
					break (id, disconnect_rx);
				}
			}
		};
		tracing::Span::current().record("conn_id", &conn_id);

		debug!("connect");

		// configure the WebRTC connection
		let config = ConnectionConfig {

			// standard WebRTC config
			rtc_config: RTCConfiguration {
				ice_servers: vec![
					RTCIceServer {
						urls: vec![
							format!("stun:{}", self.args.stun_addr)
						],
						.. Default::default()
					}
				],
				.. Default::default()
			},

			// non-standard WebRTC config
			rtc_settings: {

				let mut settings = SettingEngine::default();
				// keep attempting the connection until we ask it to stop
				settings.set_max_ice_timeouts();

				settings
			},

			// ICE candidate configuration
			candidate_options: CandidateOptions {
				map_ports: args.map_ports.clone(),
				.. Default::default()
			}
		};

		// make the WebRTC connection
		let mut connection = ConnectionRace::new(config.clone(), config)
			.await
			.context("Failed to make WebRTC connection")?
			.to_host();
		let (answer1, offer2) = connection.connect1(request.offer1)
			.await
			.context("Failed to start WebRTC connection")?;

		// make the connection confirmation channel
		let (confirm_tx, confirm_rx) = oneshot::channel();
		{
			let mut guest_connections = self.guest_connections.write()
				.await;
			let connection = guest_connections.get_mut(&conn_id)
				.context(format!("Failed to get GuestConnection for guest id {}", conn_id))?;
			connection.confirm_tx = Some(confirm_tx);
		}

		debug!("WebRTC connecting ...");

		// drive the guest connection on a dedicated task
		tokio::spawn({
			let guest_connections = self.guest_connections.clone();
			let guest_ids = self.guest_ids.clone();
			let timeout_guest_connect = args.timeout_guest_connect;
			let channel_handler = channel_handler.clone();
			async move {

				// NOTE: we own the connection in here,
				//       so make sure we abort() or open() it before dropping

				drive_connection(conn_id, connection, confirm_rx, disconnect_rx, timeout_guest_connect, channel_handler)
					.await;

				// cleanup guest connection, and check if it was the last one for this guest
				let guest_uid_to_remove = {
					let mut guest_connections = guest_connections.write()
						.await;
					guest_connections.remove(&conn_id)
						.and_then(|guest_connection| {
							let any_connections_left = guest_connections.values()
								.any(|c| &c.guest_uid == &guest_connection.guest_uid);
							if any_connections_left {
								None
							} else {
								// this was the last connection, so move this UID to the outer scope
								Some(guest_connection.guest_uid)
							}
						})
				};

				if let Some(guest_uid) = guest_uid_to_remove {
					// last connection, clean up guest UID too
					guest_ids.lock()
						.await
						.remove(&guest_uid);
				}

				debug!("connection removed");
			}
		}.in_current_span());

		// reply with the answer
		let response = hosts::host::encode_guest_connect_accept(
			&unlocked,
			&request.guest_id,
			request.request_id,
			conn_id,
			answer1,
			offer2
		).context("Failed to encrypt guest connect accept message")?;
		client.send_message(response)
			.await
			.context("Failed to send connect response")?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "Guest", fields(conn_id))]
	async fn handle_confirm(&self, args: &ArgsHost, confirm: EncodedGuestConnectConfirm) -> Result<()> {

		// find the guest id
		let guest_id = {
			let guest_ids = self.guest_ids.lock()
				.await;
			let Some(guest_id) = guest_ids.get(confirm.guest_uid())
			else { return Ok(()); };
			guest_id.clone()
		};

		// decode the message
		let unlocked = args.app_id.unlock(&args.app_key)
			.context("Failed to unlocked app identity")?;
		let confirm = confirm.decode(&unlocked, &guest_id)
			.context("Failed to decode guest connect confirm message")?;

		tracing::Span::current().record("conn_id", &confirm.connection_id);

		// find the guest connection
		let mut guest_connections = self.guest_connections.write()
			.await;
		let guest_connection = guest_connections.get_mut(&confirm.connection_id)
			.context(format!("No guest connection with id {}", confirm.connection_id))?;
		if guest_connection.guest_uid.as_slice() != guest_id.uid() {
			bail!("Confirm message guest UID doesn't match guest identity in connection number {}", confirm.connection_id);
		}

		// send the confirm message to the driving task
		let confirm_tx = guest_connection.confirm_tx.take()
			.context(format!("Guest connection {} is not expecting confirm message", confirm.connection_id))?;
		if let Err(confirm) = confirm_tx.send(confirm) {
			bail!("Failed to send confirm message to driving task for guest connection {}", confirm.connection_id);
		}

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "Guest")]
	async fn handle_ping(&self, client: &mut HostsClient, request_id: u64) -> Result<()> {

		trace!("Ping!");

		// send pong response
		client.send_message(hosts::host::encode_pong(request_id))
			.await
			.context("Failed to send pong")?;

		Ok(())
	}

	pub(super) async fn disconnect_guests(&self) {

		// send disconnection signals to all the guests
		{
			let mut guest_connections = self.guest_connections.write()
				.await;
			for (conn_id, connection) in guest_connections.iter_mut() {
				if let Some(disconnect_tx) = connection.disconnect_tx.take() {
					disconnect_tx.send(())
						.map_err(|_| anyhow!("Failed to send disconnect signal to guest connection {}", conn_id))
						.warn_err()
						.ok();
				}
			}
		}
	}

	pub(super) async fn wait_for_guests(&self) {

		// just do a dumb wait loop here
		loop {

			let num_guests = self.guest_connections.read()
				.await
				.len();
			if num_guests <= 0 {
				break;
			}

			// wait a bit and try again
			tokio::time::sleep(Duration::from_millis(100))
				.await;
		}
	}
}


struct GuestConnection {
	guest_uid: Vec<u8>,
	confirm_tx: Option<oneshot::Sender<GuestConnectConfirm>>,
	disconnect_tx: Option<oneshot::Sender<()>>
}

impl GuestConnection {

	fn new(guest_uid: Vec<u8>, disconnect_tx: oneshot::Sender<()>) -> Self {
		Self {
			guest_uid,
			confirm_tx: None,
			disconnect_tx: Some(disconnect_tx)
		}
	}
}


async fn drive_connection<H>(
	conn_id: u64,
	mut connection: HostConnectionRace,
	confirm_rx: oneshot::Receiver<GuestConnectConfirm>,
	mut disconnect_rx: oneshot::Receiver<()>,
	timeout_guest_connect: Duration,
	channel_handler: Arc<H>
)
	where
		H: ChannelHandler + Send + Sync + 'static
{

	// NOTE: we own the connection in here,
	//       so make sure we abort() or open() it before dropping

	macro_rules! abort {
		($connection:ident) => {{
			$connection.abort()
				.await;
			return;
		}}
	}

	// wait for the confirmation message
	debug!("waiting {:?} for confirmation message from beacon ...", timeout_guest_connect);
	let confirm = tokio::select! {

		result = confirm_rx => {
			let result = result
				.context("Failed to receive confirmation")
				.warn_err();
			match result {
				Ok(c) => c,
				Err(_) => abort!(connection)
			}
		}

		_ = tokio::time::sleep(timeout_guest_connect) => {
			warn!("Timed out waiting for confirmation message from guest");
			abort!(connection)
		}
		_ = &mut disconnect_rx => abort!(connection)
	};
	if confirm.connection_id != conn_id {
		warn!("Received confirm message from connection {} but expected connection {}", confirm.connection_id, conn_id);
		abort!(connection);
	}

	// start the second channel of the connection race
	let Ok(_) = connection.connect2(confirm.answer2)
		.await
		.context("Failed to confirm WebRTC connection")
		.warn_err()
		else { abort!(connection); };

	// wait for the connection to open
	debug!("waiting {:?} for data channel ...", timeout_guest_connect);
	tokio::select! {
		_ = connection.wait_for_open() => (),
		_ = tokio::time::sleep(timeout_guest_connect) => {
				warn!("Timed out waiting for WebRTC connection from guest");
				abort!(connection)
			}
		_ = &mut disconnect_rx => abort!(connection)
	}

	debug!("data channel open");

	let data_channel = match connection.open().await {
		Err(connection) => {
			warn!("Connection was not actually open");
			abort!(connection);
		}
		Ok(c) => c
	};

	// NOTE: we have an open data channel now, so don't return without close()ing it

	// apply the channel handler
	let handling = channel_handler.handle(data_channel.io().clone());

	// wait for it to finish, or be disconnected
	tokio::select! {

		result = handling => {
			result.context("Failed to handle channel")
			.warn_err()
			.ok();
		}

		_ = &mut disconnect_rx => ()
	}

	// wait for the send buffer before closing the connection
	// TODO: expose timeout as arg?
	let flush_timeout = Duration::from_secs(30);
	let result = data_channel.io().flush(flush_timeout)
		.await;
	if let Err(_) = result {
		warn!("Send buffer didn't flush within {:?}", flush_timeout)
	}

	data_channel.close()
		.await;
}
