
use std::any::type_name;
use std::fmt::Display;

use anyhow::{Context, Result};
use num_traits::{one, PrimInt};


pub struct ExponentialBackoff<T> {
	start: T,
	max: T,
	current: T
}

impl<T> ExponentialBackoff<T>
	where
		T: PrimInt + Display
{

	pub fn new(start: T, max: T) -> Self {
		Self {
			start,
			max,
			current: start
		}
	}

	pub fn current(&self) -> T {
		self.current
	}

	pub fn sample_current(&self) -> Result<T> {

		// f \in [0, 1)
		let f = rand::random::<f32>();

		// scale the current value by f to get [0, c)
		let mut s = self.current.to_f32()
			.context(format!("Failed to convert {} to f32", self.current))?;
		s *= f;
		let mut s = T::from(s)
			.context(format!("Failed to convert {} to {}", s, type_name::<T>()))?;

		// add one to get to [1, c]
		s = s + one();

		Ok(s)
	}

	pub fn reset(&mut self) {
		self.current = self.start;
	}

	pub fn failed(&mut self) -> Result<()> {

		if self.current < self.max {
			let two = T::from(2)
				.context(format!("Failed to convert 2 to {}", type_name::<T>()))?;
			self.current = self.current*two;
		}
		if self.current > self.max {
			self.current = self.max;
		}

		Ok(())
	}
}


#[cfg(test)]
mod test {

	use galvanic_assert::{assert_that, matchers::*};

	use super::*;


	#[test]
	fn test() {

		let mut eb = ExponentialBackoff::new(5, 25);
		assert_that!(&eb.current(), eq(5));

		eb.failed()
			.unwrap();
		assert_that!(&eb.current(), eq(10));

		eb.failed()
			.unwrap();
		assert_that!(&eb.current(), eq(20));

		eb.failed()
			.unwrap();
		assert_that!(&eb.current(), eq(25));

		eb.reset();
		assert_that!(&eb.current(), eq(5));
	}


	#[test]
	fn sample() {
		let eb = ExponentialBackoff::new(5, 5);
		for _ in 0 .. 100 {
			let sampled = eb.sample_current()
				.unwrap();
			assert_that!(&sampled, gt(0));
			assert_that!(&sampled, less_than_or_equal(5));
		}
	}
}
