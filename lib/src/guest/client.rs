
use std::time::Duration;

use burgerid::{IdentityApp, IdentityEncryptError, IdentityEphemeral, NewIdentityError};
use rand::RngCore;
use thiserror::Error;
use tokio::time::timeout;
use webrtc::api::setting_engine::SettingEngine;
use webrtc::ice_transport::ice_server::RTCIceServer;
use webrtc::peer_connection::configuration::RTCConfiguration;

use crate::net::{ConfigAddr, ConnectMode};
use crate::net::https_client::HttpsProtoClient;
use crate::net::messages::{ResponseError, ResponseProtobufError, ResponseTools};
use crate::protobuf::guests::{About, guest};
use crate::protobuf::HostUid;
use crate::webrtc::connection::{ConnectionConfig, ConnectionError, NewConnectionError, OpenDataChannel, SettingEngineExt};
use crate::webrtc::connection_race::ConnectionRace;
use crate::webrtc::SessionDescription;


pub struct ArgsGuestsClient {
	pub stun_addr: Option<ConfigAddr>,
	pub beacon_addr: ConfigAddr,
	pub http_config: Option<Box<dyn FnOnce(reqwest::ClientBuilder) -> reqwest::ClientBuilder>>,
	pub connect_mode: ConnectMode,
	pub webrtc_timeout: Duration
}

impl Default for ArgsGuestsClient {

	fn default() -> Self {

		let args = Self {
			stun_addr: None,
			beacon_addr: ConfigAddr::localhost(),
			http_config: None,
			connect_mode: ConnectMode::PreferIpv6,
			webrtc_timeout: Duration::from_secs(15)
		};

		#[cfg(test)]
		let args = Self {
			webrtc_timeout: Duration::from_secs(5),
			.. args
		};

		args
	}
}


/// A client for connecting to the guests server at a beacon server
pub struct GuestsClient {
	args: ArgsGuestsClient,
	client: HttpsProtoClient
}


impl GuestsClient {

	#[tracing::instrument(skip_all, level = 5, name = "GuestClient")]
	pub fn new(mut args: ArgsGuestsClient) -> Result<Self,GuestsClientNewError> {

		let client = HttpsProtoClient::new(
			args.beacon_addr.clone(),
			args.connect_mode.clone(),
			args.http_config.take()
		)?;

		Ok(Self {
			args,
			client
		})
	}

	#[tracing::instrument(skip_all, level = 5, name = "GuestClient")]
	pub async fn about(&self) -> Result<About,GuestsClientRequestError> {

		let response = self.client.get("/about")
			.await?
			.into_proto()
			.await?;
		let about = guest::decode_about(response)?;

		Ok(about)
	}

	#[tracing::instrument(skip_all, level = 5, name = "GuestClient")]
	pub async fn get_host_id(&self, host_uid: &HostUid) -> Result<Option<IdentityApp>,GuestsClientHostIdError> {

		let request = guest::encode_host_identity_request(host_uid);
		let response = self.client.post("/guests/host_id", request)
			.await?
			.into_proto()
			.await?;
		let host_id = guest::decode_host_identity_response(response)?;

		// check the UID of the returned identity
		if let Some(host_id) = &host_id {
			if host_id.uid() != host_uid {
				return Err(GuestsClientHostIdError::Invalid);
			}
		}

		Ok(host_id)
	}

	#[tracing::instrument(skip_all, level = 5, name = "GuestClient")]
	pub async fn connect(&self, host_id: &IdentityApp) -> Result<OpenDataChannel,GuestsClientConnectError> {

		// build the connection race
		let config = ConnectionConfig {
			rtc_config: {
				if let Some(stun_addr) = &self.args.stun_addr {
					RTCConfiguration {
						ice_servers: vec![
							RTCIceServer {
								urls: vec![
									format!("stun:{}", stun_addr)
								],
								.. Default::default()
							}
						],
						.. Default::default()
					}
				} else {
					RTCConfiguration::default()
				}
			},
			rtc_settings: {

				let mut settings = SettingEngine::default();
				// keep attempting the connection until we ask it to stop
				settings.set_max_ice_timeouts();

				settings
			},
			candidate_options: Default::default(),
		};
		let mut connection = ConnectionRace::new(config.clone(), config)
			.await?
			.to_guest();

		// we have a ConnectionRace now: don't leave this function without close()ing or abort()ing it
		// that means no using `?` after here!!
		macro_rules! ok_or_abort {
			($exp:expr) => {{
				match $exp {
					Ok(r) => r,
					Err(e) => {
						connection.abort()
							.await;
						return Err(e.into());
					}
				}
			}}
		}

		let offer1: SessionDescription = ok_or_abort!(connection.offer().await);

		// generate a nonce we can use to authenticate the host
		let mut nonce = vec![0u8; 32];
		rand::thread_rng().fill_bytes(nonce.as_mut());

		// generate an ephemeral identity
		let (guest_id, guest_key) = ok_or_abort!(IdentityEphemeral::new());
		let guest_id_unlocked = guest_id.unlock(&guest_key)
			.expect("ephemeral identity didn't unlock with its own key");

		// send the connection offer to the beacon server and get the acceptance back
		let request = ok_or_abort!(guest::encode_connection_request(&guest_id, &guest_id_unlocked, &host_id, offer1));
		let response = ok_or_abort!(self.client.post("/guests/connect", request).await);
		let response = ok_or_abort!(response.into_proto().await);
		let accept = ok_or_abort!(guest::decode_connection_response(response, &guest_id_unlocked, &host_id));
		let accept = ok_or_abort!(accept);

		// start the connection
		let answer2 = ok_or_abort!(connection.connect(accept.answer1, accept.offer2).await);

		// send back the confirmation
		let confirm = ok_or_abort!(guest::encode_connection_confirm(&guest_id_unlocked, &host_id, accept.connection_id, answer2));
		let response = ok_or_abort!(self.client.post("/guests/confirm", confirm).await);
		ok_or_abort!(response.into_success().await);

		// wait for the connection to open
		let result = timeout(self.args.webrtc_timeout, connection.wait_for_open())
			.await
			.map_err(|_| GuestsClientConnectError::TimedOut);
		ok_or_abort!(result);

		let result = connection.open()
			.await;
		match result {
			Ok(data_channel) => Ok(data_channel),
			Err(connection) => {
				connection.abort()
					.await;
				panic!("Failed ot open connection");
				// PANIC SAFETY: we already waited succesfully, so this shouldn't fail
			}
		}
	}
}


#[derive(Error, Debug)]
pub enum GuestsClientNewError {

	#[error("Failed to build HTTP client")]
	Reqwest(#[from] reqwest::Error)
}


#[derive(Error, Debug)]
pub enum GuestsClientRequestError {

	#[error("Failed to make HTTP request")]
	Reqwest(#[from] reqwest::Error),

	#[error("Failed to read protobuf from HTTP response")]
	ReadProtobuf(#[from] ResponseProtobufError),

	#[error("Failed to decode protobuf")]
	DecodeProtobuf(#[from] guest::DecodeError)
}

impl From<ResponseError> for GuestsClientRequestError {
	fn from(value: ResponseError) -> Self {
		GuestsClientRequestError::ReadProtobuf(value.into())
	}
}


#[derive(Error, Debug)]
pub enum GuestsClientHostIdError {

	#[error("Failed to communicate with guests server")]
	Request(#[source] GuestsClientRequestError),

	#[error("The returned host identity was invalid")]
	Invalid,
}

impl<T> From<T> for GuestsClientHostIdError
	where
		T: Into<GuestsClientRequestError>
{
	fn from(value: T) -> Self {
		GuestsClientHostIdError::Request(value.into())
	}
}


#[derive(Error, Debug)]
pub enum GuestsClientConnectError {

	#[error("WebRTC connection failure")]
	WebRTCConnection(#[source] webrtc::Error),

	#[error("Failed to generate identity for connection")]
	NewIdentity(#[from] NewIdentityError),

	#[error("Encryption failed")]
	Encrypt(#[from] IdentityEncryptError),

	#[error("Failed to communicate with guests server")]
	Request(#[source] GuestsClientRequestError),

	#[error("The beacon server rejected the connection")]
	Rejected(#[from] guest::ConnectionReject),

	#[error("Failed to create new WebRTC connection")]
	NewConnection(#[from] NewConnectionError),

	#[error("Failed to connect over WebRTC")]
	Connection(#[from] ConnectionError),

	#[error("The WebRTC connection timed out")]
	TimedOut
}

impl<T> From<T> for GuestsClientConnectError
	where
		T: Into<GuestsClientRequestError>
{
	fn from(value: T) -> Self {
		GuestsClientConnectError::Request(value.into())
	}
}
