
use std::fmt::Debug;
use std::time::Duration;

use thiserror::Error;

use crate::protobuf::fetch::{GatewayResponse, Header, Request, Response, ServerResponse};
use crate::protobuf::fetch::client;
use crate::protobuf::framed::{FrameReaderBufError, FrameReaderTimeout, FrameReceiver, FrameWriteError, FrameWriter, Reader, Writer};
use crate::protobuf::ReadProtobufError;


/// The client side of the smolweb fetch protocol
pub struct FetchClient<W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{
	receiver: FrameReceiver<W,R>
}

impl<W,R> FetchClient<W,R>
	where
		W: Writer + Send + Sync + Clone,
		R: Reader + Send
{

	pub fn new(receiver: impl Into<FrameReceiver<W,R>>) -> Self {
		Self {
			receiver: receiver.into()
		}
	}

	pub async fn send_request(&self, method: impl AsRef<str>, path: impl AsRef<str>, headers: Vec<Header>, has_body: bool) -> Result<(),FrameWriteError<W::Error>> {

		let request = Request {
			method: method.as_ref().to_string(),
			path: path.as_ref().to_string(),
			headers,
			has_body,
		};
		let request = client::encode_request(request);

		self.receiver.writer().write(request)
			.await?;

		Ok(())
	}

	pub fn writer(&self) -> &FrameWriter<W> {
		self.receiver.writer()
	}

	pub async fn recv_response(&mut self, timeout: Duration) -> Result<ServerResponse,FetchClientRecvError<R::Error>> {
		let response = self.receiver.recv_all(FrameReaderTimeout::Now(timeout))
			.await?;
		let response = client::decode_response(response)?;
		match response {
			Response::Gateway(r) => Err(FetchClientRecvError::Gateway(r)),
			Response::Server(r) => Ok(r)
		}
	}

	pub async fn recv_body(&mut self, timeout: Duration) -> Result<Vec<u8>,FrameReaderBufError<R::Error>> {
		self.receiver.recv_all(FrameReaderTimeout::Now(timeout))
			.await
	}
}


#[derive(Error, Debug)]
pub enum FetchClientRecvError<R>
	where
		R: std::error::Error + 'static
{

	#[error("Failed to read response from underlying transport")]
	Read(#[from] FrameReaderBufError<R>),

	#[error("Failed to read response")]
	Protobuf(#[from] ReadProtobufError),

	#[error("Host gateway returned an error")]
	Gateway(#[source] GatewayResponse)
}
