
use std::net::SocketAddr;

use anyhow::{Context, Result};
use rcgen::KeyPair;
pub use webrtc::peer_connection::certificate::RTCCertificate;

use crate::lang::TrimMargin;
use crate::webrtc::SessionDescription;


pub fn _gen_cert() {
	let key_pair = KeyPair::generate(&rcgen::PKCS_ED25519)
		.expect("Failed to generate key pair");
	let cert = RTCCertificate::from_key_pair(key_pair)
		.expect("Failed to generate certificate");
	println!("Certificate:\n{}", cert.serialize_pem());
}


pub fn cert_guest() -> Result<RTCCertificate> {
	RTCCertificate::from_pem(r#"
		|-----BEGIN EXPIRES-----
		|APfhng8AAAA=
		|-----END EXPIRES-----
		|
		|-----BEGIN PRIVATE_KEY-----
		|MFMCAQEwBQYDK2VwBCIEIPk1QilUfbv+OijDkwrfKI3+60u/oWzlArkjjWe2mHZz
		|oSMDIQDwj+9u4pghPRGKqNF9eap3c6Rv6uAdlkxjhweyDTDOeg==
		|-----END PRIVATE_KEY-----
		|
		|-----BEGIN CERTIFICATE-----
		|MIIBJDCB16ADAgECAhQezbpwOzv5AZP2NPbTKkqA+H0dezAFBgMrZXAwITEfMB0G
		|A1UEAwwWcmNnZW4gc2VsZiBzaWduZWQgY2VydDAgFw03NTAxMDEwMDAwMDBaGA80
		|MDk2MDEwMTAwMDAwMFowITEfMB0GA1UEAwwWcmNnZW4gc2VsZiBzaWduZWQgY2Vy
		|dDAqMAUGAytlcAMhAPCP727imCE9EYqo0X15qndzpG/q4B2WTGOHB7INMM56ox8w
		|HTAbBgNVHREEFDASghB0WEVnQ29ETW5ZR0REQVVOMAUGAytlcANBACfuQsE6+KXV
		|uhREKGbZrctrmnKMJS7xCNmIrH6B6Tzff7llMEB5qqZOynOg3HxmIMea72TKa+jd
		|2VElIFEFDwI=
		|-----END CERTIFICATE-----
	"#.to_string().trim_margin().as_str())
		.context("Failed to load certificate")
}


pub fn cert_host() -> Result<RTCCertificate> {
	RTCCertificate::from_pem(r#"
		|-----BEGIN EXPIRES-----
		|APfhng8AAAA=
		|-----END EXPIRES-----
		|
		|-----BEGIN PRIVATE_KEY-----
		|MFMCAQEwBQYDK2VwBCIEIGTtcvUFitHOTDWLrfcAi/1plaEEeevt9w1TyBuF+Qkl
		|oSMDIQAboX0/0Rq6ti2wF4lGvnnpfIQBq20LKYHewYL7+hMbLQ==
		|-----END PRIVATE_KEY-----
		|
		|-----BEGIN CERTIFICATE-----
		|MIIBJDCB16ADAgECAhQ3kpFgClFZBkpXPvEXb2SMl4b89TAFBgMrZXAwITEfMB0G
		|A1UEAwwWcmNnZW4gc2VsZiBzaWduZWQgY2VydDAgFw03NTAxMDEwMDAwMDBaGA80
		|MDk2MDEwMTAwMDAwMFowITEfMB0GA1UEAwwWcmNnZW4gc2VsZiBzaWduZWQgY2Vy
		|dDAqMAUGAytlcAMhABuhfT/RGrq2LbAXiUa+eel8hAGrbQspgd7Bgvv6Exstox8w
		|HTAbBgNVHREEFDASghBSSENTbk5iQVZ3THBCT0ZrMAUGAytlcANBAEdC0AKdCBKW
		|ZIzM/4Tq7Cp1Q4lzWCXeXQEoo8qAIleo6sS37eTWmfLxUH8QUmDUplYx26E5tJPL
		|l2uaOmitqAk=
		|-----END CERTIFICATE-----
	"#.to_string().trim_margin().as_str())
		.context("Failed to load certificate")
}


// SDP: https://www.rfc-editor.org/rfc/rfc8866.html
// SDP: SCTP/dTLS: https://www.rfc-editor.org/rfc/rfc8841.html
// SDP: DTLS fingerprints: https://www.rfc-editor.org/rfc/rfc8122.html
// SDP: ICE: https://www.rfc-editor.org/rfc/rfc8839
// SDP: Grouping: https://www.rfc-editor.org/rfc/rfc5888
// SDP: Group=BUNDLE: https://www.rfc-editor.org/rfc/rfc9143
// SDP: MSID: https://www.rfc-editor.org/rfc/rfc8830.html


// Example SDP Offer
// v=0
// o=mozilla...THIS_IS_SDPARTA-99.0 2740832382826867882 0 IN IP4 0.0.0.0
// s=-
// t=0 0

// a=sendrecv
// a=fingerprint:sha-256 AD:0A:F6:A5:AB:7F:15:A7:5D:5F:EA:F1:52:CA:3F:44:F6:51:70:B2:0B:17:F7:E8:DE:6E:F0:50:4C:13:1E:5B
// a=group:BUNDLE 0
// a=ice-options:trickle
// a=msid-semantic:WMS *

// m=application 60869 UDP/DTLS/SCTP webrtc-datachannel
// c=IN IP4 x.x.x.x
// a=candidate:0 1 UDP 2122187007 x.local 60869 typ host
// a=candidate:1 1 UDP 1685987327 x.x.x.x 60869 typ srflx raddr 0.0.0.0 rport 0
// a=sendrecv
// a=end-of-candidates
// a=ice-pwd:e681d3f58e12e[redacted]
// a=ice-ufrag:b667e[redacted]
// a=mid:0
// a=setup:actpass
// a=sctp-port:5000
// a=max-message-size:1073741823


// Example SDP Answer
// v=0
// o=- 153845883313895312 663592849 IN IP4 0.0.0.0
// s=-
// t=0 0
// a=fingerprint:sha-256 86:83:34:81:56:13:51:25:14:06:E6:82:6D:FD:E6:AC:23:DC:91:B4:0D:D9:29:BB:88:F1:F1:11:E0:80:18:A6
// a=group:BUNDLE 0

// m=application 9 UDP/DTLS/SCTP webrtc-datachannel
// c=IN IP4 0.0.0.0
// a=setup:active
// a=mid:0
// a=sendrecv
// a=sctp-port:5000
// a=ice-ufrag:apvxnS[redacted]
// a=ice-pwd:wABBzEgXBex[redacted]
// a=candidate:3991263033 1 udp 2130706431 x.x.x.x 47138 typ host
// a=candidate:3257131835 1 udp 1694498815 x.x.x.x 35555 typ srflx raddr 0.0.0.0 rport 35555
// a=candidate:3156355900 1 udp 2130706431 x:x:x:x:x:x:x:x 35148 typ host
// a=candidate:3979510264 1 udp 1694498815 x:x:x:x:x:x:x:x 48378 typ srflx raddr :: rport 48378
// a=end-of-candidates


fn sdp_origin(session_id: u64) -> String {
	// build the origin
	//   o=<username> <sess-id> <sess-version> <nettype> <addrtype> <unicast-address>
	format!("smolweb-tool {session_id} 0 IN IP4 0.0.0.0")
}


fn sdp_connection_data() -> String {
	// build the connection data
	//   c=<nettype> <addrtype> <connection-address>
	"IN IP4 0.0.0.0".to_string()
}


fn sdp_media_description() -> String {
	// build the media description for the data channel
	//   m=<media> <port> <proto> <fmt> ...
	"application 5 UDP/DTLS/SCTP webrtc-datachannel".to_string()
	// NOTE: Apparently the port here is not meaningful.
	//       The WebRTC crate always just uses 9.
}


fn sdp_fingerprint(cert: &RTCCertificate) -> Result<String> {

	let fingerprints = cert.get_fingerprints();
	let fingerprint = fingerprints
		.first()
		.context("No certificates")?;

	Ok(format!("{} {}", fingerprint.algorithm, fingerprint.value.to_ascii_uppercase()))
}


pub fn sdp_candidate_srflx(priority: u64, addr: &SocketAddr) -> String {
	// candidate: <foundation> <component-id> <transport> <priority>
	//            <connection-address> <port> <cand-type>
	//            [<rel-addr>] [<rel-port>] *<cand-extension>
	let foundation = 1;
	let component_id = 1;
	let ip = addr.ip();
	let port = addr.port();
	format!("candidate:{foundation} {component_id} UDP {priority} {ip} {port} typ srflx raddr 0.0.0.0 rport 0")
}


fn sdp(
	origin: impl AsRef<str>,
	cert: &RTCCertificate,
	setup: impl AsRef<str>,
	ice_ufrag: impl AsRef<str>,
	ice_pwd: impl AsRef<str>,
	candidates: Vec<impl AsRef<str>>
) -> Result<String> {

	let origin = origin.as_ref();
	let setup = setup.as_ref();
	let ice_ufrag = ice_ufrag.as_ref();
	let ice_pwd = ice_pwd.as_ref();

	let media_description = sdp_media_description();
	let connection_data = sdp_connection_data();

	let fingerprint = sdp_fingerprint(&cert)
		.context("Failed to get certificate fingerprint")?;

	let candidates = candidates.iter()
		.map(|c| c.as_ref())
		.collect::<Vec<_>>()
		.join("\n");

	let sdp = format!(r#"

		// SDP session-level stuff
		|v=0
		|o={origin}
		|s=-
		|t=0 0

		// SCTP/dTLS stuff
		|a=fingerprint:{fingerprint}

		// grouping/bundling
		|a=group:BUNDLE 0

		// medium: data channel
		|m={media_description}
		|c={connection_data}
		|a=sendrecv

		// grouping/bundling
		|a=mid:0

		// SCTP/dTLS stuff
		|a=setup:{setup}
		|a=sctp-port:5000
		|a=max-message-size:1073741823

		// ICE stuff
		|a=ice-ufrag:{ice_ufrag}
		|a=ice-pwd:{ice_pwd}
		|{candidates}
		|a=end-of-candidates
	"#).to_string().trim_margin();

	Ok(sdp)
}


pub fn create_offer(local_addrs: &Vec<SocketAddr>, cert: &RTCCertificate, ufrag: impl AsRef<str>, pwd: impl AsRef<str>) -> Result<SessionDescription> {

	let origin = sdp_origin(5);

	let priority = 5;
	let candidates = local_addrs.iter()
		.map(|addr| format!("a={}", sdp_candidate_srflx(priority, addr)))
		.collect::<Vec<_>>();

	let sdp = sdp(origin, &cert, "actpass", ufrag, pwd, candidates)
		.context("Failed to build session description")?;
	SessionDescription::offer(sdp)
		.context("Failed to create offer session description")
}


pub fn create_answer(local_addrs: &Vec<SocketAddr>, cert: &RTCCertificate, ufrag: impl AsRef<str>, pwd: impl AsRef<str>) -> Result<SessionDescription> {

	let origin = sdp_origin(42);

	let priority = 5;
	let candidates = local_addrs.iter()
		.map(|addr| format!("a={}", sdp_candidate_srflx(priority, addr)))
		.collect::<Vec<_>>();

	let sdp = sdp(origin, &cert, "active", ufrag, pwd, candidates)
		.context("Failed to build session description")?;
	SessionDescription::answer(sdp)
		.context("Failed to create answer session description")
}
