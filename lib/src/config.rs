
use std::any::type_name;
use std::fs;
use std::str::FromStr;
use std::time::Duration;

use anyhow::{anyhow, Context, Result};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Deserializer};

use crate::serde::{deserialize_from_str, OptStringOrStruct};


pub trait ToArgs {
	type Args;
	fn to_args(self) -> Result<Self::Args>;
}


pub fn read_config<T:DeserializeOwned + ToArgs>(path: &String) -> Result<T::Args> {

	let toml = fs::read_to_string(path)?;

	let config = toml::from_str::<T>(&toml)
		.context(format!("Failed to read {} from config file", type_name::<T>()))?;

	let args = config.to_args()
		.context("Invalid configuration")?;

	Ok(args)
}


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigDuration {
	pub value: u64,
	pub unit: ConfigDurationUnit
}

impl ConfigDuration {

	pub fn deserialize<'de,D>(deserializer: D) -> std::result::Result<Option<ConfigDuration>,D::Error>
		where
			D: Deserializer<'de>
	{
		OptStringOrStruct::<ConfigDuration>::deserialize(deserializer)
	}

	pub fn deserialize_default() -> Option<ConfigDuration> {
		OptStringOrStruct::<ConfigDuration>::default()
	}

	pub fn ms(value: u64) -> Self {
		ConfigDurationUnit::Milliseconds.duration(value)
	}

	pub fn s(value: u64) -> Self {
		ConfigDurationUnit::Seconds.duration(value)
	}

	pub fn m(value: u64) -> Self {
		ConfigDurationUnit::Minutes.duration(value)
	}

	pub fn h(value: u64) -> Self {
		ConfigDurationUnit::Hours.duration(value)
	}

	pub fn to_duration(&self) -> Duration {
		match self.unit {
			ConfigDurationUnit::Milliseconds => Duration::from_millis(self.value),
			ConfigDurationUnit::Seconds => Duration::from_secs(self.value),
			ConfigDurationUnit::Minutes => Duration::from_secs(self.value*60),
			ConfigDurationUnit::Hours => Duration::from_secs(self.value*60*60),
		}
	}
}

impl FromStr for ConfigDuration {

	type Err = anyhow::Error;

	fn from_str(s: &str) -> std::result::Result<Self,Self::Err> {

		let s = s.trim();

		// the first part should be a number
		let value_boundary = s.char_indices()
			.take_while(|(_ofs, c)| c.is_numeric())
			.last()
			.map(|(ofs, c)| ofs + c.len_utf8())
			.unwrap_or(0);
		let value_str = &s[..value_boundary];
		let value = u64::from_str(value_str)
			.context(format!("unrecognized duration value: {}", value_str))?;

		// the second part should be the unit
		let unit_str = s[value_boundary..].trim_start();
		let unit = ConfigDurationUnit::from_str(unit_str)
			.context(format!("unrecognized duration unit: {}", unit_str))?;

		Ok(Self {
			value,
			unit
		})
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ConfigDurationUnit {
	Milliseconds,
	Seconds,
	Minutes,
	Hours
}

impl ConfigDurationUnit {

	fn duration(self, value: u64) -> ConfigDuration {
		ConfigDuration {
			value,
			unit: self
		}
	}
}

impl FromStr for ConfigDurationUnit {

	type Err = anyhow::Error;

	fn from_str(s: &str) -> std::result::Result<Self,Self::Err> {
		match s {
			"ms" | "millis" | "milliseconds" => Ok(Self::Milliseconds),
			"s" | "secs" | "seconds" => Ok(Self::Seconds),
			"m" | "mins" | "minutes" => Ok(Self::Minutes),
			"h" | "hrs" | "hours" => Ok(Self::Hours),
			_ => Err(anyhow!("unrecognized unit \"{}\", expected one of:\n\
					\t\"ms\", \"millis\", \"milliseconds\",\n\
					\t\"s\", \"secs\", \"seconds\",\n\
					\t\"m\", \"mins\", \"minutes\",\n\
					\t\"h\", \"hrs\", \"hours\"\
				", s))
		}
	}
}

deserialize_from_str!(ConfigDurationUnit);


#[cfg(test)]
pub(crate) mod test {

	use galvanic_assert::{assert_that, matchers::*};
	use serde::Deserializer;
	use toml::de::ValueDeserializer;

	use smolweb_test_tools::is_match;
	use smolweb_test_tools::logging::init_test_logging;

	use super::*;


	pub fn deserialize_value_from_toml<T:DeserializeOwned>(toml: &str) -> Result<T,<ValueDeserializer as Deserializer>::Error> {
		let toml = toml.trim();
		T::deserialize(ValueDeserializer::new(toml))
	}

	pub fn deserialize_from_toml<T:DeserializeOwned>(toml: &str) -> Result<T,<ValueDeserializer as Deserializer>::Error> {
		let toml = toml.trim();
		toml::from_str::<T>(toml)
	}


	#[test]
	fn unit() {
		let _logging = init_test_logging();

		let result = deserialize_value_from_toml::<ConfigDurationUnit>(r#"
			5
		"#);
		assert_that!(&result, is_match!(Err(..)));

		let result = deserialize_value_from_toml::<ConfigDurationUnit>(r#"
			"foo"
		"#);
		assert_that!(&result, is_match!(Err(..)));

		let unit = deserialize_value_from_toml::<ConfigDurationUnit>(r#"
			"ms"
		"#).unwrap();
		assert_that!(&unit, eq(ConfigDurationUnit::Milliseconds));

		let unit = deserialize_value_from_toml::<ConfigDurationUnit>(r#"
			"hours"
		"#).unwrap();
		assert_that!(&unit, eq(ConfigDurationUnit::Hours));
	}


	#[test]
	fn duration() {
		let _logging = init_test_logging();

		// generated serde deserializer shouldn't handle this case
		// need the from_str adapter for that, which isn't here
		let result = deserialize_value_from_toml::<ConfigDuration>(r#"
			"1ms"
		"#);
		assert_that!(&result, is_match!(Err(..)));

		let result = deserialize_value_from_toml::<ConfigDuration>(r#"
			{}
		"#);
		assert_that!(&result, is_match!(Err(..)));

		let result = deserialize_value_from_toml::<ConfigDuration>(r#"
			{ value = 5 }
		"#);
		assert_that!(&result, is_match!(Err(..)));

		let result = deserialize_value_from_toml::<ConfigDuration>(r#"
			{ unit = "ms" }
		"#);
		assert_that!(&result, is_match!(Err(..)));

		let duration = deserialize_value_from_toml::<ConfigDuration>(r#"
			{ value = 5, unit = "ms" }
		"#).unwrap();
		assert_that!(&duration, eq(ConfigDuration::ms(5)));
	}


	#[test]
	fn duration_from_str() {
		let _logging = init_test_logging();

		assert_that!(&ConfigDuration::from_str(""), is_match!(Err(..)));
		assert_that!(&ConfigDuration::from_str("1"), is_match!(Err(..)));
		assert_that!(&ConfigDuration::from_str("ms"), is_match!(Err(..)));

		assert_that!(&ConfigDuration::from_str("1ms").unwrap(), eq(ConfigDuration::ms(1)));
		assert_that!(&ConfigDuration::from_str("1 ms").unwrap(), eq(ConfigDuration::ms(1)));
		assert_that!(&ConfigDuration::from_str(" 1 ms").unwrap(), eq(ConfigDuration::ms(1)));
		assert_that!(&ConfigDuration::from_str("1 ms ").unwrap(), eq(ConfigDuration::ms(1)));
		assert_that!(&ConfigDuration::from_str(" 1 ms ").unwrap(), eq(ConfigDuration::ms(1)));
		assert_that!(&ConfigDuration::from_str("1234mins").unwrap(), eq(ConfigDuration::m(1234)));
		assert_that!(&ConfigDuration::from_str("1234 mins").unwrap(), eq(ConfigDuration::m(1234)));
	}
}
