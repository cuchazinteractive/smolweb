
use std::fmt;
use std::fmt::Display;
use std::marker::PhantomData;
use std::str::FromStr;

use serde::de::{MapAccess, SeqAccess, Visitor};
use serde::{de, Deserialize, Deserializer};


/// see: https://serde.rs/string-or-struct.html
pub struct StringOrStruct<T>(PhantomData<T>);

impl<T> StringOrStruct<T>
	where
		T: FromStr,
		<T as FromStr>::Err: Display
{

	pub fn deserialize<'de,D>(deserializer: D) -> Result<T,D::Error>
		where
			D: Deserializer<'de>,
			T: Deserialize<'de>
	{
		deserializer.deserialize_any(StringOrStruct::<T>(PhantomData))
	}
}

impl<'de,T> Visitor<'de> for StringOrStruct<T>
	where
		T: FromStr + Deserialize<'de>,
		<T as FromStr>::Err: Display
{

	type Value = T;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("string or table")
	}

	fn visit_str<E>(self, value: &str) -> Result<Self::Value,E>
		where
			E: de::Error
	{
		let val = T::from_str(value)
			.map_err(|e| de::Error::custom(e))?;
		Ok(val)
	}

	fn visit_map<M>(self, map: M) -> Result<Self::Value,M::Error>
		where
			M: MapAccess<'de>
	{
		T::deserialize(de::value::MapAccessDeserializer::new(map))
	}
}


/// see: https://old.reddit.com/r/rust/comments/uhpabe/serde_string_or_struct_for_option/
pub struct OptStringOrStruct<T>(PhantomData<T>);

impl<T> OptStringOrStruct<T>
	where
		T: FromStr,
		<T as FromStr>::Err: Display
{

	pub fn deserialize<'de,D>(deserializer: D) -> Result<Option<T>,D::Error>
		where
			D: Deserializer<'de>,
			T: Deserialize<'de>
	{
		deserializer.deserialize_option(OptStringOrStruct(PhantomData))
	}

	pub fn default() -> Option<T> {
		None
	}
}

impl<'de,T> Visitor<'de> for OptStringOrStruct<T>
	where
		T: FromStr + Deserialize<'de>,
		<T as FromStr>::Err: Display
{

	type Value = Option<T>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("none or some(string or table)")
	}

	fn visit_none<E>(self) -> Result<Self::Value, E>
		where
			E: de::Error,
	{
		Ok(None)
	}

	fn visit_some<D>(self, deserializer: D) -> Result<Self::Value,D::Error>
		where
			D: Deserializer<'de>,
	{
		StringOrStruct::<T>::deserialize(deserializer)
			.map(Some)
	}
}


pub struct StringOrVecOfStringOrStructs<T>(PhantomData<T>);

impl<T> StringOrVecOfStringOrStructs<T>
	where
		T: FromStr,
		<T as FromStr>::Err: Display
{

	pub fn deserialize<'de,D>(deserializer: D) -> Result<Vec<T>,D::Error>
		where
			D: Deserializer<'de>,
			T: Deserialize<'de>
	{
		deserializer.deserialize_any(StringOrVecOfStringOrStructs::<T>(PhantomData))
	}
}

impl<'de,T> Visitor<'de> for StringOrVecOfStringOrStructs<T>
	where
		T: FromStr + Deserialize<'de>,
		<T as FromStr>::Err: Display
{

	type Value = Vec<T>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("string or array[string or table]")
	}

	fn visit_str<E>(self, value: &str) -> Result<Self::Value,E>
		where
			E: de::Error
	{
		let val = T::from_str(value)
			.map_err(|e| de::Error::custom(e))?;
		Ok(vec![val])
	}

	fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value,A::Error>
		where
			A: SeqAccess<'de>
	{
		let mut out = match seq.size_hint() {
			Some(len) => Vec::with_capacity(len),
			None => Vec::new()
		};
		loop {
			match seq.next_element::<StringOrStructDeserializer<T>>() {
				Ok(Some(elem)) => out.push(elem.0),
				Ok(None) => break,
				Err(e) => return Err(e)
			};
		}
		Ok(out)
	}
}


struct StringOrStructDeserializer<T>(T);

impl<'de,T> Deserialize<'de> for StringOrStructDeserializer<T>
	where
		T: FromStr + Deserialize<'de>,
		<T as FromStr>::Err: Display
{
	fn deserialize<D>(deserializer: D) -> Result<Self,D::Error>
		where
			D: Deserializer<'de>
	{
		StringOrStruct::deserialize(deserializer)
			.map(StringOrStructDeserializer)
	}
}


pub struct OptStringOrVecOfStringOrStructs<T>(PhantomData<T>);

impl<T> OptStringOrVecOfStringOrStructs<T>
	where
		T: FromStr,
		<T as FromStr>::Err: Display
{

	pub fn deserialize<'de,D>(deserializer: D) -> Result<Option<Vec<T>>,D::Error>
		where
			D: Deserializer<'de>,
			T: Deserialize<'de>
	{
		deserializer.deserialize_option(OptStringOrVecOfStringOrStructs(PhantomData))
	}

	pub fn default() -> Option<Vec<T>> {
		None
	}
}

impl<'de,T> Visitor<'de> for OptStringOrVecOfStringOrStructs<T>
	where
		T: FromStr + Deserialize<'de>,
		<T as FromStr>::Err: Display
{

	type Value = Option<Vec<T>>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("none or some(string or array[string or table])")
	}

	fn visit_none<E>(self) -> Result<Self::Value, E>
		where
			E: de::Error,
	{
		Ok(None)
	}

	fn visit_some<D>(self, deserializer: D) -> Result<Self::Value,D::Error>
		where
			D: Deserializer<'de>,
	{
		StringOrVecOfStringOrStructs::<T>::deserialize(deserializer)
			.map(Some)
	}
}


// because we can't just name it FromStr
pub struct VisitFromStr<T>(PhantomData<T>);

impl<T> VisitFromStr<T>
	where
		T: FromStr,
		<T as FromStr>::Err: Display
{

	pub fn deserialize<'de,D>(deserializer: D) -> Result<T,D::Error>
		where
			D: Deserializer<'de>
	{
		deserializer.deserialize_str(VisitFromStr(PhantomData))
	}
}

impl<'de,T> Visitor<'de> for VisitFromStr<T>
	where
		T: FromStr,
		<T as FromStr>::Err: Display
{

	type Value = T;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a string")
	}

	fn visit_str<E>(self, value: &str) -> Result<Self::Value,E>
		where
			E: de::Error,
	{
		T::from_str(value)
			.map_err(|e| de::Error::custom(e))
	}
}


macro_rules! deserialize_from_str {

	($t:ident) => {

		impl<'de> Deserialize<'de> for $t {

			fn deserialize<D>(deserializer: D) -> Result<Self,D::Error>
				where
					D: serde::Deserializer<'de>
			{
				crate::serde::VisitFromStr::deserialize(deserializer)
			}
		}
	}
}

pub(crate) use deserialize_from_str;


pub struct BoolOrStruct<T>(PhantomData<T>);

impl<T> BoolOrStruct<T>
	where
		T: TryFrom<bool>,
		<T as TryFrom<bool>>::Error: Display
{

	pub fn deserialize<'de,D>(deserializer: D) -> Result<T,D::Error>
		where
			D: Deserializer<'de>,
			T: Deserialize<'de>
	{
		deserializer.deserialize_any(BoolOrStruct::<T>(PhantomData))
	}
}

impl<'de,T> Visitor<'de> for BoolOrStruct<T>
	where
		T: TryFrom<bool> + Deserialize<'de>,
		<T as TryFrom<bool>>::Error: Display
{

	type Value = T;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("boolean or table")
	}

	fn visit_bool<E>(self, value: bool) -> Result<Self::Value,E>
		where
			E: de::Error
	{
		let val = T::try_from(value)
			.map_err(|e| de::Error::custom(e))?;
		Ok(val)
	}

	fn visit_map<M>(self, map: M) -> Result<Self::Value,M::Error>
		where
			M: MapAccess<'de>
	{
		T::deserialize(de::value::MapAccessDeserializer::new(map))
	}
}

pub struct OptBoolOrStruct<T>(PhantomData<T>);

impl<T> OptBoolOrStruct<T>
	where
		T: TryFrom<bool>,
		<T as TryFrom<bool>>::Error: Display
{

	pub fn deserialize<'de,D>(deserializer: D) -> Result<Option<T>,D::Error>
		where
			D: Deserializer<'de>,
			T: Deserialize<'de>
	{
		deserializer.deserialize_option(OptBoolOrStruct(PhantomData))
	}

	pub fn default() -> Option<T> {
		None
	}
}

impl<'de,T> Visitor<'de> for OptBoolOrStruct<T>
	where
		T: TryFrom<bool> + Deserialize<'de>,
		<T as TryFrom<bool>>::Error: Display
{

	type Value = Option<T>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("none or some(boolean or table)")
	}

	fn visit_none<E>(self) -> Result<Self::Value, E>
		where
			E: de::Error,
	{
		Ok(None)
	}

	fn visit_some<D>(self, deserializer: D) -> Result<Self::Value,D::Error>
		where
			D: Deserializer<'de>,
	{
		BoolOrStruct::<T>::deserialize(deserializer)
			.map(Some)
	}
}
