
use std::collections::{HashMap, HashSet};
use std::net::SocketAddr;
use std::sync::Arc;

use anyhow::{Context, Result};
use axum::http::{header, HeaderMap, HeaderName, HeaderValue, Method, StatusCode};
use axum::{Extension, middleware, Router};
use axum::body::boxed;
use axum::extract::{Path as AxumPath};
use axum::response::{Html, IntoResponse, Response};
use axum::routing::get;
use fly_accept_encoding::Encoding;
use hyper::Body;
use rust_embed::{Embed, EmbeddedFile};
use serde::Deserialize;
use tokio::sync::RwLock;
use tower_http::cors::{Any, CorsLayer};
use tracing::info;

use crate::beacon::{admin, Beacon};
use crate::beacon::database::Database;
use crate::beacon::guests::{ArgsGuests, guests_routes};
use crate::beacon::messages::ProtoTransport;
use crate::beacon::tls::https::HttpsServer;
use crate::beacon::tls::TlsState;
use crate::net::{BindMode, ConfigAddr};
use crate::net::listeners::TcpListeners;
use crate::protobuf::guests;
use crate::protobuf::guests::About;


pub const DEFAULT_HTTPS_PORT: u16 = 443;


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigBeaconHttpsServer {
	pub bind_mode: Option<BindMode>,
	#[serde(deserialize_with = "ConfigAddr::deserialize_vec")]
	#[serde(default = "ConfigAddr::deserialize_vec_default")]
	pub addrs: Option<Vec<ConfigAddr>>
}

impl ConfigBeaconHttpsServer {

	pub fn to_args(self) -> Result<ArgsBeaconHttpsServer> {
		let default = ArgsBeaconHttpsServer::default();
		Ok(ArgsBeaconHttpsServer {
			bind_mode: self.bind_mode.unwrap_or(default.bind_mode),
			addrs: self.addrs.unwrap_or(default.addrs)
		})
	}
}

impl Default for ConfigBeaconHttpsServer {

	fn default() -> Self {
		Self {
			bind_mode: None,
			addrs: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsBeaconHttpsServer {
	bind_mode: BindMode,
	addrs: Vec<ConfigAddr>
}

impl Default for ArgsBeaconHttpsServer {

	fn default() -> Self {

		let args = Self {
			bind_mode: BindMode::Both,
			addrs: ConfigAddr::all_interfaces()
		};

		#[cfg(test)]
		let args = Self {
			addrs: vec![ConfigAddr::localhost_auto_port()],
			.. args
		};

		args
	}
}


enum FileResponse {
	Found {
		headers: Vec<(HeaderName,HeaderValue)>,
		body: Body
	},
	NotFound,
	MovedPermanently {
		url: String
	}
}

impl IntoResponse for FileResponse {

	fn into_response(self) -> Response {
		match self {

			Self::Found { headers, body } => {
				let mut response = Response::new(boxed(body));
				*response.status_mut() = StatusCode::OK;
				for (name, value) in headers {
					response.headers_mut().insert(name, value);
				}
				response
			}

			Self::NotFound => {
				let mut response = Response::new(boxed(Body::empty()));
				*response.status_mut() = StatusCode::NOT_FOUND;
				response
			}

			Self::MovedPermanently { url } => {
				let mut response = Response::new(boxed(Body::empty()));
				*response.status_mut() = StatusCode::MOVED_PERMANENTLY;
				if let Ok(v) = HeaderValue::try_from(url) {
					response.headers_mut().insert(header::LOCATION, v);
				}
				response
			}
		}
	}
}


#[derive(Embed)]
#[folder = "src/beacon/admin_dashboard/build/dashboard"]
struct Assets;


struct ArgsAssetServer {
	url: &'static str,
	home: &'static str,
	content_types: HashMap<&'static str,&'static str>,
	compress_types: HashSet<&'static str>
}

struct AssetServer {
	args: ArgsAssetServer
	// NOTE: add members here in the future if the server needs state
}

impl AssetServer {

	fn new(args: ArgsAssetServer) -> Self {
		Self {
			args
		}
	}

	fn home_url(&self) -> String {
		format!("{}/{}", self.args.url, self.args.home)
	}

	fn serve(&self, path: impl AsRef<str>, request_headers: HeaderMap) -> FileResponse {

		let path = path.as_ref();

		// lookup the content type, if any
		let extension = path
			.split('/')
			.last()
			.unwrap_or(path)
			.split('.')
			.last()
			.unwrap_or("");
		let content_type = self.args.content_types.get(extension)
			.map(|s| *s);

		let Some(mut file) = Assets::get(&path)
			else { return FileResponse::NotFound };

		let mut response_headers = Vec::new();

		// TODO: last modified header?

		// add the content-type header, if needed
		if let Some(content_type) = &content_type {
			response_headers.push((header::CONTENT_TYPE, HeaderValue::from_static(content_type)));

			// serve a compressed file if we can
			if let Some((compressed_file, encoding)) = self.compressed_file(path, &content_type, &request_headers) {
				file = compressed_file;
				response_headers.push((header::CONTENT_ENCODING, encoding.to_header_value()));
			}
		}

		FileResponse::Found {
			headers: response_headers,
			body: Body::from(file.data)
		}
	}

	fn compressed_file(&self, path: &str, content_type: &str, request_headers: &HeaderMap) -> Option<(EmbeddedFile,Encoding)> {

		// if compression isn't needed, we're done here
		if !self.args.compress_types.contains(content_type) {
			return None;
		}

		// look for supported encodings
		let accept_encoding = fly_accept_encoding::encodings_iter(&request_headers)
			.filter_map(|result| result.ok())
			.map(|(e, q)| {
				// convert the f32 quality numbers to i32 so we can sort them using a total order
				let q = match q.is_nan() {
					true => 0,
					false => (q*1000.0) as u32
				};
				(e, q)
			})
			.collect::<Vec<_>>();

		// group the encodings by quality number
		let mut accept_groups = Vec::<(u32,Vec<Option<Encoding>>)>::new();
		for (encoding, quality) in accept_encoding {
			if let Some((_, group)) = accept_groups.iter_mut().find(|(q, _)| q == &quality) {
				group.push(encoding)
			} else {
				accept_groups.push((quality, vec!(encoding)));
			}
		}

		// sort in order of decreasing quality
		// NOTE: we want a stable sort here so we deterministically respond with the same type of compression each time
		accept_groups.sort_by(|(a, _), (b, _)| b.cmp(a));

		// for each (ordered) group, pick the option we like best
		for (_, encodings) in accept_groups {

			let compressed_files = encodings.into_iter()
				.filter_map(|encoding| {

					// treat `*` as `Identity`
					let encoding = encoding
						.unwrap_or(Encoding::Identity);

					// try to find a matching compressed file
					let ext = match encoding {
						Encoding::Zstd => "zstd",
						Encoding::Brotli => "br",
						Encoding::Gzip => "gz",
						Encoding::Deflate => return None, // ignore deflate: it's just stupid gzip
						Encoding::Identity => return Some(None)
					};
					let Some(file) = Assets::get(&format!("{}.{}", path, ext))
						else { return None; };

					Some(Some((file, encoding)))
				})
				.collect::<Vec<_>>();

			// if we have muliple matching compressions, prefer zstd, then brotli, then gzip, then identity
			let i =
				if let Some(i) = compressed_files.iter().position(|f| matches!(f, Some((_, Encoding::Zstd)))) {
					i
				} else if let Some(i) = compressed_files.iter().position(|f| matches!(f, Some((_, Encoding::Brotli)))) {
					i
				} else if let Some(i) = compressed_files.iter().position(|f| matches!(f, Some((_, Encoding::Gzip)))) {
					i
				} else if let Some(i) = compressed_files.iter().position(|f| matches!(f, Some((_, Encoding::Identity)))) {
					i
				} else {
					// nothing matched, advance to the next group
					continue;
				};
			return compressed_files.into_iter()
				.nth(i)
				.unwrap();
				// PANIC SAFETY: i came from iter().position()
		}

		// no matching pre-compressed files
		None
	}

	fn into_router(self) -> Router {
		let url = self.args.url;
		Router::new()
			// root routes get mapped to the home page
			.route(url, get(|server: Extension<Arc<AssetServer>>| async move {
				FileResponse::MovedPermanently {
					url: server.home_url()
				}
			}))
			.route(&format!("{}/", url), get(|server: Extension<Arc<AssetServer>>| async move {
				FileResponse::MovedPermanently {
					url: server.home_url()
				}
			}))
			// everything else gets mapped to itself
			.route(&format!("{}/*path", url), get(|
				server: Extension<Arc<AssetServer>>,
				AxumPath(path): AxumPath<String>,
				headers: HeaderMap
			| async move {
				server.serve(path, headers)
			}))
			// axum says layers have to go after routes ¯\_(ツ)_/¯
			.layer(Extension(Arc::new(self)))
	}
}


pub struct BeaconHttpsServer {
	addrs: Vec<SocketAddr>,
	https: HttpsServer
}

impl BeaconHttpsServer {

	pub fn router(beacon: Arc<RwLock<Beacon>>, database: Arc<Database>, args_guests: Option<ArgsGuests>) -> Router {

		let mut router = Router::new()
			.route("/", get(home))
			.route("/about", get(about))
			.nest("/admin/api", admin::routes())
			.merge(AssetServer::new(ArgsAssetServer {
				url: "/admin",
				home: "hosts",
				content_types: HashMap::from([
					("", "text/html"),
					("css", "text/css"),
					("js", "application/javascript"),
					("svg", "image/svg+xml")
				]),
				compress_types: HashSet::from([
					"text/html",
					"text/css",
					"application/javascript",
					"image/svg+xml"
				])
			}).into_router());

		// apply routes
		if args_guests.is_some() {
			router = router.nest("/guests", guests_routes());
		}

		// configure CORS
		router = router.route_layer(
			CorsLayer::new()
				.allow_methods([Method::GET, Method::POST])
				.allow_origin(Any)
				.allow_headers([header::CONTENT_TYPE])
		);

		// apply the authn/authz layer *before* the extensions
		router = router.route_layer(middleware::from_fn(admin::session));

		// apply layers
		// NOTE: these layers apply to nested routes as well
		router = router.route_layer(Extension(beacon));
		router = router.route_layer(Extension(database));
		if let Some(args) = args_guests {
			router = router.route_layer(Extension(args));
		}

		router
	}

	#[tracing::instrument(skip_all, level = 5, name = "HTTPs")]
	pub async fn start(
		args: ArgsBeaconHttpsServer,
		args_guests: Option<ArgsGuests>,
		beacon: Arc<RwLock<Beacon>>,
		database: Arc<Database>,
		tls_state: &TlsState
	) -> Result<Self> {

		// listen on the sockets with TCP
		let tcp_listeners = TcpListeners::bind_all(&args.addrs, DEFAULT_HTTPS_PORT, args.bind_mode)
			.await.context("Failed to bind TCP listeners")?;
		let addrs = tcp_listeners.addrs().clone();
		info!(?addrs, "Listening");

		let router = Self::router(beacon, database, args_guests);
		let https = HttpsServer::start(tcp_listeners, router, &tls_state)
			.await.context("Failed to start HTTPs server")?;

		Ok(Self {
			addrs,
			https
		})
	}

	#[allow(unused)]
	pub fn addrs(&self) -> &Vec<SocketAddr> {
		&self.addrs
	}

	/// sends a shutdown signal to the server and waits for it to stop
	#[tracing::instrument(skip_all, level = 5, name = "HTTPs")]
	pub async fn shutdown(self) {

		self.https.shutdown()
			.await;

		info!("finished");
	}
}


async fn home() -> Html<String> {
	let version = env!("CARGO_PKG_VERSION");
	Html(format!(r#"
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>SmolWeb Beacon Server</title>
</head>
<body>

	<h1>SmolWeb Beacon Server</h1>
	<p>
		Running software v{version}
	</p>

</body>
</html>
		"#))
}


async fn about() -> ProtoTransport {
	let msg = guests::beacon::encode_about(About {
		version: env!("CARGO_PKG_VERSION").to_string()
	});
	ProtoTransport::from_bytes(msg)
}


#[cfg(test)]
pub(crate) mod test {

	use galvanic_assert::{assert_that, matchers::*};

	use smolweb_test_tools::logging::init_test_logging;

	use crate::beacon::guests::ArgsGuests;
	use crate::beacon::guests::test::TestBeaconGuestsServer;
	use crate::config::test::deserialize_from_toml;
	use crate::tls::test::acmed::Acmed;

	use super::*;


	#[test]
	fn config() {
		let _logging = init_test_logging();

		let config = deserialize_from_toml::<ConfigBeaconHttpsServer>(r#"
		"#).unwrap();
		assert_that!(&config, eq(ConfigBeaconHttpsServer::default()));

		let config = deserialize_from_toml::<ConfigBeaconHttpsServer>(r#"
			bind_mode = "both"
		"#).unwrap();
		assert_that!(&config, eq(ConfigBeaconHttpsServer {
			bind_mode: Some(BindMode::Both),
			.. ConfigBeaconHttpsServer::default()
		}));

		let config = deserialize_from_toml::<ConfigBeaconHttpsServer>(r#"
			addrs = "bar"
		"#).unwrap();
		assert_that!(&config, eq(ConfigBeaconHttpsServer {
			addrs: Some(vec![ConfigAddr::new("bar", None)]),
			.. ConfigBeaconHttpsServer::default()
		}));

		let config = deserialize_from_toml::<ConfigBeaconHttpsServer>(r#"
			addrs = ["bar:5"]
		"#).unwrap();
		assert_that!(&config, eq(ConfigBeaconHttpsServer {
			addrs: Some(vec![ConfigAddr::new("bar", Some(5))]),
			.. ConfigBeaconHttpsServer::default()
		}));
	}


	#[test]
	fn to_args() {
		let _logging = init_test_logging();

		assert_that!(&ConfigBeaconHttpsServer::default().to_args().unwrap(), eq(ArgsBeaconHttpsServer::default()));
	}


	#[tokio::test]
	async fn about_cert() {
		let _logging = init_test_logging();

		let args = ArgsGuests::default();
		let server = TestBeaconGuestsServer::start(&args)
			.await;

		let about = server.client()
			.about()
			.await.unwrap();

		assert_that!(&about.version, eq(env!("CARGO_PKG_VERSION").to_string()));

		server.shutdown()
			.await;
	}


	#[tokio::test]
	async fn about_acme() {
		let _logging = init_test_logging();

		let acmed = Acmed::start()
			.await;

		let args = ArgsGuests::default();
		let server = TestBeaconGuestsServer::start_acme(&args, &acmed)
			.await;

		let about = server.client()
			.about()
			.await.unwrap();

		assert_that!(&about.version, eq(env!("CARGO_PKG_VERSION").to_string()));

		acmed.shutdown()
			.await;
		server.shutdown()
			.await;
	}
}
