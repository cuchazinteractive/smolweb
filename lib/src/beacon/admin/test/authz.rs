
use burgerid::ProtobufSave;
use galvanic_assert::{assert_that, matchers::*};
use time::OffsetDateTime;

use smolweb_test_tools::logging::init_test_logging;

use crate::beacon::admin::test::{alice, bob, login, TestAdminServer};
use crate::beacon::AdminClient;
use crate::beacon::database::personnel::PersonnelData;
use crate::protobuf::smolweb::admin as proto;


#[tokio::test]
async fn role_logged_out() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let client = server.client();

	let result = client
		.authz_role()
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthenticated")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn role_none() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (bob, bob_key) = bob();
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);

	let response = client
		.authz_role()
		.await.unwrap();

	assert_that!(&response.role(), eq(proto::PersonnelRole::None));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn role_owner() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	let response = client
		.authz_role()
		.await.unwrap();

	assert_that!(&response.role(), eq(proto::PersonnelRole::Owner));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn admins() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// shouldn't have any admins yet
	let admins = client.authz_admins_list()
		.await.unwrap();
	assert_that!(&admins.len(), eq(0));

	// make bob an admin
	let (bob, bob_key) = bob();
	client
		.authz_admins_add(proto::AddPersonnel {
			id: bob.save_bytes()
		})
		.await.unwrap();

	// bob's an admin! =D
	let admins = client.authz_admins_list()
		.await.unwrap();
	assert_that!(&admins.len(), eq(1));
	let admin = PersonnelData::from(admins[0].clone());
	assert_that!(&admin.uid.as_ref(), eq(bob.uid()));
	assert_that!(&admin.name.as_str(), eq(bob.name()));
	assert_that!(&admin.role(), eq(proto::PersonnelRole::Admin));
	assert_that!(&(admin.added().unwrap() - OffsetDateTime::now_utc()).whole_seconds(), lt(5));
	assert_that!(&admin.last_login(), eq(None));

	// login as bob and get the role
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let bob_role = client.authz_role()
		.await.unwrap();
	assert_that!(&bob_role.role(), eq(proto::PersonnelRole::Admin));

	// login as alice
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// look for bob's login time
	let admins = client.authz_admins_list()
		.await.unwrap();
	assert_that!(&admins.len(), eq(1));
	let admin = PersonnelData::from(admins[0].clone());
	assert_that!(&(admin.last_login().unwrap().unwrap() - OffsetDateTime::now_utc()).whole_seconds(), lt(5));

	// whoops, sorry bob
	client
		.authz_admins_remove(proto::RemovePersonnel {
			uid: bob.uid().to_vec()
		})
		.await.unwrap();

	// no more admins =(
	let admins = client.authz_admins_list()
		.await.unwrap();
	assert_that!(&admins.len(), eq(0));

	// login as bob and get the role
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let bob_role = client.authz_role()
		.await.unwrap();
	assert_that!(&bob_role.role(), eq(proto::PersonnelRole::None));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn mods() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// shouldn't have any mods yet
	let mods = client.authz_mods_list()
		.await.unwrap();
	assert_that!(&mods.len(), eq(0));

	// make bob a moderator
	let (bob, bob_key) = bob();
	client
		.authz_mods_add(proto::AddPersonnel {
			id: bob.save_bytes()
		})
		.await.unwrap();

	// bob's a moderator
	let mods = client.authz_mods_list()
		.await.unwrap();
	assert_that!(&mods.len(), eq(1));
	let m = PersonnelData::from(mods[0].clone());
	assert_that!(&m.uid.as_ref(), eq(bob.uid()));
	assert_that!(&m.name.as_str(), eq(bob.name()));
	assert_that!(&m.role(), eq(proto::PersonnelRole::Moderator));
	assert_that!(&(m.added().unwrap() - OffsetDateTime::now_utc()).whole_seconds(), lt(5));
	assert_that!(&m.last_login(), eq(None));

	// login as bob and get the role
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let bob_role = client.authz_role()
		.await.unwrap();
	assert_that!(&bob_role.role(), eq(proto::PersonnelRole::Moderator));

	// login as alice
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// look for bob's login time
	let mods = client.authz_mods_list()
		.await.unwrap();
	assert_that!(&mods.len(), eq(1));
	let m = PersonnelData::from(mods[0].clone());
	assert_that!(&(m.last_login().unwrap().unwrap() - OffsetDateTime::now_utc()).whole_seconds(), lt(5));

	// whoops, sorry bob
	client
		.authz_mods_remove(proto::RemovePersonnel {
			uid: bob.uid().to_vec()
		})
		.await.unwrap();

	// no more mods
	let mods = client.authz_mods_list()
		.await.unwrap();
	assert_that!(&mods.len(), eq(0));

	// login as bob and get the role
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let bob_role = client.authz_role()
		.await.unwrap();
	assert_that!(&bob_role.role(), eq(proto::PersonnelRole::None));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn admins_add_mods_remove() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// make bob an admin
	let (bob, _bob_key) = bob();
	client
		.authz_admins_add(proto::AddPersonnel {
			id: bob.save_bytes()
		})
		.await.unwrap();

	// try to remove bob as a moderator
	let result = client
		.authz_mods_remove(proto::RemovePersonnel {
			uid: bob.uid().to_vec()
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAllowed")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn mods_add_admins_remove() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// make bob a moderator
	let (bob, _bob_key) = bob();
	client
		.authz_mods_add(proto::AddPersonnel {
			id: bob.save_bytes()
		})
		.await.unwrap();

	// try to remove bob as an admin
	let result = client
		.authz_admins_remove(proto::RemovePersonnel {
			uid: bob.uid().to_vec()
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAllowed")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn promotion() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// make bob a moderator
	let (bob, _bob_key) = bob();
	client
		.authz_mods_add(proto::AddPersonnel {
			id: bob.save_bytes()
		})
		.await.unwrap();

	// promote bob to an admin
	client
		.authz_admins_add(proto::AddPersonnel {
			id: bob.save_bytes()
		})
		.await.unwrap();

	// bob's your uncle!
	let mods = client.authz_mods_list()
		.await.unwrap();
	assert_that!(&mods.len(), eq(0));
	let admins = client.authz_admins_list()
		.await.unwrap();
	assert_that!(&admins.len(), eq(1));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn demotion_owner() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// can't demote the owner
	let result = client
		.authz_admins_add(proto::AddPersonnel {
			id: alice.save_bytes()
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAllowed")));

	let result = client
		.authz_mods_add(proto::AddPersonnel {
			id: alice.save_bytes()
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAllowed")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn demotion_admin() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// make bob an admin
	let (bob, _bob_key) = bob();
	client
		.authz_admins_add(proto::AddPersonnel {
			id: bob.save_bytes()
		})
		.await.unwrap();

	// can't demote an admin
	let result = client
		.authz_mods_add(proto::AddPersonnel {
			id: bob.save_bytes()
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAllowed")));

	server.shutdown()
		.await;
}
