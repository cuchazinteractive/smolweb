
use burgerid::{add_app_context, ArgsAuthnClient, AuthnClient, ProtobufSave};
use galvanic_assert::{assert_that, matchers::*};

use smolweb_test_tools::logging::init_test_logging;

use crate::beacon::admin::test::{alice, TestAdminServer};
use crate::beacon::{AdminClient, LOGIN_CONTEXT};
use crate::protobuf::smolweb::admin as proto;


#[tokio::test]
async fn login_challenge_bad_identity() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let client = server.client();

	let result = client
		.login_challenge(proto::LoginRequest {
			identity: vec![1, 2, 3]
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("Identity")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn login_response_without_challenge() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let client = server.client();

	let result = client
		.login_response(proto::LoginResponse {
			uid: vec![1, 2, 3],
			signed: vec![4, 5, 6],
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NoChallenge")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn login_challenge_not_signed() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let client = server.client();

	let (alice, _alice_key) = alice();

	let _challenge = client
		.login_challenge(proto::LoginRequest {
			identity: alice.save_bytes()
		})
		.await.unwrap();

	let result = client
		.login_response(proto::LoginResponse {
			uid: alice.uid().clone(),
			signed: vec![1, 2, 3],
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("AuthnFail")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn login_challenge_wrong_context() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let client = server.client();

	let (alice, alice_key) = alice();
	let unlocked = alice.unlock(&alice_key)
		.unwrap();

	let challenge = client
		.login_challenge(proto::LoginRequest {
			identity: alice.save_bytes()
		})
		.await.unwrap();

	let result = client
		.login_response(proto::LoginResponse {
			uid: alice.uid().clone(),
			signed: unlocked.sign(&challenge.nonce, Some(b"not-the-right-context"))
				.unwrap()
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("AuthnFail")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn login_challenge_wrong_nonce() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let client = server.client();

	let (alice, alice_key) = alice();
	let unlocked = alice.unlock(&alice_key)
		.unwrap();

	let _challenge = client
		.login_challenge(proto::LoginRequest {
			identity: alice.save_bytes()
		})
		.await.unwrap();

	let context = add_app_context(Some(LOGIN_CONTEXT), None::<&[u8]>)
		.unwrap();
	let result = client
		.login_response(proto::LoginResponse {
			uid: alice.uid().clone(),
			signed: unlocked.sign(vec![1, 2, 3], Some(context))
				.unwrap(),
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("AuthnFail")));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn login_challenge_response() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (alice, alice_key) = alice();
	let unlocked = alice.unlock(&alice_key)
		.unwrap();

	let challenge = client
		.login_challenge(proto::LoginRequest {
			identity: alice.save_bytes()
		})
		.await.unwrap();

	let mut args_authn = ArgsAuthnClient::default(LOGIN_CONTEXT);
	args_authn.signature_has_apps = true;
	let authn_client = AuthnClient::new(args_authn, &unlocked);
	let success = client
		.login_response(proto::LoginResponse {
			uid: alice.uid().clone(),
			signed: authn_client.respond(&challenge.nonce)
				.unwrap(),
		})
		.await.unwrap();

	assert_that!(&success.token.len(), eq(32));
	client.token = Some(success.token);

	let logged_in = client
		.login_check()
		.await.unwrap();
	assert_that!(&logged_in, eq(true));

	client
		.logout()
		.await.unwrap();

	let logged_in = client
		.login_check()
		.await.unwrap();
	assert_that!(&logged_in, eq(false));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn login_check_out() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let client = server.client();

	let logged_in = client
		.login_check()
		.await.unwrap();
	assert_that!(&logged_in, eq(false));

	server.shutdown()
		.await;
}
