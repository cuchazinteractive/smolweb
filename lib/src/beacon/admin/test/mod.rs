
mod authn;
mod authz;
mod hosts;


use std::sync::{Arc, OnceLock};
use anyhow::{Context, Result};

use burgerid::{IdentityPersonal, IdentityKeyPersonal, ProtobufSave, IdentityApp, IdentityKeyApp, ArgsAuthnClient, AuthnClient};
use tokio::sync::RwLock;
use tracing::debug;

use crate::beacon::{AdminClient, ArgsAdminClient, Beacon, LOGIN_CONTEXT};
use crate::beacon::database::{ArgsDatabase, Database};
use crate::beacon::hosts::{ArgsHosts, HostsServer};
use crate::beacon::https::{ArgsBeaconHttpsServer, BeaconHttpsServer};
use crate::beacon::tls::TlsState;
use crate::host::client::{ArgsHostsClient, HostsClient, HostsClientConnectError};
use crate::net::{ConfigAddr, ConnectMode, FilterAddrs};
use crate::protobuf::smolweb::admin as proto;
use crate::tls::test::{generate_domain_cert, root_cert};


const PASSPHRASE: &str = "passphrase";

/// Owner of the test server
static ALICE: OnceLock<(IdentityPersonal,IdentityKeyPersonal)> = OnceLock::new();
pub fn alice() -> &'static (IdentityPersonal, IdentityKeyPersonal) {
	ALICE.get_or_init(|| {
		IdentityPersonal::new("Alice", PASSPHRASE)
			.unwrap()
	})
}

/// No role on test server
static BOB: OnceLock<(IdentityPersonal,IdentityKeyPersonal)> = OnceLock::new();
pub fn bob() -> &'static (IdentityPersonal, IdentityKeyPersonal) {
	BOB.get_or_init(|| {
		IdentityPersonal::new("Bob", PASSPHRASE)
			.unwrap()
	})
}


/// Owner of an app
static CAROL: OnceLock<(IdentityPersonal,IdentityKeyPersonal)> = OnceLock::new();
pub fn carol() -> &'static (IdentityPersonal, IdentityKeyPersonal) {
	CAROL.get_or_init(|| {
		IdentityPersonal::new("Carol", PASSPHRASE)
			.unwrap()
	})
}
static CAROL_APP: OnceLock<(IdentityApp,IdentityKeyApp)> = OnceLock::new();
pub fn carol_app() -> &'static (IdentityApp, IdentityKeyApp) {
	CAROL_APP.get_or_init(|| {
		let (carol, carol_key) = carol();
		let unlocked = carol.unlock(&carol_key)
			.unwrap();
		IdentityApp::new("App", &unlocked)
			.unwrap()
	})
}


struct TestAdminServer {
	beacon: Arc<RwLock<Beacon>>,
	tls_state: TlsState,
	https: BeaconHttpsServer,
	hosts: HostsServer,
	domain: String,
	database: Arc<Database>
}

impl TestAdminServer {

	pub async fn start() -> Self {

		// alice is the owner of the server
		let (alice, _alice_key) = alice();

		let args_hosts = ArgsHosts::default();

		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let database = Arc::new(database);
		let beacon = Arc::new(RwLock::new(Beacon::new(alice.uid().clone(), &args_hosts, database.clone())));

		let tls_state = generate_domain_cert("test.localhost")
			.to_tls_args()
			.state();

		let args_https = ArgsBeaconHttpsServer::default();
		let https = BeaconHttpsServer::start(
			args_https,
			None,
			beacon.clone(),
			database.clone(),
			&tls_state
		).await.unwrap();

		// start a hosts server too
		let hosts = HostsServer::start(
			args_hosts,
			beacon.clone(),
			database.clone(),
			&tls_state
		).await.unwrap();

		let domain = tls_state.any_domain();

		Self {
			beacon,
			tls_state,
			https,
			hosts,
			domain,
			database
		}
	}

	#[tracing::instrument(skip_all, level = 5, name = "TestGuestServer")]
	pub fn client(&self) -> AdminClient {

		let connect_mode = ConnectMode::PreferIpv6;

		let port = self.https.addrs()
			.filter_addrs_for_connect(connect_mode)
			.first()
			.unwrap()
			.port();
		let beacon_addr = ConfigAddr::new(self.domain.clone(), Some(port));
		debug!(%beacon_addr, "making admin client");

		let args = ArgsAdminClient {
			addr: beacon_addr,
			http_config: Some(Box::new(|builder| {
				let root_cert_der = root_cert()
					.serialize_der()
					.unwrap();
				let root_cert = reqwest::Certificate::from_der(root_cert_der.as_slice())
					.unwrap();
				builder
					.tls_built_in_root_certs(false)
					.add_root_certificate(root_cert)
			})),
			connect_mode
		};
		AdminClient::new(args)
			.unwrap()
	}

	async fn connect_host(&self, app_id: &IdentityApp, app_key: &IdentityKeyApp) -> Result<HostsClient,HostsClientConnectError> {
		let mut args = ArgsHostsClient::default(app_id, app_key);
		let port = self.hosts.addrs()
			.filter_addrs_for_connect(ConnectMode::PreferIpv6)
			.first()
			.unwrap()
			.port();
		args.addr = ConfigAddr::new(self.tls_state.any_domain(), Some(port));
		HostsClient::connect(args)
			.await
	}

	pub fn database(&self) -> &Database {
		&self.database
	}

	pub async fn shutdown(self) -> Beacon {
		self.hosts.shutdown()
			.await;
		self.https.shutdown()
			.await;
		Arc::into_inner(self.beacon)
			.expect("beacon reference leak")
			.into_inner()
	}
}


async fn login(client: &AdminClient, id: &IdentityPersonal, key: &IdentityKeyPersonal) -> Result<Vec<u8>> {

	let unlocked = id.unlock(key)
		.unwrap();

	let challenge = client
		.login_challenge(proto::LoginRequest {
			identity: id.save_bytes()
		})
		.await
		.context("Failed to get challenge")?;

	let mut args_authn = ArgsAuthnClient::default(LOGIN_CONTEXT);
	args_authn.signature_has_apps = true;
	let authn_client = AuthnClient::new(args_authn, &unlocked);
	let success = client
		.login_response(proto::LoginResponse {
			uid: id.uid().clone(),
			signed: authn_client.respond(&challenge.nonce)
				.context("Failed to respond to challenge")?,
		})
		.await
		.context("Failed authentication")?;

	Ok(success.token)
}
