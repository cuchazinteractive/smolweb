
use burgerid::ProtobufSave;
use galvanic_assert::{assert_that, matchers::*};
use time::OffsetDateTime;

use smolweb_test_tools::id::app_id;
use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::beacon::admin::client::AdminClientError;
use crate::beacon::admin::test::{alice, bob, carol_app, login, TestAdminServer};
use crate::beacon::AdminClient;
use crate::beacon::database::hosts::Authz;
use crate::net::messages::ResponseProtobufError;
use crate::protobuf::smolweb::admin as proto;


#[tokio::test]
async fn list_empty() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	// logged out
	let result = client
		.hosts_list()
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthenticated")));

	// not authorized
	let (bob, bob_key) = bob();
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let result = client
		.hosts_list()
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthorized")));

	// owner
	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);
	let list = client
		.hosts_list()
		.await.unwrap();
	assert_that!(&list.hosts.len(), eq(0));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn list_connect() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	server.database().hosts().set_default_authz(Authz::Allow)
		.await.unwrap();

	// connect a host
	let (app_id, app_key) = carol_app();
	let host_client = server.connect_host(&app_id, &app_key)
		.await.unwrap();

	// login as owner
	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	let list = client
		.hosts_list()
		.await.unwrap();
	assert_that!(&list.hosts.len(), eq(1));
	let host = &list.hosts[0];
	assert_that!(&host.uid.as_ref(), eq(app_id.uid()));
	assert_that!(&host.authz(), eq(proto::HostAuthz::None));
	assert_that!(&host.name.as_str(), eq(app_id.name()));
	assert_that!(&host.connection_status(), eq(proto::HostConnectionStatus::Connected));

	// make sure the connection times are very recent
	// (since we probably can't guess the exact connection times down to the second)
	let first_connect = host.first_connect
		.unwrap();
	let first_connect = OffsetDateTime::from_unix_timestamp(first_connect)
		.unwrap();
	assert_that!(&(first_connect - OffsetDateTime::now_utc()).whole_seconds(), lt(5));
	let last_connect = host.last_connect
		.unwrap();
	let last_connect = OffsetDateTime::from_unix_timestamp(last_connect)
		.unwrap();
	assert_that!(&(last_connect - OffsetDateTime::now_utc()).whole_seconds(), lt(5));

	// disconnect the host
	host_client.disconnect();

	let list = client
		.hosts_list()
		.await.unwrap();
	assert_that!(&list.hosts.len(), eq(1));
	let host = &list.hosts[0];
	assert_that!(&host.uid.as_ref(), eq(app_id.uid()));
	assert_that!(&host.connection_status(), eq(proto::HostConnectionStatus::Disconnected));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn list_authz() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	// authorize a host
	let (app_id, _app_key) = carol_app();
	server.database().hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// login as owner
	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	let list = client
		.hosts_list()
		.await.unwrap();
	assert_that!(&list.hosts.len(), eq(1));
	let host = &list.hosts[0];
	assert_that!(&host.uid.as_ref(), eq(app_id.uid()));
	assert_that!(&host.name.as_str(), eq(app_id.name()));
	assert_that!(&host.authz(), eq(proto::HostAuthz::Allow));
	assert_that!(&host.first_connect, is_match!(None));
	assert_that!(&host.last_connect, is_match!(None));
	assert_that!(&host.connection_status(), eq(proto::HostConnectionStatus::Disconnected));

	// de-authorize the host
	server.database().hosts().set_authz(app_id.uid(), Some(Authz::Deny))
		.await.unwrap();

	let list = client
		.hosts_list()
		.await.unwrap();
	assert_that!(&list.hosts.len(), eq(1));
	let host = &list.hosts[0];
	assert_that!(&host.uid.as_ref(), eq(app_id.uid()));
	assert_that!(&host.name.as_str(), eq(app_id.name()));
	assert_that!(&host.authz(), eq(proto::HostAuthz::Deny));
	assert_that!(&host.first_connect, is_match!(None));
	assert_that!(&host.last_connect, is_match!(None));
	assert_that!(&host.connection_status(), eq(proto::HostConnectionStatus::Disconnected));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn disconnect() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	server.database().hosts().set_default_authz(Authz::Allow)
		.await.unwrap();

	// connect a host
	let (app_id, app_key) = carol_app();
	let _host_client = server.connect_host(&app_id, &app_key)
		.await.unwrap();

	// logged out
	let result = client
		.hosts_disconnect(proto::HostDisconnect {
			uid: app_id.uid().clone()
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthenticated")));

	// not authorized
	let (bob, bob_key) = bob();
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let result = client
		.hosts_disconnect(proto::HostDisconnect {
			uid: app_id.uid().clone()
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthorized")));

	// login as owner
	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	let list = client
		.hosts_list()
		.await.unwrap();
	let host = &list.hosts[0];
	assert_that!(&host.uid.as_ref(), eq(app_id.uid()));
	assert_that!(&host.connection_status(), eq(proto::HostConnectionStatus::Connected));

	// disconnect the host
	client
		.hosts_disconnect(proto::HostDisconnect {
			uid: app_id.uid().clone()
		})
		.await.unwrap();

	let list = client
		.hosts_list()
		.await.unwrap();
	let host = &list.hosts[0];
	assert_that!(&host.uid.as_ref(), eq(app_id.uid()));
	assert_that!(&host.connection_status(), eq(proto::HostConnectionStatus::Disconnected));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn default_authz() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	server.database().hosts().set_default_authz(Authz::Allow)
		.await.unwrap();

	// logged out
	let result = client
		.hosts_get_default_authz()
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthenticated")));

	// not authorized
	let (bob, bob_key) = bob();
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let result = client
		.hosts_get_default_authz()
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthorized")));

	// owner
	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);
	let authz = client
		.hosts_get_default_authz()
		.await.unwrap();
	assert_that!(&Authz::from_proto(authz.authz()), eq(Some(Authz::Allow)));

	client
		.hosts_set_default_authz(proto::DefaultHostAuthz {
			authz: Authz::Deny.to_proto() as i32,
		})
		.await.unwrap();

	let authz = client
		.hosts_get_default_authz()
		.await.unwrap();
	assert_that!(&Authz::from_proto(authz.authz()), eq(Some(Authz::Deny)));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn add() {

	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (host_id, _host_key, _, _) = app_id("App");

	// logged out
	let result = client
		.hosts_add(proto::AddHost {
			id: host_id.save_bytes(),
			authz: Authz::Deny.to_proto() as i32
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthenticated")));

	// not authorized
	let (bob, bob_key) = bob();
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let result = client
		.hosts_add(proto::AddHost {
			id: host_id.save_bytes(),
			authz: Authz::Deny.to_proto() as i32
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthorized")));

	// log in as the owner
	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);

	// try to add a host with no authz
	let result = client
		.hosts_add(proto::AddHost {
			id: host_id.save_bytes(),
			authz: proto::HostAuthz::None as i32
		})
		.await;
	assert_that!(&result, is_match!(Err(AdminClientError::ReadProtobuf(ResponseProtobufError::NotOk(..)))));

	// finally, add the host
	client
		.hosts_add(proto::AddHost {
			id: host_id.save_bytes(),
			authz: Authz::Allow.to_proto() as i32
		})
		.await.unwrap();

	let hosts = client
		.hosts_list()
		.await.unwrap();
	assert_that!(&hosts.hosts.len(), eq(1));
	assert_that!(&hosts.hosts[0].uid.as_ref(), eq(host_id.uid()));
	assert_that!(&hosts.hosts[0].name.as_str(), eq(host_id.name()));
	assert_that!(&Authz::from_proto(hosts.hosts[0].authz()), eq(Some(Authz::Allow)));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn authz_set() {
	let _logging = init_test_logging();

	let server = TestAdminServer::start()
		.await;
	let mut client = server.client();

	let (host_id, _host_key, _, _) = app_id("App");
	server.database().hosts().add(&host_id, Authz::Allow)
		.await.unwrap();

	// logged out
	let result = client
		.hosts_set_authz(proto::SetHostAuthz {
			uid: host_id.uid().to_vec(),
			authz: Authz::Deny.to_proto() as i32
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthenticated")));

	// not authorized
	let (bob, bob_key) = bob();
	client.token = Some(login(&client, &bob, &bob_key)
		.await.unwrap()
	);
	let result = client
		.hosts_set_authz(proto::SetHostAuthz {
			uid: host_id.uid().to_vec(),
			authz: Authz::Deny.to_proto() as i32
		})
		.await;
	assert_that!(&AdminClient::fail_id(&result), eq(Some("NotAuthorized")));

	// owner
	let (alice, alice_key) = alice();
	client.token = Some(login(&client, &alice, &alice_key)
		.await.unwrap()
	);
	client
		.hosts_set_authz(proto::SetHostAuthz {
			uid: host_id.uid().to_vec(),
			authz: Authz::Deny.to_proto() as i32
		})
		.await.unwrap();

	let hosts = client
		.hosts_list()
		.await.unwrap();
	assert_that!(&hosts.hosts.len(), eq(1));
	assert_that!(&Authz::from_proto(hosts.hosts[0].authz()), eq(Some(Authz::Deny)));

	client
		.hosts_set_authz(proto::SetHostAuthz {
			uid: host_id.uid().to_vec(),
			authz: proto::HostAuthz::None as i32,
		})
		.await.unwrap();

	let hosts = client
		.hosts_list()
		.await.unwrap();
	assert_that!(&hosts.hosts.len(), eq(1));
	assert_that!(&Authz::from_proto(hosts.hosts[0].authz()), eq(None));

	server.shutdown()
		.await;
}
