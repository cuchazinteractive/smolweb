
use axum::http::{HeaderName, HeaderValue};
use axum::http::header::InvalidHeaderValue;
use base64::Engine;
use base64::engine::general_purpose::{URL_SAFE_NO_PAD as BASE64};
use prost::Message;
use thiserror::Error;
use tracing::warn;

use crate::beacon::admin::HEADER_TOKEN;
use crate::net::{ConfigAddr, ConnectMode};
use crate::net::https_client::HttpsProtoClient;
use crate::net::messages::{ResponseError, ResponseProtobufError, ResponseTools};
use crate::protobuf::{IntoProtobuf, ReadProtobufError};
use crate::protobuf::smolweb::admin as proto;


pub struct ArgsAdminClient {
	pub addr: ConfigAddr,
	pub http_config: Option<Box<dyn FnOnce(reqwest::ClientBuilder) -> reqwest::ClientBuilder>>,
	pub connect_mode: ConnectMode
}

impl Default for ArgsAdminClient {

	fn default() -> Self {

		let args = Self {
			addr: ConfigAddr::localhost(),
			http_config: None,
			connect_mode: ConnectMode::PreferIpv6,
		};

		#[cfg(test)]
			let args = Self {
			.. args
		};

		args
	}
}


/// A client for connecting to the admin API of a Beacon server
pub struct AdminClient {
	client: HttpsProtoClient,
	pub token: Option<Vec<u8>>
}

impl AdminClient {

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub fn new(mut args: ArgsAdminClient) -> Result<Self,reqwest::Error> {

		let client = HttpsProtoClient::new(
			args.addr.clone(),
			args.connect_mode.clone(),
			args.http_config.take()
		)?;

		Ok(Self {
			client,
			token: None
		})
	}

	fn token_headers(&self) -> Result<Vec<(HeaderName,HeaderValue)>,AdminClientError> {
		if let Some(token) = &self.token {
			let value = HeaderValue::from_str(BASE64.encode(token).as_str())?;
			Ok(vec![(HEADER_TOKEN, value)])
		} else {
			Ok(vec![])
		}
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn login_challenge(&self, requets: proto::LoginRequest) -> Result<proto::LoginChallenge,AdminClientError> {

		let challenge: proto::LoginChallenge = self.client.post("/admin/api/authn/challenge", requets.encode_to_vec())
			.await?
			.into_proto()
			.await?
			.into_protobuf()?;

		Ok(challenge)
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn login_response(&self, response: proto::LoginResponse) -> Result<proto::LoginSuccess,AdminClientError> {

		let success: proto::LoginSuccess = self.client.post("/admin/api/authn/response", response.encode_to_vec())
			.await?
			.into_proto()
			.await?
			.into_protobuf()?;

		Ok(success)
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn login_check(&self) -> Result<bool,AdminClientError> {

		let headers = self.token_headers()?;
		let result = self.client.get_headers("/admin/api/authn/check", headers)
			.await?
			.into_success()
			.await;
		match result {
			Ok(()) => Ok(true),
			Err(e) => {
				let Some(_) = Self::fail_id_from_body(&e)
					.filter(|id| *id == "NotAuthenticated")
					else { Err(ResponseProtobufError::NotOk(e))? };
				Ok(false)
			}
		}
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn logout(&self) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.get_headers("/admin/api/authn/logout", headers)
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn authz_role(&self) -> Result<proto::RoleResponse,AdminClientError> {

		let headers = self.token_headers()?;
		let response: proto::RoleResponse = self.client.get_headers("/admin/api/authz/role", headers)
			.await?
			.into_proto()
			.await?
			.into_protobuf()?;

		Ok(response)
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn authz_admins_add(&self, request: proto::AddPersonnel) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.post_headers("/admin/api/authz/admins/add", headers, request.encode_to_vec())
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn authz_admins_remove(&self, request: proto::RemovePersonnel) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.post_headers("/admin/api/authz/admins/remove", headers, request.encode_to_vec())
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn authz_admins_list(&self) -> Result<Vec<proto::PersonnelData>,AdminClientError> {

		let headers = self.token_headers()?;
		let response: proto::ListPersonnel = self.client.get_headers("/admin/api/authz/admins/list", headers)
			.await?
			.into_proto()
			.await?
			.into_protobuf()?;

		Ok(response.personnel)
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn authz_mods_add(&self, request: proto::AddPersonnel) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.post_headers("/admin/api/authz/mods/add", headers, request.encode_to_vec())
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn authz_mods_remove(&self, request: proto::RemovePersonnel) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.post_headers("/admin/api/authz/mods/remove", headers, request.encode_to_vec())
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn authz_mods_list(&self) -> Result<Vec<proto::PersonnelData>,AdminClientError> {

		let headers = self.token_headers()?;
		let response: proto::ListPersonnel = self.client.get_headers("/admin/api/authz/mods/list", headers)
			.await?
			.into_proto()
			.await?
			.into_protobuf()?;

		Ok(response.personnel)
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn hosts_list(&self) -> Result<proto::ListHosts,AdminClientError> {

		let headers = self.token_headers()?;
		let response = self.client.get_headers("/admin/api/hosts/list", headers)
			.await?
			.into_proto()
			.await?
			.into_protobuf()?;

		Ok(response)
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn hosts_disconnect(&self, request: proto::HostDisconnect) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.post_headers("/admin/api/hosts/disconnect", headers, request.encode_to_vec())
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn hosts_get_default_authz(&self) -> Result<proto::DefaultHostAuthz,AdminClientError> {

		let headers = self.token_headers()?;
		let response: proto::DefaultHostAuthz = self.client.get_headers("/admin/api/hosts/get_default_authz", headers)
			.await?
			.into_proto()
			.await?
			.into_protobuf()?;

		Ok(response)
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn hosts_set_default_authz(&self, request: proto::DefaultHostAuthz) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.post_headers("/admin/api/hosts/set_default_authz", headers, request.encode_to_vec())
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn hosts_add(&self, request: proto::AddHost) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.post_headers("/admin/api/hosts/add", headers, request.encode_to_vec())
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	#[tracing::instrument(skip_all, level = 5, name = "AdminClient")]
	pub async fn hosts_set_authz(&self, request: proto::SetHostAuthz) -> Result<(),AdminClientError> {

		let headers = self.token_headers()?;
		self.client.post_headers("/admin/api/hosts/set_authz", headers, request.encode_to_vec())
			.await?
			.into_success()
			.await
			.map_err(ResponseProtobufError::NotOk)?;

		Ok(())
	}

	pub fn fail_id_from_body(e: &ResponseError) -> Option<&str> {

		let Ok(body) = &e.body
			else { return None; };

		let Some(token) = body.split(' ')
			.next()
			else { return None; };

		if token.starts_with("Fail:") && token.ends_with(':') {
			Some(&token[5 .. token.len() - 1])
		} else {
			warn!("Non-fail server error: {:?}", e);
			None
		}
	}

	pub fn fail_id<T>(result: &Result<T,AdminClientError>) -> Option<&str> {

		let Err(e) = result
			else { return None; };

		let AdminClientError::ReadProtobuf(ResponseProtobufError::NotOk(e)) = e
			else { return None; };

		Self::fail_id_from_body(e)
	}
}



#[derive(Error, Debug)]
pub enum AdminClientError {

	#[error("Failed to encode request header")]
	RequestHeaders(#[from] InvalidHeaderValue),

	#[error("Failed to make HTTP request")]
	Reqwest(#[from] reqwest::Error),

	#[error("Failed to read protobuf from HTTP response")]
	ReadProtobuf(#[from] ResponseProtobufError),

	#[error("Failed to decode protobuf")]
	DecodeProtobuf(#[from] ReadProtobufError)
}
