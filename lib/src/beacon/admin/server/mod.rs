
mod authn;
mod authz;
mod hosts;

use std::sync::Arc;
use std::task::{Context, Poll};
use std::time::{Duration, Instant};

use axum::{Extension, Router};
use axum::body::Body;
use axum::http::Request;
use axum::middleware::Next;
use axum::response::{IntoResponse, Response};
use base64::Engine;
use base64::engine::general_purpose::{URL_SAFE_NO_PAD as BASE64};
use burgerid::OpenIdentityError;
use futures::future::BoxFuture;
use strum_macros::IntoStaticStr;
use thiserror::Error;
use tokio::sync::RwLock;
use tower::{Layer, Service};
use tracing::warn;

use crate::beacon::admin::HEADER_TOKEN;
use crate::beacon::authn::AuthnSessions;
use crate::beacon::Beacon;
use crate::beacon::database::Database;
use crate::beacon::messages::{into_proto_rejection, ProtoRequestRejection};
use crate::lang::ErrorReporting;
use crate::protobuf::smolweb::admin as proto;
use crate::protobuf::smolweb::admin::PersonnelRole::Owner;


pub fn routes() -> Router {
	Router::new()
		.nest("/authn", authn::routes())
		.nest("/authz", authz::routes())
		.nest("/hosts", hosts::routes())
}


pub struct AdminState {
	database: Arc<Database>,
	owner_uid: Vec<u8>,
	pub authn_sessions: AuthnSessions,
	logins: Vec<LoginInfo>
}

impl AdminState {

	pub fn new(owner_uid: Vec<u8>, database: Arc<Database>) -> Self {
		Self {
			database,
			owner_uid,
			authn_sessions: AuthnSessions::new(None, true),
			logins: Vec::new()
		}
	}

	fn find_login<P>(&mut self, predicate: P) -> Option<LoginExecutor>
		where
			P: FnMut(&LoginInfo) -> bool
	{
		let i = self.logins.iter()
			.position(predicate)?;

		let mut exec = LoginExecutor {
			state: self,
			i
		};

		if exec.is_current() {
			Some(exec)
		} else {
			exec.logout();
			None
		}
	}

	fn login(&mut self, uid: Vec<u8>, token: Vec<u8>) {
		self.find_login(|login| &login.uid == &uid)
			.map(|mut login| login.logout());
		self.logins.push(LoginInfo {
			uid,
			token,
			timestamp: Instant::now(),
		});
	}

	async fn get_role(&self, uid: &Vec<u8>) -> proto::PersonnelRole {

		// check for owner
		if &self.owner_uid == uid {
			return Owner;
		}

		// check for admin,mod in the database
		self.database.personnel()
			.get_role(uid)
			.await
			.warn_err()
			.ok()
			.flatten()
			.unwrap_or(proto::PersonnelRole::None)
	}
}


#[derive(Debug, Clone)]
struct LoginInfo {
	uid: Vec<u8>,
	token: Vec<u8>,
	timestamp: Instant
}


struct LoginExecutor<'s> {
	state: &'s mut AdminState,
	i: usize
}

impl<'s> LoginExecutor<'s> {

	fn info(&self) -> &LoginInfo {
		&self.state.logins[self.i]
	}

	fn is_current(&self) -> bool {
		// make sure the login is somewhat recent
		// TODO: expose config option, and test it
		Instant::now().duration_since(self.info().timestamp) < Duration::from_secs(1*60*60)
	}

	fn logout(&mut self) {
		self.state.logins.iter()
			.position(|login| &login.uid == &self.info().uid)
			.map(|i| self.state.logins.remove(i));
	}

	async fn role(&self) -> proto::PersonnelRole {

		// expired logins have no role
		if !self.is_current() {
			return proto::PersonnelRole::None;
		}

		self.state.get_role(&self.info().uid)
			.await
	}
}


#[derive(Error, Debug, IntoStaticStr)]
pub enum AdminError {

	#[error("Failed to open identity")]
	Identity(#[source] OpenIdentityError),

	#[error("This identity has attempted too many authentications recently. Try again later")]
	TooManyAttempts,

	#[error("Identity is no longer valid")]
	InvalidIdentity,

	#[error("No challenge for this response")]
	NoChallenge,

	#[error("Authentication failed")]
	AuthnFail,

	#[error("Not authenticated")]
	NotAuthenticated,

	#[error("Not authorized")]
	NotAuthorized,

	#[error("The operation was not allowed")]
	NotAllowed
}
into_proto_rejection!(AdminError);



#[derive(Debug, Clone)]
pub struct Session {
	uid: Option<Vec<u8>>,
	role: proto::PersonnelRole
}


pub async fn session<B>(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	mut request: Request<B>,
	next: Next<B>,
) -> Response {

	let mut session = Session {
		uid: None,
		role: proto::PersonnelRole::None
	};

	// get the login token, if any, from the request header
	let token = request.headers().get(HEADER_TOKEN)
		.and_then(|v| BASE64.decode(v.as_bytes()).ok());

	// lookup the UID and role for the token, if any
	if let Some(token) = token {

		let mut beacon = beacon.write()
			.await;

		if let Some(login) = beacon.admin.find_login(|info| &info.token == &token) {
			session.uid = Some(login.info().uid.clone());
			session.role = login.role()
				.await;
		}
	}

	// add the session to the request, so handlers can see it
	request.extensions_mut().insert(session);

	// go the next middleware in the stack
	next.run(request)
		.await
}


#[derive(Debug, Clone)]
pub struct RequiredRoles {
	roles: Vec<proto::PersonnelRole>
}

impl RequiredRoles {

	pub fn one_of(roles: impl IntoIterator<Item=proto::PersonnelRole>) -> Self {
		Self {
			roles: roles.into_iter().collect()
		}
	}
}

impl<S> Layer<S> for RequiredRoles {

	type Service = RequiredRolesService<S>;

	fn layer(&self, inner: S) -> Self::Service {
		RequiredRolesService {
			layer: self.clone(),
			inner
		}
	}
}


#[derive(Clone)]
pub struct RequiredRolesService<S> {
	layer: RequiredRoles,
	inner: S
}

impl<S> Service<Request<Body>> for RequiredRolesService<S>
where
	S: Service<Request<Body>,Response = Response> + Send + 'static,
	S::Future: Send + 'static,
{
	type Response = S::Response;
	type Error = S::Error;
	type Future = BoxFuture<'static,Result<Self::Response,Self::Error>>;

	fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
		self.inner.poll_ready(cx)
	}

	fn call(&mut self, request: Request<Body>) -> Self::Future {

		// lookup the session
		let Some(session) = request.extensions().get::<Session>()
			else {
				warn!("no session for request: this is probably a bug in the beacon server");
				return Box::pin(async move {
					let response = Err::<Response,_>(ProtoRequestRejection::<AdminError>::InternalError)
						.into_response();
					Ok(response)
				});
			};

		// check for not logged in
		if session.uid.is_none() {
			return Box::pin(async move {
				let response = Err::<Response,_>(ProtoRequestRejection::Fail(AdminError::NotAuthenticated))
					.into_response();
				Ok(response)
			});
		}

		// check for one of the required roles
		if !self.layer.roles.iter().any(|r| r == &session.role) {
			return Box::pin(async move {
				let response = Err::<Response,_>(ProtoRequestRejection::Fail(AdminError::NotAuthorized))
					.into_response();
				Ok(response)
			});
		}

		// run the next service in the chain
		let future = self.inner.call(request);
		Box::pin(async move {
			let response: Response = future.await?;
			Ok(response)
		})
	}
}
