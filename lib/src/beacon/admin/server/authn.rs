
use std::sync::Arc;

use axum::{Extension, Router};
use axum::routing::{get, post};
use burgerid::{AuthnServerError, Identity, ProtobufOpen};
use display_error_chain::ErrorChainExt;
use rand::RngCore;
use time::OffsetDateTime;
use tokio::sync::RwLock;
use tracing::warn;

use crate::beacon::admin::server::{AdminError, Session};
use crate::beacon::Beacon;
use crate::beacon::database::Database;
use crate::beacon::messages::{ProtoRequestRejection, ProtoTransport};
use crate::lang::ErrorReporting;
use crate::protobuf::IntoProtobuf;
use crate::protobuf::smolweb::admin as proto;


pub fn routes() -> Router {
	Router::new()
		.route("/challenge", post(login_challenge))
		.route("/response", post(login_response))
		.route("/check", get(login_check))
		.route("/logout", get(logout))
}


fn make_nonce() -> Vec<u8> {
	let mut nonce = vec![0u8; 32];
	rand::thread_rng().fill_bytes(nonce.as_mut());
	nonce
}


pub async fn login_challenge(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	database: Extension<Arc<Database>>,
	msg: ProtoTransport
) -> Result<ProtoTransport,ProtoRequestRejection<AdminError>> {

	// read the message
	let request: proto::LoginRequest = msg.into_protobuf()?;
	let id = Identity::open(request.identity)
		.map_err(AdminError::Identity)?;

	// start the login process for this user
	let nonce = {

		let mut beacon = beacon.write()
			.await;

		// erase any previous login
		beacon.admin.find_login(|login| &login.uid == id.uid())
			.map(|mut login| login.logout());

		// issue the authentication challenge
		beacon.admin.authn_sessions.get_or_make(&id)
			.challenge(database.as_ref())
			.await?
	};

	// return the nonce as a login challenge
	Ok(ProtoTransport::from_msg(proto::LoginChallenge {
		nonce
	}))
}


pub async fn login_response(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	database: Extension<Arc<Database>>,
	msg: ProtoTransport
) -> Result<ProtoTransport,ProtoRequestRejection<AdminError>> {

	// read the message
	let response: proto::LoginResponse = msg.into_protobuf()?;

	let mut beacon = beacon.write()
		.await;

	// lookup the authentication session
	let authn_session = beacon.admin.authn_sessions.get(&response.uid)
		.ok_or(AdminError::NoChallenge)?;

	// process the response
	authn_session.response(database.as_ref(), response.signed)
		.await?;

	// challenge passed: log in the id
	let uid = authn_session.id().uid().clone();
	database.personnel()
		.record_login(&uid, OffsetDateTime::now_utc())
		.await
		.warn_err()
		.ok();
	let token = make_nonce();
	beacon.admin.login(uid, token.clone());

	// send back the login token
	Ok(ProtoTransport::from_msg(proto::LoginSuccess {
		token
	}))
}


pub async fn logout(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	session: Extension<Session>
) -> Result<(),ProtoRequestRejection<AdminError>> {

	// if they're logged in, log them out
	if let Some(uid) = &session.uid {
		beacon.write()
			.await
			.admin
			.find_login(|login| &login.uid == uid)
			.map(|mut login| login.logout());
	}

	Ok(())
}


pub async fn login_check(
	session: Extension<Session>
) -> Result<(),ProtoRequestRejection<AdminError>> {

	match &session.uid {
		Some(..) => Ok(()),
		None => Err(AdminError::NotAuthenticated)?
	}
}


impl<E> From<AuthnServerError<E>> for ProtoRequestRejection<AdminError>
	where
		E: std::error::Error + 'static
{

	fn from(value: AuthnServerError<E>) -> Self {
		match value {
			AuthnServerError::Internal(e) => {
				warn!("authentication attempt failed: {}", e.chain());
				ProtoRequestRejection::InternalError
			},
			AuthnServerError::InvalidIdentity => ProtoRequestRejection::Fail(AdminError::InvalidIdentity),
			AuthnServerError::TooManyAttempts => ProtoRequestRejection::Fail(AdminError::TooManyAttempts),
			AuthnServerError::NoChallenge => ProtoRequestRejection::Fail(AdminError::NoChallenge),
			AuthnServerError::Fail => ProtoRequestRejection::Fail(AdminError::AuthnFail)
		}
	}
}
