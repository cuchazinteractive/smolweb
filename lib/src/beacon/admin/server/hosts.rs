
use std::sync::Arc;
use anyhow::Context;

use axum::{Extension, Router};
use axum::handler::Handler;
use axum::routing::{get, post};
use burgerid::{Identity, IdentityApp, ProtobufOpen};
use time::OffsetDateTime;
use tokio::sync::RwLock;

use crate::beacon::admin::server::{AdminError, RequiredRoles};
use crate::beacon::Beacon;
use crate::beacon::database::Database;
use crate::beacon::database::hosts::Authz;
use crate::beacon::messages::{ProtoRequestRejection, ProtoTransport};
use crate::protobuf::{IntoProtobuf, ReadProtobufError};
use crate::protobuf::smolweb::admin as proto;
use crate::protobuf::smolweb::admin::PersonnelRole::{Owner, Admin, Moderator};


pub fn routes() -> Router {
	Router::new()
		.route("/list", get(list.layer(RequiredRoles::one_of([Owner, Admin, Moderator]))))
		.route("/disconnect", post(disconnect.layer(RequiredRoles::one_of([Owner, Admin, Moderator]))))
		.route("/get_default_authz", get(get_default_authz.layer(RequiredRoles::one_of([Owner, Admin, Moderator]))))
		.route("/set_default_authz", post(set_default_authz.layer(RequiredRoles::one_of([Owner, Admin, Moderator]))))
		.route("/add", post(add.layer(RequiredRoles::one_of([Owner, Admin, Moderator]))))
		.route("/set_authz", post(set_authz.layer(RequiredRoles::one_of([Owner, Admin, Moderator]))))
}


pub async fn list(
	database: Extension<Arc<Database>>,
	beacon: Extension<Arc<RwLock<Beacon>>>
) -> Result<ProtoTransport,ProtoRequestRejection<AdminError>> {

	let beacon = beacon.read()
		.await;

	let hosts = database.hosts().list()
		.await?
		.into_iter()
		.map(|host_data| {
			let authz = host_data.authz()
				.map(|authz| authz.to_proto());
			let host = beacon.hosts.get(&host_data.uid);
			let connection_status = match host {
				Some(..) => proto::HostConnectionStatus::Connected,
				_ => proto::HostConnectionStatus::Disconnected
			};
			let last_contact = host.map(|h| {
				(OffsetDateTime::now_utc() - h.last_contact.elapsed()).unix_timestamp()
			});
			proto::HostData {
				uid: host_data.uid,
				name: host_data.name,
				authz: authz.map(|a| a as i32),
				first_connect: host_data.first_connect,
				last_connect: host_data.last_connect,
				connection_status: connection_status as i32,
				last_contact
			}
		})
		.collect::<Vec<_>>();

	Ok(ProtoTransport::from_msg(proto::ListHosts {
		hosts
	}))
}


pub async fn disconnect(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	msg: ProtoTransport
) -> Result<(),ProtoRequestRejection<AdminError>> {

	let request: proto::HostDisconnect = msg.into_protobuf()?;

	let mut beacon = beacon.write()
		.await;

	if let Some(host) = beacon.hosts.get_mut(&request.uid) {
		host.disconnect();
	}

	Ok(())
}


pub async fn get_default_authz(
	database: Extension<Arc<Database>>
) -> Result<ProtoTransport,ProtoRequestRejection<AdminError>> {

	let authz = database.hosts().get_default_authz()
		.await?;

	Ok(ProtoTransport::from_msg(proto::DefaultHostAuthz {
		authz: authz.to_proto() as i32
	}))
}


pub async fn set_default_authz(
	database: Extension<Arc<Database>>,
	msg: ProtoTransport
) -> Result<(),ProtoRequestRejection<AdminError>> {

	let request: proto::DefaultHostAuthz = msg.into_protobuf()?;

	let authz = Authz::from_proto(request.authz())
		.ok_or_else(|| ReadProtobufError::InvalidValue("authz", format!("{}", request.authz)))?;

	database.hosts().set_default_authz(authz)
		.await?;

	Ok(())
}


pub async fn add(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	database: Extension<Arc<Database>>,
	msg: ProtoTransport
) -> Result<(),ProtoRequestRejection<AdminError>> {

	let request: proto::AddHost = msg.into_protobuf()?;

	// read the host identity
	let host_id = IdentityApp::open(&request.id)
		.map_err(AdminError::Identity)?;

	// read the authz
	let authz = Authz::from_proto(request.authz())
		.ok_or(ReadProtobufError::InvalidValue("authz", "authz is required".to_string()))?;

	// save the keychain to the authn database
	let host_id = host_id.wrap();
	beacon.write()
		.await
		.hosts.authn_sessions.admin()
		.override_keychain(database.as_ref(), &host_id)
		.await
		.context("Failed to save host keychain to authn database")?;
	let Identity::App(host_id) = host_id
		else { panic!("not an app id") };
		// PANIC SAFETY: we just wrapped an app id, so unwrapping it is safe

	// add the host
	database.hosts().add(&host_id, authz)
		.await?;

	Ok(())
}


pub async fn set_authz(
	database: Extension<Arc<Database>>,
	msg: ProtoTransport
) -> Result<(),ProtoRequestRejection<AdminError>> {

	let request: proto::SetHostAuthz = msg.into_protobuf()?;

	// save the authorization
	let authz = Authz::from_proto(request.authz());
	database.hosts().set_authz(&request.uid, authz)
		.await?;

	Ok(())
}
