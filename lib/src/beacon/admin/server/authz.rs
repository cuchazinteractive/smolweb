
use std::sync::Arc;

use anyhow::Context;
use axum::{Extension, Router};
use axum::handler::Handler;
use axum::routing::{get, post};
use burgerid::{Identity, IdentityPersonal, ProtobufOpen};
use tokio::sync::RwLock;

use crate::beacon::admin::server::{AdminError, RequiredRoles, Session};
use crate::beacon::Beacon;
use crate::beacon::database::Database;
use crate::beacon::messages::{ProtoRequestRejection, ProtoTransport};
use crate::protobuf::IntoProtobuf;
use crate::protobuf::smolweb::admin as proto;
use crate::protobuf::smolweb::admin::PersonnelRole::{Owner, Admin, Moderator};


pub fn routes() -> Router {
	Router::new()
		.route("/role", get(role))
		.route("/admins/add", post(admins_add.layer(RequiredRoles::one_of([Owner]))))
		.route("/admins/remove", post(admins_remove.layer(RequiredRoles::one_of([Owner]))))
		.route("/admins/list", get(admins_list.layer(RequiredRoles::one_of([Owner, Admin, Moderator]))))
		.route("/mods/add", post(mods_add.layer(RequiredRoles::one_of([Owner, Admin]))))
		.route("/mods/remove", post(mods_remove.layer(RequiredRoles::one_of([Owner, Admin]))))
		.route("/mods/list", get(mods_list.layer(RequiredRoles::one_of([Owner, Admin, Moderator]))))
}


pub async fn role(
	session: Extension<Session>
) -> Result<ProtoTransport,ProtoRequestRejection<AdminError>> {

	// if we're not logged in, return the noauthn signal, instead of a none role
	if session.uid.is_none() {
		Err(AdminError::NotAuthenticated)?
	}

	Ok(ProtoTransport::from_msg(proto::RoleResponse {
		role: session.role as i32
	}))
}


pub async fn admins_add(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	database: Extension<Arc<Database>>,
	msg: ProtoTransport
) -> Result<(),ProtoRequestRejection<AdminError>> {

	let msg: proto::AddPersonnel = msg.into_protobuf()?;
	let id = IdentityPersonal::open(msg.id)
		.map_err(AdminError::Identity)?;

	// NOTE: hold a beacon write lock for the whole handler
	//       to prevent any concurrency issues caused by having a db write depend on a db read
	let beacon = beacon.write()
		.await;

	// make sure we're not demoting the owner
	let role = beacon.admin.get_role(id.uid())
		.await;
	if role == Owner {
		Err(AdminError::NotAllowed)?
	}

	// save the keychain to the authn database
	let id = id.wrap();
	beacon.admin.authn_sessions.admin()
		.override_keychain(database.as_ref(), &id)
		.await
		.context("Failed to save personnel keychain to authn database")?;
	let Identity::Personal(id) = id
		else { panic!("not a personal id") };
		// PANIC SAFETY: we just wrapped a personal id, so unwrapping it is safe

	database.personnel().add(&id, Admin)
		.await?;

	Ok(())
}


pub async fn admins_remove(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	database: Extension<Arc<Database>>,
	msg: ProtoTransport
) -> Result<(),ProtoRequestRejection<AdminError>> {

	let msg: proto::RemovePersonnel = msg.into_protobuf()?;

	// NOTE: hold a beacon write lock for the whole handler
	//       to prevent any concurrency issues caused by having a db write depend on a db read
	let beacon = beacon.write()
		.await;

	// make sure we're removing an admin
	let role = beacon.admin.get_role(&msg.uid)
		.await;
	if role != Admin {
		Err(AdminError::NotAllowed)?
	}

	database.personnel().remove(&msg.uid)
		.await?;

	Ok(())
}


pub async fn admins_list(
	database: Extension<Arc<Database>>
) -> Result<ProtoTransport,ProtoRequestRejection<AdminError>> {

	let admins = database.personnel()
		.list(Admin)
		.await?;

	Ok(ProtoTransport::from_msg(proto::ListPersonnel {
		personnel: admins.into_iter()
			.map(proto::PersonnelData::from)
			.collect()
	}))
}


pub async fn mods_add(
	database: Extension<Arc<Database>>,
	beacon: Extension<Arc<RwLock<Beacon>>>,
	msg: ProtoTransport
) -> Result<(),ProtoRequestRejection<AdminError>> {

	let msg: proto::AddPersonnel = msg.into_protobuf()?;
	let id = IdentityPersonal::open(msg.id)
		.map_err(AdminError::Identity)?;

	// NOTE: hold a beacon write lock for the whole handler
	//       to prevent any concurrency issues caused by having a db write depend on a db read
	let beacon = beacon.write()
		.await;

	// make sure we're not demoting an owner or admin
	let role = beacon.admin.get_role(id.uid())
		.await;
	if role == Owner || role == Admin {
		Err(AdminError::NotAllowed)?
	}

	// save the keychain to the authn database
	let id = id.wrap();
	beacon.admin.authn_sessions.admin()
		.override_keychain(database.as_ref(), &id)
		.await
		.context("Failed to save personnel keychain to authn database")?;
	let Identity::Personal(id) = id
		else { panic!("not a personal id") };
		// PANIC SAFETY: we just wrapped a personal id, so unwrapping it is safe

	database.personnel().add(&id, Moderator)
		.await?;

	Ok(())
}


pub async fn mods_remove(
	database: Extension<Arc<Database>>,
	beacon: Extension<Arc<RwLock<Beacon>>>,
	msg: ProtoTransport
) -> Result<(),ProtoRequestRejection<AdminError>> {

	let msg: proto::RemovePersonnel = msg.into_protobuf()?;

	// NOTE: hold a beacon write lock for the whole handler
	//       to prevent any concurrency issues caused by having a db write depend on a db read
	let beacon = beacon.write()
		.await;

	// make sure we're removing a moderator
	let role = beacon.admin.get_role(&msg.uid)
		.await;
	if role != Moderator {
		Err(AdminError::NotAllowed)?
	}

	database.personnel().remove(&msg.uid)
		.await?;

	Ok(())
}


pub async fn mods_list(
	database: Extension<Arc<Database>>
) -> Result<ProtoTransport,ProtoRequestRejection<AdminError>> {

	let admins = database.personnel()
		.list(Moderator)
		.await?;

	Ok(ProtoTransport::from_msg(proto::ListPersonnel {
		personnel: admins.into_iter()
			.map(proto::PersonnelData::from)
			.collect()
	}))
}
