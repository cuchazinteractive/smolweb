
pub(crate) mod server;
pub(crate) mod client;

#[cfg(test)]
mod test;


use axum::http::HeaderName;


pub(crate) use server::{routes, session};


pub const HEADER_TOKEN: HeaderName = HeaderName::from_static("smolweb-admin-token");
