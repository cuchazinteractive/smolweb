
use std::convert::Infallible;

use anyhow::Result;
use burgerid::IdentityPersonal;
use time::error::ComponentRange;
use time::OffsetDateTime;

use crate::beacon::database::codecs::DatabaseCodec;
use crate::beacon::database::Database;
use crate::protobuf::smolweb::admin as proto;


pub struct PersonnelRoleCodec;

impl PersonnelRoleCodec {

	pub fn encode(value: &proto::PersonnelRole) -> Vec<u8> {
		vec![match value {
			proto::PersonnelRole::Admin => 1,
			proto::PersonnelRole::Moderator => 2,
			_ => 0
		}]
	}

	pub fn decode(value: &Vec<u8>) -> proto::PersonnelRole {
		match value.get(0) {
			Some(1) => proto::PersonnelRole::Admin,
			Some(2) => proto::PersonnelRole::Moderator,
			_ => proto::PersonnelRole::None
		}
	}
}

impl DatabaseCodec for PersonnelRoleCodec {

	type NT = proto::PersonnelRole;
	type DT = Vec<u8>;
	type Error = Infallible;

	fn encode(value: &Self::NT) -> Result<Self::DT,Self::Error> {
		Ok(Self::encode(value))
	}

	fn decode(value: &Self::DT) -> Result<Self::NT,Self::Error> {
		Ok(Self::decode(value))
	}
}


pub struct PersonnelData {
	pub uid: Vec<u8>,
	pub name: String,
	pub role: Vec<u8>,
	pub added: i64,
	pub last_login: Option<i64>
}

impl PersonnelData {

	pub fn role(&self) -> proto::PersonnelRole {
		PersonnelRoleCodec::decode(&self.role)
	}

	pub fn added(&self) -> Result<OffsetDateTime,ComponentRange> {
		OffsetDateTime::from_unix_timestamp(self.added)
	}

	pub fn last_login(&self) -> Option<Result<OffsetDateTime,ComponentRange>> {
		self.last_login.map(OffsetDateTime::from_unix_timestamp)
	}
}


impl From<PersonnelData> for proto::PersonnelData {

	fn from(value: PersonnelData) -> Self {
		let role = value.role() as i32;
		proto::PersonnelData {
			uid: value.uid,
			name: value.name,
			role,
			added: value.added,
			last_login: value.last_login,
		}
	}
}

impl From<proto::PersonnelData> for PersonnelData {

	fn from(value: proto::PersonnelData) -> Self {
		let role = PersonnelRoleCodec::encode(&value.role());
		PersonnelData {
			uid: value.uid,
			name: value.name,
			role,
			added: value.added,
			last_login: value.last_login,
		}
	}
}


pub struct Personnel<'a> {
	database: &'a Database
}

impl<'a> Personnel<'a> {

	pub fn new(database: &'a Database) -> Self {
		Self {
			database
		}
	}

	pub async fn add(&self, id: &IdentityPersonal, role: proto::PersonnelRole) -> Result<()> {

		let uid = id.uid();
		let name = id.name();

		let role = PersonnelRoleCodec::encode(&role);
		let now = OffsetDateTime::now_utc().unix_timestamp();

		sqlx::query!("
			INSERT INTO personnel (
				uid, name, role, added
			) VALUES (
				$1, $2, $3, $4
			) ON CONFLICT(uid) DO UPDATE SET
				name=excluded.name,
				role=excluded.role
			",
			uid, name, role, now
		)
			.execute(self.database)
			.await?;

		Ok(())
	}

	pub async fn remove(&self, uid: impl AsRef<[u8]>) -> Result<bool> {

		let uid = uid.as_ref();

		let result = sqlx::query!("
			DELETE FROM personnel
			WHERE uid = $1
			",
			uid
		)
			.execute(self.database)
			.await?;
		Ok(result.rows_affected() == 1)
	}

	pub async fn get_role(&self, uid: impl AsRef<[u8]>) -> Result<Option<proto::PersonnelRole>> {

		let uid = uid.as_ref();

		let Some(result) = sqlx::query!("
			SELECT role
			FROM personnel
			WHERE uid = $1
			",
			uid
		)
			.fetch_optional(self.database)
			.await?
			else { return Ok(None); };

		let role = PersonnelRoleCodec::decode(&result.role);

		Ok(Some(role))
	}

	pub async fn list(&self, role: proto::PersonnelRole) -> Result<Vec<PersonnelData>> {

		let role = PersonnelRoleCodec::encode(&role);

		let personnel = sqlx::query_as!(PersonnelData, "
			SELECT uid, name, role, added, last_login
			FROM personnel
			WHERE role = $1
			",
			role
		)
			.fetch_all(self.database)
			.await?;

		Ok(personnel)
	}

	pub async fn record_login(&self, uid: impl AsRef<[u8]>, timestamp: OffsetDateTime) -> Result<()> {

		let uid = uid.as_ref();
		let timestamp = timestamp.unix_timestamp();

		sqlx::query!("
			UPDATE personnel
			SET last_login = $1
			WHERE uid = $2
			",
			timestamp, uid
		)
			.execute(self.database)
			.await?;

		Ok(())
	}
}
