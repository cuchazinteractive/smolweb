
use std::sync::Arc;

use anyhow::{Context, Result};
use axum::async_trait;
use base64::Engine;
use base64::engine::general_purpose::{URL_SAFE_NO_PAD as BASE64};
use burgerid::pae;
use tokio_rustls_acme::{AccountCache, CertCache};

use crate::beacon::database::Database;


pub struct AcmeCache {
	database: Arc<Database>
}


impl AcmeCache {

	pub fn new(database: Arc<Database>) -> Self {
		Self {
			database
		}
	}

	fn id(directory_url: impl AsRef<str>, parts: &[String]) -> Result<String> {

		let directory_url = directory_url.as_ref();

		// use PAE, just in case canonicalization attacks could be a thing here
		let mut bufs = Vec::<&[u8]>::new();
		bufs.push(directory_url.as_bytes());
		for part in parts {
			bufs.push(part.as_bytes());
		}
		let encoded = pae(bufs)
			.context("PAE")?
			.concat();

		Ok(BASE64.encode(&encoded))
	}
}


#[async_trait]
impl CertCache for AcmeCache {

	type EC = anyhow::Error;

	async fn load_cert(
		&self,
		domains: &[String],
		directory_url: &str,
	) -> Result<Option<Vec<u8>>,Self::EC> {

		let id = Self::id(directory_url, domains)
			.context("Failed to make id for ACME cert")?;

		let Some(record) = sqlx::query!("
			SELECT cert
			FROM acme_certs
			WHERE id = $1
			",
			id
		)
			.fetch_optional(&*self.database)
			.await?
			else { return Ok(None); };

		Ok(Some(record.cert))
	}

	async fn store_cert(
		&self,
		domains: &[String],
		directory_url: &str,
		cert: &[u8],
	) -> Result<(),Self::EC> {

		let id = Self::id(directory_url, domains)
			.context("Failed to make id for ACME cert")?;

		sqlx::query!("
			INSERT OR REPLACE INTO acme_certs (
				id, cert
			) VALUES (
				$1, $2
			)
			",
			id,
			cert
		)
			.execute(&*self.database)
			.await?;

		Ok(())
	}
}

#[async_trait]
impl AccountCache for AcmeCache {

	type EA = anyhow::Error;

	async fn load_account(
		&self,
		contact: &[String],
		directory_url: &str,
	) -> Result<Option<Vec<u8>>,Self::EA> {

		let id = Self::id(directory_url, contact)
			.context("Failed to make id for ACME account")?;

		let Some(record) = sqlx::query!("
			SELECT account
			FROM acme_accounts
			WHERE id = $1
			",
			id
		)
			.fetch_optional(&*self.database)
			.await?
			else { return Ok(None); };

		Ok(Some(record.account))
	}

	async fn store_account(
		&self,
		contact: &[String],
		directory_url: &str,
		account: &[u8],
	) -> Result<(),Self::EA> {

		let id = Self::id(directory_url, contact)
			.context("Failed to make id for ACME account")?;

		sqlx::query!("
			INSERT OR REPLACE INTO acme_accounts (
				id, account
			) VALUES (
				$1, $2
			)
			",
			id,
			account
		)
			.execute(&*self.database)
			.await?;

		Ok(())
	}
}
