
pub trait DatabaseCodec {

	type NT;
	type DT;
	type Error;

	fn encode(value: &Self::NT) -> Result<Self::DT,Self::Error>;
	fn decode(value: &Self::DT) -> Result<Self::NT,Self::Error>;
}
