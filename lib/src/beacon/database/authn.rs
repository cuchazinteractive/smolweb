
use axum::async_trait;
use burgerid::PersistenceProvider;


use crate::beacon::database::Database;


#[async_trait]
impl PersistenceProvider for Database {

	type Error = sqlx::Error;

	async fn get(&self, key: Vec<u8>) -> Result<Option<Vec<u8>>,Self::Error> {
		let value = sqlx::query!("
			SELECT value
			FROM authn
			WHERE key = $1
			",
			key
		)
			.fetch_optional(self)
			.await?
			.map(|r| r.value);
		Ok(value)
	}

	async fn insert(&self, key: Vec<u8>, value: Vec<u8>) -> Result<(),Self::Error> {
		sqlx::query!("
			INSERT OR REPLACE INTO authn (
				key, value
			) VALUES (
				$1, $2
			)
			",
			key,
			value
		)
			.execute(self)
			.await?;
		Ok(())
	}
}
