
use std::convert::Infallible;
use std::ops::Deref;

use anyhow::{Context, Result};
use burgerid::IdentityApp;
use display_error_chain::ErrorChainExt;
use time::OffsetDateTime;
use tracing::warn;

use crate::beacon::database::codecs::DatabaseCodec;
use crate::beacon::database::Database;
use crate::beacon::database::settings::Setting;
use crate::protobuf::smolweb::admin as proto;


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Authz {
	Allow,
	Deny
}

impl Authz {

	pub fn to_u8(&self) -> u8 {
		match self {
			Self::Allow => 1,
			Self::Deny => 2
		}
	}

	pub fn from_u8(i: u8) -> Self {
		match i {
			1 => Self::Allow,
			_ => Self::Deny
		}
	}

	pub fn to_proto(&self) -> proto::HostAuthz {
		match self {
			Self::Allow => proto::HostAuthz::Allow,
			Self::Deny => proto::HostAuthz::Deny
		}
	}

	pub fn from_proto(p: proto::HostAuthz) -> Option<Self> {
		match p {
			proto::HostAuthz::None => None,
			proto::HostAuthz::Allow => Some(Self::Allow),
			proto::HostAuthz::Deny => Some(Self::Deny)
		}
	}
}

impl Default for Authz {

	fn default() -> Self {
		Self::Deny
	}
}


pub struct AuthzCodec;

impl AuthzCodec {

	fn encode(value: &Authz) -> Vec<u8> {
		vec![value.to_u8()]
	}

	fn decode(value: &Vec<u8>) -> Authz {
		if let Some(i) = value.get(0) {
			Authz::from_u8(*i)
		} else {
			Authz::Deny
		}
	}
}

impl DatabaseCodec for AuthzCodec {

	type NT = Authz;
	type DT = Vec<u8>;
	type Error = Infallible;

	fn encode(value: &Self::NT) -> Result<Self::DT,Self::Error> {
		Ok(Self::encode(value))
	}

	fn decode(value: &Self::DT) -> Result<Self::NT,Self::Error> {
		Ok(Self::decode(value))
	}
}


const SETTING_DEFAULT_HOST_AUTHZ: Setting<AuthzCodec> = Setting::new("default_host_authz", Authz::Deny);


pub struct HostData {
	pub uid: Vec<u8>,
	pub name: String,
	pub authz: Option<Vec<u8>>,
	pub first_connect: Option<i64>,
	pub last_connect: Option<i64>
}

impl HostData {

	pub fn authz(&self) -> Option<Authz> {
		self.authz.as_ref()
			.map(AuthzCodec::decode)
	}

	pub fn first_connect(&self) -> Option<Result<OffsetDateTime,time::error::ComponentRange>> {
		self.first_connect.map(OffsetDateTime::from_unix_timestamp)
	}

	pub fn last_connect(&self) -> Option<Result<OffsetDateTime,time::error::ComponentRange>> {
		self.last_connect.map(OffsetDateTime::from_unix_timestamp)
	}
}


pub struct Hosts<'a> {
	database: &'a Database
}

impl<'a> Hosts<'a> {

	pub fn new(database: &'a Database) -> Self {
		Self {
			database
		}
	}

	pub async fn add(&self, id: &IdentityApp, authz: Authz) -> Result<()> {

		let uid = id.uid();
		let name = id.name();

		let authz = AuthzCodec::encode(&authz);

		sqlx::query!("
			INSERT INTO hosts (
				uid, name, authz
			) VALUES (
				$1, $2, $3
			) ON CONFLICT(uid) DO UPDATE SET
				name=excluded.name,
				authz=excluded.authz
			",
			uid, name, authz
		)
			.execute(self.database)
			.await?;

		Ok(())
	}

	pub async fn connected(&self, uid: impl AsRef<[u8]>, name: impl AsRef<str>) -> Result<()> {

		let uid = uid.as_ref();
		let name = name.as_ref();

		let now = OffsetDateTime::now_utc().unix_timestamp();

		// insert or update the host uid and name
		sqlx::query!("
			INSERT INTO hosts (
				uid, name
			) VALUES (
				$1, $2
			) ON CONFLICT(uid) DO UPDATE SET
				name=excluded.name
			",
			uid, name
		)
			.execute(self.database)
			.await?;

		// record connection times
		sqlx::query!("
			UPDATE hosts
			SET
				first_connect = coalesce(first_connect, $1),
				last_connect = $1
			WHERE uid = $2
			",
			now, uid
		)
			.execute(self.database)
			.await?;

		Ok(())
	}

	pub async fn list(&self) -> Result<Vec<HostData>> {

		let hosts = sqlx::query_as!(HostData, "
			SELECT uid, name, authz, first_connect, last_connect
			FROM hosts
			"
		)
			.fetch_all(self.database)
			.await?;

		Ok(hosts)
	}

	pub async fn get_default_authz(&self) -> Result<Authz> {
		SETTING_DEFAULT_HOST_AUTHZ.get(self.database)
			.await
	}

	pub async fn set_default_authz(&self, authz: Authz) -> Result<()> {
		SETTING_DEFAULT_HOST_AUTHZ.set(self.database, &authz)
			.await
	}

	pub async fn set_authz(&self, host_uid: impl AsRef<[u8]>, authz: Option<Authz>) -> Result<bool> {

		let host_uid = host_uid.as_ref();

		let authz = match authz {
			Some(a) => Some(AuthzCodec::encode(&a)),
			None => None
		};

		let result = sqlx::query!("
			UPDATE hosts
			SET authz = $1
			WHERE uid = $2
			",
			authz,
			host_uid
		)
			.execute(self.database)
			.await?;

		Ok(result.rows_affected() == 1)
	}

	pub async fn get(&self, host_uid: impl AsRef<[u8]>) -> Result<Option<Authz>> {

		let host_uid = host_uid.as_ref();

		let Some(record) = sqlx::query!("
			SELECT authz
			FROM hosts
			WHERE uid = $1
			",
			host_uid
		)
			.fetch_optional(self.database)
			.await
			.context("Failed to get authz")?
			else { return Ok(None); };

		let Some(authz) = record.authz
			else { return Ok(None); };

		Ok(Some(AuthzCodec::decode(&authz)))
	}

	pub async fn authorize(&self, host_uid: impl AsRef<[u8]>) -> Authz {
		match self.get(host_uid).await {
			Ok(Some(authz)) => authz,
			Ok(None) =>
				SETTING_DEFAULT_HOST_AUTHZ.get(self.database)
					.await
					.unwrap_or(Authz::Deny),
			Err(e) => {
				warn!("Failed to get default host authz: {}", e.deref().chain());
				Authz::Deny
			}
		}
	}
}



#[cfg(test)]
pub(crate) mod test {

	use galvanic_assert::{assert_that, matchers::*};

	use smolweb_test_tools::id::app_id;
	use smolweb_test_tools::logging::init_test_logging;

	use crate::beacon::database::ArgsDatabase;
	use crate::beacon::database::hosts::Authz;

	use super::*;


	#[tokio::test]
	async fn get_empty() {
		let _logging = init_test_logging();

		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let hosts = database.hosts();

		let authz = hosts.get(vec![1, 2, 3])
			.await.unwrap();
		assert_that!(&authz, eq(None));
	}


	#[tokio::test]
	async fn authorize_defaults() {
		let _logging = init_test_logging();

		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let hosts = database.hosts();

		let authz = hosts.authorize(vec![1, 2, 3])
			.await;
		assert_that!(&authz, eq(Authz::Deny));
	}


	#[tokio::test]
	async fn authorize_default_deny() {
		let _logging = init_test_logging();

		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let hosts = database.hosts();

		hosts.set_default_authz(Authz::Deny)
			.await.unwrap();

		let authz = hosts.authorize(vec![1, 2, 3])
			.await;
		assert_that!(&authz, eq(Authz::Deny));
	}


	#[tokio::test]
	async fn authorize_default_allow() {
		let _logging = init_test_logging();

		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let hosts = database.hosts();

		hosts.set_default_authz(Authz::Allow)
			.await.unwrap();

		let authz = hosts.authorize(vec![1, 2, 3])
			.await;
		assert_that!(&authz, eq(Authz::Allow));
	}


	#[tokio::test]
	async fn authorize_deny() {
		let _logging = init_test_logging();

		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let hosts = database.hosts();

		let (app_id, _app_key, _, _) = app_id("App");
		hosts.add(&app_id, Authz::Deny)
			.await.unwrap();

		let authz = hosts.authorize(app_id.uid().to_vec())
			.await;
		assert_that!(&authz, eq(Authz::Deny));
	}


	#[tokio::test]
	async fn authorize_allow() {
		let _logging = init_test_logging();

		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let hosts = database.hosts();

		let (app_id, _app_key, _, _) = app_id("App");
		hosts.add(&app_id, Authz::Allow)
			.await.unwrap();

		let authz = hosts.authorize(app_id.uid().to_vec())
			.await;
		assert_that!(&authz, eq(Authz::Allow));
	}
}
