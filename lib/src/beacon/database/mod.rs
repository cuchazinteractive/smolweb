
pub(crate) mod acme_cache;
pub(crate) mod settings;
pub(crate) mod codecs;
pub(crate) mod hosts;
pub(crate) mod authn;
pub(crate) mod personnel;


use std::path::PathBuf;
use std::str::FromStr;

use anyhow::{Context, Result};
use futures::future::BoxFuture;
use futures::stream::BoxStream;
use serde::Deserialize;
use sqlx::{Describe, Either, Error, Execute, Executor, Pool, Sqlite, Transaction};
use sqlx::database::HasStatement;
use sqlx::migrate::Migrator;
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
use tracing::info;

use crate::beacon::database::hosts::Hosts;
use crate::beacon::database::personnel::Personnel;
use crate::config::ToArgs;


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigDatabase {
	pub path: Option<String>,
	pub max_connections: Option<u32>
}


impl Default for ConfigDatabase {

	fn default() -> Self {
		Self {
			path: None,
			max_connections: None
		}
	}
}


impl ToArgs for ConfigDatabase {

	type Args = ArgsDatabase;

	fn to_args(self) -> Result<ArgsDatabase> {

		let default = ArgsDatabase::default();

		Ok(ArgsDatabase {
			path: self.path
				.map(PathBuf::from)
				.unwrap_or(default.path),
			max_connections: self.max_connections
				.unwrap_or(default.max_connections),
			#[cfg(test)]
			in_memory: default.in_memory
		})
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsDatabase {
	pub path: PathBuf,
	pub max_connections: u32,
	#[cfg(test)]
	pub in_memory: bool
}


impl Default for ArgsDatabase {

	fn default() -> Self {

		let args = Self {
			path: PathBuf::from("data.db"),
			max_connections: 2,
			#[cfg(test)]
			in_memory: true
		};

		#[cfg(test)]
		let args = Self {
			// TODO: need any overrides?
			.. args
		};

		args
	}
}


#[derive(Debug)]
pub struct Database {
	args: ArgsDatabase,
	pool: Pool<Sqlite>
}

impl Database {

	pub async fn new(args: ArgsDatabase) -> Result<Self> {

		let url = format!("sqlite://{}", args.path.to_string_lossy());

		#[cfg(test)]
		let url = match args.in_memory {
			true => "sqlite::memory:".to_string(),
			false => url
		};

		info!("Connecting to database: {}", url);

		let options = SqliteConnectOptions::from_str(&url)
			.context("Failed to parse Sqlite connection options")?
			.create_if_missing(true);

		let pool = SqlitePoolOptions::new()
			.max_connections(args.max_connections)
			.connect_with(options)
			.await
			.context("Failed to connect to database")?;

		// run all migrations
		Migrator::new(smolweb_migrations::get())
			.await
			.context("Failed to create migrator")?
			.run(&pool)
			.await
			.context("Migrations failed")?;

		Ok(Self {
			args,
			pool
		})
	}

	pub fn args(&self) -> &ArgsDatabase {
		&self.args
	}

	pub async fn transaction(&self) -> Result<Transaction<Sqlite>> {
		self.pool.begin()
			.await
			.context("Failed to start a new transaction")
	}

	pub fn hosts(&self) -> Hosts {
		Hosts::new(self)
	}

	pub fn personnel(&self) -> Personnel {
		Personnel::new(self)
	}
}


// implement sqlx::Executor for Database, but just forward all the methods to the Pool member
impl<'p> Executor<'p> for &'_ Database
	where
		for<'c> &'c mut <Sqlite as sqlx::Database>::Connection: Executor<'c, Database=Sqlite>,
{

	type Database = Sqlite;

	fn fetch_many<'e, 'q: 'e, E: 'q>(self, query: E) -> BoxStream<'e, std::result::Result<Either<<Sqlite as sqlx::Database>::QueryResult, <Sqlite as sqlx::Database>::Row>, Error>>
		where
			E: Execute<'q, Self::Database>
	{
		self.pool.fetch_many(query)
	}

	fn fetch_optional<'e, 'q: 'e, E: 'q>(self, query: E) -> BoxFuture<'e, std::result::Result<Option<<Sqlite as sqlx::Database>::Row>, Error>>
		where
			E: Execute<'q, Self::Database>
	{
		self.pool.fetch_optional(query)
	}

	fn prepare_with<'e, 'q: 'e>(self, sql: &'q str, parameters: &'e [<Sqlite as sqlx::Database>::TypeInfo]) -> BoxFuture<'e, std::result::Result<<Sqlite as HasStatement<'q>>::Statement, Error>> {
		self.pool.prepare_with(sql, parameters)
	}

	fn describe<'e, 'q: 'e>(self, sql: &'q str) -> BoxFuture<'e, std::result::Result<Describe<Self::Database>, Error>> {
		self.pool.describe(sql)
	}
}
