
use anyhow::{Context, Result};
use sqlx::{Executor, Sqlite};

use crate::beacon::database::codecs::DatabaseCodec;


pub struct Setting<C>
	where
		C: DatabaseCodec
{
	name: &'static str,
	default: C::NT
}

impl<C> Setting<C>
	where
		C: DatabaseCodec<DT=Vec<u8>>,
		C::NT: Clone,
		C::Error: std::error::Error + Send + Sync + 'static
{

	pub const fn new(name: &'static str, default: C::NT) -> Self {
		Self {
			name,
			default
		}
	}

	pub async fn get<'e,'c,E>(&self, executor: E) -> Result<C::NT>
		where
			E: 'e + Executor<'c,Database=Sqlite>,
			'c: 'e
	{

		let name = self.name;

		let Some(record) = sqlx::query!("
			SELECT value
			FROM settings
			WHERE id = $1
			",
			name
		)
			.fetch_optional(executor)
			.await
			.context("Failed to get setting")?
			else { return Ok(self.default.clone()) };

		let value = C::decode(&record.value)
			.context("Failed to decode setting value")?;
		Ok(value)
	}

	pub async fn set<'e,'c,E>(&self, executor: E, value: &C::NT) -> Result<()>
		where
			E: 'e + Executor<'c,Database=Sqlite>,
			'c: 'e
	{

		let name = self.name;
		let value = C::encode(value)
			.context("Failed to encode setting value")?;

		sqlx::query!("
			INSERT OR REPLACE INTO settings (
				id, value
			) VALUES (
				$1, $2
			)
			",
			name,
			value
		)
			.execute(executor)
			.await
			.context("Failed to set setting")?;

		Ok(())
	}
}
