use std::fs;
use std::path::PathBuf;
use std::time::Duration;
use burgerid::{IdentityPersonal, ProtobufSave};

use galvanic_assert::{assert_that, matchers::*};

use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::beacon::{ArgsBeacon, ArgsBeaconDefaults, ConfigBeacon};
use crate::beacon::database::ArgsDatabase;
use crate::beacon::guests::ArgsGuests;
use crate::beacon::hosts::ArgsHosts;
use crate::beacon::https::ArgsBeaconHttpsServer;
use crate::beacon::stun::ArgsStun;
use crate::beacon::tls::{ArgsTls, ArgsTlsAcme, ConfigTls, ConfigTlsAcme};
use crate::config::{ConfigDuration, ToArgs};
use crate::config::test::deserialize_from_toml;


#[test]
fn config() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigBeacon>(r#"
	"#).unwrap();
	assert_that!(&config, eq(ConfigBeacon::default()));
}


#[test]
fn to_args() {
	let _logging = init_test_logging();

	let config_tls = ConfigTls {
		acme: Some(ConfigTlsAcme {
			domains: Some(vec!["foo".to_string()]),
			emails: Some(vec!["bar".to_string()]),
			directory: Some("dir".to_string()),
			timeout_cert: Some(ConfigDuration::s(1))
		}),
		.. ConfigTls::default()
	};

	// write an owner identity file
	let (owner_id, _owner_key) = IdentityPersonal::new("Owner", "passphrase")
		.unwrap();
	let owner_id_path = PathBuf::from("/tmp/smolweb-beacon-owner.burger.id");
	fs::write(&owner_id_path, owner_id.save_string())
		.unwrap();

	let config = ConfigBeacon {
		owner_id: Some(owner_id_path.to_string_lossy().to_string()),
		tls: Some(config_tls.clone()),
		.. ConfigBeacon::default()
	};
	let args_defaults = ArgsBeaconDefaults::default();
	let args = ArgsBeacon {
		log: args_defaults.log,
		owner_id,
		database: ArgsDatabase::default(),
		stun: ArgsStun::default(),
		https: ArgsBeaconHttpsServer::default(),
		guests: ArgsGuests::default(),
		hosts: ArgsHosts::default(),
		tls: ArgsTls::Acme(ArgsTlsAcme {
			domains: vec!["foo".to_string()],
			emails: vec!["bar".to_string()],
			directory: "dir".to_string(),
			timeout_cert: Duration::from_secs(1)
		})
	};
	assert_that!(&config.to_args().unwrap(), eq(args));

	fs::remove_file(owner_id_path)
		.unwrap();

	// data_dir is required
	let config = ConfigBeacon {
		tls: Some(config_tls.clone()),
		.. ConfigBeacon::default()
	};
	assert_that!(&config.to_args(), is_match!(Err(..)));
}
