use std::sync::Arc;
use std::time::Duration;

use burgerid::{IdentityApp, IdentityKeyApp};
use galvanic_assert::{assert_that, matchers::*};
use tokio::sync::RwLock;
use tokio::time;
use tokio::time::{sleep, timeout};

use smolweb_test_tools::id::app_id;
use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::beacon::Beacon;
use crate::beacon::database::{ArgsDatabase, Database};
use crate::beacon::hosts::*;
use crate::beacon::tls::{ArgsTls, ArgsTlsAcme, TlsState};
use crate::config::test::deserialize_from_toml;
use crate::host::client::{ArgsHostsClient, HostsClient, HostsClientConnectError};
use crate::net::{ConfigAddr, ConnectMode, FilterAddrs};
use crate::protobuf::hosts::host::BeaconMessage;
use crate::tls::test::acmed::Acmed;
use crate::tls::test::generate_domain_cert;


#[test]
fn config() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigHosts>(r#"
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts::default()));

	let config = deserialize_from_toml::<ConfigHosts>(r#"
		bind_mode = "both"
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts {
		bind_mode: Some(BindMode::Both),
		.. ConfigHosts::default()
	}));

	let config = deserialize_from_toml::<ConfigHosts>(r#"
		addrs = "localhost"
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts {
		addrs: Some(vec![ConfigAddr::new("localhost", None)]),
		.. ConfigHosts::default()
	}));

	let config = deserialize_from_toml::<ConfigHosts>(r#"
		addrs = []
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts {
		addrs: Some(vec![]),
		.. ConfigHosts::default()
	}));

	let config = deserialize_from_toml::<ConfigHosts>(r#"
		addrs = ["localhost"]
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts {
		addrs: Some(vec![ConfigAddr::new("localhost", None)]),
		.. ConfigHosts::default()
	}));

	let config = deserialize_from_toml::<ConfigHosts>(r#"
		addrs = ["localhost:123"]
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts {
		addrs: Some(vec![ConfigAddr::new("localhost", Some(123))]),
		.. ConfigHosts::default()
	}));

	let config = deserialize_from_toml::<ConfigHosts>(r#"
		addrs = ["localhost:123", "foo.bar"]
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts {
		addrs: Some(vec![
			ConfigAddr::new("localhost", Some(123)),
			ConfigAddr::new("foo.bar", None)
		]),
		.. ConfigHosts::default()
	}));

	let config = deserialize_from_toml::<ConfigHosts>(r#"
		addrs = [{ host = "beer.localhost", port = 42 }, "foo.bar"]
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts {
		addrs: Some(vec![
			ConfigAddr::new("beer.localhost", Some(42)),
			ConfigAddr::new("foo.bar", None)
		]),
		.. ConfigHosts::default()
	}));

	let config = deserialize_from_toml::<ConfigHosts>(r#"
		timeout_ready = "5s"
	"#).unwrap();
	assert_that!(&config, eq(ConfigHosts {
		timeout_ready: Some(ConfigDuration::s(5)),
		.. ConfigHosts::default()
	}));
}


#[test]
fn to_args() {
	let _logging = init_test_logging();

	assert_that!(&ConfigHosts::default().to_args().unwrap(), eq(ArgsHosts::default()));
}


#[tokio::test]
async fn connect_disconnect_acme() {
	let _logging = init_test_logging();

	let acmed = Acmed::start()
		.await;

	// start the hosts server
	let args = ArgsHosts::default();
	let args_tls = TestServer::args_tls_acme(&acmed);
	let mut server = TestServer::new(&args, args_tls)
		.await;

	// make an app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// connect a client
	let client = server.connect(&app_id, &app_key)
		.await.unwrap();
	assert_that!(&server.server.num_connections().await, eq(1));

	// the connection should be active in the beacon
	let beacon = server.beacon.read()
		.await;
	assert_that!(&beacon.hosts.connection_ids.len(), eq(1));
	drop(beacon);

	client.disconnect();

	// the connection should end very soon
	timeout(Duration::from_millis(100), server.server.connections_finished())
		.await.unwrap();
	assert_that!(&server.server.num_connections().await, eq(0));

	server.shutdown()
		.await;
	acmed.shutdown()
		.await;
}


#[tokio::test]
async fn connect_disconnect_cert() {
	let _logging = init_test_logging();

	// start the hosts server
	let args = ArgsHosts::default();
	let args_tls = TestServer::args_tls_cert();
	let mut server = TestServer::new(&args, args_tls)
		.await;

	// make an app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// connect a client
	let client = server.connect(&app_id, &app_key)
		.await.unwrap();

	// the connection should be active in the beacon
	let beacon = server.beacon.read()
		.await;
	assert_that!(&beacon.hosts.connection_ids.len(), eq(1));
	drop(beacon);

	client.disconnect();

	// the connection should end very soon
	timeout(Duration::from_millis(100), server.server.connections_finished())
		.await.unwrap();

	server.shutdown()
		.await;
}


#[tokio::test]
async fn connect_rate_limit() {
	let _logging = init_test_logging();

	// start the hosts server
	let mut args = ArgsHosts::default();
	args.authn.delay = Duration::from_millis(200);
	args.authn.count = 1;
	let args_tls = TestServer::args_tls_cert();
	let server = TestServer::new(&args, args_tls)
		.await;

	// make an app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// connect a client
	let client = server.connect(&app_id, &app_key)
		.await.unwrap();
	client.disconnect();

	// do it again immediately
	let e = server.connect(&app_id, &app_key)
		.await
		.err()
		.unwrap();
	assert_that!(&e, is_match!(HostsClientConnectError::Authn(AuthnError::TooManyAttempts)));

	// wait a bit and try again
	time::sleep(Duration::from_millis(200))
		.await;
	let client = server.connect(&app_id, &app_key)
		.await.unwrap();
	client.disconnect();

	server.shutdown()
		.await;
}


#[tokio::test]
async fn connect_not_authorized() {
	let _logging = init_test_logging();

	// start the hosts server
	let args = ArgsHosts::default();
	let args_tls = TestServer::args_tls_cert();
	let server = TestServer::new(&args, args_tls)
		.await;

	// make an app id, but don't authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");

	let e = server.connect(&app_id, &app_key)
		.await
		.err()
		.unwrap();
	assert_that!(&e, is_match!(HostsClientConnectError::NotAuthorized));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn reload_authzs() {
	let _logging = init_test_logging();

	// start the hosts server
	let args = ArgsHosts::default();
	let args_tls = TestServer::args_tls_cert();
	let server = TestServer::new(&args, args_tls)
		.await;

	// make an app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// connect a client
	server.connect(&app_id, &app_key)
		.await.unwrap();

	// de-authorize the app
	server.hosts().set_authz(app_id.uid(), Some(Authz::Deny))
		.await.unwrap();

	// client connection should fail now
	let e = server.connect(&app_id, &app_key)
		.await
		.err()
		.unwrap();
	assert_that!(&e, is_match!(HostsClientConnectError::NotAuthorized));

	server.shutdown()
		.await;
}


#[tokio::test]
async fn connect_already_ready() {
	let _logging = init_test_logging();

	// start the hosts server
	let args = ArgsHosts::default();
	let args_tls = TestServer::args_tls_cert();
	let server = TestServer::new(&args, args_tls)
		.await;

	// make an app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// connect a client
	let client = server.connect(&app_id, &app_key)
		.await.unwrap();

	// try to conect another host
	let e = server.connect(&app_id, &app_key)
		.await
		.err()
		.unwrap();
	assert_that!(&e, is_match!(HostsClientConnectError::AlreadyReady));

	client.disconnect();
	server.shutdown()
		.await;
}


#[tokio::test]
async fn ping() {
	let _logging = init_test_logging();

	// start the hosts server
	let mut args = ArgsHosts::default();
	args.interval_ping_poll = Duration::from_millis(200);
	args.interval_ping_check = Duration::from_millis(100);
	let args_tls = TestServer::args_tls_cert();
	let server = TestServer::new(&args, args_tls)
		.await;

	// make an app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// connect a client
	let mut client = server.connect(&app_id, &app_key)
		.await.unwrap();

	// wait for the first ping
	let msg = timeout(Duration::from_millis(400), client.recv_message())
		.await
		.unwrap()
		.unwrap();
	let request_id = match msg {
		BeaconMessage::Ping { request_id } => request_id,
		_ => panic!("unexpected message: {:?}", msg)
	};

	// send back the pong
	client.send_message(hosts::host::encode_pong(request_id))
		.await
		.unwrap();

	// wait a bit for the hosts server to receive it
	sleep(Duration::from_millis(100))
		.await;

	// we should have one connected host
	{
		let beacon = server.beacon.read()
			.await;
		assert_that!(&beacon.hosts.hosts.len(), eq(1));
	}

	client.disconnect();
	server.shutdown()
		.await;
}


#[tokio::test]
async fn ping_timeout() {
	let _logging = init_test_logging();

	// start the hosts server
	let mut args = ArgsHosts::default();
	args.interval_ping_poll = Duration::from_millis(200);
	args.interval_ping_check = Duration::from_millis(100);
	let args_tls = TestServer::args_tls_cert();
	let server = TestServer::new(&args, args_tls)
		.await;

	// make an app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// connect a client
	let mut client = server.connect(&app_id, &app_key)
		.await.unwrap();

	// wait for the first ping
	let msg = timeout(Duration::from_millis(400), client.recv_message())
		.await
		.unwrap()
		.unwrap();
	let _request_id = match msg {
		BeaconMessage::Ping { request_id } => request_id,
		_ => panic!("unexpected message: {:?}", msg)
	};

	// don't respond with a pong

	// wait a bit for the hosts server to timeout the connection
	sleep(Duration::from_millis(200))
		.await;

	// we shouldn't have any connected hosts now
	{
		let beacon = server.beacon.read()
			.await;
		assert_that!(&beacon.hosts.hosts.len(), eq(0));
	}

	client.disconnect();
	server.shutdown()
		.await;
}



#[tokio::test]
async fn ping_takeover() {
	let _logging = init_test_logging();

	// start the hosts server
	let mut args = ArgsHosts::default();
	args.interval_ping_recheck = Duration::from_millis(200);
	let args_tls = TestServer::args_tls_cert();
	let server = TestServer::new(&args, args_tls)
		.await;

	// make an app id and authorize it
	let (app_id, app_key, _owner_id, _owner_key) = app_id("Test");
	server.hosts().add(&app_id, Authz::Allow)
		.await.unwrap();

	// connect a client
	let client1 = server.connect(&app_id, &app_key)
		.await.unwrap();

	// wait a bit
	sleep(Duration::from_millis(100))
		.await;

	// try to connect another client
	let e = server.connect(&app_id, &app_key)
		.await
		.err()
		.unwrap();
	assert_that!(&e, is_match!(HostsClientConnectError::AlreadyReady));

	// wait until after the re-check period
	sleep(Duration::from_millis(100))
		.await;

	// try connecting again
	let e = server.connect(&app_id, &app_key)
		.await
		.err()
		.unwrap();
	assert_that!(&e, is_match!(HostsClientConnectError::AlreadyReady));

	// wait for the ping to client1 to timeout
	sleep(Duration::from_millis(200))
		.await;

	// once more with feeling!
	let client2 = server.connect(&app_id, &app_key)
		.await.unwrap();

	client2.disconnect();
	client1.disconnect();
	server.shutdown()
		.await;
}

struct TestServer {
	tls_state: TlsState,
	beacon: Arc<RwLock<Beacon>>,
	server: HostsServer
}

impl TestServer {

	fn args_tls_acme(acmed: &Acmed) -> ArgsTls {
		ArgsTls::Acme(ArgsTlsAcme {
			domains: vec!["test.hosts.localhost".to_string()],
			emails: vec!["me@domain.tld".to_string()],
			directory: acmed.directory(),
			timeout_cert: ArgsTlsAcme::DEFAULT_TIMEOUT_CERT
		})
	}

	fn args_tls_cert() -> ArgsTls {
		ArgsTls::Cert(
			generate_domain_cert("test.hosts.localhost")
				.to_tls_args()
		)
	}

	async fn new(args: &ArgsHosts, args_tls: ArgsTls) -> Self {

		let owner_uid = vec![1, 2, 3];
		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let database = Arc::new(database);
		let beacon = Arc::new(RwLock::new(Beacon::new(owner_uid, args, database.clone())));
		let tls_state = args_tls.state(database.clone());
		let server = HostsServer::start(args.clone(), beacon.clone(), database, &tls_state)
			.await.unwrap();

		Self {
			tls_state,
			beacon,
			server
		}
	}

	fn hosts(&self) -> Hosts {
		self.server.hosts()
	}

	async fn shutdown(mut self) {
		self.server.connections_finished()
			.await;
		self.server.shutdown()
			.await;
	}

	fn addr(&self, connect_mode: ConnectMode) -> ConfigAddr {
		let port = self.server.addrs()
			.filter_addrs_for_connect(connect_mode)
			.first()
			.unwrap()
			.port();
		ConfigAddr::new(self.tls_state.any_domain(), Some(port))
	}

	async fn connect(&self, app_id: &IdentityApp, app_key: &IdentityKeyApp) -> Result<HostsClient,HostsClientConnectError> {
		let mut args = ArgsHostsClient::default(app_id, app_key);
		args.addr = self.addr(args.connect_mode);
		HostsClient::connect(args)
			.await
	}
}
