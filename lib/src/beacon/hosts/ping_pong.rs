
use std::sync::Arc;
use std::time::{Duration, Instant};

use anyhow::{anyhow, Context};
use base64::Engine;
use base64::engine::general_purpose::{URL_SAFE_NO_PAD as BASE64};
use tokio::select;
use tokio::sync::{oneshot, RwLock};
use tokio::task::JoinHandle;
use tokio::time::{sleep, timeout};
use tracing::{trace, warn};

use crate::beacon::{Beacon, Host};
use crate::beacon::bridge::BridgeGuest;
use crate::lang::ErrorReporting;


pub(crate) struct PingPonger {
	shutdown_tx: Option<oneshot::Sender<()>>,
	task_handle: JoinHandle<()>
}

impl PingPonger {

	#[tracing::instrument(skip_all, level = 5, name = "Hosts:ping_pong")]
	pub fn start(
		beacon: Arc<RwLock<Beacon>>,
		poll_interval: Duration,
		check_interval: Duration,
		pong_timeout: Duration
	) -> Self {

		// start the ping/pong task
		let (shutdown_tx, shutdown_rx) = oneshot::channel();
		let task_handle = tokio::spawn({
			let beacon = beacon.clone();
			async move {
				task(beacon, poll_interval, check_interval, pong_timeout, shutdown_rx)
					.await;
			}
		});

		Self {
			shutdown_tx: Some(shutdown_tx),
			task_handle
		}
	}

	#[tracing::instrument(skip_all, level = 5, name = "Hosts:ping_pong")]
	pub async fn shutdown(mut self) {

		// send the shutdown signal
		if let Some(shutdown_tx) = self.shutdown_tx.take() {
			shutdown_tx.send(())
				.map_err(|_| anyhow!("Failed to send shutdown signal to ping pong task"))
				.warn_err()
				.ok();
		} else {
			warn!("ping pong task already shut down")
		}

		// wait a bit for the task to end
		timeout(Duration::from_secs(5), self.task_handle)
			.await
			.context("Timed out waiting for ping pong task to end after shutdown")
			.warn_err()
			.ok();
	}
}


pub struct PingPongChecker {
	beacon: Arc<RwLock<Beacon>>,
	interval_recheck: Duration,
	timeout: Duration
}

impl PingPongChecker {

	pub fn new(beacon: Arc<RwLock<Beacon>>, interval_recheck: Duration, timeout: Duration) -> Self {
		Self {
			beacon,
			interval_recheck,
			timeout
		}
	}

	pub fn check(&self, host: &Host) -> bool {

		// don't re-check too often, to avoid DoS attacks on hosts
		if Instant::now().duration_since(host.last_contact) < self.interval_recheck {
			return false;
		}

		let beacon = self.beacon.clone();
		let host_uid = host.app_id.uid().clone();
		let bridge = host.bridge.clone();
		let timeout = self.timeout;
		tokio::spawn(async move {
			ping(beacon, host_uid, bridge, timeout)
				.await;
		});
		return true;
	}
}


#[tracing::instrument(skip_all, level = 5, name = "Hosts:ping_pong")]
async fn task(
	beacon: Arc<RwLock<Beacon>>,
	poll_interval: Duration,
	check_interval: Duration,
	timeout: Duration,
	mut shutdown_rx: oneshot::Receiver<()>
) {

	loop {

		select! {

			// wait a bit before checking the hosts
			_ = sleep(poll_interval) => (),

			// but check for the shutdown signal too
			result = &mut shutdown_rx => match result {

				Err(_) => {
					warn!("Shutdown channel closed, aborting ping pong task");
					break;
				}

				// shutdown signal received
				Ok(()) => break
			}
		}

		// then check the last contact times of all the connected hosts
		let now = Instant::now();
		{
			let beacon_read = beacon.read()
				.await;
			for host in beacon_read.hosts.hosts.values() {

				if now.duration_since(host.last_contact) > check_interval {

					// we haven't contacted this host in a while, so send a ping now
					let beacon = beacon.clone();
					let host_uid = host.app_id.uid().clone();
					let bridge = host.bridge.clone();
					tokio::spawn(async move {
						ping(beacon, host_uid, bridge, timeout)
							.await;
					});
				}
			}
		}
	}
}


#[tracing::instrument(skip_all, level = 5, name = "Hosts:ping_pong", fields(host_uid))]
async fn ping(beacon: Arc<RwLock<Beacon>>, host_uid: Vec<u8>, bridge: BridgeGuest, pong_timeout: Duration) {

	tracing::Span::current().record("host_uid", BASE64.encode(&host_uid));
	trace!("Ping!");

	let result = timeout(pong_timeout, bridge.ping_pong())
		.await;

	// try to find the host
	let mut beacon = beacon.write()
		.await;
	if let Some(host) = beacon.hosts.get_mut(&host_uid) {
		match result {

			// pong received: update last contact
			Ok(_) => {
				trace!("Pong!");
				host.update_last_contact();
			}

			// no pong received before the timeout: disconnect the host from the beacon side
			Err(_) => {
				trace!("Pong timeout: disconnecting host");
				host.disconnect();
			}
		}
	}
}
