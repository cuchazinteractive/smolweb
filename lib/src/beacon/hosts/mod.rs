
mod ping_pong;

#[cfg(test)]
pub(crate) mod test;


use std::collections::HashMap;
use std::net::SocketAddr;
use std::sync::Arc;
use std::time::{Duration, Instant};

use anyhow::{Context, Result};
use axum::async_trait;
use base64::Engine;
use base64::engine::general_purpose::{URL_SAFE_NO_PAD as BASE64};
use burgerid::{AuthnServerError, Identity};
use bytes::BytesMut;
use display_error_chain::ErrorChainExt;
use serde::Deserialize;
use tokio::io::{AsyncRead, AsyncWrite};
use tokio::net::TcpStream;
use tokio::sync::{RwLock, oneshot};
use tokio::time::timeout;
use tokio_rustls::server::TlsStream;
use tracing::{debug, error, info, warn};

use crate::beacon::{AddHostError, Beacon, ConnectionId, Host};
use crate::beacon::authn::ArgsAuthn;
use crate::beacon::bridge::{BridgeRequest, BridgeRequestChannel, BridgeResponse, new_bridge};
use crate::beacon::database::Database;
use crate::beacon::database::hosts::{Authz, Hosts};
use crate::beacon::hosts::ping_pong::{PingPongChecker, PingPonger};
use crate::beacon::tls::server::{TlsServer, TlsServerHandler, TlsServerSpawnerNop};
use crate::beacon::tls::TlsState;
use crate::config::ConfigDuration;
use crate::lang::ErrorReporting;
use crate::net::{BindMode, ConfigAddr};
use crate::net::codec::{FramedProtos, RecvError};
use crate::net::listeners::TcpListeners;
use crate::protobuf::hosts;
use crate::protobuf::hosts::{AuthnError, DEFAULT_PORT};


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigHosts {
	pub bind_mode: Option<BindMode>,
	#[serde(deserialize_with = "ConfigAddr::deserialize_vec")]
	#[serde(default = "ConfigAddr::deserialize_vec_default")]
	pub addrs: Option<Vec<ConfigAddr>>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_ready: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub interval_ping_poll: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub interval_ping_check: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub interval_ping_recheck: Option<ConfigDuration>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_pong: Option<ConfigDuration>,
}

impl ConfigHosts {

	pub fn to_args(self) -> Result<ArgsHosts> {
		let default = ArgsHosts::default();
		Ok(ArgsHosts {
			bind_mode: self.bind_mode.unwrap_or(default.bind_mode),
			addrs: self.addrs.unwrap_or(default.addrs),
			timeout_ready: self.timeout_ready.as_ref()
				.map(|t| t.to_duration())
				.unwrap_or(default.timeout_ready),
			interval_ping_poll: self.interval_ping_poll.as_ref()
				.map(|t| t.to_duration())
				.unwrap_or(default.interval_ping_poll),
			interval_ping_check: self.interval_ping_check.as_ref()
				.map(|t| t.to_duration())
				.unwrap_or(default.interval_ping_check),
			interval_ping_recheck: self.interval_ping_recheck.as_ref()
				.map(|t| t.to_duration())
				.unwrap_or(default.interval_ping_recheck),
			timeout_pong: self.timeout_pong.as_ref()
				.map(|t| t.to_duration())
				.unwrap_or(default.timeout_pong),
			authn: ArgsAuthn::default()
		})
	}
}

impl Default for ConfigHosts {

	fn default() -> Self {
		Self {
			bind_mode: None,
			addrs: None,
			timeout_ready: None,
			interval_ping_poll: None,
			interval_ping_check: None,
			interval_ping_recheck: None,
			timeout_pong: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsHosts {
	pub bind_mode: BindMode,
	pub addrs: Vec<ConfigAddr>,
	pub timeout_ready: Duration,
	pub interval_ping_poll: Duration,
	pub interval_ping_check: Duration,
	pub interval_ping_recheck: Duration,
	pub timeout_pong: Duration,
	pub authn: ArgsAuthn
}

impl Default for ArgsHosts {

	fn default() -> Self {

		let args = Self {
			bind_mode: BindMode::Both,
			addrs: ConfigAddr::all_interfaces(),
			timeout_ready: Duration::from_secs(5),
			interval_ping_poll: Duration::from_secs(30),
			// ping once per hour per host: at ~64k hosts, it's ~18 ping/s
			interval_ping_check: Duration::from_secs(60*60),
			interval_ping_recheck: Duration::from_secs(60),
			timeout_pong: Duration::from_secs(15),
			authn: ArgsAuthn::default()
		};

		#[cfg(test)]
		let args = Self {
			addrs: vec![ConfigAddr::localhost_auto_port()],
			timeout_ready: Duration::from_millis(100),
			timeout_pong: Duration::from_millis(100),
			.. args
		};

		args
	}
}


pub struct HostsServer {
	tls_server: TlsServer,
	addrs: Vec<SocketAddr>,
	database: Arc<Database>,
	beacon: Arc<RwLock<Beacon>>,
	ping_ponger: PingPonger,
}


impl HostsServer {

	#[tracing::instrument(skip_all, level = 5, name = "Hosts")]
	pub async fn start(args: ArgsHosts, beacon: Arc<RwLock<Beacon>>, database: Arc<Database>, tls_state: &TlsState) -> Result<Self> {

		// listen on the sockets with TCP
		let tcp_listeners = TcpListeners::bind_all(&args.addrs, DEFAULT_PORT, args.bind_mode)
			.await.context("Failed to bind TCP listener")?;
		let addrs = tcp_listeners.addrs().clone();
		info!(?addrs, "Listening");

		// start the TLS server
		let spawner = TlsServerSpawnerNop::new();
		let handler = Handler::new(
			args.clone(),
			beacon.clone(),
			database.clone(),
			PingPongChecker::new(beacon.clone(), args.interval_ping_recheck, args.timeout_pong)
		);
		let tls_server = TlsServer::start(tcp_listeners, tls_state, spawner, handler)
			.await.context("Failed to start TLS server")?;

		// start the ping ponger
		let ping_ponger = PingPonger::start(
			beacon.clone(),
			args.interval_ping_poll,
			args.interval_ping_check,
			args.timeout_pong
		);

		Ok(Self {
			tls_server,
			addrs,
			database,
			beacon,
			ping_ponger
		})
	}

	#[allow(unused)]
	pub fn addrs(&self) -> &Vec<SocketAddr> {
		&self.addrs
	}

	#[allow(unused)]
	pub fn hosts(&self) -> Hosts {
		self.database.hosts()
	}

	#[allow(unused)]
	pub async fn num_connections(&self) -> usize {
		self.tls_server.num_connections()
			.await
	}

	/// waits until there are no connections
	pub async fn connections_finished(&mut self) {
		self.tls_server.connections_finished()
			.await
	}

	pub async fn stop_listening(&mut self) {
		self.tls_server.stop_listening()
			.await;
	}

	pub async fn disconnect_hosts(&mut self) {
		// NOTE: do not disconnect all the connections at the TLS server level,
		//       since that won't let the host server do any per-connection cleanup
		//       (the tasks just get dropped and won't poll to completion)
		let mut beacon = self.beacon.write()
			.await;
		for host in beacon.hosts.hosts.values_mut() {
			host.disconnect();
		}
	}

	/// sends a shutdown signal to the server and waits for it to stop
	pub async fn shutdown(mut self) {

		// gracefully disconnect all the hosts first
		self.stop_listening()
			.await;
		self.disconnect_hosts()
			.await;
		self.connections_finished()
			.await;

		// shutdown the ping ponger
		self.ping_ponger.shutdown()
			.await;

		// then shutdown the TLS server
		self.tls_server.shutdown()
			.await;
	}
}


struct Handler {
	args: ArgsHosts,
	beacon: Arc<RwLock<Beacon>>,
	database: Arc<Database>,
	ping_pong_checker: PingPongChecker
}

#[async_trait]
impl TlsServerHandler<TlsServerSpawnerNop> for Handler {

	/// this function acts like a state machine that manages the connection with the host
	#[tracing::instrument(skip_all, level = 5, name = "Hosts", fields(conn_id))]
	async fn handle(&self, _spawned: (), stream: &mut TlsStream<TcpStream>) {

		// track the connection
		let conn_id = self.beacon.write()
			.await
			.hosts
			.connect();
		tracing::Span::current().record("conn_id", conn_id);
		debug!("Connected");

		let proto_stream = FramedProtos::from(stream);
		self.handle_host(conn_id, proto_stream)
			.await;

		// end the connection
		self.beacon.write()
			.await
			.hosts
			.disconnect(conn_id);
		debug!("Disconnected");
	}
}

impl Handler {

	fn new(args: ArgsHosts, beacon: Arc<RwLock<Beacon>>, database: Arc<Database>, ping_pong_checker: PingPongChecker) -> Self {
		Self {
			args,
			beacon,
			database,
			ping_pong_checker
		}
	}

	async fn handle_host<IO>(&self, conn_id: ConnectionId, mut stream: FramedProtos<IO>)
		where
			IO: AsyncRead + AsyncWrite + Unpin
	{

		// wait for the host ready request, with a timeout
		let host_id = match timeout(self.args.timeout_ready, stream.recv()).await {

			Err(_) => {
				warn!("Timed out waiting for ready request");
				return;
			}

			Ok(Err(e)) => {
				error!(err = %e.into_chain(), "Error waiting for ready request");
				return;
			}

			Ok(Ok(proto)) => {
				let Ok(request) = hosts::beacon::decode_ready_request(proto)
					.context("Failed to decode ready request")
					.warn_err()
					else { return; };
				request.host_id.wrap()
			}
		};

		tracing::Span::current().record("app_uid", BASE64.encode(host_id.uid()));
		info!(name = host_id.name(), "App connecting");

		// see if this host is already ready
		{
			let beacon = self.beacon.read()
				.await;
			if let Some(host) = beacon.hosts.get(host_id.uid()) {

				warn!("host already ready: can't ready again");
				stream.send(hosts::beacon::encode_ready_challenge_error_already_ready())
					.await
					.context("Failed to send ready challenge error to the host")
					.warn_err()
					.ok();

				// send a ping now to see if the host is still there
				// if not, we'll boot the current zombie host, and the new host can reconnect
				self.ping_pong_checker.check(&host);

				return;
			}
		}

		// start an authentication session and generate a challenge
		let challenge_nonce = self.beacon.write()
			.await
			.hosts.authn_sessions.get_or_make(&host_id)
			.challenge(self.database.as_ref())
			.await;
		let challenge_nonce = match challenge_nonce {
			Ok(n) => n,
			Err(e) => {
				stream.send(hosts::beacon::encode_ready_challenge_error_authn(AuthnError::from(e)))
					.await
					.context("Failed to send ready challenge error to the host")
					.warn_err()
					.ok();
				return;
			}
		};

		// send the ready challenge
		let challenge = hosts::beacon::encode_ready_challenge_nonce(challenge_nonce);
		let result = stream.send(challenge)
			.await;
		if let Err(e) = result {
			error!(err = %e.into_chain(), "Failed to send ready challenge to the host");
			return;
		}

		// wait for the host ready response, with a timeout
		let response = match timeout(self.args.timeout_ready, stream.recv()).await {

			Err(_) => {
				warn!("Timed out waiting for ready response");
				return;
			}

			Ok(Err(e)) => {
				error!(err = %e.into_chain(), "Error waiting for ready response");
				return;
			}

			Ok(Ok(proto)) => {
				let Ok(response) = hosts::beacon::decode_ready_response(proto)
					.context("Failed to decode ready response")
					.warn_err()
					else { return; };
				response
			}
		};

		// authenticate the host
		let result = {
			let mut beacon = self.beacon.write()
				.await;
			match beacon.hosts.authn_sessions.get(host_id.uid()) {
				None => Err(AuthnServerError::NoChallenge),
				Some(authn_session) => {
					authn_session.response(self.database.as_ref(), response.signed_nonce)
						.await
				}
			}
		};
		if let Err(e) = result {
			warn!("Failed authentication: {}", e);
			stream.send(hosts::beacon::encode_ready_result_error_authn(AuthnError::from(e)))
				.await
				.context("Failed to send ready result error to the host")
				.warn_err()
				.ok();
			return;
		}

		// if we got here, the host authenticated successfully. Woo!
		debug!("Authenticated");

		// authorize the host
		let authz = self.database.hosts().authorize(host_id.uid())
			.await;
		if let Authz::Deny = authz {
			warn!("Failed authorization");
			stream.send(hosts::beacon::encode_ready_result_error_not_authorized())
				.await
				.context("Failed to send ready response error to the host")
				.warn_err()
				.ok();
			return;
		}
		debug!("Authorized");

		// try to register the host with the beacon
		let host_uid = host_id.uid().clone();
		let (bridge_guest, mut bridge_host) = new_bridge();
		let (disconnect_tx, mut disconnect_rx) = oneshot::channel();
		let result = {
			let mut beacon = self.beacon.write()
				.await;
			let Identity::App(app_id) = host_id
				else { panic!("host id was not an app id") };
				// PANIC SAFETY: the host id is a wrapped app id, so unwrapping as an app id here is safe
			beacon.hosts.add(Host {
				app_id,
				conn_id,
				bridge: bridge_guest,
				disconnect_tx: Some(disconnect_tx),
				last_contact: Instant::now()
			}).await
		};
		match result {

			Ok(()) => {

				debug!("Added host to beacon");

				// tell the host all is well
				stream.send(hosts::beacon::encode_ready_result_ok())
					.await
					.context("Failed to send ready response to the host")
					.warn_err()
					.ok();
			}

			Err(AddHostError::AlreadyAdded) => {

				warn!("Can't add host, already added");

				// tell the connection and then disconnect
				stream.send(hosts::beacon::encode_ready_result_error_authn(AuthnError::Internal))
					.await
					.context("Failed to send ready result error to the host")
					.warn_err()
					.ok();
				return;
			}
		}

		// NOTE: The host has been added to the beacon now.
		//       Don't return from this function without cleaning it up.

		// keep the connection open and listen for messages from either side
		let mut bridge_txs = BridgeTransmitters::new();
		loop {

			// listen to the host and the guest endpoint simultaneously
			let result = tokio::select! {
				msg = bridge_host.recv() => self.handle_msg_from_guest(&mut bridge_txs, msg, &mut stream)
					.await,
				msg = stream.recv() => self.handle_msg_from_host(&self.beacon, &host_uid, &mut bridge_txs, msg)
					.await,
				_ = &mut disconnect_rx => ListenResult::Close
			};
			if let ListenResult::Close = result {
				break;
			}
		}

		debug!("Connection ending, removing host from beacon");

		// unregister the host with the beacon
		self.beacon.write()
			.await
			.hosts
			.remove(&host_uid);
	}

	async fn handle_msg_from_guest<IO>(
		&self,
		bridge_txs: &mut BridgeTransmitters,
		msg: Option<BridgeRequestChannel>,
		stream: &mut FramedProtos<IO>
	) -> ListenResult
		where
			IO: AsyncRead + AsyncWrite + Unpin
	{
		match msg {

			// bridge broken, get out of here
			None => {
				debug!("Bridge Disconnected");
				return ListenResult::Close;
			}

			Some((BridgeRequest::GuestConnectRequest(request), tx)) => {

				let Ok(tx) = tx
					.context("Expected response sender for GuestConnectRequest")
					.warn_err()
					else { return ListenResult::Close; };

				// forward connection requests from the guest
				let request_id = bridge_txs.add(tx);
				let connect = hosts::beacon::encode_guest_connect(request_id, request.identity, request.secret);
				let Ok(_) = stream.send(connect)
					.await
					.context("Failed to send connect message to host")
					.warn_err()
					else { return ListenResult::Close; };
			}

			Some((BridgeRequest::GuestConnectConfirm(confirm), _)) => {

				// forward connection confirmations from the guest
				let confirm = hosts::beacon::encode_guest_confirm(confirm.guest_uid, confirm.secret);
				let Ok(_) = stream.send(confirm)
					.await
					.context("Failed to send confirm message to host")
					.warn_err()
					else { return ListenResult::Close; };
			}

			Some((BridgeRequest::Ping, tx)) => {

				let Ok(tx) = tx
					.context("Expected response sender for Ping")
					.warn_err()
					else { return ListenResult::Close; };

				let request_id = bridge_txs.add(tx);
				let ping = hosts::beacon::encode_ping(request_id);
				let Ok(_) = stream.send(ping)
					.await
					.context("Failed to send ping to host")
					.warn_err()
					else { return ListenResult::Close; };
			}
		}

		ListenResult::KeepListening
	}

	async fn handle_msg_from_host(
		&self,
		beacon: &RwLock<Beacon>,
		host_uid: &Vec<u8>,
		bridge_txs: &mut BridgeTransmitters,
		msg: Result<BytesMut,RecvError>
	) -> ListenResult {
		match msg {

			// connection closed by the host
			Err(RecvError::Closed) => {
				debug!("Host closed connection");
				ListenResult::Close
			}

			Err(e) => {
				warn!(err = %e.into_chain(), "Error receiving message from host");
				ListenResult::KeepListening
			}

			Ok(msg) => {

				match hosts::beacon::decode_msg(msg) {

					Err(e) => {
						warn!(err = %e.into_chain(), "Error decoding message from host");
					}

					Ok(hosts::beacon::HostMessage::GuestConnectResponse(response)) => {

						let Ok(tx) = bridge_txs.remove(response.request_id)
							.context(format!("Failed to find bridge txs for request id {}", response.request_id))
							.warn_err()
							else { return ListenResult::KeepListening; };

						// forward the message to the guest side of the bridge
						let response = BridgeResponse::GuestConnectAccept {
							secret: response.secret
						};
						if let Err(_) = tx.send(response) {
							warn!("Failed to send connection response to guest bridge");
						}

						// also update the last contact for this host
						beacon.write()
							.await
							.hosts.get_mut(host_uid)
							.map(|h| h.update_last_contact());
					}

					Ok(hosts::beacon::HostMessage::Pong { request_id }) => {

						let Ok(tx) = bridge_txs.remove(request_id)
							.context(format!("Failed to find bridge txs for request id {}", request_id))
							.warn_err()
							else { return ListenResult::KeepListening; };

						// forward the message across the bridge
						if let Err(_) = tx.send(BridgeResponse::Pong) {
							warn!("Failed to send pong across bridge");
						}
					}
				}

				ListenResult::KeepListening
			}
		}
	}
}


impl<E> From<AuthnServerError<E>> for AuthnError
where
	E: std::error::Error + 'static
{

	fn from(value: AuthnServerError<E>) -> Self {
		match value {
			AuthnServerError::Internal(e) => {
				warn!("authentication attempt failed: {}", e.chain());
				AuthnError::Internal
			},
			AuthnServerError::InvalidIdentity => AuthnError::InvalidIdentity,
			AuthnServerError::TooManyAttempts => AuthnError::TooManyAttempts,
			AuthnServerError::NoChallenge => AuthnError::NoChallenge,
			AuthnServerError::Fail => AuthnError::Fail
		}
	}
}



enum ListenResult {
	KeepListening,
	Close
}


struct BridgeTransmitters {
	txs: HashMap<u64,oneshot::Sender<BridgeResponse>>
}

impl BridgeTransmitters {

	fn new() -> Self {
		Self {
			txs: HashMap::new()
		}
	}

	fn add(&mut self, tx: oneshot::Sender<BridgeResponse>) -> u64 {

		// find a new request id
		let request_id = {
			loop {
				let id = rand::random::<u64>();
				if !self.txs.contains_key(&id) {
					break id;
				}
			}
		};

		self.txs.insert(request_id, tx);

		request_id
	}

	fn remove(&mut self, request_id: u64) -> Option<oneshot::Sender<BridgeResponse>> {
		let out = self.txs.remove(&request_id);
		if out.is_none() {
			warn!(request_id, "Received response for unknown request");
		}
		out
	}
}
