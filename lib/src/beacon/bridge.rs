
use anyhow::{bail, Result};
use tokio::sync::{mpsc, oneshot};

use crate::protobuf::guests::beacon::{ConnectionConfirm, ConnectionRequest};


pub fn new_bridge() -> (BridgeGuest, BridgeHost) {

	let (guest_tx, host_rx) = mpsc::channel(1);

	let guest = BridgeGuest {
		tx: guest_tx
	};

	let host = BridgeHost {
		rx: host_rx
	};

	(guest, host)
}


pub type BridgeRequestChannel = (BridgeRequest,Option<oneshot::Sender<BridgeResponse>>);

#[derive(Clone)]
pub struct BridgeGuest {
	tx: mpsc::Sender<BridgeRequestChannel>
}

impl BridgeGuest {

	pub async fn request_connect(&self, request: ConnectionRequest) -> Result<Vec<u8>> {

		// send to the host side and wait for a response
		let response = self.request(BridgeRequest::GuestConnectRequest(request))
			.await?;
		match response {
			BridgeResponse::GuestConnectAccept { secret } => Ok(secret),
			r => bail!("unexpected response to connection request: {}", r.name())
		}
	}

	pub async fn send_confirm(&self, confirm: ConnectionConfirm) -> Result<()> {

		// send to the host side
		self.send(BridgeRequest::GuestConnectConfirm(confirm))
			.await?;

		Ok(())
	}

	pub async fn ping_pong(&self) -> Result<()> {

		let response = self.request(BridgeRequest::Ping)
			.await?;
		match response {
			BridgeResponse::Pong => Ok(()),
			r => bail!("unexpected response to ping: {}", r.name())
		}
	}

	async fn send(&self, msg: BridgeRequest) -> Result<()> {
		self.tx.send((msg, None))
			.await?;
		Ok(())
	}

	async fn request(&self, msg: BridgeRequest) -> Result<BridgeResponse> {
		let (response_tx, response_rx) = oneshot::channel();
		self.tx.send((msg, Some(response_tx)))
			.await?;
		let response = response_rx
			.await?;
		Ok(response)
	}
}


pub struct BridgeHost {
	rx: mpsc::Receiver<BridgeRequestChannel>
}

impl BridgeHost {

	pub async fn recv(&mut self) -> Option<BridgeRequestChannel> {
		self.rx.recv()
			.await
	}
}


#[derive(Debug, Clone)]
pub enum BridgeRequest {
	GuestConnectRequest(ConnectionRequest),
	GuestConnectConfirm(ConnectionConfirm),
	Ping
}


#[derive(Debug, Clone)]
pub enum BridgeResponse {
	GuestConnectAccept {
		secret: Vec<u8>
	},
	Pong
}

impl BridgeResponse {

	pub fn name(&self) -> &'static str {
		match self {
			Self::GuestConnectAccept { .. } => "GuestConnectAccept",
			Self::Pong { .. } => "Pong"
		}
	}
}