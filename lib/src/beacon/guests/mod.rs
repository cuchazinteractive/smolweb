
#[cfg(test)]
pub(crate) mod test;


use std::ops::Deref;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{Context, Result};
use display_error_chain::ErrorChainExt;
use axum::{Extension, Router};
use axum::routing::post;
use serde::Deserialize;
use tokio::sync::RwLock;
use tokio::time::timeout;
use tracing::error;

use crate::beacon::{Beacon, BeaconSync};
use crate::config::ConfigDuration;
use crate::lang::ErrorReporting;
use crate::beacon::messages::{ProtoRequestRejection, ProtoTransport};
use crate::protobuf::guests;


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigGuests {
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_host: Option<ConfigDuration>
}

impl ConfigGuests {

	pub fn to_args(self) -> Result<ArgsGuests> {
		let default = ArgsGuests::default();
		Ok(ArgsGuests {
			timeout_host: self.timeout_host
				.map(|d| d.to_duration())
				.unwrap_or(default.timeout_host)
		})
	}
}

impl Default for ConfigGuests {

	fn default() -> Self {
		Self {
			timeout_host: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsGuests {
	pub timeout_host: Duration
}

impl Default for ArgsGuests {

	fn default() -> Self {

		let args = Self {
			timeout_host: Duration::from_secs(5)
		};

		#[cfg(test)]
		let args = {
			Self {
				timeout_host: Duration::from_millis(200),
				.. args
			}
		};

		args
	}
}


pub fn guests_routes() -> Router {
	Router::new()
		.route("/host_id", post(host_id))
		.route("/connect", post(connect))
		.route("/confirm", post(confirm))
}


#[tracing::instrument(skip_all, level = 5, name = "Guests:host_id")]
async fn host_id(
	beacon: Extension<Arc<RwLock<Beacon>>>,
	request: ProtoTransport
) -> Result<ProtoTransport,ProtoRequestRejection> {

	// read the message
	let host_uid = guests::beacon::decode_host_identity_request(request)?;

	// look for the host in the beacon
	let beacon = beacon.read()
		.await;
	let host_id = beacon.hosts.get(&host_uid)
		.map(|host| host.app_id.clone());

	let response = guests::beacon::encode_host_identity_response(host_id.as_ref());

	Ok(ProtoTransport::from_bytes(response))
}


#[tracing::instrument(skip_all, level = 5, name = "Guests:connect")]
async fn connect(
	args: Extension<ArgsGuests>,
	beacon: Extension<Arc<RwLock<Beacon>>>,
	request: ProtoTransport
) -> Result<ProtoTransport,ProtoRequestRejection> {

	// read the message
	let request = guests::beacon::decode_connection_request(request)?;

	// get the host bridge
	let bridge = beacon.host_bridge(&request.host_uid)
		.await;
	let response = match bridge {
		None => guests::beacon::encode_connection_response_host_unknown(),
		Some(bridge) => {

			// send the request to the host over the bridge and wait for the response
			let result = timeout(args.timeout_host, bridge.request_connect(request))
				.await;
			match result {
				Ok(Ok(secret)) => {
					guests::beacon::encode_connection_response_accept(secret)
				}
				Ok(Err(e)) => {
					error!(err = %e.deref().chain(), "Failed to send connection request");
					guests::beacon::encode_connection_response_host_unreachable()
				}
				Err(_) => {
					// timeout
					guests::beacon::encode_connection_response_host_unreachable()
				}
			}
		}
	};

	Ok(ProtoTransport::from_bytes(response))
}


#[tracing::instrument(skip_all, level = 5, name = "Guests:confirm")]
async fn confirm(
	args: Extension<ArgsGuests>,
	beacon: Extension<Arc<RwLock<Beacon>>>,
	confirm: ProtoTransport
) -> Result<(),ProtoRequestRejection> {

	// read the message
	let confirm = guests::beacon::decode_connection_confirm(confirm)?;

	// get the host bridge
	let Some(bridge) = beacon.host_bridge(&confirm.host_uid)
		.await
		else { return Ok(()); };

	// send the session description to the host over the bridge, but don't wait for any response
	timeout(args.timeout_host, bridge.send_confirm(confirm))
		.await
		.context("Failed to send message over bridge")
		.warn_err()
		.ok();

	Ok(())
}
