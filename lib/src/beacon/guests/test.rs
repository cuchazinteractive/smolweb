
use std::sync::Arc;
use std::time::{Duration, Instant};

use burgerid::IdentityApp;
use galvanic_assert::{assert_that, matchers::*};
use tokio::sync::RwLock;
use tracing::{debug, trace};

use smolweb_test_tools::id::app_id;
use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::beacon::bridge::{BridgeHost, BridgeRequest, BridgeResponse, new_bridge};
use crate::beacon::{Beacon, ConnectionId, Host};
use crate::beacon::database::{ArgsDatabase, Database};
use crate::beacon::guests::{ArgsGuests, ConfigGuests};
use crate::beacon::hosts::ArgsHosts;
use crate::beacon::https::{ArgsBeaconHttpsServer, BeaconHttpsServer};
use crate::beacon::stun::StunServer;
use crate::beacon::tls::{ArgsTls, ArgsTlsAcme, ArgsTlsCert, TlsState};
use crate::config::ConfigDuration;
use crate::config::test::deserialize_from_toml;
use crate::guest::client::{ArgsGuestsClient, GuestsClient, GuestsClientConnectError};
use crate::net::{ConfigAddr, ConnectMode, FilterAddrs};
use crate::protobuf::guests::About;
use crate::protobuf::hosts;
use crate::protobuf::guests::guest::ConnectionReject;
use crate::protobuf::hosts::beacon::HostMessage;
use crate::protobuf::hosts::host::BeaconMessage;
use crate::tls::test::acmed::Acmed;
use crate::tls::test::{generate_domain_cert, root_cert};
use crate::webrtc::connection::ConnectionConfig;
use crate::webrtc::connection_race::ConnectionRace;


#[test]
fn config() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigGuests>(r#"
	"#).unwrap();
	assert_that!(&config, eq(ConfigGuests::default()));

	let config = deserialize_from_toml::<ConfigGuests>(r#"
		timeout_host = { value = 1, unit = "s" }
	"#).unwrap();
	assert_that!(&config, eq(ConfigGuests {
		timeout_host: Some(ConfigDuration::s(1)),
		.. ConfigGuests::default()
	}));
}


#[test]
fn to_args() {
	let _logging = init_test_logging();

	assert_that!(&ConfigGuests::default().to_args().unwrap(), eq(ArgsGuests::default()));
}


#[tokio::test]
async fn about() {
	let _logging = init_test_logging();

	let args = ArgsGuests::default();
	let server = TestBeaconGuestsServer::start(&args)
		.await;

	let guest = server.client();
	let about = guest.about()
		.await.unwrap();

	assert_that!(&about, eq(About {
		version: env!("CARGO_PKG_VERSION").to_string()
	}));

	// cleanup
	server.shutdown()
		.await;
}


#[tokio::test]
async fn host_identity_not_found() {
	let _logging = init_test_logging();

	let args = ArgsGuests::default();
	let server = TestBeaconGuestsServer::start(&args)
		.await;

	let guest = server.client();
	let host_id = guest.get_host_id(&vec![1, 2, 3])
		.await.unwrap();

	assert_that!(&host_id, is_match!(None));

	// cleanup
	server.shutdown()
		.await;
}


#[tokio::test]
async fn host_identity() {
	let _logging = init_test_logging();

	let args = ArgsGuests::default();
	let server = TestBeaconGuestsServer::start(&args)
		.await;

	// make a host ready
	let (host_id, _host_key, _owner_id, _owner_key) = app_id("Host");
	let _host = TestHost::new(&server.beacon, 0, host_id.clone())
		.await;

	let guest = server.client();
	let host_id2 = guest.get_host_id(host_id.uid())
		.await.unwrap()
		.expect("no host id");

	assert_that!(&&host_id2, eq(&host_id));

	// cleanup
	server.shutdown()
		.await;
}


#[tokio::test]
async fn connect_unknown_host() {
	let _logging = init_test_logging();

	let args = ArgsGuests::default();
	let server = TestBeaconGuestsServer::start(&args)
		.await;

	let (host_id, _host_key, _owner_id, _owner_key) = app_id("Host");

	// try to connect
	let guest = server.client();
	let result = guest.connect(&host_id)
		.await;

	assert_that!(&result, is_match!(Err(GuestsClientConnectError::Rejected(ConnectionReject::HostUnknown))));

	// cleanup
	server.shutdown()
		.await;
}


#[tokio::test]
async fn connect_host_doesnt_respond() {
	let _logging = init_test_logging();

	let mut args = ArgsGuests::default();
	args.timeout_host = Duration::from_millis(100);
	let server = TestBeaconGuestsServer::start(&args)
		.await;

	// make a host ready
	let (host_id, _host_key, _owner_id, _owner_key) = app_id("Host");
	let _host = TestHost::new(&server.beacon, 0, host_id.clone())
		.await;

	// try to connect
	let guest = server.client();
	let result = guest.connect(&host_id)
		.await;

	assert_that!(&result, is_match!(Err(GuestsClientConnectError::Rejected(ConnectionReject::HostUnreachable))));

	// cleanup
	server.shutdown()
		.await;
}


#[tokio::test]
async fn connect_host() {
	let _logging = init_test_logging();

	let mut args = ArgsGuests::default();
	args.timeout_host = Duration::from_millis(1000);
	let server = TestBeaconGuestsServer::start(&args)
		.await;

	// make a host ready
	let (host_id, host_key, _owner_id, _owner_key) = app_id("Host");
	let mut host = TestHost::new(&server.beacon, 0, host_id.clone())
		.await;

	// set up the host responder to follow the connection protocol
	tokio::spawn({
		let host_id = host_id.clone();
		async move {

			// TODO: we don't need to make a full WebRTC connection to test the guests server
			//       but the GuestsClient expects nothing less
			//       this feels like massive overkill

			// receive the connect message
			let (request, tx) = host.bridge.recv()
				.await.unwrap();
			let request = match request {
				BridgeRequest::GuestConnectRequest(r) => r,
				_ => panic!("Unexpected bridge request")
			};
			let tx = tx
				.unwrap();

			// repackage the connect message for the host
			let host_id_unlocked = host_id.unlock(&host_key)
				.unwrap();
			let request_id = 5;
			let msg = hosts::beacon::encode_guest_connect(request_id, request.identity, request.secret);

			// decrypt the secret on the host
			let msg = hosts::host::decode_msg(msg)
				.unwrap();
			let request = match msg {
				BeaconMessage::GuestConnectRequest(r) =>
					r.decode(&host_id_unlocked)
						.unwrap(),
				_ => panic!("Unexpected beacon msg")
			};

			// build the accept response
			let config = ConnectionConfig::default();
			let mut connection = ConnectionRace::new(config.clone(), config)
				.await.unwrap()
				.to_host();
			let (answer1, offer2) = connection.connect1(request.offer1)
				.await.unwrap();
			let conn_id = 42;
			let msg = hosts::host::encode_guest_connect_accept(
				&host_id_unlocked, &request.guest_id, request_id, conn_id, answer1, offer2
			).unwrap();

			// send it back to the guest
			let msg = hosts::beacon::decode_msg(msg)
				.unwrap();
			let secret = match msg {
				HostMessage::GuestConnectResponse(r) => r.secret,
				m => panic!("unexpected message from host: {:?}", m)
			};
			tx
				.send(BridgeResponse::GuestConnectAccept{
					secret
				})
				.unwrap();

			// receive the confirm message
			let (confirm, _) = host.bridge.recv()
				.await.unwrap();
			let confirm = match confirm {
				BridgeRequest::GuestConnectConfirm(c) => c,
				_ => panic!("Unexpected bridge request")
			};

			// repackage the confirm message for the host
			let msg = hosts::beacon::encode_guest_confirm(request.guest_id.uid(), confirm.secret);

			// decrypt the secret on the host
			let confirm = hosts::host::decode_msg(msg)
				.unwrap();
			let confirm = match confirm {
				BeaconMessage::GuestConnectConfirm(c) =>
					c.decode(&host_id_unlocked, &request.guest_id)
						.unwrap(),
				_ => panic!("Unexpected beacon msg")
			};

			// make the connection, then close
			connection.connect2(confirm.answer2)
				.await.unwrap();
			connection.wait_for_open()
				.await;
			match connection.open().await {
				Ok(data_channel) => {
					trace!("Server: connection open");
					data_channel.close()
						.await;
				}
				Err(connection) => {
					trace!("Server: abort connection");
					connection.abort()
						.await;
				}
			};
		}
	});

	// try to connect
	let guest = server.client();
	let data_channel = guest.connect(&host_id)
		.await.unwrap();

	// cleanup
	data_channel.close()
		.await;
	server.shutdown()
		.await;
}


pub struct TestBeaconGuestsServer {
	pub beacon: Arc<RwLock<Beacon>>,
	_tls_state: TlsState,
	pub guests_server: TestGuestsServer
}

impl TestBeaconGuestsServer {

	pub async fn start(args_guests: &ArgsGuests) -> Self {
		let tls_args = TestGuestsServer::args_tls_cert();
		Self::start_tls(args_guests, ArgsTls::Cert(tls_args))
			.await
	}

	pub async fn start_acme(args_guests: &ArgsGuests, acmed: &Acmed) -> Self {
		let tls_args = TestGuestsServer::args_tls_acme(&acmed);
		Self::start_tls(args_guests, ArgsTls::Acme(tls_args))
			.await
	}

	pub async fn start_tls(args_guests: &ArgsGuests, tls_args: ArgsTls) -> Self {
		let owner_uid = vec![1, 2, 3];
		let args_hosts = ArgsHosts::default();
		let database = Database::new(ArgsDatabase::default())
			.await.unwrap();
		let database = Arc::new(database);
		let beacon = Arc::new(RwLock::new(Beacon::new(owner_uid, &args_hosts, database.clone())));
		let tls_state = tls_args.state(database.clone());
		let guests_server = TestGuestsServer::start(beacon.clone(), database, args_guests, &tls_state)
			.await;
		Self {
			beacon,
			_tls_state: tls_state,
			guests_server
		}
	}

	pub fn client(&self) -> GuestsClient {
		self.guests_server.client(None, ConnectMode::PreferIpv6)
	}

	pub async fn shutdown(self) -> Beacon {
		self.guests_server.shutdown()
			.await;
		Arc::into_inner(self.beacon)
			.expect("beacon reference leak")
			.into_inner()
	}
}


pub struct TestGuestsServer {
	server: BeaconHttpsServer,
	domain: String
}

impl TestGuestsServer {

	pub fn args_tls_acme(acmed: &Acmed) -> ArgsTlsAcme {
		ArgsTlsAcme {
			domains: vec!["test.guests.localhost".to_string()],
			emails: vec!["me@domain.tld".to_string()],
			directory: acmed.directory(),
			timeout_cert: ArgsTlsAcme::DEFAULT_TIMEOUT_CERT
		}
	}

	pub fn args_tls_cert() -> ArgsTlsCert {
		generate_domain_cert("test.guests.localhost")
			.to_tls_args()
	}

	pub async fn start(beacon: Arc<RwLock<Beacon>>, database: Arc<Database>, args_guests: &ArgsGuests, tls_state: &TlsState) -> Self {

		// start a real guests server
		let args = ArgsBeaconHttpsServer::default();
		let server = BeaconHttpsServer::start(
			args,
			Some(args_guests.clone()),
			beacon,
			database,
			tls_state
		)
			.await.unwrap();

		let domain = tls_state.any_domain();

		Self {
			server,
			domain
		}
	}

	#[tracing::instrument(skip_all, level = 5, name = "TestGuestServer")]
	pub fn client(&self, stun_server: Option<&StunServer>, connect_mode: ConnectMode) -> GuestsClient {

		let port = self.server.addrs()
			.filter_addrs_for_connect(connect_mode)
			.first()
			.unwrap()
			.port();
		let beacon_addr = ConfigAddr::new(self.domain.clone(), Some(port));
		debug!(%beacon_addr, "making client");

		let args = ArgsGuestsClient {
			stun_addr: stun_server.map(|stun_server| {
				let stun_addr = stun_server.addrs()
					.filter_addrs_for_connect(connect_mode)
					.first()
					.unwrap()
					.clone();
				ConfigAddr::new(stun_addr.ip().to_string(), Some(stun_addr.port()))
			}),
			beacon_addr,
			http_config: Some(Box::new(|builder| {
				let root_cert_der = root_cert()
					.serialize_der()
					.unwrap();
				let root_cert = reqwest::Certificate::from_der(root_cert_der.as_slice())
					.unwrap();
				builder
					.tls_built_in_root_certs(false)
					.add_root_certificate(root_cert)
			})),
			connect_mode,
			webrtc_timeout: Duration::from_secs(2),
		};
		GuestsClient::new(args)
			.unwrap()
	}

	pub async fn shutdown(self) {
		self.server.shutdown()
			.await;
	}
}


struct TestHost {
	bridge: BridgeHost
}

impl TestHost {

	async fn new(beacon: &Arc<RwLock<Beacon>>, conn_id: ConnectionId, app_id: IdentityApp) -> Self {

		let (guest, host) = new_bridge();

		let mut beacon = beacon.write()
			.await;
		beacon.hosts.add(Host {
			app_id,
			conn_id,
			bridge: guest,
			disconnect_tx: None,
			last_contact: Instant::now()
		})
			.await.unwrap();

		Self {
			bridge: host
		}
	}
}
