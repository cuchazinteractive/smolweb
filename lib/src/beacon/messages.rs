
use std::ops::Deref;

use axum::{async_trait, BoxError};
use axum::body::{boxed, HttpBody};
use axum::extract::FromRequest;
use axum::extract::rejection::BytesRejection;
use axum::http::header::CONTENT_TYPE;
use axum::http::{HeaderValue, Request, StatusCode};
use axum::response::{IntoResponse, Response};
use bytes::Bytes;
use display_error_chain::ErrorChainExt;
use hyper::Body;
use strum_macros::IntoStaticStr;
use thiserror::Error;
use tracing::error;

use crate::net::messages::CONTENT_TYPE_PROTO;
use crate::protobuf::ReadProtobufError;


#[derive(Debug)]
pub struct ProtoTransport(pub Bytes);

impl ProtoTransport {

	pub fn from_bytes(src: impl Into<Bytes>) -> Self {
		Self(src.into())
	}

	pub fn from_msg<T:prost::Message>(msg: T) -> Self {
		Self::from_bytes(msg.encode_to_vec())
	}
}

impl AsRef<[u8]> for ProtoTransport {

	fn as_ref(&self) -> &[u8] {
		self.0.as_ref()
	}
}


#[async_trait]
impl<S,B> FromRequest<S,B> for ProtoTransport
where
	B: HttpBody + Send + 'static,
	B::Data: Send,
	B::Error: Into<BoxError>,
	S: Send + Sync
{
	type Rejection = ProtoRequestRejection;

	async fn from_request(req: Request<B>, state: &S) -> Result<Self,Self::Rejection> {

		// check the request content type
		let content_type = req.headers().get(CONTENT_TYPE)
			.ok_or(ProtoRequestRejection::InvalidContentType { content_type: None })?;
		let content_type = String::from_utf8_lossy(content_type.as_bytes())
			.to_string();
		if &content_type != CONTENT_TYPE_PROTO {
			Err(ProtoRequestRejection::InvalidContentType { content_type: Some(content_type) })?;
		}

		// read the request body
		let bytes = Bytes::from_request(req, state)
			.await?;

		Ok(ProtoTransport(bytes))
	}
}

impl IntoResponse for ProtoTransport {

	fn into_response(self) -> Response {

		// build the response
		let body = Body::from(self.0);
		let mut response = Response::new(boxed(body));
		response.headers_mut().insert(CONTENT_TYPE, HeaderValue::from_str(CONTENT_TYPE_PROTO).unwrap());
		*response.status_mut() = StatusCode::OK;

		response
	}
}


#[derive(Error, Debug, IntoStaticStr)]
pub enum ProtoRequestRejection<E=NoValidation> {

	#[error("Invalid content type for a Protobuf request: {content_type:?}")]
	InvalidContentType {
		content_type: Option<String>
	},

	#[error("Failed to read request")]
	Read(#[from] BytesRejection),

	#[error("Failed to read	protobuf")]
	Protobuf(#[from] ReadProtobufError),

	#[error("The request failed")]
	Fail(#[source] E),

	#[allow(unused)]
	#[error("The request failed due to an internal server error")]
	InternalError
}

impl<T> From<anyhow::Error> for ProtoRequestRejection<T> {
	fn from(err: anyhow::Error) -> Self {
		error!("{}", err.deref().chain());
		ProtoRequestRejection::InternalError
	}
}


#[derive(Error, Debug)]
#[error("")]
pub struct NoValidation {}

impl<'a> From<&'a NoValidation> for &'static str {
	fn from(_value: &'a NoValidation) -> Self {
		""
	}
}


#[allow(unused)]
macro_rules! into_proto_rejection {
	($t:tt) => {
		impl From<$t> for ProtoRequestRejection<$t> {
			fn from(value: $t) -> Self {
				ProtoRequestRejection::Fail(value)
			}
		}
	}
}

#[allow(unused)]
pub(crate) use into_proto_rejection;


impl<E> IntoResponse for ProtoRequestRejection<E>
	where
		E: std::error::Error + 'static,
		for<'a> &'a E: Into<&'static str>
{

	fn into_response(self) -> Response {

		// build the error body
		let outer_id: &'static str = (&self).into();
		let err_id = match &self {
			ProtoRequestRejection::Fail(e) => {
				let inner_id: &'static str = e.into();
				format!("{}:{}", outer_id, inner_id)
			}
			_ => format!("{}", outer_id)
		};
		let body = Body::from(format!("{}: {}", err_id, self.chain()));

		// build the response as text/plain and either HTTP 400, or 500
		let mut response = Response::new(boxed(body));

		response.headers_mut().insert(CONTENT_TYPE, HeaderValue::from_str("text/plain").unwrap());
		*response.status_mut() = match self {
			Self::InvalidContentType { .. } => StatusCode::BAD_REQUEST,
			Self::Read(..) => StatusCode::BAD_REQUEST,
			Self::Protobuf(..) => StatusCode::BAD_REQUEST,
			Self::Fail(..) => StatusCode::BAD_REQUEST,
			Self::InternalError => StatusCode::INTERNAL_SERVER_ERROR,
		};

		response
	}
}
