
use std::collections::HashMap;
use std::ops::Deref;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{anyhow, Context, Result};
use axum::async_trait;
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use tokio::sync::{RwLock, oneshot};
use tokio::task::JoinHandle;
use tokio_rustls::LazyConfigAcceptor;
use tokio_rustls::rustls::ServerConfig;
use tokio_rustls::server::TlsStream;
use tracing::{debug, error_span, field, Instrument, warn};

use crate::beacon::tls::{AcmeCertChecker, TlsState};
use crate::lang::ErrorReporting;
use crate::net::is_close_like_error;
use crate::net::listeners::TcpListeners;


#[async_trait]
pub trait TlsServerSpawner {
	type Target;
	async fn spawn(&mut self) -> Self::Target;
}

/// A spawner that doesn't spawn anything,
/// for when you don't need a spawner at all
pub struct TlsServerSpawnerNop {}

impl TlsServerSpawnerNop {

	pub fn new() -> Self {
		Self {}
	}
}

#[async_trait]
impl TlsServerSpawner for TlsServerSpawnerNop {
	type Target = ();
	async fn spawn(&mut self) -> Self::Target {
		()
	}
}


#[async_trait]
pub trait TlsServerHandler<S:TlsServerSpawner> {
	async fn handle(&self, spawned: S::Target, stream: &mut TlsStream<TcpStream>);
}


pub struct TlsServer {
	connections: Arc<RwLock<HashMap<u64,ConnectionInfo>>>,
	shutdown_tx: Option<oneshot::Sender<()>>,
	listener_task: Option<JoinHandle<()>>,
}

impl TlsServer {

	#[tracing::instrument(skip_all, level = 5, name = "TLSServer")]
	pub async fn start<S,H>(tcp_listeners: TcpListeners, tls_state: &TlsState, mut spawner: S, handler: H) -> Result<Self>
		where
			S: TlsServerSpawner + Send + 'static,
			S::Target: Send + 'static,
			H: TlsServerHandler<S> + Send + Sync + 'static
	{

		// set the TLS port for ACMEd
		#[cfg(test)]
		{
			use crate::net::{ConnectMode, FilterAddrs};

			let port = tcp_listeners.addrs()
				.filter_addrs_for_connect(ConnectMode::PreferIpv6)
				.first()
				.unwrap()
				.port();
			tls_state.set_tls_port(port);
		}

		// get TLS config
		let tls_config = tls_state.config()
			.await.context("Failed to configure TLS")?;
		let tls_config = Arc::new(tls_config);
		let acme_checker = tls_state.acme_checker()
			.await;

		let (shutdown_tx, mut shutdown_rx) = oneshot::channel();
		let connections = Arc::new(RwLock::new(HashMap::<u64,ConnectionInfo>::new()));

		// start a TCP listener task
		let listener_task = tokio::spawn({
			let connections = connections.clone();
			async move {

				let handler = Arc::new(handler);

				debug!("Listening");

				loop {

					tokio::select! {

						// handle incoming connections
						// NOTE: acccept() is cancel safe, so no incoming connections will be missed by dropping/canceling futures
						result = tcp_listeners.accept() => {
							if let Some(Ok((stream, _client_addr))) = result {

								let acme_checker = acme_checker.clone();
								let tls_config = tls_config.clone();
								let connections = connections.clone();
								let handler = handler.clone();

								// run the spawner
								let spawned = spawner.spawn()
									.await;

								// spawn a task for each connection
								tokio::spawn(async move {

									// NOTE: don't panic here, or the client will just hang

									// create the connection info
									let (connection_id, disconnect_rx) = {

										let mut connections = connections.write()
											.await;

										// assign an arbitrary connection id
										let connection_id = loop {
											let id = rand::random::<u64>();
											if !connections.contains_key(&id) {
												break id;
											}
										};

										let (tx, rx) = oneshot::channel();
										connections.insert(connection_id, ConnectionInfo {
											disconnect_tx: Some(tx)
										});

										(connection_id, rx)
									};

									let span = tracing::span::Span::current();
									span.record("id", connection_id);

									// NOTE: Failed HTTPs/TLS connection events are too noisy in prouction to log.
									//       They generate too much spam and drown out useful signals,
									//       so don't do that.

									tls_connect(tls_config, acme_checker, stream, spawned, handler, disconnect_rx)
										.await;

									// cleanup the connection info
									connections.write()
										.await
										.remove(&connection_id);

								}.instrument(error_span!("Connection", id = field::Empty)));
							}
						}

						// handle the shutdown signal
						_ = &mut shutdown_rx => break
					}
				}

				debug!("Finished");
			}
		}.in_current_span());

		Ok(Self {
			connections,
			shutdown_tx: Some(shutdown_tx),
			listener_task: Some(listener_task)
		})
	}

	#[allow(unused)]
	pub async fn num_connections(&self) -> usize {
		self.connections.read()
			.await
			.len()
	}

	/// waits until there are no connections
	pub async fn connections_finished(&self) {

		// just use a dumb wait loop here
		loop {
			let num_connections = self.connections.read()
				.await
				.len();
			if num_connections <= 0 {
				break;
			}

			// wait a bit and try again
			tokio::time::sleep(Duration::from_millis(100))
				.await;
		}
	}

	#[allow(unused)]
	pub async fn disconnect_all(&mut self) {

		let txs = {
			let mut connections = self.connections.write()
				.await;
			connections
				.values_mut()
				.filter_map(|c| c.disconnect_tx.take())
				.collect::<Vec<_>>()
		};

		for tx in txs {
			tx.send(())
				.map_err(|_| anyhow!("Failed to send disconnect signal to TLS connection"))
				.warn_err()
				.ok();
		}
	}

	#[allow(unused)]
	pub async fn is_listening(&self) -> bool {
		self.shutdown_tx.is_some()
	}

	#[tracing::instrument(skip_all, level = 5, name = "TLSServer")]
	pub async fn stop_listening(&mut self) {

		// stop the connection listener, if needed
		if let Some(shutdown_tx) = self.shutdown_tx.take() {
			let result = shutdown_tx
				.send(());
			if let Err(_) = result {
				warn!("Failed to send shutdown signal");
			} else if let Some(listener_task) = self.listener_task.take() {
				listener_task
					.await
					.context("Failed to wait for listen task")
					.warn_err()
					.ok();
			}
		}
	}

	/// sends a shutdown signal to the server and waits for it to stop
	#[tracing::instrument(skip_all, level = 5, name = "TLSServer")]
	pub async fn shutdown(mut self) {

		self.stop_listening()
			.await;

		// also close any open connections
		self.disconnect_all()
			.await;
		self.connections_finished()
			.await;
	}
}


struct ConnectionInfo {
	disconnect_tx: Option<oneshot::Sender<()>>
}


async fn tls_connect<S,H>(
	tls_config: Arc<ServerConfig>,
	acme_checker: Option<AcmeCertChecker>,
	stream: TcpStream,
	spawned: S::Target,
	handler: Arc<H>,
	disconnect_rx: oneshot::Receiver<()>
)
	where
		S: TlsServerSpawner + Send + 'static,
		S::Target: Send + 'static,
		H: TlsServerHandler<S> + Send + Sync + 'static
{

	// start the TLS handshake
	let tls_handshake = match acme_checker.as_ref() {

		// start the connection and handle any ACME ALPN challenge requests
		Some(acme_checker) => {
			let tls_handshake = acme_checker.acceptor.accept(stream)
				.await;
			match tls_handshake {
				Ok(Some(h)) => Ok(h),
				Ok(None) => {
					// handled a challenge request, we can just drop the connection now
					debug!("TLS ALPN challenge");
					return;
				},
				Err(e) => Err(e)
			}
		}

		// start the connection without any ALPN handlers
		None => {
			LazyConfigAcceptor::new(Default::default(), stream)
				.await
		}
	};
	let tls_handshake = match tls_handshake {
		Ok(h) => h,
		Err(_) => {
			// TLS connection failed, ignore any errors
			// production servers get a lot of failed connections from spam,bots,etc and logging this just spams the log
			//error!(err = %e.into_chain(), "TLS start handshake failed");
			return;
		}
	};

	if let Some(acme_checker) = acme_checker.as_ref() {
		// make the connection wait until we have our TLS certificate, or timeout and close the connection
		let has_cert = acme_checker.has_cert(tls_config.cert_resolver.deref(), &tls_handshake)
			.await;
		if !has_cert {
			warn!("Timed out waiting for server cert from ACME client, closing TLS client connection");
			return;
		}
	}

	// try to start the TLS session
	let tls_stream = tls_handshake.into_stream(tls_config)
		.await;
	let mut tls_stream = match tls_stream {
		Ok(s) => s,
		Err(_) => {
			// TLS connection failed, ignore any errors
			// production servers get a lot of failed connections from spam,bots,etc and logging this just spams the log
			//error!(err = %e.into_chain(), "TLS session failed");
			return;
		}
	};

	// send successful connection to the caller
	tokio::select! {
		_ = handler.handle(spawned, &mut tls_stream) => (),
		_ = disconnect_rx => ()
	}

	// gracefully close the connection
	let result = tls_stream.shutdown()
		.await;
	match result {
		Ok(_) => (),
		Err(e) if is_close_like_error(&e) => (), // already closed, error safe to ignore
		Err(e) => {
			Err::<(),_>(e)
				.context("Failed to shut down the TLS stream")
				.warn_err()
				.ok();
		}
	}
}
