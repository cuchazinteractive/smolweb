
use galvanic_assert::{assert_that, matchers::*};

use smolweb_test_tools::is_match;
use smolweb_test_tools::logging::init_test_logging;

use crate::config::test::deserialize_from_toml;

use super::*;


#[test]
fn config() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigTls>(r#"
		"#).unwrap();
	assert_that!(&config, eq(ConfigTls::default()));

	let config = deserialize_from_toml::<ConfigTls>(r#"
			[acme]
		"#).unwrap();
	assert_that!(&config, eq(ConfigTls {
		acme: Some(ConfigTlsAcme::default()),
			.. ConfigTls::default()
		}));

	let config = deserialize_from_toml::<ConfigTls>(r#"
			[cert]
		"#).unwrap();
	assert_that!(&config, eq(ConfigTls {
		cert: Some(ConfigTlsCert::default()),
			.. ConfigTls::default()
		}));
}


#[test]
fn config_acme() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigTlsAcme>(r#"
		"#).unwrap();
	assert_that!(&config, eq(ConfigTlsAcme::default()));

	let config = deserialize_from_toml::<ConfigTlsAcme>(r#"
			domains = ["foo", "bar"]
			emails = ["me", "you"]
			directory = "http://dir"
			timeout_cert = "1s"
		"#).unwrap();
	assert_that!(&config, eq(ConfigTlsAcme {
			domains: Some(vec!["foo".to_string(), "bar".to_string()]),
			emails: Some(vec!["me".to_string(), "you".to_string()]),
			directory: Some("http://dir".to_string()),
			timeout_cert: Some(ConfigDuration::s(1))
		}));
}


#[test]
fn config_cert() {
	let _logging = init_test_logging();

	let config = deserialize_from_toml::<ConfigTlsCert>(r#"
		"#).unwrap();
	assert_that!(&config, eq(ConfigTlsCert::default()));

	let config = deserialize_from_toml::<ConfigTlsCert>(r#"
			cert = "cert"
			key = "key"
		"#).unwrap();
	assert_that!(&config, eq(ConfigTlsCert {
			cert: Some("cert".to_string()),
			key: Some("key".to_string())
		}));
}


#[test]
fn to_args() {
	let _logging = init_test_logging();

	assert_that!(&ConfigTls::default().to_args(), is_match!(Err(..)));

	assert_that!(&ConfigTls {
			acme: Some(ConfigTlsAcme {
				domains: Some(vec!["foo".to_string()]),
				emails: Some(vec!["me".to_string()]),
				directory: Some("dir".to_string()),
				timeout_cert: Some(ConfigDuration::s(1))
			}),
			.. ConfigTls::default()
		}.to_args().unwrap(), eq(ArgsTls::Acme(ArgsTlsAcme {
			domains: vec!["foo".to_string()],
			emails: vec!["me".to_string()],
			directory: "dir".to_string(),
			timeout_cert: Duration::from_secs(1)
		})));

	let (_tmpdir, cert_path, key_path, cert_der, key_der) = crate::tls::test::write_tmp_cert_key();

	assert_that!(&ConfigTls {
			cert: Some(ConfigTlsCert {
				cert: Some(cert_path.clone()),
				key: Some(key_path.clone())
			}),
			.. ConfigTls::default()
		}.to_args().unwrap(), eq(ArgsTls::Cert(ArgsTlsCert {
			cert_chain: vec![Certificate(cert_der)],
			private_key: PrivateKey(key_der)
		})));

	assert_that!(&ConfigTls {
			acme: Some(ConfigTlsAcme {
				domains: Some(vec!["foo".to_string()]),
				emails: Some(vec!["me".to_string()]),
				directory: Some("dir".to_string()),
				timeout_cert: Some(ConfigDuration::s(1))
			}),
			cert: Some(ConfigTlsCert {
				cert: Some(cert_path),
				key: Some(key_path)
			})
		}.to_args(), is_match!(Err(_)));
}


#[test]
fn to_args_acme() {
	let _logging = init_test_logging();

	// some things are required
	assert_that!(&ConfigTlsAcme::default().to_args(), is_match!(Err(..)));

	assert_that!(&ConfigTlsAcme {
			domains: Some(vec!["foo".to_string()]),
			emails: Some(vec!["me".to_string()]),
			directory: Some("dir".to_string()),
			timeout_cert: Some(ConfigDuration::s(1))
		}.to_args().unwrap(), eq(ArgsTlsAcme {
			domains: vec!["foo".to_string()],
			emails: vec!["me".to_string()],
			directory: "dir".to_string(),
			timeout_cert: Duration::from_secs(1)
		}));

	// domains are required
	assert_that!(&ConfigTlsAcme {
			domains: Some(vec![]),
			emails: Some(vec!["me".to_string()]),
			.. ConfigTlsAcme::default()
		}.to_args(), is_match!(Err(..)));

	assert_that!(&ConfigTlsAcme {
			emails: Some(vec!["me".to_string()]),
			.. ConfigTlsAcme::default()
		}.to_args(), is_match!(Err(..)));

	// emails are required
	assert_that!(&ConfigTlsAcme {
			domains: Some(vec!["foo".to_string()]),
			emails: Some(vec![]),
			.. ConfigTlsAcme::default()
		}.to_args(), is_match!(Err(..)));

	assert_that!(&ConfigTlsAcme {
			domains: Some(vec!["foo".to_string()]),
			.. ConfigTlsAcme::default()
		}.to_args(), is_match!(Err(..)));
}


#[test]
fn to_args_cert() {
	let _logging = init_test_logging();

	assert_that!(&ConfigTlsCert::default().to_args(), is_match!(Err(..)));

	let (_tmpdir, cert_path, key_path, cert_der, key_der) = crate::tls::test::write_tmp_cert_key();

	assert_that!(&ConfigTlsCert {
			cert: Some(cert_path.clone()),
			key: Some(key_path.clone())
		}.to_args().unwrap(), eq(ArgsTlsCert {
			cert_chain: vec![Certificate(cert_der)],
			private_key: PrivateKey(key_der)
		}));

	// cert path is required
	assert_that!(&ConfigTlsCert {
			cert: Some("".to_string()),
			key: Some(key_path.clone())
		}.to_args(), is_match!(Err(..)));

	assert_that!(&ConfigTlsCert {
			key: Some(key_path),
			.. ConfigTlsCert::default()
		}.to_args(), is_match!(Err(..)));

	// key path is required
	assert_that!(&ConfigTlsCert {
			cert: Some(cert_path.clone()),
			key: Some("".to_string())
		}.to_args(), is_match!(Err(..)));

	assert_that!(&ConfigTlsCert {
			cert: Some(cert_path),
			.. ConfigTlsCert::default()
		}.to_args(), is_match!(Err(..)));
}