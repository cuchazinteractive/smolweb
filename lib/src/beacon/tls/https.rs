
use anyhow::{Context, Result};
use axum::{async_trait, Router};
use axum::body::Body;
use axum::routing::IntoMakeService;
use display_error_chain::ErrorChainExt;
use hyper::server::conn::Http;
use tokio::net::TcpStream;
use tokio_rustls::server::TlsStream;
use tower::MakeService;
use tracing::error;

use crate::beacon::tls::server::{TlsServer, TlsServerHandler, TlsServerSpawner};
use crate::beacon::tls::TlsState;
use crate::net::listeners::TcpListeners;


/// an HTTP server based on axum that accepts TLS connections
pub struct HttpsServer {
	pub tls_server: TlsServer
}


impl HttpsServer {

	pub async fn start(tcp_listeners: TcpListeners, router: Router<(),Body>, tls_state: &TlsState) -> Result<Self> {

		let spawner = Spawner {
			make_service: router.into_make_service()
		};

		let handler = Handler {
			http: Http::new()
		};

		let tls_server = TlsServer::start(tcp_listeners, tls_state, spawner, handler)
			.await
			.context("Failed to start TLS server")?;

		Ok(Self {
			tls_server
		})
	}

	pub async fn shutdown(self) {
		self.tls_server.shutdown()
			.await;
	}
}


struct Spawner {
	make_service: IntoMakeService<Router<(),Body>>
}

#[async_trait]
impl TlsServerSpawner for Spawner {

	type Target = Router<(),Body>;

	async fn spawn(&mut self) -> Self::Target {
		self.make_service.make_service(())
			.await.unwrap() // infallibe
	}
}


struct Handler {
	http: Http
}

#[async_trait]
impl TlsServerHandler<Spawner> for Handler {

	async fn handle(&self, service: Router<(),Body>, stream: &mut TlsStream<TcpStream>) {

		// handle the HTTP request
		let result = self.http.serve_connection(stream, service)
			.await;
		if let Err(e) = result {
			let msg = e.into_chain().to_string();
			// hyper doesn't expose their error kinds in the API,
			// so the best we can do to filter errors is to pattern match on the message T_T
			if msg.starts_with("error shutting down connection:") {
				// the client closed the connection before we did, nothing interesting
			} else {
				error!(err = msg, "Failed to handle request");
			};
		}
	}
}
