
pub(crate) mod server;
pub(crate) mod https;

#[cfg(test)]
mod test;


use std::path::PathBuf;
use std::sync::{Arc, OnceLock};
use std::time::Duration;

use anyhow::{bail, Context};
use display_error_chain::ErrorChainExt;
use futures::StreamExt;
use serde::Deserialize;
use tokio::io::{AsyncRead, AsyncWrite};
use tokio_rustls::rustls::{Certificate, ConfigBuilder, PrivateKey, ServerConfig};
use tokio_rustls::rustls::server::{ResolvesServerCert, WantsServerCert};
use tokio_rustls::StartHandshake;
use tokio_rustls_acme::acme::{LETS_ENCRYPT_PRODUCTION_DIRECTORY, LETS_ENCRYPT_STAGING_DIRECTORY};
use tokio_rustls_acme::{AcmeAcceptor, AcmeConfig, ResolvesServerCertAcme};
use tracing::{debug, error, error_span, Instrument};

use crate::beacon::database::acme_cache::AcmeCache;
use crate::beacon::database::Database;
use crate::config::ConfigDuration;
use crate::tls::{ArgsTlsClient, read_pem_certs, read_pem_key, tls_config_client};


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigTls {
	pub acme: Option<ConfigTlsAcme>,
	pub cert: Option<ConfigTlsCert>
}

impl ConfigTls {

	pub fn to_args(self) -> anyhow::Result<ArgsTls> {
		match (self.acme, self.cert) {

			(Some(acme), None) => {
				let acme = acme.to_args()
					.context("Failed to read acme config.")?;
				Ok(ArgsTls::Acme(acme))
			}

			(None, Some(cert)) => {
				let cert = cert.to_args()
					.context("Failed to read cert config.")?;
				Ok(ArgsTls::Cert(cert))
			}

			_ => bail!("Exactly one of tls.acme or tls.cert is required.")
		}
	}
}

impl Default for ConfigTls {

	fn default() -> Self {
		Self {
			acme: None,
			cert: None
		}
	}
}


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigTlsAcme {
	pub domains: Option<Vec<String>>,
	pub emails: Option<Vec<String>>,
	pub directory: Option<String>,
	#[serde(deserialize_with = "ConfigDuration::deserialize")]
	#[serde(default = "ConfigDuration::deserialize_default")]
	pub timeout_cert: Option<ConfigDuration>
}

impl ConfigTlsAcme {

	pub fn to_args(self) -> anyhow::Result<ArgsTlsAcme> {
		Ok(ArgsTlsAcme {
			domains: self.domains
				.filter(|domains| !domains.is_empty())
				.context("At least one domain is required")?,
			emails: self.emails
				.filter(|emails| !emails.is_empty())
				.context("At least one email address is required")?,
			directory: self.directory
				.unwrap_or(ArgsTlsAcme::DEFAULT_DIRECTORY.to_string()),
			timeout_cert: self.timeout_cert
				.map(|t| t.to_duration())
				.unwrap_or(ArgsTlsAcme::DEFAULT_TIMEOUT_CERT)
		})
	}
}

impl Default for ConfigTlsAcme {

	fn default() -> Self {
		Self {
			domains: None,
			emails: None,
			directory: None,
			timeout_cert: None
		}
	}
}


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigTlsCert {
	pub cert: Option<String>,
	pub key: Option<String>
}

impl ConfigTlsCert {

	pub fn to_args(self) -> anyhow::Result<ArgsTlsCert> {
		let path_cert = self.cert
			.map(PathBuf::from)
			.context("The certificate path is required.")?;
		let path_key = self.key
			.map(PathBuf::from)
			.context("The private key path is required.")?;
		Ok(ArgsTlsCert {
			cert_chain: read_pem_certs(&path_cert)?,
			private_key: read_pem_key(&path_key)?,
		})
	}
}

impl Default for ConfigTlsCert {

	fn default() -> Self {
		Self {
			cert: None,
			key: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ArgsTls {
	Acme(ArgsTlsAcme),
	Cert(ArgsTlsCert)
}

impl ArgsTls {

	pub fn state(&self, database: Arc<Database>) -> TlsState {
		match self {
			ArgsTls::Acme(args) => args.state(database),
			ArgsTls::Cert(args) => args.state()
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsTlsAcme {
	pub domains: Vec<String>,
	pub emails: Vec<String>,
	pub directory: String,
	pub timeout_cert: Duration
}

impl ArgsTlsAcme {

	pub const DEFAULT_DIRECTORY: &'static str =
		if cfg!(test) {
			LETS_ENCRYPT_STAGING_DIRECTORY
		} else {
			LETS_ENCRYPT_PRODUCTION_DIRECTORY
		};

	pub const DEFAULT_TIMEOUT_CERT: Duration =
		if cfg!(test) {
			Duration::from_secs(2)
		} else {
			Duration::from_secs(15)
		};

	pub fn state(&self, database: Arc<Database>) -> TlsState {
		TlsState::Acme(TlsStateAcme::new(self.clone(), database))
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsTlsCert {
	pub cert_chain: Vec<Certificate>,
	pub private_key: PrivateKey
}

impl ArgsTlsCert {

	pub fn state(&self) -> TlsState {
		TlsState::Cert(TlsStateCert::new(self.clone()))
	}
}


pub enum TlsState {
	Acme(TlsStateAcme),
	Cert(TlsStateCert)
}

impl TlsState {

	#[cfg(test)]
	pub fn set_tls_port(&self, val: u16) {
		match self {
			Self::Acme(acme) => acme.set_tls_port(val),
			_ => ()
		}
	}

	pub async fn acme_checker(&self) -> Option<AcmeCertChecker> {
		match &self {
			Self::Acme(acme) => {
				acme.checker()
					.await
			},
			_ => None
		}
	}

	pub async fn config(&self) -> anyhow::Result<ServerConfig> {

		let builder = ServerConfig::builder()
			.with_safe_defaults()
			.with_no_client_auth();

		match &self {

			TlsState::Acme(state) => {
				let config = state.config(builder)
					.await
					.context("Failed to configure ACME client")?;
				Ok(config)
			}

			TlsState::Cert(state) => state.config(builder)
		}
	}

	#[cfg(test)]
	pub fn any_domain(&self) -> String {

		use x509_parser::prelude::{FromDer, GeneralName, X509Certificate};

		match &self {

			TlsState::Acme(acme) => {
				acme.args.domains.first()
					.unwrap()
					.clone()
			}

			TlsState::Cert(cert) => {
				let (_, cert) = X509Certificate::from_der(cert.args.cert_chain.last().unwrap().0.as_slice())
					.expect("bad cert");
				let alt_names = cert.subject_alternative_name()
					.unwrap()
					.expect("no cert alt names");
				let domain = alt_names.value.general_names.first()
					.expect("no cert first alt name");
				if let GeneralName::DNSName(name) = domain {
					name.to_string()
				} else {
					panic!("cert alt name not dns")
				}
			}
		}
	}
}


pub struct TlsStateAcme {
	pub args: ArgsTlsAcme,
	database: Arc<Database>,
	#[cfg(test)]
	tls_port: std::sync::Mutex<Option<u16>>,
	inner: OnceLock<Option<TlsStateAcmeInner>>
}

struct TlsStateAcmeInner {
	resolver: Arc<ResolvesServerCertAcme>,
	acceptor: Arc<AcmeAcceptor>
}

impl TlsStateAcme {

	pub fn new(args: ArgsTlsAcme, database: Arc<Database>) -> Self {
		Self {
			args,
			database,
			#[cfg(test)]
			tls_port: std::sync::Mutex::new(None),
			inner: OnceLock::new()
		}
	}

	#[cfg(test)]
	pub fn set_tls_port(&self, val: u16) {
		let mut tls_port = self.tls_port.lock()
			.unwrap();
		// only set it once
		if let None = *tls_port {
			*tls_port = Some(val);
		}
	}

	#[cfg(not(test))]
	fn directory(&self) -> &str {
		self.args.directory.as_str()
	}

	#[cfg(test)]
	fn directory(&self) -> String {
		// add the port number to the ACMEd calls, so ACMEd knows where to send challenge requests
		// by default, ACME spec says to use 443, but no one is listening to that port in tests
		let tls_port = self.tls_port.lock()
			.unwrap()
			.expect("In test builds, call set_tls_port() before using the ACME state");
		format!("{}/{}", self.args.directory.as_str(), tls_port)
	}

	async fn inner(&self) -> &Option<TlsStateAcmeInner> {
		self.inner.get_or_init(|| {

			// add `mailto:` to all the email addresses
			let contacts = self.args.emails.iter()
				.map(|email| format!("mailto:{}", email))
				.collect::<Vec<_>>();

			// configure the ACME client
			let mut acme_config = AcmeConfig::new(&self.args.domains)
				.contact(contacts)
				.directory(self.directory())
				.cache(AcmeCache::new(self.database.clone()));

			if cfg!(test) {
				// add our root cert to the cert store for the ACME client
				let tls_config = tls_config_client(&ArgsTlsClient::default())
					.expect("test config is invalid");
				acme_config = acme_config.client_tls_config(Arc::new(tls_config));
			}

			// initialize ACME state
			let mut state = acme_config.state();
			let resolver = state.resolver();
			let acceptor = Arc::new(state.acceptor());

			// move the ACME state into a task and poll it to provision the certificate, when it's needed
			tokio::spawn(async move {
				loop {
					let event = state.next()
						.await;
					match event {
						None => break,
						Some(Ok(event)) => debug!("event: {:?}", event),
						Some(Err(e)) => error!(err = %e.into_chain(), "Error event"),
					}
				}
			}.instrument(error_span!("TlsStateAcme")));

			Some(TlsStateAcmeInner {
				resolver,
				acceptor
			})
		})
	}

	pub async fn config(&self, builder: ConfigBuilder<ServerConfig,WantsServerCert>) -> Option<ServerConfig> {
		self.inner()
			.await
			.as_ref()
			.map(|i| builder.with_cert_resolver(i.resolver.clone()))
	}

	pub async fn checker(&self) -> Option<AcmeCertChecker> {
		self.inner()
			.await
			.as_ref()
			.map(|i| {
				AcmeCertChecker {
					timeout: self.args.timeout_cert,
					acceptor: i.acceptor.clone()
				}
			})
	}
}


#[derive(Clone)]
pub struct AcmeCertChecker {
	timeout: Duration,
	pub acceptor: Arc<AcmeAcceptor>
}

impl AcmeCertChecker {

	pub async fn has_cert<IO>(&self, resolver: &dyn ResolvesServerCert, tls_handshake: &StartHandshake<IO>) -> bool
	where
		IO: AsyncRead + AsyncWrite + Unpin
	{

		let mut waited = Duration::ZERO;

		loop {

			// see if we have a cert yet
			let has_cert = resolver.resolve(tls_handshake.client_hello()).is_some();
			if has_cert {

				// yeah, we're good
				return true;

			} else if waited > self.timeout {

				// nope
				return false;

			} else {

				// dunno yet, wait a bit and check again
				let wait = Duration::from_millis(200);
				tokio::time::sleep(wait)
					.await;
				waited += wait;
			}
		}
	}
}


pub struct TlsStateCert {
	pub args: ArgsTlsCert
}

impl TlsStateCert {

	pub fn new(args: ArgsTlsCert) -> Self {
		Self {
			args
		}
	}

	pub fn config(&self, builder: ConfigBuilder<ServerConfig,WantsServerCert>) -> anyhow::Result<ServerConfig> {
		builder.with_single_cert(self.args.cert_chain.clone(), self.args.private_key.clone())
			.context("Failed to configure TLS server with given certificate")
	}
}
