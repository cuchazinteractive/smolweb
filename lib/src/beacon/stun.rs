
use std::net::SocketAddr;

use anyhow::Result;
use bytecodec::{DecodeExt, EncodeExt};
use display_error_chain::ErrorChainExt;
use serde::Deserialize;
use stun_codec::{rfc5389, MessageDecoder, MessageEncoder, Message, MessageClass};
use stun_codec::rfc5389::Attribute;
use tokio::sync::mpsc;
use tokio::task::JoinHandle;
use tracing::{debug, error, info, Instrument};

use crate::net::{BindMode, ConfigAddr};
use crate::net::listeners::UdpListeners;


pub const DEFAULT_PORT: u16 = 3478;


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigStun {
	pub bind_mode: Option<BindMode>,
	#[serde(deserialize_with = "ConfigAddr::deserialize_vec")]
	#[serde(default = "ConfigAddr::deserialize_vec_default")]
	pub addrs: Option<Vec<ConfigAddr>>
}

impl ConfigStun {

	pub fn to_args(self) -> Result<ArgsStun> {
		let default = ArgsStun::default();
		Ok(ArgsStun {
			bind_mode: self.bind_mode.unwrap_or(default.bind_mode),
			addrs: self.addrs.unwrap_or(default.addrs)
		})
	}
}

impl Default for ConfigStun {

	fn default() -> Self {
		Self {
			bind_mode: None,
			addrs: None
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsStun {
	pub bind_mode: BindMode,
	pub addrs: Vec<ConfigAddr>
}

impl Default for ArgsStun {

	fn default() -> Self {

		let args = Self {
			bind_mode: BindMode::Both,
			addrs: ConfigAddr::all_interfaces()
		};

		#[cfg(test)]
			let args = Self {
			addrs: vec![ConfigAddr::localhost_auto_port()],
			.. args
		};

		args
	}
}


/// A STUN server that listens to the address using UDP.
///
/// Tries to follow RFC 5389.
///
/// https://www.rfc-editor.org/rfc/rfc5389
pub struct StunServer {
	addrs: Vec<SocketAddr>,
	shutdown_tx: mpsc::Sender<()>,
	join_handle: JoinHandle<()>
}


impl StunServer {

	/// starts the server listening on the address using UDP
	#[tracing::instrument(skip_all, level = 5, name = "STUN")]
	pub async fn start_udp(args: ArgsStun) -> Result<Self> {

		// NOTE: the RFC says message sizes over UDP should be smaller than the MTU, or 576 bytes, whichever is smaller
		const BUF_SIZE: usize = 1024;

		// start a UDP server
		let listeners = UdpListeners::bind_all(&args.addrs, DEFAULT_PORT, args.bind_mode, BUF_SIZE)
			.await?;
		let addrs = listeners.addrs().clone();
		info!(?addrs, "Listening");

		let (shutdown_tx, mut shutdown_rx) = mpsc::channel::<()>(1);

		let join_handle = tokio::spawn(async move {

			// listen
			loop {

				// wait for the next thing to happen
				let (datagram, client_address, socket) = tokio::select! {

					// got a datagram from the UDP socket
					// NOTE: recv_from() is cancel safe, so no incoming datagrams will be missed by dropping/canceling futures
					result = listeners.recv_from() => {
						match result {
							Some(Ok(a)) => a,
							Some(Err(e)) => {
								error!(err = %e.into_chain(), "Failed to receive UDP message");
								continue;
							}
							None => continue
						}
					},

					// got shutdown signal
					_ = shutdown_rx.recv() => {
						debug!("received shutdown signal");
						break;
					}
				};

				// handle each request concurrently
				tokio::spawn(async move {

					// decode the message
					let message = MessageDecoder::<Attribute>::new()
						.decode_from_bytes(datagram.as_slice());
					let message = match message {
						Ok(Ok(m)) => m,
						_ => return // the RFC says to just ignore invalid requests
					};

					// handle the message
					let response = match (message.class(), message.method()) {

						// STUN really only defines one message type for the server
						(MessageClass::Request, rfc5389::methods::BINDING) => {

							// respond with the client's network address, as the server sees it
							let mut response = Message::<Attribute>::new(
								MessageClass::SuccessResponse,
								rfc5389::methods::BINDING,
								message.transaction_id()
							);
							response.add_attribute(rfc5389::attributes::XorMappedAddress::new(client_address));
							response
						}

						_ => return // the RFC says to just ignore invalid requests
					};

					// send the response message
					let bytes = MessageEncoder::new()
						.encode_into_bytes(response);
					let bytes = match bytes {
						Ok(b) => b,
						Err(e) => {
							error!(err = %e.into_chain(), "Failed to encode response message");
							return;
						}
					};
					let result = socket.send_to(&bytes, client_address)
						.await;
					if let Err(e) = result {
						error!(err = %e.into_chain(), "Failed to send response");
					}
					// send failed, not much we can do about it, just wait for the client to retry
				});
			}

			info!("finished");
		}.in_current_span());

		Ok(Self {
			addrs,
			shutdown_tx,
			join_handle
		})
	}

	#[allow(unused)]
	pub fn addrs(&self) -> &Vec<SocketAddr> {
		&self.addrs
	}

	/// sends a shutdown signal to the server and waits for it to stop
	#[tracing::instrument(skip_all, level = 5, name = "STUN")]
	pub async fn shutdown(self) {
		if !self.shutdown_tx.is_closed() {
			if let Err(e) = self.shutdown_tx.send(()).await {
				error!(err = %e.into_chain(), "Failed to send shutdown signal");
			} else if let Err(e) = self.join_handle.await {
				error!(err = %e.into_chain(), "Failed to wait for server to shutdown");
			}
		}
	}
}


#[cfg(test)]
mod test {

	use galvanic_assert::{assert_that, matchers::*};
	use stun_codec::TransactionId;
	use tokio::net::UdpSocket;

	use smolweb_test_tools::logging::init_test_logging;

	use crate::config::test::deserialize_from_toml;
	use crate::net::{ConnectMode, FilterAddrs, matching_localhost_addr};

	use super::*;


	#[test]
	fn config() {
		let _logging = init_test_logging();

		let config = deserialize_from_toml::<ConfigStun>(r#"
		"#).unwrap();
		assert_that!(&config, eq(ConfigStun::default()));

		let config = deserialize_from_toml::<ConfigStun>(r#"
			bind_mode = "both"
		"#).unwrap();
		assert_that!(&config, eq(ConfigStun {
			bind_mode: Some(BindMode::Both),
			.. ConfigStun::default()
		}));

		let config = deserialize_from_toml::<ConfigStun>(r#"
			addrs = "foo"
		"#).unwrap();
		assert_that!(&config, eq(ConfigStun {
			addrs: Some(vec![ConfigAddr::new("foo", None)]),
			.. ConfigStun::default()
		}));

		let config = deserialize_from_toml::<ConfigStun>(r#"
			addrs = ["foo:5"]
		"#).unwrap();
		assert_that!(&config, eq(ConfigStun {
			addrs: Some(vec![ConfigAddr::new("foo", Some(5))]),
			.. ConfigStun::default()
		}));
	}


	#[test]
	fn to_args() {
		let _logging = init_test_logging();

		assert_that!(&ConfigStun::default().to_args().unwrap(), eq(ArgsStun::default()));
	}


	#[tokio::test]
	async fn test() {
		let _logging = init_test_logging();

		// start the STUN server
		let args = ArgsStun::default();
		let server = StunServer::start_udp(args.clone())
			.await.unwrap();

		// check IPv4 and IPv6
		request_response(server.addrs()
			.filter_addrs_for_connect(ConnectMode::OnlyIpv4)
			.first()
			.unwrap()
		).await;
		request_response(server.addrs()
			.filter_addrs_for_connect(ConnectMode::OnlyIpv6)
			.first()
			.unwrap()
		).await;

		// cleanup
		server.shutdown()
			.await;
	}

	async fn request_response(addr: &SocketAddr) {

		// send a STUN request
		let socket = UdpSocket::bind(matching_localhost_addr(addr))
			.await.unwrap();
		let mut request = Message::<Attribute>::new(
			MessageClass::Request,
			rfc5389::methods::BINDING,
			TransactionId::new([5u8; 12])
		);
		let attr = rfc5389::attributes::Software::new("test".to_string())
			.unwrap();
		request.add_attribute(attr);
		let msg = MessageEncoder::new()
			.encode_into_bytes(request)
			.unwrap();
		socket.send_to(&msg, addr)
			.await.unwrap();

		// wait for the response
		let mut buf = [0; 1024];
		let (response_size, _) = socket.recv_from(&mut buf)
			.await.unwrap();
		let response = MessageDecoder::<Attribute>::new()
			.decode_from_bytes(&buf[..response_size])
			.unwrap()
			.unwrap();

		// check it
		assert_eq!(response.class(), MessageClass::SuccessResponse);
		assert_eq!(response.method(), rfc5389::methods::BINDING);
		let attr = response.get_attribute::<rfc5389::attributes::XorMappedAddress>()
			.unwrap();
		assert_eq!(attr.address(), socket.local_addr().unwrap());
	}
}

