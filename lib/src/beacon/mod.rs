
pub(crate) mod stun;
pub(crate) mod guests;
pub(crate) mod hosts;
pub(crate) mod https;
pub(crate) mod database;
pub(crate) mod tls;
mod bridge;
mod admin;
mod authn;
mod messages;

#[cfg(test)]
mod test;

pub use admin::client::{ArgsAdminClient, AdminClient};


use std::collections::{HashMap, HashSet};
use std::{fmt, fs};
use std::sync::Arc;
use std::time::Instant;

use anyhow::{anyhow, Context, Result};
use axum::async_trait;
use base64::Engine;
use base64::engine::general_purpose::{URL_SAFE_NO_PAD as BASE64};
use burgerid::{IdentityApp, IdentityPersonal, ProtobufOpen};
use thiserror::Error;
use serde::Deserialize;
use tokio::sync::{RwLock, oneshot};
use tracing::{error, info};

use crate::beacon::admin::server::AdminState;
use crate::beacon::authn::AuthnSessions;
use crate::beacon::bridge::BridgeGuest;
use crate::beacon::database::{ArgsDatabase, ConfigDatabase, Database};
use crate::beacon::hosts::{HostsServer, ArgsHosts, ConfigHosts};
use crate::beacon::stun::{ArgsStun, ConfigStun, StunServer};
use crate::beacon::tls::{ArgsTls, ConfigTls};
use crate::beacon::guests::{ArgsGuests, ConfigGuests};
use crate::beacon::https::{ArgsBeaconHttpsServer, BeaconHttpsServer, ConfigBeaconHttpsServer};
use crate::config::ToArgs;
use crate::lang::ErrorReporting;
use crate::protobuf::HostUid;


pub const LOGIN_CONTEXT: &[u8; 20] = b"smolweb/beacon/login";
pub const KEYCHAIN_CONTEXT: &[u8; 23] = b"smolweb/beacon/keychain";


#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct ConfigBeacon {
	pub log: Option<String>,
	pub owner_id: Option<String>,
	pub database: Option<ConfigDatabase>,
	pub stun: Option<ConfigStun>,
	pub https: Option<ConfigBeaconHttpsServer>,
	pub guests: Option<ConfigGuests>,
	pub hosts: Option<ConfigHosts>,
	pub tls: Option<ConfigTls>
}

impl Default for ConfigBeacon {

	fn default() -> Self {
		Self {
			log: None,
			owner_id: None,
			database: None,
			stun: None,
			https: None,
			guests: None,
			hosts: None,
			tls: None
		}
	}
}

impl ToArgs for ConfigBeacon {

	type Args = ArgsBeacon;

	fn to_args(self) -> Result<ArgsBeacon> {

		let default_top = ArgsBeaconDefaults::default();

		// read the owner identity
		let owner_id_path = self.owner_id
			.context("owner_uid is required")?;
		let owner_id_buf = fs::read_to_string(&owner_id_path)
			.context(format!("Failed to read owner identity file: {}", &owner_id_path))?;
		let owner_id = IdentityPersonal::open(owner_id_buf)
			.context("Failed to open owner identity")?;

		Ok(ArgsBeacon {
			log: self.log.unwrap_or(default_top.log),
			owner_id,
			database: match self.database {
				Some(d) => d.to_args()
					.context("Error reading [database] config section.")?,
				None => ArgsDatabase::default()
			},
			stun: match self.stun {
				Some(c) => c.to_args()
					.context("Error reading [stun] config section.")?,
				None => ArgsStun::default()
			},
			https: match self.https {
				Some(h) => h.to_args()
					.context("Error reading [https] config section.")?,
				None => ArgsBeaconHttpsServer::default()
			},
			guests: match self.guests {
				Some(c) => c.to_args()
					.context("Error reading [guests] config section.")?,
				None => ArgsGuests::default()
			},
			hosts: match self.hosts {
				Some(h) => h.to_args()
					.context("Error reading [hosts] config section.")?,
				None => ArgsHosts::default()
			},
			tls: self.tls
				.context("[tls] config section is required.")?
				.to_args()
				.context("Error reading [tls] config section.")?
		})
	}
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsBeacon {
	pub log: String,
	pub owner_id: IdentityPersonal,
	pub database: ArgsDatabase,
	pub stun: ArgsStun,
	pub https: ArgsBeaconHttpsServer,
	pub guests: ArgsGuests,
	pub hosts: ArgsHosts,
	pub tls: ArgsTls
}


/// default values for top-level beacon arguments
struct ArgsBeaconDefaults {
	log: String
}

impl Default for ArgsBeaconDefaults {

	fn default() -> Self {

		let args = Self {
			log: "smolweb=info".to_string()
		};

		#[cfg(test)]
		let args = Self {
			log: "smolweb=trace".to_string(),
			.. args
		};

		args
	}
}


pub struct BeaconServer {
	stun_server: StunServer,
	https_server: BeaconHttpsServer,
	hosts_server: HostsServer
}

impl BeaconServer {


	#[tracing::instrument(skip_all, level = 5, name = "Beacon")]
	pub async fn start(args: ArgsBeacon) -> Result<Self> {

		info!("starting ...");

		// start the STUN server
		let stun_server = StunServer::start_udp(args.stun)
			.await.context("Failed to start STUN server")?;

		// start the database
		let database = Database::new(args.database)
			.await?;
		let database = Arc::new(database);

		// start the beacon state
		let beacon = Beacon::new(args.owner_id.uid().clone(), &args.hosts, database.clone());

		// put the latest owner keychain in the authn db
		beacon.admin.authn_sessions.admin().override_keychain(database.as_ref(), &args.owner_id.wrap())
			.await
			.context("Failed to set owner identity keychain for authorization")?;

		// make the beacon into a share toy =)
		let beacon = Arc::new(RwLock::new(beacon));

		// init TLS
		let tls_state = args.tls.state(database.clone());

		// start the HTTPs server
		let https_server = BeaconHttpsServer::start(
			args.https.clone(),
			Some(args.guests.clone()),
			beacon.clone(),
			database.clone(),
			&tls_state
		)
			.await.context("Failed to start beacon HTTPs server")?;

		// start the hosts server
		let hosts_server = HostsServer::start(args.hosts, beacon.clone(), database.clone(), &tls_state)
			.await.context("failed to start Hosts server")?;

		info!("started!");

		Ok(Self {
			stun_server,
			https_server,
			hosts_server
		})
	}

	#[tracing::instrument(skip_all, level = 5, name = "Beacon")]
	pub async fn shutdown(self) {

		info!("shutting down ...");

		// cleanup (cuncurrently)
		tokio::join!(
			self.stun_server.shutdown(),
			self.https_server.shutdown(),
			self.hosts_server.shutdown()
		);

		info!("finished");
	}
}



/// central state for the beacon server
pub(crate) struct Beacon {
	pub admin: AdminState,
	pub hosts: Hosts
}

impl Beacon {

	pub fn new(owner_uid: Vec<u8>, args_hosts: &ArgsHosts, database: Arc<Database>) -> Self {
		Self {
			admin: AdminState::new(owner_uid, database.clone()),
			hosts: Hosts::new(args_hosts, database)
		}
	}
}


#[async_trait]
pub(crate) trait BeaconSync {
	async fn host_bridge(&self, host_uid: &HostUid) -> Option<BridgeGuest>;
}

#[async_trait]
impl BeaconSync for Arc<RwLock<Beacon>> {

	async fn host_bridge(&self, host_uid: &HostUid) -> Option<BridgeGuest> {
		let beacon = self.read()
			.await;
		beacon.hosts.get(host_uid)
			.map(|h| h.bridge.clone())
	}
}


pub(crate) struct Hosts {
	database: Arc<Database>,
	pub authn_sessions: AuthnSessions,
	connection_ids: HashSet<ConnectionId>,
	hosts: HashMap<HostUid,Host>
}

impl Hosts {

	fn new(args: &ArgsHosts, database: Arc<Database>) -> Self {
		Self {
			database,
			authn_sessions: AuthnSessions::new(Some(&args.authn), false),
			connection_ids: HashSet::new(),
			hosts: HashMap::new()
		}
	}

	fn connect(&mut self) -> ConnectionId {

		// find a new connection id
		let id = loop {
			let id = rand::random::<ConnectionId>().abs();
			if !self.connection_ids.contains(&id) {
				break id;
			}
		};

		// track the connection
		self.connection_ids.insert(id);

		id
	}

	fn disconnect(&mut self, id: ConnectionId) {
		self.connection_ids.remove(&id);
	}

	async fn add(&mut self, host: Host) -> Result<(),AddHostError> {

		if self.hosts.contains_key(host.app_id.uid()) {
			return Err(AddHostError::AlreadyAdded);
		}

		self.database.hosts()
			.connected(host.app_id.uid(), host.app_id.name())
			.await
			.log_err()
			.ok();

		self.hosts.insert(host.app_id.uid().clone(), host);

		Ok(())
	}

	fn get(&self, host_id: &HostUid) -> Option<&Host> {
		self.hosts.get(host_id)
	}

	fn get_mut(&mut self, host_id: &HostUid) -> Option<&mut Host> {
		self.hosts.get_mut(host_id)
	}

	fn remove(&mut self, host_uid: &HostUid) {
		self.hosts.remove(host_uid);
	}
}


#[derive(Error, Debug)]
enum AddHostError {

	#[error("This host has already been added")]
	AlreadyAdded
}


type ConnectionId = i64;


struct Host {
	app_id: IdentityApp,
	conn_id: ConnectionId,
	bridge: BridgeGuest,
	disconnect_tx: Option<oneshot::Sender<()>>,
	last_contact: Instant
}

impl Host {

	fn disconnect(&mut self) {
		if let Some(tx) = self.disconnect_tx.take() {
			tx.send(())
				.map_err(|_| anyhow!("Failed to send disconnect signal to host"))
				.warn_err()
				.ok();
		}
	}

	fn update_last_contact(&mut self) {
		self.last_contact = Instant::now();
	}
}

impl fmt::Debug for Host {

	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "Host[uid={}, conn={}]", BASE64.encode(&self.app_id.uid()), self.conn_id)
	}
}
