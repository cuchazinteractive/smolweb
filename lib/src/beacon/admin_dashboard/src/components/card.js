

import { html, text, remove } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';


export class Card extends HTMLElement {

	static tagName = 'admin-card';

	static stylesheet = `
		
		display: inline-block;
		background-color: var(--colors-bg-lightest);
		border: solid 1px var(--colors-bg-lessLight);
		padding-top: 20px;
		padding-bottom: 20px;
		padding-left: 20px;
		padding-right: 20px;
		color: var(--colors-text-darkest);
	
		header {
			display: flex;
			flex-direction: row;
			gap: 20px;
			padding-bottom: 20px;
			
			.buttons {
				display: flex;
				flex-direction: row;
			}
			
			button,
			.buttons button {
				background-color: transparent;
				border: none;
				padding: 0;
				cursor: pointer;
			}
		}
		
		.title {
			flex-grow: 1;
			color: var(--colors-text-darker);
			font-size: 110%;
			font-weight: normal;
		}
	`;

	content = webComponents.content();
	slots = webComponents.slots('title', 'buttons');

	#renderer = new Renderer(this, () => {
		return [
			html('header', {
				_children: [
					html('span', {
						class: 'title',
						_children: this.slots['title']
					}),
					html('div', {
						class: 'buttons',
						_children: [
							... this.slots['buttons'],
							this.#closeRenderer.elem
						]
					})
				]
			}),
			... this.content
		];
	});

	#closeRenderer = new Renderer(html('span'), () => {
		const closable = this.getAttribute('closable') != null;
		if (closable) {
			return html('button', {
				_children: [
					text('X')
				],
				_events: {
					click: () => this.close()
				}
			});
		} else {
			return [];
		}
	});

	/** @type {?function()} */
	onclose = null;


	init() {
		this.#renderer.render();
		this.#closeRenderer.render();
	}

	connectedCallback() {
		webComponents.connected(this);
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}

	close() {
		remove(this.parentElement, this);
		this.onclose?.();
	}
}

webComponents.register(Card);
