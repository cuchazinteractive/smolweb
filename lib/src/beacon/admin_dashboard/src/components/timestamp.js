
import { DateTime } from 'luxon';

import { text } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';


export class Timestamp extends HTMLElement {

	static tagName = 'admin-timestamp';

	static stylesheet = `
	
		cursor: help;
	`;


	#renderer = new Renderer(this, () => {

		const dt = DateTime.fromSeconds(Number(this.timestamp));

		// set the title to be an absolute human-readable datetime
		this.title = dt.toLocaleString(DateTime.DATETIME_MED);

		// set the text to be a relative datetime, but only if it's recent
		if (dt.diffNow().shiftTo('days').days >= -7) {
			return text(dt.toRelative());
		} else {
			return text(dt.toLocaleString(DateTime.DATE_MED));
		}
	});

	/** @type {?(number | bigint)} */
	timestamp;


	connectedCallback() {
		webComponents.connected(this);

		this.#renderer.render();
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}
}

webComponents.register(Timestamp);
