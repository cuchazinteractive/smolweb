
import { svg } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';

import * as nav from 'smolweb-beacon-admin/src/lib/nav.js';


export class Icon extends HTMLElement {

	static tagName = 'admin-icon';

	static stylesheet = `
	
		--icon-size: 1lh;
		--icon-color: black;
		--vdrop: 0.15em; /* looks good when the icon is aligned to the text baseline */
		
		display: inline-block;
		width: var(--icon-size);
		height: var(--icon-size);
		
		position: relative;
		top: var(--vdrop);
	
		svg {
			fill: var(--icon-color);
			width: 100%;
			height: 100%;
		}
	`;


	#renderer = new Renderer(this, () => {
		return svg('svg', {
			_children: [
				svg('use', {
					href: `${nav.url('/images/icons.svg')}#${this.name}`
				})
			]
		});
	});


	init() {
		this.#renderer.render();
	}

	#name = webComponents.attrStr(this, 'name');
	get name() { return this.#name.get(); }
	set name(v) { this.#name.set(v); }

	connectedCallback() {
		webComponents.connected(this);
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}

	attributeChangedCallback(name, _oldVal, _newVal) {
		switch (name) {
			case this.#name.name:
				this.#renderer.render();
		}
	}
}

webComponents.register(Icon);
