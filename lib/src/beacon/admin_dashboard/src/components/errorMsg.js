
import * as errors from 'food-runner/src/errors.js';

import { html, text, comp } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';


import { Icon } from 'smolweb-beacon-admin/src/components/icon.js';
import { ICONS } from 'smolweb-beacon-admin/src/gen/icons.js';


export class ErrorMsg extends HTMLElement {

	static tagName = 'admin-error-msg';

	static stylesheet = `
		
		display: flex;
		flex-direction: row;
		gap: 8px;
		align-items: baseline;
		color: var(--colors-text-error);
		
		admin-icon {
			--icon-color: var(--colors-text-error);
		}
	`;


	#renderer = new Renderer(this, () => {
		return [
			comp(Icon, { name: ICONS.error.solid }),
			html('div', {
				_children: [
					text(errors.format(this.err))
				]
			})
		];
	});

	/** @type {?Error} */
	err = null;


	connectedCallback() {
		webComponents.connected(this);
		this.#renderer.render();
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}
}

webComponents.register(ErrorMsg);
