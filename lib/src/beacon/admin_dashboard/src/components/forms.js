
import { iterSync } from 'food-runner/src/iter.js';

import { html, text, refill, comp } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';

import { BusyButton } from 'smolweb-beacon-admin/src/components/busyButton.js';
import { Messages } from 'smolweb-beacon-admin/src/components/messages.js';


/**
 * @template V
 * @callback FormFieldGetter
 * @returns {V}
 */

/**
 * @template V
 * @callback FormFieldSetter
 * @param {V} value
 */

/**
 * @template T,V
 * @typedef {Object} FormFieldOptions
 * @property {string} name
 * @property {string} label
 * @property {?string} [description]
 * @property {boolean} [focused]
 * @property {?function} [onenter]
 * @property {?function} [onescape]
 */

/**
 * @template V,C
 * @callback FormFieldGetAdapter
 * @param {V} value
 * @returns {C}
 */

/**
 * @template V,C
 * @callback FormFieldSetAdapter
 * @param {C} value
 * @returns {V}
 */


export class FormField {

	static #nextId = 1;

	/** @returns {string} */
	static #makeId() {
		const id = `admin-form-field-${this.#nextId}`;
		this.#nextId += 1;
		return id;
	}

	/**
	 * @param {FormFieldOptions} options
	 * @returns {FormField.<HTMLInputElement,?string>}
	 */
	static text(options) {

		const elem = /** @type {HTMLInputElement} */ html('input', {
			id: this.#makeId(),
			part: options.name
		});

		const getter = () => {
			if (elem.value.length > 0) {
				return elem.value;
			} else {
				return null;
			}
		};

		const setter = /**?string*/ value => {
			elem.value = value ?? '';
		};

		return new this(options, elem, getter, setter);
	}

	/**
	 * @param {FormFieldOptions} options
	 * @returns {FormField.<HTMLInputElement,?string>}
	 */
	static password(options) {

		const elem = /** @type {HTMLInputElement} */ html('input', {
			id: this.#makeId(),
			part: options.name,
			type: 'password'
		});

		const getter = () => {
			if (elem.value.length > 0) {
				return elem.value;
			} else {
				return null;
			}
		};

		const setter = /**?string*/ value => {
			elem.value = value ?? '';
		};

		return new this(options, elem, getter, setter);
	}

	/**
	 * @param {FormFieldOptions} options
	 * @returns {FormField.<HTMLInputElement,?File>}
	 */
	static file(options) {

		const elem = /** @type {HTMLInputElement} */ html('input', {
			id: this.#makeId(),
			part: options.name,
			type: 'file'
		});

		const getter = () => {
			if (elem.files.length > 0) {
				return elem.files[0];
			} else {
				return null;
			}
		};

		const setter = () => {
			// nothing to do
		};

		return new this(options, elem, getter, setter);
	}

	/**
	 * @param {[string,string][]} values
	 * @param {FormFieldOptions} options
	 * @returns {FormField.<HTMLInputElement,?string>}
	 */
	static select(values, options) {

		const elem = /** @type {HTMLSelectElement} */ html('select', {
			id: this.#makeId(),
			part: options.name,
			_children: iterSync(values)
				.map(([id,label]) => html('option', {
					value: id,
					_children: [
						text(label)
					]
				}))
				.toArray()
		});

		const getter = () => {
			if (elem.value.length > 0) {
				return elem.value;
			} else {
				return null;
			}
		};

		const setter = /**?string*/ value => {
			elem.value = value ?? '';
		};

		return new this(options, elem, getter, setter);
	}


	/** @type {FormFieldOptions} */
	options;

	/** @type {T} */
	#elem;

	/** @type {FormFieldGetter.<V>} */
	#getter;

	/** @type {FormFieldSetter.<V>} */
	#setter;

	/** @type {Messages} */
	#messages = new Messages();

	/** @type {HTMLLabelElement} */
	label;

	/** @type {HTMLDivElement} */
	description;


	/**
	 * @template T,V
	 * @param {FormFieldOptions} options
	 * @param {T} elem
	 * @param {FormFieldGetter.<V>} getter
	 * @param {FormFieldSetter.<V>} setter
	 */
	constructor(options, elem, getter, setter) {

		this.options = options;
		this.#elem = elem;
		this.#getter = getter;
		this.#setter = setter;

		this.label = html('label', {
			for: elem.id,
			_children: [
				text(options.label)
			]
		});

		this.description = html('div', {
			class: 'description',
			_children: [
				text(options.description ?? "")
			]
		});

		// wire up field events
		elem.addEventListener('keydown', event => {
			if (!event.isComposing && event.key === 'Enter') {
				options.onenter?.();
			}
		});
		elem.addEventListener('keydown', (event) => {
			if (!event.isComposing && event.key === 'Escape') {
				options.onescape?.();
			}
		});
	}


	/** @returns {T} */
	get elem() {
		return this.#elem;
	}

	/** @returns {Messages} */
	get messages() {
		return this.#messages;
	}

	/** @returns {V} */
	get value() {
		return this.#getter();
	}

	/** @param {V} v */
	set value(v) {
		this.#setter(v);
	}

	/**
	 * @template C
	 * @param {FormFieldGetAdapter.<V,C>} getAdapter
	 * @param {FormFieldSetAdapter.<V,C>} setAdapter
	 * @returns {FormField.<C>}
	 */
	adapted(getAdapter, setAdapter) {
		return new this.constructor(
			this.options,
			this.#elem,
			() => getAdapter(this.#getter()),
			value => this.#setter(setAdapter(value))
		);
	}
}


/**
 * @callback FormProcessor
 * @returns {void | Promise.<void>}
 */


export class Form extends HTMLElement {

	static tagName = 'admin-form';

	static stylesheet = `
	
		div.container:not(.inline) {
				
			.fields {
				display: grid;
				grid-template-columns: max-content max-content;
	
				label {
					margin-right: 20px;
					margin-top: 4px;
					text-align: right;
				}
				
				.field {
					display: flex;
					flex-direction: column;
					align-items: flex-start;
					margin-bottom: 10px;
		
					input {
						font-size: 1rem;
					}
					
					.description {
						margin-top: 4px;
						color: var(--colors-text-darker);
						max-width: 400px;
					}
				}
			}
		
			.buttons {
				display: flex;
				flex-direction: row;
				justify-content: flex-end;
				gap: 20px;
				margin-top: 10px;
			}
			
			admin-messages {
				color: var(--colors-text-error);
				padding-top: 4px;
			}
		}
		
		div.container.inline {
		
			display: grid;
			grid-template-columns: max-content max-content;
			grid-template-rows: max-content max-content;
			grid-template-areas:
				'fields buttons'
				'messages messages';
			gap: 8px;
			
			.fields {
				grid-area: fields;
				display: flex;
				flex-direction: row;
				align-items: baseline;
				gap: 8px;
			}
			
			.buttons {
				grid-area: buttons;
				display: flex;
				flex-direction: row;
				align-items: baseline;
				gap: 8px;
			}
			
			admin-messages {
				grid-area: messages;
				color: var(--colors-text-error);
			}
		}
	`;


	/** @type {FormField[]} */
	fields = [];

	#fieldsRenderer = new Renderer(html('div', { class: 'fields' }), () => {

		const rendered = [];
		for (const field of this.fields) {
			rendered.push(field.label);
			rendered.push(html('div', {
				class: 'field',
				_children: [
					field.elem,
					field.messages,
					field.description
				]
			}));

			// handle focus, after the element lands in the DOM
			if (field.options.focused) {
				window.setTimeout(() => {
					field.elem.focus();
				}, 0);
			}
		}

		return rendered;
	});

	#messages = new Messages();

	#submitButton = comp(BusyButton, {
		idleText: "Submit",
		busyText: "Submitting ...",
		_let: b => {
			b.onaction = async () => {

				// clear messages
				for (const field of this.fields) {
					field.messages.clear();
				}
				this.#messages.clear();

				// run the processor
				await this.processor();
			};
		}
	});

	#container = html('div', {
		class: 'container',
		_children: [
			this.#fieldsRenderer.elem,
			html('div', {
				class: 'buttons',
				_children: [
					this.#submitButton
				]
			}),
			this.#messages
		]
	});

	// noinspection JSUnusedGlobalSymbols (used in redom params)
	/** @returns {string} */
	get submitIdleText() { return this.#submitButton.idleText; }
	// noinspection JSUnusedGlobalSymbols (used in redom params)
	/** @param {string} value */
	set submitIdleText(value) { this.#submitButton.idleText = value; }

	// noinspection JSUnusedGlobalSymbols (used in redom params)
	/** @returns {string} */
	get submitBusyText() { return this.#submitButton.busyText; }
	// noinspection JSUnusedGlobalSymbols (used in redom params)
	/** @param {string} value */
	set submitBusyText(value) { this.#submitButton.busyText = value; }

	/** @type {boolean} */
	inline = false;

	/** @type {FormProcessor} */
	processor;


	init() {
		refill(this, this.#container);
	}

	connectedCallback() {
		webComponents.connected(this);

		this.#container.classList.toggle('inline', this.inline);
		this.#submitButton.className = this.inline ? 'btn-small' : 'btn';
		this.#fieldsRenderer.render();
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}

	/** @returns {Messages} */
	get messages() {
		return this.#messages;
	}

	submit() {
		this.#submitButton.click();
	}

	/**
	 * Returns true if the form, or any fields, have messages.
	 * @returns {boolean}
	 */
	get hasMessages() {
		for (const field of this.fields) {
			if (!field.messages.isEmpty()) {
				return true;
			}
		}
		return !this.#messages.isEmpty();
	}
}

webComponents.register(Form);
