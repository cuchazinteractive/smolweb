
import { text } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';


export class BusyButton extends HTMLButtonElement {

	static tagName = 'admin-busy-button';
	static tagExtends = 'button';


	#idleText = webComponents.attrStr(this, 'idleText');
	get idleText() { return this.#idleText.get(); }
	set idleText(v) { this.#idleText.set(v); }

	#busyText = webComponents.attrStr(this, 'busyText');
	get busyText() { return this.#busyText.get(); }
	set busyText(v) { this.#busyText.set(v); }

	enabled = true;

	#isBusy = false;
	get isBusy() { return this.#isBusy; }

	/** @type {function():Promise.<void>} */
	onaction = async () => {};

	#renderer = new Renderer(this, () => {
		if (this.#isBusy) {
			this.disabled = true;
			return text(this.busyText);
		} else {
			this.disabled = !this.enabled;
			return text(this.idleText);
		}
	});


	init() {
		this.onclick = () => this.activate();
		this.#renderer.render();
	}

	activate() {

		if (this.#isBusy) {
			return;
		}

		(async () => {
			this.#isBusy = true;
			this.#renderer.render();
			try {
				await this.onaction();
			} finally {
				this.#isBusy = false;
				this.#renderer.render();
			}
		})();
	}

	connectedCallback() {
		webComponents.connected(this);
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}
}

webComponents.register(BusyButton);
