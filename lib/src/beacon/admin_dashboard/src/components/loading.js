

import { html, text, comp } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';

import { Icon } from 'smolweb-beacon-admin/src/components/icon.js';
import { ICONS } from 'smolweb-beacon-admin/src/gen/icons.js';


export class Loading extends HTMLElement {

	static tagName = 'admin-loading';

	static stylesheet = `
		
		display: flex;
		flex-direction: row;
		gap: 8px;
		align-items: baseline;
		color: var(--colors-text-dark);
		font-style: italic;
		
		admin-icon {
			--icon-color: var(--colors-text-dark);
			animation-name: spin;
			animation-duration: 1s;
			animation-iteration-count: infinite;
			animation-timing-function: steps(8);
		}
		
		@keyframes spin {
			from {
				rotate: 0;
			}
			to {
				rotate: 1turn;
			}
		}
	`;


	#renderer = new Renderer(this, () => {
		return [
			comp(Icon, { name: ICONS.spinner.solid }),
			html('div', {
				_children: [
					text(this.message)
				]
			})
		];
	});

	/** @type {string} */
	message = "Loading ...";


	connectedCallback() {
		webComponents.connected(this);
		this.#renderer.render();
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}
}

webComponents.register(Loading);
