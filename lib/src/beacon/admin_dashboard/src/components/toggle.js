
import { html, text } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';


export class Toggle extends HTMLElement {

	static tagName = 'admin-toggle';

	static stylesheet = `
	
		--field-color-off: #999999;
		--field-color-on: #aaccaa;
		--field-height: 1lh;
		--field-width: calc(var(--field-height)*1.8);
		--handle-margin: calc(var(--field-height)*0.15);
		--baseline-drop: 0.15lh;
		
		display: block;
		position: relative;
		
		label {
			cursor: pointer;
		
			/* hide the actual checkbox control */
			input {
				display: none;
			}
			
			input + span {
				display: flex;
				flex-direction: row;
				gap: 0.4lh;
				align-items: baseline;
				position: relative;
				
				.field {
					height: var(--field-height);
					width: var(--field-width);
					position: relative;
					top: var(--baseline-drop);
					background: var(--field-color-off);
					border-radius: 9999px;
					transition: all .3s ease;
				}
				
				.handle {
					display: block;
					position: absolute;
					top: calc(var(--baseline-drop) + var(--handle-margin));
					left: var(--handle-margin);
					height: calc(var(--field-height) - var(--handle-margin)*2);
					aspect-ratio: 1;
					background: #fff;
					border-radius: 9999px;
					box-shadow: 0 1px 3px rgba(#121621, .1);
					transition: all .45s ease;
				}
				
				.text {
					/* use a grid here so we can stack the two text options */
					display: grid;
					grid-template-columns: 1fr;
					grid-template-rows: 1fr;
					white-space: nowrap;
					
					.off,
					.on {
						grid-area: 1 / 1 / 1 / 1;
					}
					
					.off {
						opacity: 1;
						visibility: visible;
						transition: all .3s ease .2s;
					}
					
					.on {
						opacity: 0;
						visibility: hidden;
						transition: all .3s ease;
					}
				}
			}
			
			input:checked + span {
				
				.field {
					background: var(--field-color-on);
				}

				.handle {
					transform: translate(calc(var(--field-width) - var(--field-height)), 0);
				}
				
				.text {
				
					.off {
						opacity: 0;
						visibility: hidden;
						transition: all .3s ease;
					}
					
					.on {
						opacity: 1;
						visibility: visible;
						transition: all .3s ease .2s;
					}
				}
			}
		}
	`;

	#checkbox = html('input', { type: 'checkbox' });
	
	#renderer = new Renderer(this, () => {
		return html('label', {
			_children: [
				this.#checkbox,
				html('span', {
					_children: [
						html('span', {
							class: 'field',
							part: 'field'
						}),
						html('span', {
							class: 'handle'
						}),
						html('span', {
							class: 'text',
							_children: [
								html('span', {
									class: 'off',
									_children: [
										text(this.offLabel ?? "Off")
									]
								}),
								html('span', {
									class: 'on',
									_children: [
										text(this.onLabel ?? "On")
									]
								})
							]
						})
					]
				})
			]
		});
	});

	/** @type {?string} */
	offLabel = null;

	/** @type {?string} */
	onLabel = null;


	connectedCallback() {
		webComponents.connected(this);

		this.#renderer.render();
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}

	/** @returns {HTMLInputElement} */
	get checkbox() {
		return this.#checkbox;
	}
}

webComponents.register(Toggle);
