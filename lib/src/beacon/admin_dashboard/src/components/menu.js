
import { html, text, fill } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';

import { pages } from 'smolweb-beacon-admin/src/lib/nav.js';


export class Menu extends HTMLElement {

	static tagName = 'admin-menu';

	static stylesheet = `
		
		display: flex;
		flex-direction: column;
		gap: 8px;
		
		a {
			background-color: transparent;
			font-size: 100%;
			text-align: left;
			color: var(--colors-text-light);
			padding-left: 40px;
			padding-top: 12px;
			padding-bottom: 8px;
			border-style: none;
			border-radius: 8px;
			cursor: pointer;
			text-decoration: none;
			
			&:visited {
				color: var(--colors-text-light);
			}
			
			&:hover {
				background-color: var(--colors-bg-darkHighlight);
			}
	
			&.selected {
				background-color: var(--colors-text-light);
				color: var(--colors-bg-dark);
			}
		}
	`;


	init() {
		for (const page of Object.values(pages)) {

			const a = html('a', {
				pageId: page.id,
				href: page.url,
				_children: [
					text(page.name)
				]
			});

			// add selector styles
			if (page.isCurrent()) {
				a.classList.add('selected');
			}

			fill(this, a);
		}
	}

	connectedCallback() {
		webComponents.connected(this);
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}
}

webComponents.register(Menu);
