
import { html, fill, refill } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';


const CARET_BORDER_SIZE = 10;


export class Popover extends HTMLElement {

	static tagName = 'admin-popover';

	static stylesheet = `
	
		--popover-color: #ffffff;
		--popover-max-width: 100vw;
		--popover-max-height: 100vh;
		--popover-padding: 16px;
		--popover-spacing: 0px;
		
		position: absolute;
		margin: 0;
		padding: 0;
		border: none;
		overflow: visible;
		background-color: transparent;
		
		.body {
			background-color: var(--popover-color);
			border: none;
			box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
			overflow: auto;
			padding: var(--popover-padding);
			margin-top: ${CARET_BORDER_SIZE}px;
		}
		
		.caret {
			position: absolute;
			width: 0;
			height: 0;
			border-width: ${CARET_BORDER_SIZE}px;
			border-style: solid;
			border-color: transparent;
		}
	`;

	/**
	 * @typedef {'below-left' | 'below-center'} PopoverLocation
	 */

	static Locations = {
		BelowLeft: /** @type {PopoverLocation} */ 'below-left',
		BelowCenter: /** @type {PopoverLocation} */ 'below-center'
	}


	/** @type {?HTMLElement} */
	target = null;

	/** @type {PopoverLocation} */
	location = Popover.Locations.BelowLeft;

	/** @type {?boolean} */
	toggle = null;

	content = webComponents.content();
	#body = html('div', { class: 'body' });
	#caret = html('div', { class: 'caret' });


	init() {

		fill(this, [
			this.#caret,
			this.#body
		]);
		refill(this.#body, this.content);

		this.onbeforetoggle = /**ToggleEvent*/ event => {
			if (event.newState === 'open') {
				this.#position();
			}
		};
	}

	connectedCallback() {
		webComponents.connected(this);

		this.toggleAttribute('popover', true);

		if (this.toggle != null) {
			this.togglePopover(this.toggle);
		}
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}

	#position() {

		// position the dialog at the target
		const target = this.target;
		if (target != null) {

			const rect = target.getBoundingClientRect();

			// first handle vertical positioning
			switch (this.location) {

				case Popover.Locations.BelowLeft:
				case Popover.Locations.BelowCenter: {
					this.style.top = `calc(${rect.bottom}px + var(--popover-spacing))`;
					this.#caret.style.top = `${1 - CARET_BORDER_SIZE}px`;
					this.#caret.style.borderBottomColor = 'var(--popover-color)';
					this.#body.style.maxHeight = `min(calc(100vh - ${rect.bottom + CARET_BORDER_SIZE}px - 2*var(--popover-padding)), var(--popover-max-height))`;
				} break;
			}

			// second, handle horizontal positioning
			switch (this.location) {

				case Popover.Locations.BelowLeft: {
					this.style.left = `${rect.left}px`;
					this.#caret.style.left = `0`;
					this.#body.style.maxWidth = `min(calc(100vw - ${rect.left}px - 2*var(--popover-padding)), var(--popover-max-width))`;
				} break;

				case Popover.Locations.BelowCenter: {
					this.style.left = `${rect.left + rect.width/2}px`;
					this.style.transform = 'translate(-50%)';
					this.#caret.style.left = `calc(50% - ${CARET_BORDER_SIZE}px)`;
				} break;
			}
		}
	}
}

webComponents.register(Popover);
