
import * as errors from 'food-runner/src/errors.js';
import { Cache } from 'food-runner/src/promises.js';
import { proxy, isError } from 'food-runner/src/comlink.js';

import { html, text, refill, comp, fill } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';

import { IdentityWidget } from 'burger/src/api/identityWidget.js';
import { BurgerService } from 'burger/src/api/service.js';
import { IdentityChoices, PersonalIdentityView, NoIdentityError } from 'burger/src/api/identityView.js';

import { AdminClient } from 'smolweb-beacon-admin/src/lib/client.js';
import { Popover } from 'smolweb-beacon-admin/src/components/popover.js';
import { Icon } from 'smolweb-beacon-admin/src/components/icon.js';
import { ICONS } from 'smolweb-beacon-admin/src/gen/icons.js';
import { smolweb } from 'smolweb-beacon-admin/src/gen/protobuf.js';


const BURGER_NOT_AVAILABLE = {};


export class Login extends HTMLElement {

	static tagName = 'admin-login';

	static stylesheet = `
		
		display: flex;
		flex-direction: column;
		gap: 8px;
		
		.role {
			display: flex;
			flex-direction: row;
			gap: 10px;
			align-items: baseline;
			
			/* make the icon a little bigger to match the burger icon */
			admin-icon {
				width: 1.2lh;
				height: 1.2lh;
			}
			
			&.empty {
			
				span {
					color: var(--colors-text-dark);
					text-style: italic;
				}
				
				admin-icon {
					--icon-color: var(--colors-text-dark);
				}
			}
			
			&.ok {
				admin-icon {
					--icon-color: var(--colors-text-ok);
				}
			}
			
			&.error {
			
				span {
					color: var(--colors-text-error);
					text-style: italic;
				}
				
				admin-icon {
					cursor: pointer;
					--icon-color: var(--colors-text-error);
				}
			}
			
			.labeled {
				cursor: help;
			}
			
			admin-icon.error {
				--icon-color: red;
				cursor: pointer;
			}
			
			admin-popover.error {
				--popover-max-width: 400px; 
				
				h1 {
					font-size: 120%;
				}
				
				.msg {
					font-family: monospace;
					white-space: pre;
				}
			}
		}
	`;


	#idWidget = /** @type {IdentityWidget} */ html(IdentityWidget.tagName);

	/** @type {Cache.<?PersonalIdentityView>} */
	#id = new Cache(async () => {

		// do we have an identity?
		const burger = await BurgerService.get();
		if (burger == null) {
			return BURGER_NOT_AVAILABLE;
		}
		const id = await burger.identity();

		// listen for identity changes
		await id.listeners.add(proxy(() => {

			// identity changed, reset login credentials
			this.#loginErr = null;
			this.#role.clear();
			(async () => {

				const client = await this.#client.get();
				await client.logout();

				// try to get the next role
				await this.#role.get();
			})();
		}));

		return id;

	}).also(cache => {
		cache.listeners.add(() => this.#renderer.render());
	});

	/** @type {Cache.<?AdminClient>} */
	#client = new Cache(async () => {

		// get the id, if any
		const id = await this.#id.get();
		if (id == null) {
			return null;
		}

		const client = await AdminClient.new(id);

		// listen for login changes
		client.loginErrorListeners.add(err => {

			if (isError(err, NoIdentityError)) {
				// ignore these errors, since the UI already makes it obvious when there's no identity chosen
				return
			}

			this.#loginErr = err;
			// NOTE: don't clear the role here: it will lead to a rendering loop
			this.#renderer.render();
		});

		return client;
	});

	/** @type {Cache.<number>} */
	#role = new Cache(async () => {

		// get the identity, if any
		const id = await this.#id.get();
		if (id === BURGER_NOT_AVAILABLE) {
			return smolweb.admin.PersonnelRole.PERSONNEL_ROLE_NONE;
		}
		switch (await id.choice()) {
			case IdentityChoices.PENDING:
			case IdentityChoices.NONE:
				return smolweb.admin.PersonnelRole.PERSONNEL_ROLE_NONE;
		}

		// get the client, if any
		const client = await this.#client.get();
		if (client == null) {
			return smolweb.admin.PersonnelRole.PERSONNEL_ROLE_NONE;
		}

		return (await client.role()) ?? smolweb.admin.PersonnelRole.PERSONNEL_ROLE_NONE;

	}).also(cache => {
		cache.listeners.add(() => this.#renderer.render());
	});

	/** @type {?Error} */
	#loginErr = null;

	#renderer = new Renderer(this, () => {

		const roleElem = html('div', { class: 'role' });

		/**
		 * @param {string} icon
		 * @param {string} label
		 * @param {?string} [title]
		 * @returns {Icon}
		 */
		function renderRole(icon, label, title=null) {
			const iconElem = comp(Icon, { name: icon });
			refill(roleElem, [
				iconElem,
				(() => {
					if (title != null) {
						return html('span', {
							class: 'labeled',
							title,
							_children: [
								text(label)
							]
						});
					} else {
						return html('span', {
							_children: [
								text(label)
							]
						});
					}
				})()
			]);
			return iconElem;
		}

		// show the role, if any
		const id = this.#id.peek();
		if (id === undefined) {
			roleElem.classList.add('empty');
			renderRole(ICONS.personQuestion.solid, "Getting identity ...");
		} else if (id === BURGER_NOT_AVAILABLE) {
			roleElem.classList.add('empty');
			renderRole(ICONS.personMinus.solid, "None");
		} else {
			switch (this.#role.peek()) {

				case undefined: {
					roleElem.classList.add('empty');
					if (this.#loginErr != null) {

						// show error info
						roleElem.classList.add('error');
						const icon = renderRole(ICONS.personExclamation.solid, "Login failed");

						const popover = comp(Popover, {
							class: 'error',
							_let: p => {
								p.target = icon;
								p.location = Popover.Locations.BelowLeft;
								p.toggle = true;
							},
							_content: [
								html('h1', {
									_children: [
										text("Failed to log into the server")
									],
								}),
								html('p', {
									class: 'msg',
									_children: [
										text(errors.format(this.#loginErr))
									]
								})
							]
						});
						fill(roleElem, popover);

						icon.onclick = () => popover.togglePopover(true);

					} else {
						renderRole(ICONS.personQuestion.solid, "Logging in ...");
					}
				} break;

				case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_NONE: {
					roleElem.classList.add('empty');
					renderRole(ICONS.personMinus.solid, "None", "This identity doesn't have access to this server.");
				} break;

				case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_OWNER: {
					roleElem.classList.add('ok');
					renderRole(ICONS.personCheck.solid, "Owner");
				} break;

				case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_ADMIN: {
					roleElem.classList.add('ok');
					renderRole(ICONS.personCheck.solid, "Administrator");
				} break;

				case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_MODERATOR: {
					roleElem.classList.add('ok');
					renderRole(ICONS.personCheck.solid, "Moderator");
				} break;
			}
		}

		return [this.#idWidget, roleElem];
	});


	connectedCallback() {
		webComponents.connected(this);

		this.#renderer.render();
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}


	/** @returns {Promise.<number>} */
	async login() {
		return await this.#role.get();
	}

	/** @returns {smolweb.admin.PersonnelRole | undefined} */
	role() {
		return this.#role.peek();
	}

	/** @returns {LocalListenersAPI.<function()>} */
	roleListeners() {
		return this.#role.listeners;
	}

	/** @returns {Promise.<?AdminClient>} */
	client() {
		return this.#client.get();
	}
}

webComponents.register(Login);
