
import * as errors from 'food-runner/src/errors.js';

import { html, text } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';


export class Messages extends HTMLElement {

	static tagName = 'admin-messages';


	/** @type {(string | Error)[]} */
	#messages = [];

	#renderer = new Renderer(this, () => {
		return this.#messages
			.map(msg => {

				// if the input is an error object, coerce it into a string message
				if (msg instanceof Error) {
					msg = errors.format(msg);
				}

				return html('div', {
					_children: [
						text(msg)
					]
				});
			});
	});


	/** @returns {string[]} */
	get messages() {
		// make a defensive copy
		return Array.from(this.#messages);
	}

	clear() {
		this.#messages = [];
		this.#renderer.render();
	}

	/**
	 * @param {string | Error} msg
	 */
	add(msg) {

		if (msg instanceof Error) {
			console.error(msg);
		}

		this.#messages.push(msg);
		this.#renderer.render();
	}

	/**
	 * @param {(string | Error)[]} messages
	 */
	set(messages) {
		this.#messages = Array.from(messages);
		this.#renderer.render();
	}

	/** @returns {boolean} */
	isEmpty() {
		return this.#messages.length <= 0;
	}

	connectedCallback() {
		webComponents.connected(this);
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}
}

webComponents.register(Messages);
