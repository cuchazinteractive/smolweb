

import { html, text } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer } from 'table-flag/src/renderer.js';

import * as nav from 'smolweb-beacon-admin/src/lib/nav.js';


export class LoginView extends HTMLElement {

	static tagName = 'admin-views-login';

	static stylesheetBase = `
	
		@import 'css/toasty.css';

		html {
			width: 100%;
			height: 100%;
		}
	
		body {
			margin: 0px;
			padding: 0px;
			width: 100%;
			height: 100%;
			background-color: var(--colors-bg-light);
		}
	`;

	static stylesheet = `
		
		display: grid;
		grid-template-columns: 240px 1fr;
		grid-template-rows: 80px 1fr;
		grid-template-areas:
			'logo head'
			'menu main'
			'menu foot';
		width: 100%;
		height: 100%;
		
		a {
			color: var(--colors-text-darkest);
			
			&:visited {
				color: var(--colors-text-darkest);
			}
		}
		
		.logo {
			grid-area: logo;
			background-color: var(--colors-bg-dark);
			color: var(--colors-text-light);
			display: flex;
			flex-direction: row;
			text-align: center;
			align-items: start;
			justify-content: center;
			gap: 12px;
			
			img {
				margin-top: 12px;
			}
			
			.text {
				font-size: 1.8rem;
				font-weight: bold;
				line-height: 56px;
				margin-top: 30px;
			}
		}
		
		header {
			grid-area: head;
			background-color: var(--colors-bg-light);
			color: var(--colors-text-darkest);
			display: flex;
			flex-direction: row;
			align-items: start;
			gap: 20px;
			padding-left: 40px;
			padding-right: 40px;
			
			#title {
				flex-grow: 1;
				font-size: 130%;
				font-weight: normal;
				color: var(--colors-text-darkest);
				line-height: 56px;
				margin-top: 30px;
				margin-bottom: 0px;
			}
			
			.login {
				margin-top: 30px;
			}
		}
		
		nav {
			grid-area: menu;
			background-color: var(--colors-bg-dark);
			color: var(--colors-text-light);
			padding-left: 10px;
			padding-right: 20px;
			padding-top: 40px;
			display: flex;
			flex-direction: column;
			gap: 20px;
		}
		
		main {
			grid-area: main;
			display: flex;
			flex-direction: column;
			align-items: flex-start;
			gap: 40px;
			margin-left: 40px;
			margin-right: 40px;
			margin-top: 40px;
			margin-bottom: 40px;
			
			.row {
				display: flex;
				flex-direction: row;
				gap: 40px;
				flexWrap: wrap;
			}
		}
		
		footer {
			grid-area: foot;
			border-top: solid 1px var(--colors-text-dark);
			padding-top: 20px;
			padding-bottom: 40px;
			margin-left: 40px;
			margin-right: 40px;
			color: var(--colors-text-darker);
		}
	`;


	content = webComponents.content();

	#renderer = new Renderer(this, () => {
		return [
			html('header', {
				class: 'logo',
				_children: [
					html('img', {
						src: nav.url('/images/logo.svg'),
						width: "64",
						height: "64"
					}),
					html('span', {
						class: 'text',
						_children: [
							text("SmolWeb")
						]
					})
				]
			}),
			html('header', {
				class: 'header',
				_children: [
					html('h1', {
						id: 'title',
						_children: [
							text("Log in")
						]
					}),
				]
			}),

			html('nav', {
				_children: [
					// TODO: ?
				]
			}),

			html('main', {
				_children: this.content
			}),

			html('footer',  {
				_children: [
					html('div', {
						_children: [
							text("SmolWeb is free and open-source software that was developed by Cuchaz Interactive, LLC")
						]
					}),
					html('div', {
						_children: [
							html('a', {
								href: 'https://codeberg.org/cuchazinteractive/smolweb',
								target: '_blank',
								_children: [
									text("Get the source code.")
								]
							})
						]
					})
				]
			})
		]
	});


	connectedCallback() {
		webComponents.connected(this);

		// update the title
		window.document.title = "Log in";

		this.#renderer.render();
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}
}

webComponents.register(LoginView);
