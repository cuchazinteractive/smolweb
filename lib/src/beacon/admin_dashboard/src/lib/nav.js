
export class PageNav {

	/** @type {string} */
	#id;

	/** @type {string} */
	#name;


	/**
	 * @param {string} id
	 * @param {string} name
	 */
	constructor(id, name) {
		this.#id = id;
		this.#name = name;
	}


	/** @returns {string} */
	get id() {
		return this.#id;
	}

	/** @returns {string} */
	get name() {
		return this.#name;
	}

	/** @returns {string} */
	get url() {
		return this.#id;
	}

	/** @returns {boolean} */
	isCurrent() {
		return window.location.pathname === url('/' + this.#id);
	}

	go() {
		window.location = this.url;
	}
}


/** @type {Object.<string,PageNav>} */
export const pages = {
	hosts: new PageNav('hosts', "Hosts"),
	personnel: new PageNav('personnel', "Personnel")
};


/**
 * @param {string} path
 * @returns {string}
 */
export function url(path) {
	if (!path.startsWith('/')) {
		throw new Error(`Path must be absolute: ${path}`);
	}
	return `/admin${path}`;
}
