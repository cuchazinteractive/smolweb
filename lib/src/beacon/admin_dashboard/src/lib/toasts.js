
import 'egalink-toasty.js';

import * as errors from 'food-runner/src/errors.js';


function toast() {
	// http://jakim.me/Toasty.js/
	return new Toasty({
		transition: 'pinItUp',
		progressBar: true
	});
}


/**
 * @param {string} msg
 */
export function info(msg) {
	toast().info(msg);
}


/**
 * @param {string} msg
 */
export function success(msg) {
	toast().success(msg);
}


/**
 * @param {string} msg
 */
export function warning(msg) {
	toast().warning(msg);
}


/**
 * @param {string | Error} msg
 */
export function error(msg) {
	if (msg instanceof Error) {
		msg = errors.format(msg);
	}
	toast().error(msg);
}
