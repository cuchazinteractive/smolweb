
import * as errors from 'food-runner/src/errors.js';


/**
 * @param {string} url
 * @param {RequestInit} init
 * @returns {Promise.<Response>}
 */
async function fetchWithTimeout(url, init) {

	// get the timeout
	// TODO: expose timeout options to callers?
	const timeoutSeconds = 30;

	// set the timeout
	const aborter = new AbortController();
	const timeoutId = setTimeout(() => {
		aborter.abort();
	}, timeoutSeconds*1000);

	// build the request
	const request = new Request(url);
	init = {
		... init,
		signal: aborter.signal
	};

	try {
		return await fetch(request, init);
	} catch (error) {
		throw new NetworkError(error);
	} finally {
		clearTimeout(timeoutId);
	}
}


/**
 * @param {string} url
 * @param {RequestInit} init
 * @returns {Promise.<CommResponse>}
 * @throws {NetworkError, HTTPError}
 */
export async function comm(url, init) {

	/* NOTE:
		There seems to be no way to tell fetch() to not send the Origin header, or to replace it with something benign.
		It's kind of a useless header in a browser extension that has no canonical host URL.
		Firefox usually sends something like: moz-extension://xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
		Ideally, we wouldn't send any extra info like that to the host, but alas, browsers were designed for a host-centric world.
		In theory, is possible to strip the Origin headers out of the request using something like `onBeforeSendHeaders`,
		but that seems to require scary read/write permissions for ALL traffic, which is waaaaay more power than we want here.
		So let's just let the browser send a few tidbits of extra info with the requests and hope nothing bad happens.
		Maybe the fetch APIs will let us stop sending the Origin headers someday ...
	*/
	//headers['origin'] = 'override';

	init = {
		... init,
		mode: 'cors',
		credentials: 'omit'
	};

	// send the request, wait for a response
	let response;
	try {
		response = await fetchWithTimeout(url, init);
	} catch (err) {
		throw new NetworkError(err);
	}

	// check for HTTP errors
	if (!response.ok) {
		throw await HTTPError.fromResponse(response);
	}

	return new CommResponse(response);
}


export class CommResponse {

	/** @type {Response} */
	#response;


	/**
	 * @param {Response} response
	 */
	constructor(response) {
		this.#response = response;
	}


	/**
	 * @returns {number}
	 */
	status() {
		return this.#response.status;
	}

	/**
	 * @returns {?string}
	 */
	contentType() {
		return this.#response.headers.get('content-type')
	}


	/**
	 * @param {string} exp
	 */
	#checkContentType(exp) {
		const obs = this.contentType();
		function display(val) {
			if (val == null) {
				return "none";
			} else {
				return `'${val}'`;
			}
		}
		if (obs !== exp) {
			throw new Error(`expected content type ${display(exp)}, but got ${display(obs)} instead`);
		}
	}

	/**
	 * @returns {Promise.<string>}
	 */
	async text() {
		this.#checkContentType('text/plain');
		await this.#response.text();
	}

	/**
	 * @template T
	 * @param {ProtobufOpen.<T>} opener
	 * @returns {Promise.<T>}
	 */
	async proto(opener) {
		this.#checkContentType('application/protobuf');
		const body = await this.#response.blob();
		const buf = await body.arrayBuffer();
		try {
			return opener.fromBuffer(buf);
		} catch (err) {
			throw errors.wrap(new Error("Can't decode Protobuf response"), err);
		}
	}
}


export class NetworkError extends Error {

	name = "NetworkError";

	/**
	 * @param {Error} cause
	 */
	constructor(cause) {
		super("The server could not be reached due to an error");
		errors.wrap(this, cause);
	}
}


export class HTTPError extends Error {

	/**
	 * @param {Response} response
	 * @return {Promise.<HTTPError>}
	 */
	static async fromResponse(response) {

		// read the error body, if any
		let /** @type {?string} */ body = null;
		if (response.headers.get('content-type') === 'text/plain') {
			body = await response.text();
		}

		return new HTTPError(response.status, response.statusText, body);
	}


	name = "HTTPError";

	/** @type {number} */
	code;

	/** @type {string} */
	text;

	/** @type {string} */
	body;


	/**
	 * @param {number} code
	 * @param {string} text
	 * @param {string} [body]
	 */
	constructor(code, text, body) {
		super(`${code}: ${text}: ${body}`);
		this.code = code;
		this.text = text;
		this.body = body;
	}


	/** @returns {?string} */
	failId() {
		const token = this.body.split(' ')[0] ?? '';
		if (token.startsWith('Fail:') && token.endsWith(':')) {
			return token.substring(5, token.length - 1);
		}
		return null;
	}
}
