
import * as buffers from 'food-runner/src/buffers.js';
import * as base64 from 'food-runner/src/base64.js';
import { LocalListeners, proxy } from 'food-runner/src/comlink.js';
import 'burgerid';
import { Identity } from 'burgerid/src/identity.js';
import { PersonalIdentityView } from 'burger/src/api/identityView.js';

import * as comms from 'smolweb-beacon-admin/src/lib/comms.js';
import { smolweb } from 'smolweb-beacon-admin/src/gen/protobuf.js';


const TOKEN_KEY = 'smolweb-admin-token';


class Inner {

	url = `/admin/api`;

	/** @type {PersonalIdentityView} */
	id;

	/** @type {?ArrayBuffer} */
	uid = null;

	/** @type {LocalListeners.<function(Error)>} */
	loginErrorListeners = new LocalListeners();


	/**
	 * @param {PersonalIdentityView} id
	 */
	constructor(id) {
		this.id = id;
	}


	/** @returns {?ArrayBuffer} */
	get token() {
		const val = window.sessionStorage.getItem(TOKEN_KEY);
		if (val == null) {
			return null;
		}
		return base64.decode(val);
	}

	/** @param {?ArrayBuffer} value */
	set token(value) {
		if (value != null) {
			window.sessionStorage.setItem(TOKEN_KEY, base64.encode(value));
		} else {
			window.sessionStorage.removeItem(TOKEN_KEY);
		}
	}

	/**
	 * @param {string} path
	 * @param {?ProtobufSave} data
	 * @returns {Promise.<CommResponse>}
	 */
	async fetch(path, data=null) {

		// build the url
		if (!path.startsWith('/')) {
			throw new Error(`Path should be absolute: ${path}`);
		}
		const url = `${this.url}${path}`;

		const init = {
			headers: []
		};

		// handle authorization tokens
		if (this.token != null) {
			init.headers.push(["smolweb-admin-token", base64.encode(this.token)]);
		}

		if (data != null) {
			init.method = 'POST';
			init.body = data.toBuffer();
			init.headers.push(['content-type', 'application/protobuf']);
		} else {
			init.method = 'GET';
		}

		return await comms.comm(url, init);
	}

	/** @returns {Promise.<void>} */
	async login() {
		try {

			// ask for a challenge
			const challengeResponse = await this.fetch('/authn/challenge', smolweb.admin.LoginRequest.new({
				identity: (await this.id.saveBuf()).okOrThrow
			}));
			const challenge = await challengeResponse.proto(smolweb.admin.LoginChallenge.opener());

			// send a response
			const successResponse = await this.fetch('/authn/response', smolweb.admin.LoginResponse.new({
				uid: (await this.id.uid()).okOrThrow,
				signed: (await this.id.sign(challenge.nonce, buffers.fromString('smolweb/beacon/login'), false)).okOrThrow
			}));
			const success = await successResponse.proto(smolweb.admin.LoginSuccess.opener());

			this.token = success.token;

		} catch (err) {

			// got an error while trying to log in: forward to listeners
			for (const listener of this.loginErrorListeners) {
				listener(err);
			}

			throw err;
		}
	}

	/** @type {Promise.<void>} */
	async logout() {

		if (this.token == null) {
			return;
		}

		try {
			await this.fetch('/authn/logout');
		} finally {
			this.token = null;
		}
	}

	/**
	 * @template T
	 * @param {function():Promise.<T>} func
	 * @returns {Promise.<?T>}
	 */
	async loginLoop(func) {

		for (let i=0; i<2; i++) {

			// if we're not logged in, try to log in
			if (this.token == null) {
				await this.login();
			}

			// try the task
			try {
				return await func();
			} catch (err) {
				if (err instanceof comms.HTTPError) {
					if (err.failId() === 'NotAuthenticated') {

						// we thought we were logged in, but the server disagreed
						// update our understanding of the situation
						this.token = null;
					} else {
						throw err;
					}
				} else {
					throw err;
				}
			}

			// if we got here, we must have tried an operation and found out we were logged out
			// loop back to the top and try (only) once more
		}

		return undefined;
	}
}


export class AdminClient {

	/** @type {Inner} */
	#inner;

	/** @type {AdminClientHosts} */
	#hosts;

	/** @type {AdminClientAuthz} */
	#authz;


	/**
	 * @param {PersonalIdentityView} id
	 * @returns {Promise.<AdminClient>}
	 */
	static async new(id) {

		const client = new this();
		client.#inner = new Inner(id);

		// get the current UID, if any
		client.#inner.uid = (await id.uid()).ok ?? null;

		// listen to id changes and keep the UID up-to-date
		await id.listeners.add(proxy(async () => {

			// if the uid changed, log us out
			const outcome = await id.uid();
			if (client.#inner.uid != null) {
				if (outcome.err != null) {
					// switched to no identity
					client.#inner.uid = null;
					await client.#inner.logout();
				} else if (outcome.ok != null && !buffers.equals(outcome.ok, client.#inner.uid)) {
					// switched to a different identity
					client.#inner.uid = outcome.ok;
					await client.#inner.logout();
				}
			} else if (outcome.ok != null) {
				// switched from no identity to an identity
				client.#inner.uid = outcome.ok;
			}
		}));

		client.#hosts = new AdminClientHosts(client.#inner);
		client.#authz = new AdminClientAuthz(client.#inner);

		return client;
	}


	/** @returns {PersonalIdentityView} */
	get id() {
		return this.#inner.id;
	}

	/** @returns {LocalListenersAPI.<function(Error)>} */
	get loginErrorListeners() {
		return this.#inner.loginErrorListeners.api;
	}

	/** @returns {boolean} */
	hasToken() {
		return this.#inner.token != null;
	}

	/** @type {Promise.<void>} */
	async login() {

		if (this.#inner.token != null) {
			return;
		}

		await this.#inner.login();
	}

	/** @type {Promise.<void>} */
	async logout() {
		await this.#inner.logout();
	}

	/** @returns {Promise.<?number>} */
	async role() {
		return await this.#inner.loginLoop(async () => {

			const httpResponse = await this.#inner.fetch('/authz/role');
			const response = await httpResponse.proto(smolweb.admin.RoleResponse.opener());

			return response.role;
		});
	}

	/** @returns {AdminClientHosts} */
	get hosts() {
		return this.#hosts;
	}

	/** @returns {AdminClientAuthz} */
	get authz() {
		return this.#authz;
	}
}


export class AdminClientHosts {

	/** @type {Inner} */
	#inner;


	/** @param {Inner} inner */
	constructor(inner) {
		this.#inner = inner;
	}


	/** @returns {Promise.<smolweb.admin.HostData[]>} */
	async list() {
		return await this.#inner.loginLoop(async () => {

			const httpResponse = await this.#inner.fetch('/hosts/list');
			const response = await httpResponse.proto(smolweb.admin.ListHosts.opener());

			return response.hosts;
		});
	}

	/**
	 * @param {ArrayBuffer} uid
	 */
	async disconnect(uid) {
		return await this.#inner.loginLoop(async () => {
			await this.#inner.fetch('/hosts/disconnect', smolweb.admin.HostDisconnect.new({
				uid
			}));
		});
	}


	/**
	 * @returns {Promise.<smolweb.admin.HostAuthz>}
	 */
	async getDefaultAuthz() {
		return await this.#inner.loginLoop(async () => {
			const httpResponse = await this.#inner.fetch('/hosts/get_default_authz');
			const response = await httpResponse.proto(smolweb.admin.DefaultHostAuthz.opener());
			return response.authz;
		});
	}

	/**
	 * @param {smolweb.admin.HostAuthz} authz
	 * @returns {Promise.<void>}
	 */
	async setDefaultAuthz(authz) {
		return await this.#inner.loginLoop(async () => {
			await this.#inner.fetch('/hosts/set_default_authz', smolweb.admin.DefaultHostAuthz.new({
				authz
			}));
		});
	}

	/**
	 * @param {Identity} id
	 * @param {smolweb.admin.HostAuthz} authz
	 * @returns {Promise.<void>}
	 */
	async add(id, authz) {
		return await this.#inner.loginLoop(async () => {
			await this.#inner.fetch('/hosts/add', smolweb.admin.AddHost.new({
				id: id.save().toBuffer(),
				authz
			}));
		});
	}

	/**
	 * @param {ArrayBuffer} uid
	 * @param {smolweb.admin.HostAuthz} authz
	 * @returns {Promise.<void>}
	 */
	async setAuthz(uid, authz) {
		return await this.#inner.loginLoop(async () => {
			await this.#inner.fetch('/hosts/set_authz', smolweb.admin.SetHostAuthz.new({
				uid,
				authz
			}));
		});
	}
}


export class AdminClientAuthz {

	/** @type {Inner} */
	#inner;

	/** @type {AdminClientAuthzAdmins} */
	#admins;

	/** @type {AdminClientAuthzMods} */
	#mods;


	/** @param {Inner} inner */
	constructor(inner) {
		this.#inner = inner;
		this.#admins = new AdminClientAuthzAdmins(inner);
		this.#mods = new AdminClientAuthzMods(inner);
	}


	get admins() {
		return this.#admins;
	}

	get mods() {
		return this.#mods;
	}
}


export class AdminClientAuthzAdmins {

	/** @type {Inner} */
	#inner;


	/** @param {Inner} inner */
	constructor(inner) {
		this.#inner = inner;
	}


	/**
	 * @param {Identity} id
	 * @returns {Promise.<void>}
	 */
	async add(id) {
		return await this.#inner.loginLoop(async () => {
			await this.#inner.fetch('/authz/admins/add', smolweb.admin.AddPersonnel.new({
				id: id.save().toBuffer()
			}));
		});
	}

	/**
	 * @param {ArrayBuffer} uid
	 * @returns {Promise.<void>}
	 */
	async remove(uid) {
		return await this.#inner.loginLoop(async () => {
			await this.#inner.fetch('/authz/admins/remove', smolweb.admin.RemovePersonnel.new({
				uid
			}));
		});
	}

	/**
	 * @returns {Promise.<smolweb.admin.PersonnelData[]>}
	 */
	async list() {
		return await this.#inner.loginLoop(async () => {
			const response = await this.#inner.fetch('/authz/admins/list');
			const list = await response.proto(smolweb.admin.ListPersonnel.opener());
			return list.personnel;
		});
	}
}


export class AdminClientAuthzMods {

	/** @type {Inner} */
	#inner;


	/** @param {Inner} inner */
	constructor(inner) {
		this.#inner = inner;
	}


	/**
	 * @param {Identity} id
	 * @returns {Promise.<void>}
	 */
	async add(id) {
		return await this.#inner.loginLoop(async () => {
			await this.#inner.fetch('/authz/mods/add', smolweb.admin.AddPersonnel.new({
				id: id.save().toBuffer()
			}));
		});
	}

	/**
	 * @param {ArrayBuffer} uid
	 * @returns {Promise.<void>}
	 */
	async remove(uid) {
		return await this.#inner.loginLoop(async () => {
			await this.#inner.fetch('/authz/mods/remove', smolweb.admin.RemovePersonnel.new({
				uid
			}));
		});
	}

	/**
	 * @returns {Promise.<smolweb.admin.PersonnelData[]>}
	 */
	async list() {
		return await this.#inner.loginLoop(async () => {
			const response = await this.#inner.fetch('/authz/mods/list');
			const list = await response.proto(smolweb.admin.ListPersonnel.opener());
			return list.personnel;
		});
	}
}
