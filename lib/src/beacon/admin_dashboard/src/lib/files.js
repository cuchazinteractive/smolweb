
import { html } from 'table-flag/src/dom.js';


/**
 * @returns {Promise<File>}
 */
export function pick() {
	return new Promise((resolve, _reject) => {

		// make an invisible file picker
		const fileInput = /** @type {HTMLInputElement} */ html('input', { type: 'file' });

		// respond to the file pick event
		fileInput.onchange = () => {
			resolve(fileInput.files[0]);
		};

		// poke the file picker to show the UI to the user
		fileInput.click();
	});
}


/**
 * @param {File} file
 * @returns {Promise.<string>}
 */
export function readAsString(file) {
	return new Promise((resolve, reject) => {

		const reader = new FileReader();

		// wire up events
		reader.onerror = () => {
			reject(reader.error);
		};

		reader.onload = () => {
			resolve(reader.result);
		};

		reader.readAsText(file, 'UTF-8');
	});
}
