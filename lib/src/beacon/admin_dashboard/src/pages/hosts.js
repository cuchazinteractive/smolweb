
import * as base64 from 'food-runner/src/base64.js';
import { iterSync } from 'food-runner/src/iter.js';
import { html, text, comp } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer, AsyncRenderer } from 'table-flag/src/renderer.js';
import 'burgerid';
import { Identity, IdentityType } from 'burgerid/src/identity.js';

import * as files from 'smolweb-beacon-admin/src/lib/files.js';
import * as toasts from 'smolweb-beacon-admin/src/lib/toasts.js';
import { Dashboard } from 'smolweb-beacon-admin/src/components/dashboard.js';
import { Login } from 'smolweb-beacon-admin/src/components/login.js';
import { LoginView } from 'smolweb-beacon-admin/src/components/loginView.js';
import { smolweb } from 'smolweb-beacon-admin/src/gen/protobuf.js';
import { Card } from 'smolweb-beacon-admin/src/components/card.js';
import { ErrorMsg } from 'smolweb-beacon-admin/src/components/errorMsg.js';
import { Loading } from 'smolweb-beacon-admin/src/components/loading.js';
import { Popover } from 'smolweb-beacon-admin/src/components/popover.js';
import { Form, FormField } from 'smolweb-beacon-admin/src/components/forms.js';
import { Toggle } from 'smolweb-beacon-admin/src/components/toggle.js';
import { BusyButton } from 'smolweb-beacon-admin/src/components/busyButton.js';
import { Timestamp } from 'smolweb-beacon-admin/src/components/timestamp.js';
import { Icon } from 'smolweb-beacon-admin/src/components/icon.js';
import { ICONS } from 'smolweb-beacon-admin/src/gen/icons.js';


export class Hosts extends HTMLElement {

	static tagName = 'admin-pages-hosts';

	static stylesheet = `
	
		.hosts {
			display: flex;
			flex-direction: column;
			gap: 20px;
			
			admin-toggle.authz {
				--field-color-off: var(--colors-text-error);
				--field-color-on: var(--colors-text-ok);
			}
			
			.controls {
				display: flex;
				flex-direction: row;
				gap: 20px;
				align-items: center;
				
				.default-authz {
					display: flex;
					flex-direction: column;
					gap: 8px;
				}
			}
			
			table {
				border-spacing: 32px 10px;
				
				> thead {
					text-transform: uppercase;
				}
				
				> tbody {
					> tr {
					
						> td {
							vertical-align: baseline;
							white-space: nowrap;
						}
						
						> td.name {
							white-space: normal;
							
							a {
								color: var(--colors-text-accent);
								
								&:visited {
									color: var(--colors-text-accent);
								}
							}
						}
					
						> td.authz {
							button {
								margin-left: 10px;
							}
						}
					
						> td.connection {
							button {
								margin-left: 10px;
							}
						}
					}
				}
				
				.uid {
					font-family: monospace;
				}
				
				admin-icon.connected {
					--icon-size: 1.2lh;
					--icon-color: var(--colors-text-ok);
					margin-right: 8px;
				}

				admin-icon.disconnected {
					--icon-size: 1.25lh;
					--icon-color: var(--colors-text-error);
					margin-right: 8px;
				}
				
				admin-icon.none {
					--icon-color: var(--colors-text-dark);
					margin-right: 8px;
				}

				admin-icon.allow {
					--icon-color: var(--colors-text-ok);
					margin-right: 8px;
				}
				
				admin-icon.deny {
					--icon-color: var(--colors-text-error);
					margin-right: 8px;
				}
			}
		}
		
		.button-popover {
			--popover-color: var(--colors-bg-lighter);
			white-space: normal;
			
			h1 {
				font-size: 120%;
			}
		}
		
		.button-popover.edit-authz {
			.buttons {
				display: flex;
				flex-direction: row;
				gap: 20px;
			}
		}
		
		p.info {
			display: flex;
			flex-direction: row;
			gap: 10px;
			align-items: baseline;
			
			> *:first-child {
				min-width: 1em;
			}
			
			> *:last-child {
				flex-grow: 1;
				max-width: 400px;
			}
		}
	`;


	#login = comp(Login);

	#addHostButton = /** @type {HTMLButtonElement} */ html('button', {
		class: 'btn',
		title: "Allow (or Deny) a new host",
		_children: [
			text("Add a Host ...")
		],
		_events: {
			click: () => this.#addHostRenderer.render()
		}
	});

	#renderer = new Renderer(this, () => {

		const role = this.#login.role();
		if (role == null || role === smolweb.admin.PersonnelRole.PERSONNEL_ROLE_NONE) {
			return comp(LoginView, {
				_content: [
					this.#login
				]
			});
		}

		const out = comp(Dashboard, {
			pageTitle: "Hosts",
			_slots: {
				login: html('div', {
					slot: 'login',
					_children: [
						this.#login
					]
				})
			},
			_content: [
				comp(Card, {
					_slots: {
						title: text("All Known Hosts")
					},
					_content: [
						html('div', {
							slot: 'title',
							_children: [

							]
						}),
						html('div', {
							class: 'hosts',
							_children: [
								html('div', {
									class: 'controls',
									_children: [
										html('button', {
											class: 'btn',
											_children: [
												text("Refresh")
											],
											_events: {
												click: async () => await this.#hostsRenderer.render()
											}
										}),
										this.#addHostButton,
										html('div', {
											class: 'defaultAuthz',
											_children: [
												html('div', {
													_children: [
														text("Default Authorization")
													],
												}),
												this.#defaultAuthzRenderer.elem
											]
										})
									]
								}),
								this.#hostsRenderer.elem,
								this.#addHostRenderer.elem
							]
						})
					]
				})
			]
		});

		this.#defaultAuthzRenderer.render().then();
		this.#hostsRenderer.render().then();

		return out;
	});

	#defaultAuthzRenderer = new AsyncRenderer(html('div'), async () => {

		const client = await this.#login.client();
		if (client == null) {
			return [];
		}

		const toggle = comp(Toggle, {
			class: 'authz',
			offLabel: "Deny",
			onLabel: "Allow"
		});

		const defaultAuthz = await client.hosts.getDefaultAuthz();
		toggle.checkbox.checked = defaultAuthz === smolweb.admin.HostAuthz.HOST_AUTHZ_ALLOW;
		toggle.checkbox.onclick = async () => {

			const checked = toggle.checkbox.checked;
			const newAuthz = checked ? smolweb.admin.HostAuthz.HOST_AUTHZ_ALLOW : smolweb.admin.HostAuthz.HOST_AUTHZ_DENY;

			try {
				await client.hosts.setDefaultAuthz(newAuthz);
			} catch (err) {
				// server update failed: undo the change to the UI to re-sync
				toggle.checkbox.checked = !checked;
				toasts.error(err);
			}
		};

		return toggle;

	}, {
		waiting: () => comp(Loading),
		failed: err => comp(ErrorMsg, { _let: e => e.err = err })
	});

	#hostsRenderer = new AsyncRenderer(html('div', { class: 'hosts' }), async () => {

		const client = await this.#login.client();
		if (client == null) {
			return [];
		}

		const hosts = await client.hosts.list();
		if (hosts.length <= 0) {
			return html('div', {
				class: 'empty',
				_children: [
					text("No hosts are known to this server.")
				]
			})
		}

		return html('table', {
			_children: [
				html('thead', {
					_children: [
						html('tr', {
							_children: [
								html('td', { _children: [text("Name")] }),
								html('td', { _children: [text("Authorization")] }),
								html('td', { _children: [text("Status")] }),
								html('td', { _children: [text("First Connected")] }),
								html('td', { _children: [text("Last Connected")] }),
								html('td', { _children: [text("Last Contact")] })
							]
						})
					]
				}),
				html('tbody', {
					_children: iterSync(hosts)
						.map(host => {

							// handle the name (and UID)
							const nameElem = html('a', {
								href: `ext+smolweb://${base64.encode(host.uid)}@${window.location.host}`,
								target: '_blank',
								title: `UID: ${base64.encode(host.uid)}`,
								_children: [
									text(host.name)
								]
							});

							// handle authorization
							const authzDisplay = (() => {
								switch (host.authz) {

									case null:
									case undefined:
									case smolweb.admin.HostAuthz.HOST_AUTHZ_NONE: return [
										comp(Icon, {
											class: 'none',
											name: ICONS.circleMinus.solid
										}),
										html('span', {
											class: 'empty',
											_children: [
												text("Default")
											]
										})
									];

									case smolweb.admin.HostAuthz.HOST_AUTHZ_DENY: return [
										comp(Icon, {
											class: 'deny',
											name: ICONS.circleXmark.solid,
											_children: [
												text("Deny")
											]
										})
									];

									case smolweb.admin.HostAuthz.HOST_AUTHZ_ALLOW: return [
										comp(Icon, {
											class: 'allow',
											name: ICONS.circleCheck.solid,
											_children: [
												text("Allow")
											]
										})
									];
								}
							})();

							const editAuthzButton = html('button', {
								class: 'btn-small',
								title: "Edit Authorization",
								_children: [
									comp(Icon, { name: ICONS.edit.solid })
								],
								_events: {
									click: () => editAuthzRenderer.render()
								}
							});

							const editAuthzRenderer = new Renderer(html('div'), () => {

								/**
								 * @param {smolweb.admin.HostAuthz} authz
								 * @returns {Promise.<void>}
								 */
								const setAuthz = async (authz) => {
									try {
										await client.hosts.setAuthz(host.uid, authz)
									} catch (err) {
										toasts.error(err);
									} finally {
										editAuthzRenderer.clear();
										await this.#hostsRenderer.render();
									}
								};

								return comp(Popover, {
									class: 'button-popover edit-authz',
									_let: p => {
										p.target = editAuthzButton;
										p.location = Popover.Locations.BelowLeft;
										p.toggle = true;
									},
									_content: [
										html('h1', { _children: [text("Edit Authorization")] }),
										html('div', {
											class: 'buttons',
											_children: [
												comp(BusyButton, {
													class: 'btn',
													idleText: "Allow",
													busyText: "Allowing ...",
													title: "Authorize this host to connect to the server",
													_let: b => {
														b.onaction = async () => await setAuthz(smolweb.admin.HostAuthz.HOST_AUTHZ_ALLOW);
													}
												}),
												comp(BusyButton, {
													class: 'btn',
													idleText: "Deny",
													busyText: "Denying ...",
													title: "Deny this host from connecting to the server",
													_let: b => {
														b.onaction = async () => await setAuthz(smolweb.admin.HostAuthz.HOST_AUTHZ_DENY);
													}
												}),
												comp(BusyButton, {
													class: 'btn',
													idleText: "Clear",
													busyText: "Clearing ...",
													title: "Clear authorization for this host and rely on the default authorization for all hosts",
													_let: b => {
														b.onaction = async () => await setAuthz(smolweb.admin.HostAuthz.HOST_AUTHZ_NONE);
													}
												})
											]
										})
									]
								});
							});

							// handle connection

							const connectionDisplay = (() => {
								switch (host.connectionStatus) {

									case smolweb.admin.HostConnectionStatus.HOST_CONNECTION_STATUS_DISCONNECTED: return [
										comp(Icon, {
											class: 'disconnected',
											name: ICONS.plugXmark.solid,
											_children: [
												text("Disconnected")
											]
										})
									];

									case smolweb.admin.HostConnectionStatus.HOST_CONNECTION_STATUS_CONNECTED: {

										const disconnectButton = html('button', {
											class: 'btn-small',
											title: "Disconnect this Host",
											_events: {
												click: () => disconnectRenderer.render()
											},
											_children: [
												comp(Icon, { name: ICONS.plugXmark.solid })
											]
										});

										const disconnectRenderer = new Renderer(html('div'), () => {
											return comp(Popover, {
												class: 'button-popover',
												_let: p => {
													p.target = disconnectButton;
													p.location = Popover.Locations.BelowLeft;
													p.toggle = true;
												},
												_content: [
													html('h1', { _children: [text("Disconnect Host")] }),
													html('p', { _children: [text("Really disconnect this host?")] }),
													html('blockquote', { _children: [text(host.name)] }),
													html('div', {
														class: 'buttons',
														_children: [
															comp(BusyButton, {
																class: 'btn',
																idleText: "Disconnect",
																busyText: "Disconnecting ...",
																title: "Immediately disconnect this host from the server",
																_let: b => {
																	b.onaction = async () => {
																		try {
																			await client.hosts.disconnect(host.uid)
																		} catch (err) {
																			toasts.error(err);
																			return;
																		} finally {
																			disconnectRenderer.clear();
																		}
																		await this.#hostsRenderer.render();
																	};
																}
															})
														]
													}),
													html('p', {
														class: 'info',
														_children: [
															comp(Icon, { name: ICONS.circleInfo.solid }),
															html('div', {
																_children: [
																	text(`
																		The host may immediately try to reconnect.
																		If you want to keep the host from reconnecting in the future, set their authorization to Deny.
																	`)
																]
															})
														]
													})
												]
											});
										});

										return [
											comp(Icon, {
												class: 'connected',
												name: ICONS.plugCheck.solid
											}),
											text("Connected"),
											disconnectButton,
											disconnectRenderer.elem
										];
									}
								}
							})();

							return html('tr', {
								class: 'host',
								_children: [
									html('td', {
										class: 'name',
										_children: [
											nameElem
										]
									}),
									html('td', {
										class: 'authz',
										_children: [
											... authzDisplay,
											editAuthzButton,
											editAuthzRenderer.elem
										]
									}),
									html('td', {
										class: 'connection',
										_children: connectionDisplay
									}),
									html('td', {
										_children: [
											(() => {
												if (host.firstConnect != null) {
													return comp(Timestamp, { _let: t => t.timestamp = host.firstConnect });
												} else {
													return html('span.empty', { _children: [text("Never connected")] });
												}
											})()
										]
									}),
									html('td', {
										_children: [
											(() => {
												if (host.lastConnect != null) {
													return comp(Timestamp, { _let: t => t.timestamp = host.lastConnect });
												} else {
													return html('span', {
														class: 'empty',
														_children: [
															text("Never connected")
														]
													});
												}
											})()
										]
									}),
									html('td', {
										_children: [
											(() => {
												if (host.lastContact != null) {
													return comp(Timestamp, { _let: t => t.timestamp = host.lastContact });
												} else {
													return html('span', {
														class: 'empty',
														_children: [
															text("Not connected")
														]
													});
												}
											})()
										]
									})
								]
							});
						})
						.toArray()
				})
			]
		});

	}, {
		waiting: () => comp(Loading),
		failed: err => comp(ErrorMsg, { _let: e => e.err = err })
	});

	#addHostRenderer = new Renderer(html('div'), () => {

		const identityField = FormField.file({
			name: 'identity',
			label: "Identity",
			description: "Choose the identity file for this Host"
		});

		const /** @type {FormField.<number>} */ authzField = FormField.select([
			['allow', "Allow"],
			['deny', "Deny"]
		], {
			name: 'authz',
			label: "Authorized?"
		}).adapted(
			value => {
				switch (value) {
					case 'allow': return smolweb.admin.HostAuthz.HOST_AUTHZ_ALLOW;
					default: return smolweb.admin.HostAuthz.HOST_AUTHZ_DENY;
				}
			},
			value => {
				switch (value) {
					case smolweb.admin.HostAuthz.HOST_AUTHZ_ALLOW: return 'allow';
					default: return 'deny';
				}
			}
		);
		authzField.value = smolweb.admin.HostAuthz.HOST_AUTHZ_ALLOW;

		const form = comp(Form, {
			submitIdleText: "Add",
			submitBusyText: "Adding ..."
		});
		form.fields = [
			identityField,
			authzField
		];
		form.processor = async () => {

			// validation

			const idFile = identityField.value;
			if (idFile == null) {
				identityField.messages.add("Please choose host's Identity file");
				return;
			}

			// read the identity file
			let /** @type {?Identity} */ id;
			try {
				const idStr = await files.readAsString(idFile);
				id = await Identity.open(idStr);
			} catch (err) {
				identityField.messages.add("The chosen file does not appear to be an identity file.");
				identityField.messages.add(err);
				return;
			}
			if (id == null) {
				identityField.messages.add("The identity was not verified.");
				return;
			}
			if (id.type() !== IdentityType.IDENTITY_TYPE_APP) {
				identityField.messages.add("The identity was not an app identity.");
				return;
			}

			const authz = authzField.value;

			const client = await this.#login.client();
			if (client == null) {
				form.messages.add("Not logged in");
				return;
			}

			// update the server
			try {
				await client.hosts.add(id, authz);
			} catch (err) {
				form.messages.add(err);
				return;
			}

			// all done, update the UI
			this.#addHostRenderer.clear();
			await this.#hostsRenderer.render();
		}


		return comp(Popover, {
			class: 'button-popover',
			_let: p => {
				p.target = this.#addHostButton;
				p.location = Popover.Locations.BelowLeft;
				p.toggle = true;
			},
			_content: [
				html('h1', { _children: [text("Add a New Host")] }),
				form
			]
		});
	});


	constructor() {
		super();

		this.#login.roleListeners().add(() => this.#renderer.render());
	}


	connectedCallback() {
		webComponents.connected(this);

		this.#renderer.render();
		(async () => {
			await this.#login.login();
			this.#renderer.render();
		})();
	}

	disconnectedCallback() {
		webComponents.disconnected(this);
	}
}

webComponents.register(Hosts);
