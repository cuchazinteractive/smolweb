
import * as base64 from 'food-runner/src/base64.js';
import { iterSync } from 'food-runner/src/iter.js';
import { html, text, comp } from 'table-flag/src/dom.js';
import * as webComponents from 'table-flag/src/webComponents.js';
import { Renderer, AsyncRenderer } from 'table-flag/src/renderer.js';
import 'burgerid';
import { Identity, IdentityType } from 'burgerid/src/identity.js';

import * as files from 'smolweb-beacon-admin/src/lib/files.js';
import * as toasts from 'smolweb-beacon-admin/src/lib/toasts.js';
import { Dashboard } from 'smolweb-beacon-admin/src/components/dashboard.js';
import { Login } from 'smolweb-beacon-admin/src/components/login.js';
import { LoginView } from 'smolweb-beacon-admin/src/components/loginView.js';
import { smolweb } from 'smolweb-beacon-admin/src/gen/protobuf.js';
import { Card } from 'smolweb-beacon-admin/src/components/card.js';
import { Timestamp } from 'smolweb-beacon-admin/src/components/timestamp.js';
import { ErrorMsg } from 'smolweb-beacon-admin/src/components/errorMsg.js';
import { Loading } from 'smolweb-beacon-admin/src/components/loading.js';
import { Form, FormField } from 'smolweb-beacon-admin/src/components/forms.js';
import { Popover } from 'smolweb-beacon-admin/src/components/popover.js';
import { BusyButton } from 'smolweb-beacon-admin/src/components/busyButton.js';
import { Icon } from 'smolweb-beacon-admin/src/components/icon.js';
import { ICONS } from 'smolweb-beacon-admin/src/gen/icons.js';


export class Personnel extends HTMLElement {

	static tagName = 'admin-pages-personnel';

	static stylesheet = `
	
		.personnel {
			display: flex;
			flex-direction: column;
			gap: 20px;
			
			.controls {
				display: flex;
				flex-direction: row;
				gap: 20px;
				align-items: center;
			}
			
			table {
				border-spacing: 32px 10px;
				
				> thead {
					text-transform: uppercase;
				}
				
				> tbody {
					> tr {
					
						> td {
							vertical-align: baseline;
							white-space: nowrap;
						}
						
						> td.name {
							white-space: normal;
						}
					}
				}
				
				.uid {
					font-family: monospace;
				}
			}
		}
		
		.button-popover {
			--popover-color: var(--colors-bg-lighter);
			white-space: normal;
			
			h1 {
				font-size: 120%;
			}
		}
	`;


	#login = comp(Login);

	#renderer = new Renderer(this, () => {

		const role = this.#login.role();
		if (role == null || role === smolweb.admin.PersonnelRole.PERSONNEL_ROLE_NONE) {
			return comp(LoginView, {
				_children: [
					this.#login
				]
			});
		}

		const out = comp(Dashboard, {
			pageTitle: "Personnel",
			_slots: {
				login: html('div', {
					slot: 'login',
					_children: [
						this.#login
					]
				})
			},
			_content: [
				comp(Card, {
					_slots: {
						title: text("Administrators")
					},
					_content: [
						html('div', {
							class: 'personnel',
							_children: [
								(() => {
									return html('div', {
										class: 'controls',
										_children: [
											html('button', {
												class: 'btn',
												_children: [
													text("Refresh")
												],
												_events: {
													click: async () => await this.#adminsRenderer.render()
												}
											}),
											(() => {
												if (canManage(role, smolweb.admin.PersonnelRole.PERSONNEL_ROLE_ADMIN)) {
													return this.#addAdminButton;
												}
											})(),
											this.#addAdminRenderer.elem
										]
									});
								})(),
								this.#adminsRenderer.elem
							]
						})
					]
				}),

				comp(Card, {
					_slots: {
						title: text("Moderators")
					},
					_content: [
						html('div', {
							class: 'personnel',
							_children: [
								(() => {
									return html('div', {
										class: 'controls',
										_children: [
											html('button', {
												class: 'btn',
												_children: [
													text("Refresh")
												],
												_events: {
													click: async () => await this.#modsRenderer.render()
												}
											}),
											(() => {
												if (canManage(role, smolweb.admin.PersonnelRole.PERSONNEL_ROLE_MODERATOR)) {
													return this.#addModButton;
												}
											})(),
											this.#addModRenderer.elem
										]
									});
								})(),
								this.#modsRenderer.elem
							]
						})
					]
				})
			]
		});

		this.#adminsRenderer.render().then();
		this.#modsRenderer.render().then();

		return out;
	});

	#addAdminButton = html('button', {
		class: 'btn',
		title: "Add a new Administrator",
		_children: [
			text("Add ...")
		],
		_events: {
			click: () => this.#addAdminRenderer.render()
		}
	});

	#adminsRenderer = new AsyncRenderer(html('div'), async () => {

		const client = await this.#login.client();
		if (client == null) {
			return [];
		}
		const role = await this.#login.login();

		const admins = await client.authz.admins.list();
		if (admins.length <= 0) {
			return html('div', {
				class: 'empty',
				_children: [
					text("No administrators yet.")
				]
			});
		}

		return html('table', {
			_chidren: [
				html('thead', {
					_children: [
						html('tr', {
							_children: [
								html('td', { _children: [text("Name")] }),
								html('td', { _children: [text("Added")] }),
								html('td', { _children: [text("Last Login")] }),
								html('td')
							]
						})
					]
				}),
				html('tbody', {
					_children: iterSync(admins)
						.map(admin => this.#personRow(client, role, admin))
						.toArray()
				})
			]
		});
	}, {
		waiting: () => comp(Loading),
		failed: err => comp(ErrorMsg, { _let: e => e.err = err })
	});

	#addAdminRenderer = new Renderer(html('div'), () => {
		return this.#renderAddPersonnel(
			this.#addAdminButton,
			async (form, client, id) => {

				// update the server
				try {
					await client.authz.admins.add(id);
				} catch (err) {
					form.messages.add(err);
					return;
				}

				// all done, update the UI
				this.#addAdminRenderer.clear();
				await this.#adminsRenderer.render();
				await this.#modsRenderer.render();
			}
		);
	});

	#addModButton = html('button', {
		class: 'btn',
		title: "Add a new Moderator",
		_children: [
			text("Add ...")
		],
		_events: {
			click: () => this.#addModRenderer.render()
		}
	});

	#modsRenderer = new AsyncRenderer(html('div'), async () => {

		const client = await this.#login.client();
		if (client == null) {
			return [];
		}
		const role = await this.#login.login();

		const admins = await client.authz.mods.list();
		if (admins.length <= 0) {
			return html('div', {
				class: 'empty',
				_children: [
					text("No moderators yet.")
				]
			});
		}

		return html('table', {
			_children: [
				html('thead', {
					_children: [
						html('tr', {
							_children: [
								html('td', { _children: [text("Name")] }),
								html('td', { _children: [text("Added")] }),
								html('td', { _children: [text("Last Login")] }),
								html('td')
							]
						})
					]
				}),
				html('tbody', {
					_children: iterSync(admins)
						.map(mod => this.#personRow(client, role, mod))
						.toArray()
				})
			]
		});
	}, {
		waiting: () => comp(Loading),
		failed: err => comp(ErrorMsg, { _let: e => e.err = err })
	});

	#addModRenderer = new Renderer(html('div'), () => {
		return this.#renderAddPersonnel(
			this.#addModButton,
			async (form, client, id) => {

				// update the server
				try {
					await client.authz.mods.add(id);
				} catch (err) {
					form.messages.add(err);
					return;
				}

				// all done, update the UI
				this.#addModRenderer.clear();
				await this.#adminsRenderer.render();
				await this.#modsRenderer.render();
			}
		);
	});


	constructor() {
		super();

		this.#login.roleListeners().add(() => this.#renderer.render());
	}


	connectedCallback() {
		webComponents.connected(this);

		this.#renderer.render();
		(async () => {
			await this.#login.login();
			this.#renderer.render();
		})();
	}

	disconnectedCallback() {
		webComponents.disconnected(this)
	}


	/**
	 * @param {AdminClient} client
	 * @param {smolweb.admin.PersonnelRole} role
	 * @param {smolweb.admin.PersonnelData} person
	 * @returns {Node | Node[]}
	 */
	#personRow(client, role, person) {
		return html('tr', {
			_children: [
				html('td', {
					class: 'name',
					_children: [
						html('span', {
							title: `UID: ${base64.encode(person.uid)}`,
							_children: [
								text(person.name)
							]
						})
					]
				}),
				html('td', {
					_children: [
						comp(Timestamp, { _let: t => t.timestamp = person.added })
					]
				}),
				html('td', {
					_children: [
						(() => {
							if (person.lastLogin != null) {
								return comp(Timestamp, { _let: t => t.timestamp = person.lastLogin });
							} else {
								return html('span', {
									class: 'empty',
									_children: [
										text("Never logged in")
									]
								});
							}
						})()
					]
				}),
				html('td', {
					_children: (() => {

						if (!canManage(role, person.role)) {
							return [];
						}

						const removeButton = html('button', {
							class: 'btn-small',
							title: "Remove this person",
							_children: [
								comp(Icon, { name: ICONS.personMinus.solid })
							],
							_events: {
								click: () => removeRenderer.render()
							}
						});

						const removeRenderer = new Renderer(html('div'), () => {
							return comp(Popover, {
								class: 'button-popover',
								_let: p => {
									p.target = removeButton;
									p.location = Popover.Locations.BelowLeft;
									p.toggle = true;
								},
								_content: [
									html('h1', {
										_children: [text(
											(() => {
												switch (person.role) {
													case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_ADMIN:
														return "Remove Administrator?";
													case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_MODERATOR:
														return "Remove Moderator?";
													default:
														return "Remove Person?";
												}
											})()
										)]
									}),
									html('blockquote', {
										_children: [
											text(person.name)
										]
									}),
									html('div', {
										class: 'buttons',
										_children: [
											comp(BusyButton, {
												class: 'btn',
												idleText: "Remove",
												busyText: "Removing ...",
												_let: b => {
													b.onaction = async () => {

														switch (person.role) {

															case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_ADMIN:
																try {
																	await client.authz.admins.remove(person.uid);
																} catch (err) {
																	toasts.error(err);
																	return;
																} finally {
																	removeRenderer.clear();
																}
																await this.#adminsRenderer.render();
															break;

															case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_MODERATOR:
																try {
																	await client.authz.mods.remove(person.uid);
																} catch (err) {
																	toasts.error(err);
																	return;
																} finally {
																	removeRenderer.clear();
																}
																await this.#modsRenderer.render();
															break;
														}
													};
												}
											})
										]
									})
								]
							});
						});

						return [
							removeButton,
							removeRenderer.elem
						];
					})()
				})
			]
		});
	}

	/**
	 * @param {HTMLElement} addButton
	 * @param {function(Form,AdminClient,Identity):Promise.<void>} onSubmit
	 * @returns {Node | Node[]}
	 */
	#renderAddPersonnel(addButton, onSubmit) {

		const identityField = FormField.file({
			name: 'identity',
			label: "Identity",
			description: "Choose the identity file for this person"
		});

		const form = comp(Form, {
			submitIdleText: "Add",
			submitBusyText: "Adding ..."
		});
		form.fields = [
			identityField
		];
		form.processor = async () => {

			// validation

			const idFile = identityField.value;
			if (idFile == null) {
				identityField.messages.add("Please choose person's Identity file");
				return;
			}

			// read the identity file
			let /** @type {?Identity} */ id;
			try {
				const idStr = await files.readAsString(idFile);
				id = await Identity.open(idStr);
			} catch (err) {
				identityField.messages.add("The chosen file does not appear to be an identity file.");
				identityField.messages.add(err);
				return;
			}
			if (id == null) {
				identityField.messages.add("The identity was not verified.");
				return;
			}
			if (id.type() !== IdentityType.IDENTITY_TYPE_PERSONAL) {
				identityField.messages.add("The identity was not a personal identity.");
				return;
			}

			const client = await this.#login.client();
			if (client == null) {
				form.messages.add("Not logged in");
				return;
			}

			await onSubmit(form, client, id);
		};

		return comp(Popover, {
			class: 'button-popover',
			_let: p => {
				p.target = addButton;
				p.location = Popover.Locations.BelowLeft;
				p.toggle = true;
			},
			_content: [
				html('h1', {
					_children: [
						text(addButton.title)
					]
				}),
				form
			]
		});
	}
}

webComponents.register(Personnel);


/**
 * @param {smolweb.admin.PersonnelRole} srcRole
 * @param {smolweb.admin.PersonnelRole} targetRole
 * @returns {boolean}
 */
function canManage(srcRole, targetRole) {
	switch (targetRole) {

		case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_ADMIN:
			switch (srcRole) {
				case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_OWNER:
					return true;
				default:
					return false;
			}

		case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_MODERATOR:
			switch (srcRole) {
				case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_OWNER:
				case smolweb.admin.PersonnelRole.PERSONNEL_ROLE_ADMIN:
					return true;
				default:
					return false;
			}

		default:
			return false;
	}
}
