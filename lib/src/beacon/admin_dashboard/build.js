
import fs from 'node:fs/promises';
import process from 'node:process';
import path from 'node:path';

import color from 'color';

import { collectTestFiles, bundle, buildTestWebpage, compileProtobufs } from 'food-runner-build';
import { statOrNull } from 'food-runner-build/src/fs.js';
import * as icons from 'food-runner-build/src/icons.js';
import * as compression from 'food-runner-build/src/compression.js';


(async () => await main())();


async function main() {

	// parse the arguments
	// ignore the first two arguments: `node build.js`
	let args = process.argv.slice(2);

	let target = null;
	if (args.length >= 1) {
		target = args[0];
	} else {
		throw new Error("required arguments: target");
	}

	switch (target) {
		case "dashboard": return await buildDashboard();
		case "tests": return await buildTests();
		case "protobuf": return await buildProtobuf();
		default: throw Error(`Invalid target: ${target}`);
	}
}


const suppressWarnings = [
	// ignore warnings in dependencies that we can't do anything about
	/^Circular dependency: node_modules\/chai\//,
	/^Circular dependency: node_modules\/luxon\//,
	/^Circular dependency: .*node_modules\/protobufjs\//,
	/^.*node_modules\/@protobufjs\/inquire\/index\.js \(12:18\): Use of eval in/
];


async function buildDashboard() {

	await buildStyles();

	const outDir = 'build/dashboard';

	// cleanup the old folder first
	try {
		await fs.rm(outDir, { recursive: true });
	} catch {}

	// build the icons
	const iconDefs = {
		error: ['triangle-exclamation', ['solid']],
		spinner: ['spinner', ['solid']],
		edit: ['pen-to-square', ['solid']],

		personQuestion: ['person-circle-question', ['solid']],
		personCheck: ['person-circle-check', ['solid']],
		personMinus: ['person-circle-minus', ['solid']],
		personExclamation: ['person-circle-exclamation', ['solid']],

		plugCheck: ['plug-circle-check', ['solid']],
		plugXmark: ['plug-circle-xmark', ['solid']],

		circleCheck: ['circle-check', ['solid']],
		circleXmark: ['circle-xmark', ['solid']],
		circleMinus: ['circle-minus', ['solid']],
		circleInfo: ['circle-info', ['solid']]
	};
	const iconSheets = {
		solid: 'src/images/font-awesome-solid.svg'
	};
	await icons.buildSvg(iconDefs, iconSheets, path.join(outDir, 'images/icons.svg'));
	await icons.buildJs(iconDefs, 'src/gen/icons.js');

	// make an entry point for each page
	const pagesDir = 'src/pages';
	const pages = (await fs.readdir(pagesDir))
		.map(filename => path.parse(filename))
		.filter(file => file.ext === '.js')
		.map(file => file.name);
	const input = {};
	for (const page of pages) {
		input[`pages/${page}`] = path.join(pagesDir, `${page}.js`);
	}

	// compile the js
	await bundle(outDir, input, {
		suppressWarnings
	});

	// write the page files
	for (const page of pages) {
		await writePageHtml(outDir, page, true);
	}

	// copy over static files
	console.log('Copied:');
	await copyDir('static/images', outDir, 'images');
	await copyDir('static/css', outDir, 'css');
	await copyFile('node_modules/egalink-toasty.js/dist/toasty.css', outDir, 'css/toasty.css');

	// precompress text files, if needed
	const precompressFiles = true;
	if (precompressFiles) {
		console.log('Compressed:');
		for (const d of ['pages', 'chunks']) {
			for (const filename of await fs.readdir(path.join(outDir, d))) {
				await compression.precompress(outDir, path.join(d, filename));
			}
		}
		for (const d of ['images']) {
			for (const filename of await fs.readdir(path.join(outDir, d))) {
				if (filename.endsWith('.svg')) {
					await compression.precompress(outDir, path.join(d, filename));
				}
			}
		}
	}
}


/**
 * @param {string} outDir
 * @param {string} page
 * @param {boolean} tag
 * @returns {Promise.<void>}
 */
async function writePageHtml(outDir, page, tag) {
	const out = path.join(outDir, page);

	let tagHtml = '';
	if (tag) {
		tagHtml = `<admin-pages-${page}></admin-pages-${page}>`;
	}

	await fs.writeFile(out, `
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<title>${page}</title>
		<link rel="stylesheet" href="css/style.css">
		<script src="pages/${page}.js" type="module"></script>
	</head>
	<body>
		${tagHtml}
	</body>
</html>
	`);
}


async function buildTests() {

	await buildTestWebpage(
		'build/test',
		{
			testFiles: await collectTestFiles('test/tests'),
			suppressWarnings
		}
	);

	// TODO: host website?
}


async function buildStyles() {

	// base colors
	const colors = {

		bg: {
			dark: '#09234c',
			light: '#ededed',
			lightest: '#ffffff',
			accent: '#3da4e1'
		},

		text: {
			accent: '#0050b8',
			light: '#eff9ff',
			error: '#eb3232',
			ok: '#35a808'
		}
	};

	// derived colors
	colors.bg.darkHighlight = color(colors.bg.dark)
		.mix(color(colors.text.light), 0.2)
		.hex();
	colors.bg.lighter = color(colors.bg.light)
		.mix(color(colors.bg.lightest), 0.3)
		.hex();
	colors.bg.accentLighter = color(colors.bg.accent)
		.lighten(0.2)
		.hex();
	colors.bg.accentDarker = color(colors.bg.accent)
		.darken(0.2)
		.hex();
	colors.bg.accentGreyer = color(colors.bg.accent)
		.desaturate(0.8)
		.lighten(0.5)
		.hex();
	colors.bg.accentLighterGreyer = color(colors.bg.accentLighter)
		.desaturate(0.8)
		.lighten(0.5)
		.hex();
	colors.bg.accentDarkerGreyer = color(colors.bg.accentDarker)
		.desaturate(0.8)
		.lighten(0.5)
		.hex();
	colors.bg.lessLight = color(colors.bg.lightest)
		.mix(color(colors.bg.light), 2.0)
		.hex()

	colors.text.darkest = colors.bg.dark;
	colors.text.darker = color(colors.bg.dark)
		.mix(color(colors.bg.light), 0.2)
		.hex();
	colors.text.dark = color(colors.bg.dark)
		.mix(color(colors.bg.light), 0.4)
		.hex();

	// render to css vars
	let vars = '';
	function render(obj, path=[]) {
		switch (typeof obj) {

			// recurse
			case 'object': {
				for (const [k, v] of Object.entries(obj)) {
					render(v, [... path, k]);
				}
			} break;

			// render the property
			default: {
				const name = path.join('-');
				vars += `\t--${name}: ${obj};\n`;
			} break;
		}
	}
	render({
		colors
	});

	await fs.writeFile('static/css/gen.css', `
/*
	This file is generated by \`build.js styles\`.
	Editing it directly would not be wise.
*/
:root {

${vars}
}
	`);
}


async function buildProtobuf() {
	await compileProtobufs(
		'../../protobuf',
		'admin.proto',
		'type.cuchazinteractive.org',
		'src/gen/protobuf.js'
	);
}


/**
 * @param {string} src
 * @param {string} outDir
 * @param {?string} [dst]
 * @returns {Promise.<void>}
 */
async function copyFile(src, outDir, dst) {
	if (dst == null) {
		dst = path.basename(src);
	}
	await fs.copyFile(src, path.join(outDir, dst));
	console.log(`\t${dst}`);
}


/**
 * @param {string} srcDir
 * @param {string} outDir
 * @param {?string} [dst]
 * @returns {Promise.<void>}
 */
async function copyDir(srcDir, outDir, dst='') {

	// make the dst folder if needed
	const dstDir = path.join(outDir, dst);
	if ((await statOrNull(dstDir))?.isDirectory() !== true) {
		await fs.mkdir(dstDir, { recursive: true });
	}

	for (const filename of await fs.readdir(srcDir, { recursive: true })) {
		await copyFile(path.join(srcDir, filename), outDir, path.join(dst, filename));
	}
}
