
use std::collections::HashMap;
use std::time::Duration;

use burgerid::{ArgsAuthnServer, AuthnAdmin, AuthnSession, Identity};

use crate::beacon::{KEYCHAIN_CONTEXT, LOGIN_CONTEXT};


pub struct AuthnSessions {
	args: ArgsAuthnServer,
	sessions: HashMap<Vec<u8>,AuthnSession>,
}

impl AuthnSessions {

	pub fn new(args: Option<&ArgsAuthn>, signature_has_apps: bool) -> Self {
		Self {
			args: {
				let mut out = ArgsAuthnServer::default(
					LOGIN_CONTEXT,
					KEYCHAIN_CONTEXT
				);
				if let Some(args) = args {
					out.delay = args.delay;
					out.count = args.count;
					out.ttl = args.ttl;
				}
				out.signature_has_apps = signature_has_apps;
				out
			},
			sessions: HashMap::new()
		}
	}

	/// get or start a new authentication session
	pub fn get_or_make(&mut self, id: &Identity) -> &mut AuthnSession {

		if self.get(id.uid()).is_none() {
			let session = AuthnSession::new(self.args.clone(), id.clone());
			self.sessions.insert(id.uid().clone(), session);
		}

		self.sessions.get_mut(id.uid())
			.expect("come on ... we just added it")
	}

	pub fn get(&mut self, uid: &Vec<u8>) -> Option<&mut AuthnSession> {
		self.sessions.get_mut(uid)
	}

	pub fn admin(&self) -> AuthnAdmin {
		AuthnAdmin::new(self.args.clone())
	}
}



#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArgsAuthn {
	pub delay: Duration,
	pub count: usize,
	pub ttl: Duration
}

impl Default for ArgsAuthn {

	fn default() -> Self {
		Self {
			delay: ArgsAuthnServer::DEFAULT_DELAY,
			count: ArgsAuthnServer::DEFAULT_COUNT,
			ttl: ArgsAuthnServer::DEFAULT_TTL,
		}
	}
}
