
mod static_migrations;
mod migrations;

use std::str::FromStr;
use sqlx::migrate::Migrator;
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};

pub use migrations::get;


pub fn run_all(path: impl AsRef<str>) {

	let path = path.as_ref();

	// build the connect options
	let url = format!("sqlite://{}", path);
	let connect_options = SqliteConnectOptions::from_str(&url)
		.expect("Failed to parse Sqlite connection options")
		.create_if_missing(true);

	// start a single-threaded tokio runtime
	let runtime = tokio::runtime::Builder::new_current_thread()
		.enable_time()
		.build()
		.expect("Failed to build tokio runtime");
	runtime.block_on(async {
		let local_set = tokio::task::LocalSet::new();
		local_set.run_until(async {

			let pool = SqlitePoolOptions::new()
				.connect_with(connect_options)
				.await
				.expect("Failed to connect to database");

			println!("Running migrations at {} ...", &path);

			// run all migrations
			Migrator::new(get())
				.await
				.expect("Failed to create migrator")
				.run(&pool)
				.await
				.expect("Migrations failed");

			println!("Done!");

		}).await
	});
}
