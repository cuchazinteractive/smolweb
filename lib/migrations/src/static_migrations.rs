
use std::borrow::Cow;

use futures::future::BoxFuture;
use sqlx::error::BoxDynError;
use sqlx::migrate::{Migration, MigrationSource, MigrationType};


#[derive(Debug)]
pub struct StaticMigrations {
	migrations: Vec<Migration>
}

impl StaticMigrations {

	pub fn new() -> Self {
		Self {
			migrations: Vec::new()
		}
	}

	pub fn add(
		&mut self,
		version: i64,
		description: &'static str,
		sql_up: &'static str,
		sql_down: &'static str
	) {
		self.migrations.push(Migration::new(
			version,
			Cow::Borrowed(description),
			MigrationType::ReversibleUp,
			Cow::Borrowed(sql_up)
		));
		self.migrations.push(Migration::new(
			version,
			Cow::Borrowed(description),
			MigrationType::ReversibleDown,
			Cow::Borrowed(sql_down)
		));
	}
}

impl<'s> MigrationSource<'s> for StaticMigrations {

	fn resolve(self) -> BoxFuture<'s,Result<Vec<Migration>,BoxDynError>> {
		Box::pin(async move {
			Ok(self.migrations)
		})
	}
}
