
use crate::static_migrations::StaticMigrations;


pub fn get() -> StaticMigrations {

	let mut migrations = StaticMigrations::new();

	// https://www.sqlite.org/lang.html
	// https://www.sqlite.org/datatype3.html

	migrations.add(
		1,
		"Initial creation",
		r#"

			/*
			Keys for these tables are determined by the ACME client and are complicated (variable-length lists),
			so the application code just flattens them to a single string to make life easier in database-land.
			But that means we have an externally-defined primary key that's a long string,
			so sadly we can't use sqlite's fast internal integer primary key here.
			The good news is these tables will be tiny, so performance is just not a concern.
			Since we flattened the complicated key values in app code, all we really need here is a simple key-value store.
			*/
			CREATE TABLE acme_accounts (
				id TEXT PRIMARY KEY NOT NULL,
				account BLOB NOT NULL
			) WITHOUT ROWID;
			CREATE TABLE acme_certs (
				id TEXT PRIMARY KEY NOT NULL,
				cert BLOB NOT NULL
			) WITHOUT ROWID;

			CREATE TABLE settings (
				id TEXT PRIMARY KEY NOT NULL,
				value BLOB NOT NULL
			) WITHOUT ROWID;

			CREATE TABLE hosts (
				uid BLOB PRIMARY KEY NOT NULL,
				name TEXT NOT NULL,
				authz BLOB,
				first_connect INTEGER,
				last_connect INTEGER
			) WITHOUT ROWID;

			CREATE TABLE authn (
				key PRIMARY KEY NOT NULL,
				value BLOB NOT NULL
			) WITHOUT ROWID;

			CREATE TABLE personnel (
				uid BLOB PRIMARY KEY NOT NULL,
				name TEXT NOT NULL,
				role BLOB NOT NULL,
				added INTEGER NOT NULL,
				last_login INTEGER
			) WITHOUT ROWID;
		"#,
		r#"
			DROP TABLE acme_accounts;
			DROP TABLE acme_certs;
			DROP TABLE settings;
			DROP TABLE hosts;
			DROP TABLE authn;
		"#
	);

	migrations
}
