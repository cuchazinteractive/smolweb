
use std::{env, fs};
use std::io::Result;
use std::path::PathBuf;


fn main() -> Result<()> {

	let package_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));

	let protos_dir = package_dir.join("src/protobuf");
	let proto_main = protos_dir.join("smolweb.proto");
	let proto_files = fs::read_dir(&protos_dir)
		.expect("can't read protos dir")
		.map(|f| f.expect("can't list proto file").path())
		.collect::<Vec<_>>();

	// tell cargo to recompile if any of these proto files are changed
	for proto_file in &proto_files {
		println!("cargo:rerun-if-changed={}", proto_file.display());
	}

	prost_build::Config::new()

		// turn on support for optional fields
		.protoc_arg("--experimental_allow_proto3_optional")

		.compile_protos(&[&proto_main], &[protos_dir])?;

	// configure sqlx to look for the schema database
	let schema_path = package_dir.join("migrations/schema.db");
	println!("cargo:rustc-env=DATABASE_URL=sqlite:{}", schema_path.to_string_lossy());

	Ok(())
}
